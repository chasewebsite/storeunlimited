<?php

use App\Models\SaleSummary;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('phpinfo', function () {
	return view('phpinfo');
});
Route::get('/test', function(){
	
	$files = File::allFiles(storage_path().'/uploads/sftp');

	foreach ($files as $file)
	{	
		$info = pathinfo($file);
		$type = 1;
		$filename = $info['filename'];
		if(substr($filename,0,9) == "INVENTORY" ){
			$type = 2;
			$branch_code = substr($filename,12,4);
			$transact_date = substr($filename,17,4)."-".substr($filename,21,2)."-".substr($filename,23,2);
		}else{
			$branch_code = substr($filename,8,4);
			$transact_date = substr($filename,13,4)."-".substr($filename,17,2)."-".substr($filename,19,2);
		}
		// dd($transact_date);

	 	App/Models/SalesInventory::firstOrCreate(['type' => $type, 
	 		'branch_code' => $branch_code, 
	 		'file' => $filename,
	 		'transact_date' => $transact_date]);

	}
});

Route::get('/webhooks', function(){
	
	$channelSecret = "c466dce5c4a6d05ce9cb34ef7d49edbf"; // Channel secret string
	$httpRequestBody = " "; // Request body string
	$hash = hash_hmac('sha256', $httpRequestBody, $channelSecret, true);
	$signature = base64_encode($hash);
	echo $signature;
	
});


Route::get('/daily',function(Request $request){

	// $datas = SaleSummary::getDailyandMonthlySales($request);
	// // dd($datas);die;
	// \Mail::send('email.daily_sales',$data = [
	// 	'datas' => $datas], function ($message) use($data){
 //        $message->from('admin@chasetech.com', 'Daily Sales Report');
 //        $message->to("jpoxcz@gmail.com")->subject('Sales Report');
 //        });

	dd(date('G'));

});

// Route::auth();

Route::group(array('prefix' => 'api'), function(){
	Route::post('uploadpo', 'Api\PurchaseOrderController@uploadpo');
	Route::post('upselltype', 'Api\SalesSummaryController@branchtype');
	Route::post('incomingbranch', 'Api\IncomingItemController@stocksstorebranch');
	Route::post('incomingcategory', 'Api\IncomingItemController@stocksstorecategory');
	Route::post('incomingbrand', 'Api\IncomingItemController@stocksstorebrand');
	Route::post('uploadincoming', 'Api\IncomingItemController@uploadincoming'); 

	Route::post('outgoingbranch', 'Api\OutgoingItemController@stocksstorebranch');
	Route::post('outgoingcategory', 'Api\OutgoingItemController@stocksstorecategory');
	Route::post('outgoingbrand', 'Api\OutgoingItemController@stocksstorebrand');
	Route::post('uploadoutgoing', 'Api\OutgoingItemController@uploadoutgoing'); 

	Route::post('uploadsales', 'Api\SalesSummaryController@uploadsales');
	Route::post('storebranch', 'Api\SalesSummaryController@storebranch');
	Route::post('branchterminal', 'Api\SalesSummaryController@branchterminal');
	Route::post('globalstatus', 'Api\SalesSummaryController@globalstatus');
	
	Route::post('uploadrefund', 'Api\RefundItemController@uploadrefund'); 
	Route::post('uploadreturn', 'Api\ReturnExchangeController@uploadreturn'); 

	Route::post('uploadpostvoid', 'Api\PostVoidContoller@uploadpostvoid'); 

	Route::get('checkgiftcard/{code?}', 'Api\GiftCardController@checkgiftcard'); 
	Route::post('uploadgiftcard', 'Api\GiftCardController@uploadgiftcard'); 

	Route::post('uploadinventory', 'Api\ItemStockController@uploadinventory');
	Route::post('stocksstorebranch', 'Api\ItemStockController@stocksstorebranch');

	Route::post('coupon', 'Api\CouponController@store'); 
	Route::get('coupon/{code?}', 'Api\CouponController@verify'); 

	Route::get('downloadmasterfile', 'Api\ItemMasterfileController@downloadmasterfile');

	Route::get('outgoing/{company_code}/{branch_code}', 'Api\OutgoingItemController@outgoing');

	Route::post('sftp', 'Api\SftpController@sftp'); 
	Route::post('salesinventory', 'Api\SalesInventoryController@store'); 

	Route::post('sftp_excel', 'Api\SftpController@excel_upload');
	Route::post('get_companies','Api\SftpController@get_companies');
	Route::post('masterfile', 'Api\MasterfileController@upload'); 
	Route::get('masterfile', 'Api\MasterfileController@download');

	Route::get('download_voucher','Api\ManualInventoryController@download');

	
	Route::post('uploadpatient', 'Api\PatientController@uploadpatient'); 

	Route::post('outgoingitem', 'Api\OutgoingItemController@getitem'); 

	Route::post('salesfromtokyo','Api\CrossSiteController@uploadsalestokyo');
	Route::post('retexfromtokyo','Api\CrossSiteController@uploadretextokyo');
	Route::post('incomingfromtokyo','Api\CrossSiteController@uploadincomingtokyo');
	//new added
	Route::post('storebrand','Api\SalesSummaryController@storebrand');
	Route::post('outgoinguser', 'Api\OutgoingItemController@stocksuser'); 
	//close new added
});

Route::get('login', 'Auth\AuthController@getLogin');
Route::get('logout', 'Auth\AuthController@getLogout');
Route::post('login', 'Auth\AuthController@postLogin');

Route::group(['middleware' => ['auth']], function () {
	Route::group(['middleware' => ['role:admin']], function(){
		Route::resource('items', 'FileMaintenance\ItemController');
		Route::resource('companies', 'FileMaintenance\CompanyController');
		Route::resource('branches', 'FileMaintenance\BranchController');
		Route::resource('branchesettings', 'FileMaintenance\BranchSettingController');
		Route::resource('roles', 'FileMaintenance\RoleController');
		Route::resource('giftcards', 'FileMaintenance\GiftCardController');
		Route::resource('brand' ,'FileMaintenance\BrandController');
		Route::resource('duplicate','FileMaintenance\DuplicateItemController');
		Route::resource('changeitem','FileMaintenance\ChangeItemController');
		Route::get('users/{id}/branch', ['as' => 'users.branch', 'uses' => 'FileMaintenance\UserController@branch']);
		Route::post('users/{id}/branch', ['as' => 'users.storebranch', 'uses' => 'FileMaintenance\UserController@storebranch']);
		
		Route::resource('zap', 'FileMaintenance\ZapController');
		Route::resource('zap_per_day', 'FileMaintenance\ZapPerDayController');
		Route::get('branch_item/{id}',['as'=>'branch.item','uses'=>'FileMaintenance\ItemController@branch_item']);
	});
	Route::resource('users', 'FileMaintenance\UserController');
		Route::get('users/changepassword',['as'=>'users.changepassword','uses'=>'FileMaintenance\UserController@show']);
		Route::post('users/update/{password}',['as'=>'users.updatepassword','uses'=>'FileMaintenance\UserController@updatepassword']);
	Route::get('/', 'HomeController@index');
	Route::resource('outgoing', 'InventoryReport\OutgoingReportController');
	Route::resource('incoming', 'InventoryReport\IncomingReportController');
	Route::resource('purchase', 'InventoryReport\PurchaseOrderController');

	Route::get('stocks/{id}/history', ['as' => 'stocks.history', 'uses' => 'InventoryReport\ItemStockController@history']);
	Route::resource('stocks', 'InventoryReport\ItemStockController');

	Route::get('sales/{id}/items', ['as' => 'sales.items', 'uses' => 'SalesReport\SalesSummaryController@items']);
	Route::get('sales/{id}/transaction', ['as' => 'sales.transaction', 'uses' => 'SalesReport\SalesSummaryController@transaction']);
	Route::post('items_sales_summary_report',['as' => 'sales.items.report','uses' => 'SalesReport\SalesSummaryController@download_items_sales']);
	Route::resource('sales', 'SalesReport\SalesSummaryController');
	Route::resource('salemans', 'SalesReport\SalesManController');
	Route::resource('refunds', 'SalesReport\RefundReportController');
	Route::resource('returnexchange', 'SalesReport\ReturnExchnageReportController');
	Route::resource('postvoid', 'SalesReport\PostVoidController');
	Route::resource('coupons', 'SalesReport\CouponController');
	Route::resource('itemrank', 'SalesReport\ItemRankController');
	Route::resource('upsell', 'SalesReport\UpsellController');

	Route::resource('salesinventory', 'Utility\SalesInventoryExportController');
	Route::resource('inventorysummary', 'Utility\SalesInventorySummaryExportController');
	Route::get('salessummary',['as' => 'salessummary.index', 'uses' => 'Utility\SalesInventorySummaryExportController@show']);
	Route::post('salessummary/generate',['as' => 'salessummary.generate', 'uses' => 'Utility\SalesInventorySummaryExportController@generate']);
	// Route::get('salesinventorysummary',['as'=>'salesinventorysummary.index','uses'=>'Utility\SalesInventoryExportController@sindex']);
	// Route::post('salesinventorysummary/generate',['as'=>'salesinventorysummary.sgenerate','uses'=>'Utility\SalesInventoryExportController@sgenerate']);
	Route::resource('manualinventory', 'Utility\ManualInventoryController');
	Route::resource('patient', 'Utility\PatientHistoryController');
	Route::get('other_payment',['as' => 'other.payment' , 'uses' => 'FileMaintenance\GiftCardController@other_payment']);

	Route::get('check_excel',['as' =>'check.excel','uses' => 'Utility\CheckUploadController@excel_index']);
	Route::post('check_excel',['as' => 'excel.post' , 'uses' => 'Utility\CheckUploadController@excel_post']);

	Route::get('check_csv',['as' =>'check.csv','uses' => 'Utility\CheckUploadController@csv_index']);

	//inventory stock excel report

	// Route::get('stock_transfer_in',['as' => 'stock_transfer_in.index','uses'=>'InventoryReport\ExcelReportController@stock_transfer_in_index']);

	// Route::post('stock_transfer_in',['as' => 'stock_transfer_in.store','uses'=>'InventoryReport\ExcelReportController@stock_transfer_in_store']);

	// Route::get('stock_transfer_out',['as' => 'stock_transfer_out.index','uses'=>'InventoryReport\ExcelReportController@stock_transfer_out_index']);

	// Route::post('stock_transfer_out',['as' => 'stock_transfer_out.store','uses'=>'InventoryReport\ExcelReportController@stock_transfer_out_store']);

	// Route::get('stock_movement_detailed',['as' => 'stock_movement_detailed.index','uses'=>'InventoryReport\ExcelReportController@stock_movement_detailed_index']);


	// Route::post('stock_movement_detailed',['as' => 'stock_movement_detailed.store','uses'=>'InventoryReport\ExcelReportController@stock_movement_detailed_store']);



	// Route::get('stock_movement_summary',['as' => 'stock_movement_summary.index','uses'=>'InventoryReport\ExcelReportController@stock_movement_summary_index']);

	// Route::post('stock_movement_summary',['as' => 'stock_movement_summary.store','uses'=>'InventoryReport\ExcelReportController@stock_movement_summary_store']);


	//inventory stock excel report

	Route::get('stock_transfer_in',['as' => 'stock_transfer_in.index','uses'=>'InventoryReport\ExcelReportController@stock_transfer_in_index']);

	Route::post('stock_transfer_in',['as' => 'stock_transfer_in.store','uses'=>'InventoryReport\ExcelReportController@stock_transfer_in_store']);

	Route::get('stock_transfer_out',['as' => 'stock_transfer_out.index','uses'=>'InventoryReport\ExcelReportController@stock_transfer_out_index']);

	Route::post('stock_transfer_out',['as' => 'stock_transfer_out.store','uses'=>'InventoryReport\ExcelReportController@stock_transfer_out_store']);

	Route::get('stock_movement_detailed',['as' => 'stock_movement_detailed.index','uses'=>'InventoryReport\ExcelReportController@stock_movement_detailed_index']);


	Route::post('stock_movement_detailed',['as' => 'stock_movement_detailed.store','uses'=>'InventoryReport\ExcelReportController@stock_movement_detailed_store']);



	Route::get('stock_movement_summary',['as' => 'stock_movement_summary.index','uses'=>'InventoryReport\ExcelReportController@stock_movement_summary_index']);

	Route::post('stock_movement_summary',['as' => 'stock_movement_summary.store','uses'=>'InventoryReport\ExcelReportController@stock_movement_summary_store']);

	Route::get('sale_register',['as' => 'sale_register' , 'uses' => 'InventoryReport\ExcelReportController@sale_register_index']);

	Route::post('sale_register',['as' => 'sale_register.store' , 'uses' => 'InventoryReport\ExcelReportController@sale_register_post']);

	Route::get('stock_issue',['as' => 'stock_issue.index','uses'=>'InventoryReport\ExcelReportController@stock_issue_index']);
	Route::post('stock_issue',['as' => 'stock_issue.store','uses'=>'InventoryReport\ExcelReportController@stock_issue_post']);


	Route::get('stock_issue_return',['as' => 'stock_issue_return.index','uses'=>'InventoryReport\ExcelReportController@stock_issue_return_index']);
	Route::post('stock_issue_return',['as' => 'stock_issue_return.store','uses'=>'InventoryReport\ExcelReportController@stock_issue_return_post']);


	Route::get('stock_adjustment',['as' => 'stock_adjustment.index','uses'=>'InventoryReport\ExcelReportController@stock_adjustment_index']);
	Route::post('stock_adjustment',['as' => 'stock_adjustment.store','uses'=>'InventoryReport\ExcelReportController@stock_adjustment_post']);



	Route::get('audit_stock',['as' =>'audit_stock.index' , 'uses' => 'InventoryReport\ExcelReportController@audit_stock_index']);

	Route::post('audit_stock',['as' => 'audit_stock.store','uses'=>'InventoryReport\ExcelReportController@audit_stocks_store']);

	//sales inventory report


	Route::get('detailed_sales',['as' =>'detailed_sales.index' , 'uses' => 'SalesReport\DetailedSalesController@index']);

	Route::post('detailed_sales',['as' => 'detailed_sales.store','uses'=>'SalesReport\DetailedSalesController@post']);

	Route::get('summary_sales',['as' =>'summary_sales.index' , 'uses' => 'SalesReport\SummarySalesController@index']);

	Route::post('summary_sales',['as' => 'summary_sales.store','uses'=>'SalesReport\SummarySalesController@post']);

	Route::get('detailed_card',['as' =>'detailed_card.index' , 'uses' => 'SalesReport\DetailedCardController@index']);

	Route::post('detailed_card',['as' =>'detailed_card.store' , 'uses' => 'SalesReport\DetailedCardController@post']);

	Route::get('summary_card',['as' =>'summary_card.index' , 'uses' => 'SalesReport\SummaryCardController@index']);

	Route::post('summary_card',['as' =>'summary_card.store' , 'uses' => 'SalesReport\SummaryCardController@post']);


	Route::get('consolidated',['as' => 'consolidated.index' , 'uses' => 'SalesReport\ConsolidatedCardController@index']);


	Route::post('consolidated',['as' => 'consolidated.store' , 'uses' => 'SalesReport\ConsolidatedCardController@post']);
	//store operation

	Route::resource('store_operation', 'Utility\StoreOperationController');
	//marketing report

	Route::resource('marketing_budget', 'MarketingReport\UploadController');
	Route::resource('marketing_summary', 'MarketingReport\SummaryController');
	Route::resource('monthly_profile','MarketingReport\MonthlyProfileController');
	
	// Route::resource('consolidated_sales_report','SalesReport\ConsolidatedSalesReportController');
	Route::resource('con_sales_rep','SalesReport\ConsolidatedSReportController');

	// Route::resource('consolidated_inventory_report','SalesReport\ConsolidatedInventoryReportController');
	Route::resource('con_inv_rep','SalesReport\ConsolidatedInvReportController');


	//new added
	Route::get('notyet',['as' =>'notyet.index' , 'uses' => 'InventoryReport\NotYetReceivedController@index']);
	Route::post('notyet',['as' =>'notyet.post' , 'uses' => 'InventoryReport\NotYetReceivedController@store']);
	Route::resource('ending_inventory','Api\EndingInventoryController');
	Route::get('audit_stock',['as' =>'audit_stock.index' , 'uses' => 'InventoryReport\ExcelReportController@audit_stock_index']);
	Route::post('audit_stock',['as' => 'audit_stock.store','uses'=>'InventoryReport\ExcelReportController@audit_stocks_store']);
	Route::resource('detailed_sale','SalesReport\DetailedSalesSummaryController');

	//gift card
	Route::resource('giftscards', 'FileMaintenance\GiftsCardController');
	Route::get('giftsin',['as' => 'giftscards.incoming','uses' => 'FileMaintenance\GiftsCardController@incoming_index']);
	Route::post('giftsin',['as' => 'giftscards.incoming','uses' => 'FileMaintenance\GiftsCardController@incoming_post']);
	Route::get('giftsout',['as' => 'giftscards.outgoing','uses' => 'FileMaintenance\GiftsCardController@outgoing_index']);
	Route::post('giftsout',['as' => 'giftscards.outgoing','uses' => 'FileMaintenance\GiftsCardController@outgoing_post']);
	Route::post('giftscards.post',['as' => 'giftscards.post','uses' => 'FileMaintenance\GiftsCardController@post']);
	//new close
});



