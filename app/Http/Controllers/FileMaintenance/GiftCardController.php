<?php

namespace App\Http\Controllers\FileMaintenance;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

use App\Models\GiftCard;

class GiftCardController extends Controller
{
    public function index(Request $request){
    	$giftcards = GiftCard::search($request);
    	return view('giftcard.index',compact('giftcards'));
    }

    public function create(){
    	return view('giftcard.create');
    }

    public function store(Request $request){
    	$this->validate($request, [
            'file' => 'required',
        ]);

    	if(!GiftCard::uploadGiftCard($request)){
    		Session::flash('flash_message', 'Error while uploading Gift Cards!');
        	Session::flash('flash_class', 'alert-danger');
        	return redirect()->route("giftcards.create");
    	}

    	Session::flash('flash_message', 'Gift Card successfully updated!');
        Session::flash('flash_class', 'alert-success');
        return redirect()->route("giftcards.index");
    }
    public function other_payment(Request $request) {
        $sel_branch = [];
        $sel_gift = [];
        $date_from = date('m/d/Y');
        $date_to = date('m/d/Y');
        $branches = GiftCard::getCompaniesBranch();
        $gifts =  GiftCard::getGiftNames();
        $items = GiftCard::search1($request);
        if(!empty($request->branch)) {
            $sel_branch = $request->branch;

        }
        if(!empty($request->gifts)) {

            $sel_gift = $request->gifts;
        }

        if(!empty($request->date_from)) {

            $date_from = $request->date_from;
        }

        if(!empty($request->date_to)) {

            $date_to = $request->date_to;
        }

        return view('other_payment.index',compact('date_from','date_to','gifts','branches','items','sel_gift','sel_branch'));
    }
   
}
