<?php

namespace App\Http\Controllers\FileMaintenance;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\GiftsCard;
use App\Models\OutgoingGiftCard;
use App\Models\IncomingGiftCard;
use Session;
class GiftsCardController extends Controller
{
    //

     public function index(Request $request){


        $date_from = date('m/d/Y');
        $date_to = date('m/d/Y');
    	
        $sel_branches = GiftsCard::getBranches();
        $status = GiftsCard::getStatus();
        $giftcards = GiftsCard::all();
        // $giftcards = GiftsCard::all();
    	return view('giftscard.index',compact('giftcards','date_from','date_to','sel_branches','status'));
    }



    public function post(Request $request) {

        // dd($request); die;

       // if(in_array('Open', $request->status)){  
       //  echo "HI";

       // }
       // die;
        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $sel_branches = GiftsCard::getBranches();
        $status = GiftsCard::getStatus();
        $giftcards = GiftsCard::search($request);
        // dd($giftcards); die;

        return view('giftscard.index',compact('giftcards','date_from','date_to','sel_branches','status'));








    }







    public function create(){
    	return view('giftscard.create');
    }



    public function store(Request $request){
    	$this->validate($request, [
            'file' => 'required',
        ]);

    	if(!GiftsCard::uploadGiftCard($request)){
    		Session::flash('flash_message', 'Error while uploading Gift Cards!');
        	Session::flash('flash_class', 'alert-danger');
        	return redirect()->route("giftscards.create");
    	}

    	Session::flash('flash_message', 'Gift Card successfully updated!');
        Session::flash('flash_class', 'alert-success');
        return redirect()->route("giftscards.index");
    }
     public function incoming_index(Request $request){
        
         $sel_branch = [];
        $branches = IncomingGiftCard::getBranches();
        $date_from = date('m/d/Y');
        $date_to = date('m/d/Y');  
        $items = IncomingGiftCard::search($branches,$date_from,$date_to);



        return view('giftscard.incoming',compact('items','branches','date_from','date_to','sel_branch'));
    }


    public function incoming_post(Request $request){
       
        $sel_branch = [];
        if(count($request->branch) > 0) {

            $sel_branch = $request->branch;
        }
        $branches = IncomingGiftCard::getBranches(); 
        $date_from = $request->date_from;
        $date_to =    $request->date_to;


        $items = IncomingGiftCard::search($sel_branch,$date_from,$date_to);




        return view('giftscard.incoming',compact('items','branches','date_from','date_to','sel_branch'));
    }
















      public function outgoing_index(Request $request){


          $sel_branch = [];
        $branches = OutgoingGiftCard::getBranches();
        $date_from = date('m/d/Y');
        $date_to = date('m/d/Y');  
        $items = OutgoingGiftCard::search($branches,$date_from,$date_to);



        return view('giftscard.outgoing',compact('items','branches','date_from','date_to','sel_branch'));
    }
    public function outgoing_post(Request $request){
       
        $sel_branch = [];
        if(count($request->branch) > 0) {

            $sel_branch = $request->branch;
        }
        $branches = OutgoingGiftCard::getBranches(); 
        $date_from = $request->date_from;
        $date_to =    $request->date_to;


        $items = OutgoingGiftCard::search($sel_branch,$date_from,$date_to);




        return view('giftscard.outgoing',compact('items','branches','date_from','date_to','sel_branch'));
    }




}
