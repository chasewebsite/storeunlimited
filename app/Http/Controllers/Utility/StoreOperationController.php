<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\EmailContent;
use App\Models\Email;
use App\Models\CompanyBranch;

class StoreOperationController extends Controller
{
    //



    public function index(){
    	$contents = EmailContent::orderBy('date','desc')->get();  
    	return view('operation.index',compact('contents'));
    }

    public function store(Request $request){
    	   $user_id = \Auth::user()->id;
           $branch = CompanyBranch::where('branch_code',$request->branch)->first();
           $date = $request->date;

           $company = new CompanyBranch();
           $company->user_id      = $user_id;
           $company->branch       = $branch->branch;
           $company->branch_code  = $branch->branch_code;
           $company->date         = $date;
           $company->reason       = $requset->reason;
           $company->save();

           $email = Email::find(1)->email;

            \Mail::send('site.email.email_message',
            $data = [

                'code'     => $postdata['confirmation_code'],
                'fullname' => $postdata['firstname'].' '.$postdata['lastname'].',',
                'url'      => $url

            ], function ($message) use($data, $request){

            $email_add = $request->get('email_address');
            $message->from( 'admin@chasetech.com', 'Carpark Reservation' );
            $message->to($email_add)->subject( 'Confirmation Code for your Registration!' );
        });





    }

    public function create() {

        $date = date('m/d/Y');
        $branches = CompanyBranch::select('branch', 'branch_code')
                    ->orderBy('branch')
                    ->groupBy('branch_code')
                    ->lists('branch', 'branch_code');


        return view('operation.create',compact('date','branches'));
    }

   

}
