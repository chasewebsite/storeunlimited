<?php

namespace App\Http\Controllers\Utility;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Company;
use App\Models\CompanyBranch;
use App\Models\SalesInventoryExcel;
use App\Models\SalesInventory;
use App\Models\UserBranch;
use Carbon\Carbon;
use DatePeriod;
use DateInterval;
class CheckUploadController extends Controller
{
    //

    //  public function excel_index(Request $request){

    //  	 $companies = Company::getList();
    //  	 $date = date('m/d/Y');
    //  	 $sel_branches   = [];
    //  	 $data = [];
    //  	 $company_sel = [];
    //  	 $branch_sel =  " ";
    

    // 	 return view('check.excel_index', compact('date','companies','sel_branches','data','company_sel','branch_sel','data'));
    // }

	 // public function excel_post(Request $request){

		// $request->flash();
  //   	$companies = Company::getList();
  //    	$date = date('m/d/Y');
  //    	$sel_branches   = [];
  //    	$company_sel  = " ";
  //    	$branch_sel   =  " ";
  //    	if(!empty($request->branch)) {
  //          $branch_sel = $request->branch; 
  //       }
  //       if(!empty($request->company)) {
  //          $company_sel = $request->company; 
  //       }

  //       if(!empty($request->date)) {
  //          $date = $request->date; 
  //       }

  //       $branch = CompanyBranch::where('branch_code',$request->branch)->first();
  //    	$data = [];
  //    	if(count($branch) > 0 ) {
  //    			$data[] = ['branch_name' => $branch->branch,
  //    			 'date' => $date, 
  //    			 'file_type' => '1',
  //    			 'file_name' => ' ',
  //    			 'upload' => 0];
  //    			$data[] = ['branch_name' => $branch->branch,
  //    			 'date' => $date , 
  //    			 'file_type' => 2 ,
  //    			 'file_name' => " " ,
  //    			 'upload' => 0];
  //    	}
  //    	$formatted_date = date("Y-m-d", strtotime($date));
  //    	$sub_branchcode =  substr($branch->branch_code,4);
     
  //    	$excels = SalesInventoryExcel::where('transact_date',$formatted_date)->where('branch_code',$sub_branchcode)->get();

    

  //    	if(count($excels) > 0 ) {

  //    		foreach ($excels as $excel) {
  //    			for($x = 0; $x <  count($data); $x++){
     			
  //    				 if($excel->type == $data[$x]['file_type']){
  //    				 	$data[$x]['file_name'] = $excel->file;
  //    				 	$data[$x]['upload'] = 1;


  //    				 }


  //    			}
  //    		}
  //    	}


     	

  //   	 return view('check.excel_index', compact('date','companies','sel_branches','data','company_sel','branch_sel','data'));
  //   }





    public function excel_index(){

		
    	$branches    = UserBranch::getAllowedBranchUp(\Auth::user()->id);
    	$_end_date = date('Y-m-d');
    	$_start_date   =  date('Y-m-d', strtotime('-14 days'));
        $dates      = UserBranch::getDatesFromRange($_start_date,$_end_date);
        $sub_branchcode = [];
         	$data = [];
        foreach ($branches as $b) {
        	if(!in_array(substr($b, 4), $sub_branchcode)) {
        		$sub_branchcode[] =  substr($b,4);
        	}
        	
        	foreach ($dates as $d) {

        		 $branch = CompanyBranch::where('branch_code',$b)->first();
        		 if(count($branch) > 0 ) {
	        		 $data[] = ['branch_name' => $branch->branch,
	        		 'branch_code' => substr($b, 4),
	     			 'date' => $d, 
	     			 'file_type' => '1',
	     			 'file_name' => ' ',
	     			 'upload' => 0];
	     			$data[] = ['branch_name' => $branch->branch,
	     			'branch_code' => substr($b, 4),
	     			 'date' => $d ,

	     			 'file_type' => 2 ,
	     			 'file_name' => " " ,
	     			 'upload' => 0];
     			 }

        		
        	}
        }


      




       
    
     	
   
     
     	$excels = SalesInventoryExcel::whereIn('transact_date',$dates)->whereIn('branch_code',$sub_branchcode)->orderBy('transact_date')->get();

    
   
     	if(count($excels) > 0 ) {

     		foreach ($excels as $excel) {
     			for($x = 0; $x <  count($data); $x++){
     			
     				 if($excel->type == $data[$x]['file_type'] && $excel->transact_date == $data[$x]['date'] && $excel->branch_code == $data[$x]['branch_code']){
     				 	$data[$x]['file_name'] = $excel->file;
     				 	$data[$x]['upload'] = 1;


     				 }


     			}
     		}
     	}



    	 return view('check.excel_index', compact('date','companies','sel_branches','data','company_sel','branch_sel','data'));
    }




    public function csv_index(){

		
    	$branches    = UserBranch::getAllowedBranchUp(\Auth::user()->id);
    	$_end_date = date('Y-m-d');
    	$_start_date   =  date('Y-m-d', strtotime('-14 days'));
        $dates      = UserBranch::getDatesFromRange($_start_date,$_end_date);
        $sub_branchcode = [];
         	$data = [];
        foreach ($branches as $b) {
        	if(!in_array(substr($b, 4), $sub_branchcode)) {
        		$sub_branchcode[] =  substr($b,4);
        	}
        	
        	foreach ($dates as $d) {

        		 $branch = CompanyBranch::where('branch_code',$b)->first();
        		 if(count($branch) > 0 ) {
	        		 $data[] = ['branch_name' => $branch->branch,
	        		 'branch_code' => substr($b, 4),
	     			 'date' => $d, 
	     			 'file_type' => '1',
	     			 'file_name' => ' ',
	     			 'upload' => 0];
	     			$data[] = ['branch_name' => $branch->branch,
	     			'branch_code' => substr($b, 4),
	     			 'date' => $d ,

	     			 'file_type' => 2 ,
	     			 'file_name' => " " ,
	     			 'upload' => 0];
     			 }

        		
        	}
        }


      




       
    
     	
   
     
     	$excels = SalesInventory::whereIn('transact_date',$dates)->whereIn('branch_code',$sub_branchcode)->get();

    
   
     	if(count($excels) > 0 ) {

     		foreach ($excels as $excel) {
     			for($x = 0; $x <  count($data); $x++){
     			
     				 if($excel->type == $data[$x]['file_type'] && $excel->transact_date == $data[$x]['date'] && $excel->branch_code == $data[$x]['branch_code']){
     				 	$data[$x]['file_name'] = $excel->file;
     				 	$data[$x]['upload'] = 1;


     				 }


     			}
     		}
     	}



    	 return view('check.csv_index', compact('date','companies','sel_branches','data','company_sel','branch_sel','data'));
    }


}
