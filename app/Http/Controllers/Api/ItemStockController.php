<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ItemStock;
use App\Models\StockHistory;
use DB;
use App\Models\QueingItemStock;

class ItemStockController extends Controller
{
    public function stocksstorebranch(Request $request){
    	$data['selection'] = ItemStock::getCompaniesBranch($request);
        return \Response::json($data,200);
    }

    public function uploadinventory(Request $request){
    	 set_time_limit(0);
        $fileName = $request->file('data')->getClientOriginalName();

        $c_folder = substr($fileName, 0,4);
        $b_folder = substr($fileName, 4,4);
        $destinationPath = storage_path().'/uploads/inventory/'.$c_folder.'/'.$b_folder;
        
        if (!\File::exists($destinationPath))
        {
            mkdir($destinationPath, 0755, true); 
        }

        $request->file('data')->move($destinationPath, $fileName);

        $filePath = $destinationPath ."/". $fileName;

        // $que = new QueingItemStock();
        // $que->filename = $filePath;
        // $que->que = 1;
        // $que->save();
        // return response()->json(array('msg' => 'file uploaded', 'status' => 0));
      //   // dd($filePath);
         // $start = StockHistory::gettime();
        DB::beginTransaction();
        try {
            $reader = ReaderFactory::create(Type::CSV); // for XLSX files
            $reader->setFieldDelimiter('|');
            $reader->open($filePath);
            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $row) {
                    // dd($row);
                    $company_code = str_pad($row[0], 4, "0", STR_PAD_LEFT);
                    $branch_code = $company_code.str_pad($row[2], 4, "0", STR_PAD_LEFT);
                    $cost = " ";
                    $srp  = " ";
                    if(!empty($row[13])) {

                        $cost = $row[13];
                    }

                    if(!empty($row[14])) {

                        $srp = $row[14];

                    }
                    $in = " ";
                    $out = " ";
                    

                            
                    $data['company_code'] = $company_code;
                    $data['company_name'] = $row[1];
                    $data['branch_code'] = $branch_code;
                    $data['branch_name'] = $row[3];
                    $data['barcode'] = $row[4];
                    $data['begin'] = $row[5];
                    $data['end'] = $row[6];
                    $data['in_qty'] = $in;
                    $data['out_qty'] = $out;
                    $data['date'] = date('Y-m-d', strtotime($row[7]));
                    $data['itemcode'] = $row[8];
                    $data['description'] = $row[9];
                    $data['qty'] = $row[6];
                    $data['series_name'] = $row[7];
                    $data['composition'] = $row[8];
                    $data['brand'] = $row[9]; 
                    $data['abrev_name'] = $row[10]; 
                    $data['shirt_fit'] = $row[11]; 
                    $data['supplier'] = $row[12]; 
                    $data['collar_type'] = $row[13]; 
                    $data['color'] = $row[14]; 
                    $data['woven_style'] = $row[15]; 
                    $data['pattern_style'] = $row[16]; 
                    $data['sleeve_length'] = $row[17]; 
                    $data['neck_size'] = $row[18]; 
                    $data['cost'] = $cost;
                    $data['srp'] = $srp;
                    $data['movement_description'] = " ";
                    $data['purpose']  = " ";
                    $data['move_ref'] = " ";
                    // dd($data);
                    $item = ItemStock::getItem($data);
                    
                    if(!empty($item)){
                        $data['item_stock_id'] = $item->id;
                        StockHistory::updateHistory($data);
                    }else{
                        ItemStock::addStocks($data);
                        $item = ItemStock::getItem($data);
                        $data['item_stock_id'] = $item->id;
                        StockHistory::updateHistory($data);
                    }
                    
                }
            }
            $reader->close();
            DB::commit();
            //  $finish = StockHistory::gettime();
            // $total_time = round(($finish - $start), 4);
            return response()->json(array('msg' => 'file uploaded', 'status' => 0 ));
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(array('msg' => 'file uploaded error', 'status' => 1));
        }

         
           


                    // if($row[5] != $row[6]) {

                    //         if($row[5] > $row[6]) {
                    //             $out = $row[5]- $row[6];
                    //         }
                    //         else if ($row[5] < $row[6]) {

                    //             $in = $row[6] - $row[5];
                    //         }

                    // }
    }
}
