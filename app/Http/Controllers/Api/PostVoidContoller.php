<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use DB;

use App\Models\PostVoid;
use App\Models\SaleSummary;
use App\Models\SaleDetail;
use App\Models\ItemStock;

class PostVoidContoller extends Controller
{
    public function uploadpostvoid(Request $request){
        $fileName = $request->file('data')->getClientOriginalName();

        $c_folder = substr($fileName, 0,4);
        $b_folder = substr($fileName, 4,4);
        $t_folder = substr($fileName, 8,2);
        $destinationPath = storage_path().'/uploads/postvoid/'.$c_folder.'/'.$b_folder.'/'.$t_folder;
        
        if (!\File::exists($destinationPath))
        {
            mkdir($destinationPath, 0755, true); 
        }

        $request->file('data')->move($destinationPath, $fileName);

        $filePath = $destinationPath ."/". $fileName;
       
        DB::beginTransaction();
        try {
            $reader = ReaderFactory::create(Type::CSV); // for XLSX files
            $reader->setFieldDelimiter('|');
            $reader->open($filePath);
            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $row) {
                    $company_code = str_pad($row[0], 4, "0", STR_PAD_LEFT);
                    $branch_code = $company_code.str_pad($row[2], 4, "0", STR_PAD_LEFT);
                    $terminal_code = $branch_code.str_pad($row[4], 2, "0", STR_PAD_LEFT);

                    $data['company_code'] = $company_code;
                    $data['company_name'] = $row[1];
                    $data['branch_code'] = $branch_code;
                    $data['branch_name'] = $row[3];
                    $data['terminal_code'] = $terminal_code;
                    $data['terminal_no'] = $row[4];
                    $data['user'] = $row[5];
                    $data['transaction_no'] = $row[6];
                    $data['local_time'] = date('Y-m-d H:i:s', strtotime($row[7].' '.$row[8]));
                  

                    $transaction = PostVoid::recordExist($data);

                    if(empty($transaction)){
                        PostVoid::create([
	                        'company_code' => $data['company_code'],
	                        'company_name' => $data['company_name'],
	                        'branch_code' => $data['branch_code'],
	                        'branch_name' => $data['branch_name'],
	                        'terminal_code' => $data['terminal_code'],
	                        'terminal_no' => $data['terminal_no'],
	                        'user' => $data['user'],
	                        'transaction_no' => $data['transaction_no'],
	                        'local_time' => $data['local_time']]);

                       	$sale_summary = SaleSummary::getTransaction($data);
                       	if(!empty($sale_summary)){
                       		$items = SaleDetail::where('sale_summary_id', $sale_summary->id)->get();
                       		foreach ($items as $item) {
                       			$itemdata['company_code'] = $data['company_code'];
                       			$itemdata['company_name'] = $data['company_name'];
                       			$itemdata['branch_code'] = $data['branch_code'];
                       			$itemdata['branch_name'] = $data['branch_name'];
                       			$itemdata['barcode'] = $item->barcode;
                       			$itemdata['itemcode'] = $item->itemcode;
                       			$itemdata['description'] = $item->description;
                       			$itemdata['qty'] = $item->qty;
                       			$itemdata['movement_description'] = 'Post Void';
                        		$itemdata['move_ref'] = $data['transaction_no'];
                        		$itemdata['purpose'] = "Post Void";

                       			ItemStock::addStocks($itemdata);
                       		}

                       		$sale_summary->post_void = 1;
                       		$sale_summary->update();
                       	}
                    }

                    
                }
            }
            $reader->close();
            DB::commit();
            return response()->json(array('msg' => 'file uploaded', 'status' => 0));
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(array('msg' => 'file uploaded error', 'status' => 1));
        }
    }
}
