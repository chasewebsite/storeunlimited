<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use DB;

use App\Models\PurchaseOrder;

class PurchaseOrderController extends Controller
{
    public function uploadpo(Request $request){
    	$fileName = $request->file('data')->getClientOriginalName();

        $c_folder = substr($fileName, 0,4);
        $b_folder = substr($fileName, 4,4);
        $t_folder = substr($fileName, 8,2);
        $destinationPath = storage_path().'/uploads/po/'.$c_folder.'/'.$b_folder.'/'.$t_folder;
        
        if (!\File::exists($destinationPath))
        {
            mkdir($destinationPath, 0755, true); 
        }

        $request->file('data')->move($destinationPath, $fileName);

        $filePath = $destinationPath ."/". $fileName;
       
        DB::beginTransaction();
        try {
            $reader = ReaderFactory::create(Type::CSV); // for XLSX files
            $reader->setFieldDelimiter('|');
            $reader->open($filePath);
            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $row) {

                    $company_code = str_pad($row[0], 4, "0", STR_PAD_LEFT);
                    $branch_code = $company_code.str_pad($row[2], 4, "0", STR_PAD_LEFT);
                    $terminal_code = $branch_code.str_pad($row[5], 2, "0", STR_PAD_LEFT);

                    $data['company_code'] = $company_code;
                    $data['company_name'] = $row[1];
                    $data['branch_code'] = $branch_code;
                    $data['branch_name'] = $row[3];
                    $data['supplier'] = $row[4];
                    $data['terminal_code'] = $terminal_code;
                    $data['terminal_no'] = $row[5];
                    $data['user'] = $row[6];
                    $data['po_no'] = $row[7];
                    $data['local_date'] = $row[8];
                    $data['local_time'] = date('Y-m-d H:i:s', strtotime($row[8].' '.$row[9]));
                    $data['barcode'] = $row[10];
                    $data['itemcode'] = $row[11];
                    $data['description'] = $row[12];
                    $data['qty'] = $row[13];
                    $data['price'] = $row[14];
                    $data['amount'] = $row[15];
                    $data['ref_po'] = $row[16];
                    $data['series_name'] = $row[17];
                    $data['composition'] = $row[18];
                    $data['brand'] = $row[19];
                    $data['abrev_desc'] = $row[20];
                    $data['shirt_fit'] = $row[21];
                    $data['collar_type'] = $row[22];
                    $data['color'] = $row[23];
                    $data['woven_style'] = $row[24];
                    $data['pattern_style'] = $row[25];
                    $data['sleeve_length'] = $row[26];
                    $data['neck_size'] = $row[27];

                    $item = PurchaseOrder::recordExist($data);
                    if(empty($item)){
                    	PurchaseOrder::create([
                            'company_code' => $data['company_code'],
	                        'company_name' => $data['company_name'],
                            'branch_code' => $data['branch_code'],
	                        'branch_name' => $data['branch_name'],
	                        'supplier' => $data['supplier'],
                            'terminal_code' => $data['terminal_code'],
	                        'terminal_no' => $data['terminal_no'],
                            'ref_po' => $data['ref_po'],
	                        'user' => $data['user'],
	                        'po_no' => $data['po_no'],
	                        'local_time' => $data['local_time'],
	                        'barcode' => $data['barcode'],
	                        'itemcode' => $data['itemcode'],
	                        'description' => $data['description'],
	                        'qty' => $data['qty'],
	                        'price' => $data['price'],
	                        'amount' => $data['amount'],
                            'department' => $data['department'],
                            'category' => $data['category'],
                            'brand' => $data['brand'],
                            'local_date' => $data['local_date'],
                            'series_name' => $data['series_name'],
                            'composition' => $data['composition'],
                            'shirt_fit' => $data['shirt_fit'],
                            'supplier' => $data['supplier'],
                            'collar_type' => $data['collar_type'],
                            'color' => $data['color'],
                            'woven_style' => $data['woven_style'],
                            'pattern_style' => $data['pattern_style'],
                            'sleeve_length' => $data['sleeve_length'],
                            'neck_size' => $data['neck_size'],
                            'brand' => $data['brand'],
                        ]);
                    }
                }
            }
            $reader->close();
            DB::commit();
            return response()->json(array('msg' => 'file uploaded', 'status' => 0));
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(array('msg' => 'file uploaded error', 'status' => 1));
        }
    }
}
