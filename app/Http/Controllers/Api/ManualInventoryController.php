<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ManualInventory;

class ManualInventoryController extends Controller
{
    public function download(Request $request){

    	$voucher = $request->get('voucher_number');

    	$file = ManualInventory::where('voucher_number',$voucher)->first();
    	
    	if(!empty($file)){
    		$pathToFile = storage_path().'/uploads/manualinventory/'.$file->filename;
			return response()->download($pathToFile);
    	}   
    	else{

    		return response()->json(array('msg' => 'voucher number not found!', 'status' => 0));
    	} 	
    }
}
