<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

use Symfony\Component\HttpFoundation\StreamedResponse;

use App\Models\Item;
use Artisan;

class ItemMasterfileController extends Controller
{
    public function downloadmasterfile(Request $request, $company_id, $pos_type, $timestamps = null){

        $data['date'] = $timestamps;
        $data['company_id'] = $company_id;
        $data['pos_type_id'] = $pos_type;

        $writer = WriterFactory::create(Type::CSV); // for CSV files
        $writer->openToBrowser('items.csv'); // stream data directly to the browser
        $take = 1000; // adjust this however you choose
        $skip = 0; // used to skip over the ones you've already processed
        $cnt = 1;

        $writer->addRow(['count',Item::recordCount($data)]); // add a row at a time

        while($rows = Item::getPartial($data,$take,$skip))
        {
            if(count($rows) == 0){
                break;
            }
            $skip ++;
            $plunck_data = [];
            foreach($rows as $row)
            {
                $row_data = [];
                $attrs = $row->getAttributes();
                foreach ($attrs as $key => $value) {
                    $row_data[] = $value;
                }
                $plunck_data[] = $row_data;
                
            }
            $writer->addRows($plunck_data);
        }

        $writer->close();

        // $filename = $company_id.$branch_terminal.$pos_type;
        // Artisan::call('generate:masterfile', ['company_id' => $company_id, 'pos_type' => $pos_type, 'branch_terminal' => $branch_terminal, 'timestamps' => $timestamps]);
        // return response()->download(storage_path().'/download/'.$filename.'.txt', 'items.txt');  

        // dd(strtotime( '-1 days' ));
        
        // dd($data);

        // $response = new StreamedResponse(function() use ($data){
        //     // Open output stream
        //     $handle = fopen('php://output', 'w');

        //     $take = 1000; // adjust this however you choose
        //     $skip = 0; // used to skip over the ones you've already processed
        //     $cnt = 1;

        //     if(Item::haveUpdate($data)){
        //         while($rows = Item::getPartial($data,$take,$skip))
        //         {
        //             if(count($rows) == 0){
        //                 break;
        //             }
        //             $skip ++;
        //             $plunck_data = [];
        //             $header = true;
        //             foreach($rows as $key => $row)
        //             {
        //                 $attrs = $row->getAttributes();
        //                 $row_data = [];
                        
        //                 foreach ($attrs as $key => $value) {
        //                     $row_data[] = $value;
        //                     $row_header[] = $key;
        //                 }

        //                 if($header){
        //                     fputcsv($handle,  $row_header);
        //                     $header = false;
        //                 }else{
        //                     fputcsv($handle,  $row_data);
        //                 }
        //             }
        //         }
        //     }
            
        //     // Close the output stream
        //     fclose($handle);

        //     }, 200, [
        //         'Content-Type' => 'text/csv',
        //         'Content-Disposition' => 'attachment; filename="items.csv"',
        //     ]);

        // return $response;    

        // \Excel::create('Filename', function($excel) use($data) {

        //     $excel->sheet('Sheetname', function($sheet) use($data) {
        //         $take = 1000; // adjust this however you choose
        //         $skip = 0; // used to skip over the ones you've already processed
        //         $cnt = 1;
        //         while($rows = Item::getPartial($data,$take,$skip))
        //         {
        //             if(count($rows) == 0){
        //                 break;
        //             }
        //             $skip ++;
        //             $sheet->fromModel($rows);
        //         }

        //     });

        // })->export('xls');
    }
}
