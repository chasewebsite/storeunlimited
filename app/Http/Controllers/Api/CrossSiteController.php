<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\SaleSummary;
use App\Models\SaleDetail;
use App\Models\ItemStock;
use App\Models\CardDetail;
use App\Models\OtherPayment;
use App\Models\AtmDetail;
use App\Models\CorporateDetail;
use App\Models\CheckDetail;
use App\Models\DeferredDetail;
use App\Models\GiftCard;
use App\Models\TextFile;

class CrossSiteController extends Controller
{
    public function uploadsalestokyo(Request $request)
    {
        $fileName = $request->file('data')->getClientOriginalName();

        $c_folder = substr($fileName, 0,4);
        $b_folder = substr($fileName, 4,4);
        $t_folder = substr($fileName, 8,2);
        $destinationPath = storage_path().'/uploads/cors/sales/'.$c_folder.'/'.$b_folder.'/'.$t_folder;

        if (!\File::exists($destinationPath))
        {
            mkdir($destinationPath, 0755, true); 
        }

        if($request->hasFile('data')){
            
            $file = Input::file('data');

            $filename = $file->getClientOriginalName();
            $date = date('Y-m-d', strtotime($request->date));
            $file->move($destinationPath,$filename);

            $new_file = new TextFile();
            $new_file->filename = $destinationPath.'/'.$filename;
            $new_file->type = 'SALES';
            $new_file->save();

            $data = 'success';
            return $data;
            
        }
        $data = 'failed';
        return $data;
    }

    public function uploadretextokyo(Request $request)
    {
    	$fileName = $request->file('data')->getClientOriginalName();

        $c_folder = substr($fileName, 0,4);
        $b_folder = substr($fileName, 4,4);
        $t_folder = substr($fileName, 8,2);
        $destinationPath = storage_path().'/uploads/cors/return/'.$c_folder.'/'.$b_folder.'/'.$t_folder;

        if (!\File::exists($destinationPath))
        {
            mkdir($destinationPath, 0755, true); 
        }

    	if($request->hasFile('data')){
            
            $file = Input::file('data');

            $filename = $file->getClientOriginalName();
            $date = date('Y-m-d', strtotime($request->date));
            $file->move($destinationPath,$filename);

            $new_file = new TextFile();
            $new_file->filename = $destinationPath.'/'.$filename;
            $new_file->type = 'RETURN';
            $new_file->save();

            $data = 'success';
            return $data;
            
        }
        $data = 'failed';
        return $data;
    }

    public function uploadincomingtokyo(Request $request)
    {
        $fileName = $request->file('data')->getClientOriginalName();

        $c_folder = substr($fileName, 0,4);
        $b_folder = substr($fileName, 4,4);
        $t_folder = substr($fileName, 8,2);
        $destinationPath = storage_path().'/uploads/cors/incoming/'.$c_folder.'/'.$b_folder.'/'.$t_folder;

        if (!\File::exists($destinationPath))
        {
            mkdir($destinationPath, 0755, true); 
        }

        if($request->hasFile('data')){
            
            $file = Input::file('data');

            $filename = $file->getClientOriginalName();
            $date = date('Y-m-d', strtotime($request->date));
            $file->move($destinationPath,$filename);

            $new_file = new TextFile();
            $new_file->filename = $destinationPath.'/'.$filename;
            $new_file->type = 'INCOMING';
            $new_file->save();

            $data = 'success';
            return $data;
            
        }
        $data = 'failed';
        return $data;
    }
}
