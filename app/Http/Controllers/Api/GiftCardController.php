<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use DB;

use App\Models\GiftCard;

class GiftCardController extends Controller
{
    public function checkgiftcard(Request $request,$code = null){
    	$giftcard = GiftCard::where('serial_no', $code)->first();
    	if(!empty($giftcard)){
    		if($giftcard->transaction_no != ''){
        		$giftcard = ['status' => '2',
        		'serial_no' => $code];
        	}else{
        		$giftcard = ['status' => '1',
        		'serial_no' => $code,
        		'start_date' => date_format(date_create($giftcard->start_date), 'm/d/Y'),
        		'end_date' => date_format(date_create($giftcard->end_date), 'm/d/Y')];
        	}
    	}else{
    		$giftcard = ['status' => '0',
            	'serial_no' => $code
            ];
    	}

    	\Excel::create('giftcard', function($excel)  use ($giftcard){
            $excel->sheet('Sheet1', function($sheet) use($giftcard) {
                $sheet->fromArray($giftcard, null, 'A1', false);
            });
        })->download('csv');
    }

    public function uploadgiftcard(Request $request){
        $fileName = $request->file('data')->getClientOriginalName();

        $c_folder = substr($fileName, 0,4);
        $b_folder = substr($fileName, 4,4);
        $t_folder = substr($fileName, 8,2);
        $destinationPath = storage_path().'/uploads/giftcard/'.$c_folder.'/'.$b_folder.'/'.$t_folder;
        
        if (!\File::exists($destinationPath))
        {
            mkdir($destinationPath, 0755, true); 
        }

        $request->file('data')->move($destinationPath, $fileName);

        $filePath = $destinationPath ."/". $fileName;
       
        DB::beginTransaction();
        try {
            $reader = ReaderFactory::create(Type::CSV); // for XLSX files
            $reader->setFieldDelimiter('|');
            $reader->open($filePath);
            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $row) {
                    $company_code = str_pad($row[0], 4, "0", STR_PAD_LEFT);
                    $branch_code = $company_code.str_pad($row[2], 4, "0", STR_PAD_LEFT);
                    $terminal_code = $branch_code.str_pad($row[4], 2, "0", STR_PAD_LEFT);

                    $data['company_code'] = $company_code;
                    $data['company_name'] = $row[1];
                    $data['branch_code'] = $branch_code;
                    $data['branch_name'] = $row[3];
                    $data['terminal_code'] = $terminal_code;
                    $data['terminal_no'] = $row[4];
                    $data['user'] = $row[5];
                    $data['member'] = $row[6];
                    $data['transaction_no'] = $row[7];
                    $data['local_time'] = date('Y-m-d H:i:s', strtotime($row[8].' '.$row[9]));
                    $data['serial_no'] = $row[10];

                    $giftcard = GiftCard::recordExist($data);
                    
                    if(!empty($giftcard)){
                        $giftcard->branch_code = $data['branch_code'];
                        $giftcard->branch_name = $data['branch_name'];
                        $giftcard->terminal_code = $data['terminal_code'];
                        $giftcard->terminal_no = $data['terminal_no'];
                        $giftcard->user = $data['user'];
                        $giftcard->member = $data['member'];
                        $giftcard->transaction_no = $data['transaction_no'];
                        $giftcard->local_time = $data['local_time'];
                        $giftcard->update();
                    }

                    
                }
            }
            $reader->close();
            DB::commit();
            return response()->json(array('msg' => 'file uploaded', 'status' => 0));
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(array('msg' => 'file uploaded error', 'status' => 1));
        }
    }
}
