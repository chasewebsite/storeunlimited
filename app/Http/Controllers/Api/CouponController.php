<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Coupon;

class CouponController extends Controller
{
    public function store(Request $request){
    	$coupon = Coupon::checkCode($request->code);
    	if(!empty($coupon)){
    		return response()->json(array('msg' => 'coupon already exist', 'status' => 1));
    	}else{
    		Coupon::create(['coupon_code' => $request->code,
                'c_company_name' => $request->company,
                'c_branch_name' => $request->branch,
                'c_terminal_no' => $request->terminal,
    			'c_cahier' => $request->cashier,
    			'date_time_created' => $request->datetime]);
    		return response()->json(array('msg' => 'coupon successfuly created', 'status' => 0));
    	}
    }


    public function update(Request $request){
        // $coupon = Coupon::checkCode($request->code);
        // if(!empty($coupon)){
        //     $coupon->redeemed_by = =$request->redeemed_by;
        //     $coupon->r_cashier = $request->cashier;
        //     $coupon->r_company_name = $request->company;
        //     $coupon->r_branch_name = $request->branch;
        //     $coupon->r_terminal_no = $request->terminal;
        //     $coupon->r_transaction_no = $request->datetime;
        //     $coupon->redeemed = 1;
        //     $coupon->update();

        //     return response()->json(array('msg' => 'coupon successfuly updated', 'status' => 0));
        // }else{
        //     return response()->json(array('msg' => 'coupon not found', 'status' => 1));

        // }
    }

    public function verify(Request $request,$code = null){
    	$coupon = Coupon::checkCode($code);
    	if(!empty($coupon)){
    		return response()->json($coupon);
    	}else{
    		return response()->json(array('msg' => 'coupon not found', 'status' => 1));
    	}
    }
}
