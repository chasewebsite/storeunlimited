<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use DB;

use App\Models\RefundItem;
use App\Models\ItemStock;

class RefundItemController extends Controller
{
    public function uploadrefund(Request $request){
    	$fileName = $request->file('data')->getClientOriginalName();

        $c_folder = substr($fileName, 0,4);
        $b_folder = substr($fileName, 4,4);
        $t_folder = substr($fileName, 8,2);
        $destinationPath = storage_path().'/uploads/refund/'.$c_folder.'/'.$b_folder.'/'.$t_folder;
        
        if (!\File::exists($destinationPath))
        {
            mkdir($destinationPath, 0755, true); 
        }

        $request->file('data')->move($destinationPath, $fileName);

        $filePath = $destinationPath ."/". $fileName;
       
        DB::beginTransaction();
        try {
            $reader = ReaderFactory::create(Type::CSV); // for XLSX files
            $reader->setFieldDelimiter('|');
            $reader->open($filePath);
            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $row) {
                    $company_code = str_pad($row[0], 4, "0", STR_PAD_LEFT);
                    $branch_code = $company_code.str_pad($row[2], 4, "0", STR_PAD_LEFT);
                    $terminal_code = $branch_code.str_pad($row[4], 2, "0", STR_PAD_LEFT);

                    $data['company_code'] = $company_code;
                    $data['company_name'] = $row[1];
                    $data['branch_code'] = $branch_code;
                    $data['branch_name'] = $row[3];
                    $data['terminal_code'] = $terminal_code;
                    $data['terminal_no'] = $row[4];
                    $data['user'] = $row[5];
                    $data['transaction_no'] = $row[6];
                    $data['ref_no'] = $row[7];
                    $data['local_time'] = date('Y-m-d H:i:s', strtotime($row[8].' '.$row[9]));
                    $data['ctr'] = $row[10];
                    $data['barcode'] = $row[11];
                    $data['itemcode'] = $row[12];
                    $data['description'] = $row[13];
                    $data['qty'] = $row[14];
                    $data['price'] = $row[15];
                    $data['amount'] = $row[16];
                    $data['customer_name'] = $row[17];
                    $data['series_name'] = $row[18];
                    $data['composition'] = $row[19];
                    $data['brand'] = $row[20];
                    $data['abrev_desc'] = $row[21];
                    $data['shirt_fit'] = $row[22];
                    $data['supplier'] = $row[23];
                    $data['collar_type'] = $row[24];
                    $data['color'] = $row[25];
                    $data['woven_style'] = $row[26];
                    $data['pattern_style'] = $row[27];
                    $data['sleeve_length'] = $row[28];
                    $data['neck_size'] = $row[29];

                    $item = RefundItem::recordExist($data);

                    if(empty($item)){
                        RefundItem::create([
                        'company_code' => $data['company_code'],
                        'company_name' => $data['company_name'],
                        'branch_code' => $data['branch_code'],
                        'branch_name' => $data['branch_name'],
                        'terminal_code' => $data['terminal_code'],
                        'terminal_no' => $data['terminal_no'],
                        'user' => $data['user'],
                        'transaction_no' => $data['transaction_no'],
                        'ref_no' => $data['ref_no'],
                        'local_time' => $data['local_time'],
                        'ctr' => $data['ctr'],
                        'barcode' => $data['barcode'],
                        'itemcode' => $data['itemcode'],
                        'description' => $data['description'],
                        'qty' => $data['qty'],
                        'price' => $data['price'],
                        'amount' => $data['amount'],
                        'customer_name' => $data['customer_name'],
                        'series_name' => $data['series_name'],
                        'composition' => $data['composition'],
                        'shirt_fit' => $data['shirt_fit'],
                        'supplier' => $data['supplier'],
                        'collar_type' => $data['collar_type'],
                        'color' => $data['color'],
                        'woven_style' => $data['woven_style'],
                        'pattern_style' => $data['pattern_style'],
                        'sleeve_length' => $data['sleeve_length'],
                        'neck_size' => $data['neck_size'],
                        'brand' => $data['brand'],
                        ]);

                        $data['movement_description'] = 'Refund Items';
                        $data['move_ref'] = $data['transaction_no'];
                        $data['purpose'] = 'Refund Item';

                        ItemStock::addStocks($data);
                    }

                    
                }
            }
            $reader->close();
            DB::commit();
            return response()->json(array('msg' => 'file uploaded', 'status' => 0));
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(array('msg' => 'file uploaded error', 'status' => 1));
        }
    }
}
