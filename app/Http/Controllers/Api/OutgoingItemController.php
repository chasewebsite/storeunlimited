<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;

use App\Models\OutgoingItem;
use App\Models\ItemStock;
use DB;


class OutgoingItemController extends Controller
{
    public function stocksstorebranch(Request $request){
        $data['selection'] = OutgoingItem::getCompaniesBranch($request);
        return \Response::json($data,200);
    }

    public function stocksstorecategory(Request $request){
        $data['selection'] = OutgoingItem::getCompaniesBranchCategory($request);
        return \Response::json($data,200);
    }

    public function stocksstorebrand(Request $request){
        $data['selection'] = OutgoingItem::getCompaniesBranchCategoryBrand($request);
        return \Response::json($data,200);
    }

    public function uploadoutgoing(Request $request){        
    	$fileName = $request->file('data')->getClientOriginalName();

        $c_folder = substr($fileName, 0,4);
        $b_folder = substr($fileName, 4,4);
        $t_folder = substr($fileName, 8,2);
        $destinationPath = storage_path().'/uploads/outgoing/'.$c_folder.'/'.$b_folder.'/'.$t_folder;
        
        if (!\File::exists($destinationPath))
        {
            mkdir($destinationPath, 0755, true); 
        }

        $request->file('data')->move($destinationPath, $fileName);

        $filePath = $destinationPath ."/". $fileName;
       
        DB::beginTransaction();
        try {
            $reader = ReaderFactory::create(Type::CSV); // for XLSX files
            $reader->setFieldDelimiter('|');
            $reader->open($filePath);
            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $row) {

                    $company_code = str_pad($row[0], 4, "0", STR_PAD_LEFT);
                    $branch_code = $company_code.str_pad($row[2], 4, "0", STR_PAD_LEFT);
                    $terminal_code = $branch_code.str_pad($row[4], 2, "0", STR_PAD_LEFT);

                    $data['company_code'] = $company_code;
                    $data['company_name'] = $row[1];
                    $data['branch_code'] = $branch_code;
                    $data['branch_name'] = $row[3];
                    $data['terminal_code'] = $terminal_code;
                    $data['terminal_no'] = $row[4];
                    $data['user'] = $row[5];
                    $data['outgoing_no'] = $row[6];
                    $data['to_company_code'] = str_pad($row[7], 4, "0", STR_PAD_LEFT);
                    $data['to_company'] = $row[8];
                    $data['to_branch_code'] = str_pad($row[7], 4, "0", STR_PAD_LEFT).str_pad($row[9], 4, "0", STR_PAD_LEFT);
                    $data['to_branch'] = $row[10];
                    $data['local_time'] = date('Y-m-d H:i:s', strtotime($row[11].' '.$row[12]));
                    $data['barcode'] = $row[13];
                    $data['itemcode'] = $row[14];
                    $data['description'] = $row[15];
                    $data['qty'] = $row[16];
                    $data['price'] = $row[17];
                    $data['amount'] = $row[18];
                    $data['box_no'] = $row[19];
                    $data['pouch_no'] = $row[20];
                    $data['unique_code'] = $row[21];
                    $data['purpose'] = $row[22];
                    $data['local_date'] = date('Y-m-d',strtotime($row[11]));
                    $data['series_name'] = $row[23];
                    $data['composition'] = $row[24];
                    $data['brand'] = $row[25];
                    $data['abrev_desc'] = $row[26];
                    $data['shirt_fit'] = $row[27];
                    $data['supplier'] = $row[28];
                    $data['collar_type'] = $row[29];
                    if(count($row) > 29){
                        if(!empty($row[30])){
                            $data['color'] = $row[30];
                        }else{
                            $data['color'] = '';    
                        }
                    }else{
                        $data['color'] = '';
                    }
                    if(count($row) > 30){
                        if(!empty($row[31])){
                            $data['woven_style'] = $row[31];
                        }else{
                            $data['woven_style'] = '';    
                        }
                    }else{
                        $data['woven_style'] = '';
                    }
                    if(count($row) > 31){
                        if(!empty($row[32])){
                            $data['pattern_style'] = $row[32];
                        }else{
                            $data['pattern_style'] = '';    
                        }
                    }else{
                        $data['pattern_style'] = '';
                    }
                    if(count($row) > 32){
                        if(!empty($row[33])){
                            $data['sleeve_length'] = $row[33];
                        }else{
                            $data['sleeve_length'] = '';    
                        }
                    }else{
                        $data['sleeve_length'] = '';
                    }
                    if(count($row) > 33){
                        if(!empty($row[33])){
                            $data['neck_size'] = $row[34];
                        }else{
                            $data['neck_size'] = '';    
                        }
                    }else{
                        $data['neck_size'] = '';
                    }


                    $item = OutgoingItem::recordExist($data);
                    if(empty($item)){
                        OutgoingItem::create([
                            'company_code' => $data['company_code'],
                            'company_name' => $data['company_name'],
                            'branch_code' => $data['branch_code'],
                            'branch_name' => $data['branch_name'],
                            'terminal_code' => $data['terminal_code'],
                            'terminal_no' => $data['terminal_no'],
                            'user' => $data['user'],
                            'outgoing_no' => $data['outgoing_no'],
                            'to_company_code' => $data['to_company_code'],
                            'to_company' => $data['to_company'],
                            'to_branch_code' => $data['to_branch_code'],
                            'to_branch' => $data['to_branch'],
                            'local_time' => $data['local_time'],
                            'barcode' => $data['barcode'],
                            'itemcode' => $data['itemcode'],
                            'description' => $data['description'],
                            'qty' => $data['qty'],
                            'price' => $data['price'],
                            'amount' => $data['amount'],
                            'box_no' => $data['box_no'],
                            'pouch_no' => $data['pouch_no'],
                            'unique_code' => $data['unique_code'],
                            'local_date' => $data['local_date'],
                            'purpose' => $data['purpose'],
                            'series_name' => $data['series_name'],
                            'composition' => $data['composition'],
                            'shirt_fit' => $data['shirt_fit'],
                            'supplier' => $data['supplier'],
                            'collar_type' => $data['collar_type'],
                            'color' => $data['color'],
                            'woven_style' => $data['woven_style'],
                            'pattern_style' => $data['pattern_style'],
                            'sleeve_length' => $data['sleeve_length'],
                            'neck_size' => $data['neck_size'],
                            'brand' => $data['brand'],

                        ]);

                        $data['movement_description'] = 'Outgoing Items';
                        $data['move_ref'] = $data['outgoing_no'];
                        $data['purpose'] = $data['purpose'];

                        ItemStock::removeStocks($data);
                    }else{
                        print_r($item);
                        echo "<br>";
                    }
                    
                }
            }
            $reader->close();
            DB::commit();
            return response()->json(array('msg' => 'file uploaded', 'status' => 0));
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(array('msg' => 'file uploaded error', 'status' => 1));
        }
    }

    public function outgoing($company_code, $branch_code){

        $company_code =  str_pad($company_code, 4, "0", STR_PAD_LEFT);
        $branch_code  =  $company_code.str_pad($branch_code, 4, "0", STR_PAD_LEFT);

        $date_to    = date('Y-m-d');
        $date_from  = date('Y-m-d', strtotime('-15 days'));


        $items = OutgoingItem::select('company_code', 'branch_code', 'barcode', 'qty', 'box_no', 'pouch_no', 'unique_code', 'outgoing_no')
            ->where('to_company_code', $company_code)
            ->where('to_branch_code', $branch_code)
            ->where('incoming_no', '')
            ->where('local_date','>=', $date_from)
            ->where('local_date', '<=', $date_to)
            ->get();
        
        \Excel::create($company_code.$branch_code, function($excel)  use ($items){
            $excel->sheet('Sheet1', function($sheet) use($items) {
                $sheet->fromArray($items, null, 'A1', false);
            });
        })->download('csv');
    }

    public function getitem(Request $request) {

        $outgoing_no = $request->outgoing_no;
        $outgoing_items = OutgoingItem::select('barcode','qty','description','price')
                        ->where('outgoing_no')
                        ->get();

        if(!empty($outgoing_items)) {

            $writer = WriterFactory::create(Type::CSV);
            $writer->openToBrowser('outgoing_items.txt');
             foreach ($outgoing_items as $item) {

                    $data[0] = $item->barcode;
                    $data[1] = $item->qty;
                    $data[2] = $item->description;
                    $data[3] = $item->price;
                    $writer->addRow($data);
            }

            $writer->close();
        }

    }
}
