<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MasterfileController extends Controller
{
    public function upload(Request $request){
    	$fileName = $request->file('data')->getClientOriginalName();

        $destinationPath = storage_path().'/uploads/masterfile/';
        
        if (!\File::exists($destinationPath))
        {
            mkdir($destinationPath, 0755, true); 
        }

        $request->file('data')->move($destinationPath, $fileName);

        $filePath = $destinationPath ."/". $fileName;

        return response()->json(array('msg' => 'file uploaded', 'status' => 0));
    }

    public function download(){
    	$pathToFile = storage_path().'/uploads/masterfile/masterfile.arj';
		return response()->download($pathToFile);
    }
}
