<?php

namespace App\Http\Controllers\InventoryReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use File;
use Alchemy\Zippy\Zippy;

use App\Models\UserBranch;
use App\Models\Company;
use App\Models\CompanyBranch;
use App\Models\IncomingItem;
use App\Models\OutgoingItem;
use App\Models\ItemStock;
use App\Models\SaleSummary;

use DB;

use Artisan;

class ExcelReportController extends Controller
{
    //

     public static function audit_stock_index(Request $request){

        $date_from = date('m/d/Y');
        $date_to   = date('m/d/Y');
        $branches  = UserBranch::getUserBranch(Auth::user()->id);

        return view('reports.inventory.audit_stocks',compact('date_from','branches','date_to'));

    }


    public static function audit_stocks_store(Request $request)
    {

        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $date_from      = $request->date_from;
        $date_to        = $request->date_to;
        $_branch        = $request->branch;
        $folder_path = storage_path().'/audit_report/'.date('Y-m-d-H-i-s');
        if(!File::exists($folder_path)) {
                
                    
                        File::makeDirectory($folder_path,0755, true);
                    
        }

        Artisan::call('generate:audit_report', ['folder_path'=> $folder_path,'branch' => $_branch, 'date_from' => $date_from ,'date_to' =>$date_to]);
        $zip_path = $folder_path.'/AuditInventoryReport.zip';
        return response()->download($zip_path);



        // $stock_in       = IncomingItem::transfer_in( $branch,$date_from,$date_to );
        // $stock_out      = OutgoingItem::transfer_out( $branch,$date_from,$date_to );
        // $stock_summary  = ItemStock::getStockMovementSummary( $branch,$date_from,$date_to );
        // $stock_detailed = ItemStock::getStockMovementDetailed( $branch,$date_from,$date_to );

        // $filename = "INVENTORY_".$branch->branch."_FROM".$date_from."_TO".$date_to;

        // \Excel::create($filename, function($excel)  use ($stock_in,$stock_out,$stock_summary,$stock_detailed,$branch,$date_from,$date_to){
        //                 $excel->sheet("STOCK TRANSFER IN REGISTER", function($sheet) use($stock_in,$branch,$date_from,$date_to) {


        //                     $sheet->row(1, array($branch->company->company));
        //                     $sheet->row(2, array("STOCK TRANSFER IN REGISTER"));
        //                     $sheet->row(3, array("FROM ".$date_from." "."TO ".$date_to));
        //                     $sheet->row(6,array("DATE","DOC #","CATEGORY","MODEL/DESCRIPTION","STOCK CODE","FROM","TO","UOM","QTY","UNIT PRICE","AMOUNT"));

        //                     $cnt = 7;

        //                     foreach ($stock_in as $stock) {
        //                             $sheet->row($cnt,array(
        //                                 $stock->local_date,
        //                                 " ",
        //                                 $stock->category,
        //                                 $stock->description,
        //                                 $stock->itemcode,
        //                                 $stock->from_branch,
        //                                 $stock->branch_name,
        //                                 "pcs",
        //                                 $stock->qty,
        //                                 $stock->price,
        //                                 $stock->amount,

        //                             ));

        //                             $cnt++;
        //                     }
                           
                           

        //                 });
        //                 $excel->sheet("STOCK TRANSFER OUT REGISTER", function($sheet) use($stock_out,$branch,$date_from,$date_to) {
        //                     $sheet->row(1, array($branch->company->company));
        //                     $sheet->row(2, array("STOCK TRANSFER OUT REGISTER"));
        //                     $sheet->row(3, array("FROM ".$date_from." "."TO ".$date_to));
        //                     $sheet->row(6,array("DATE","DOC #","CATEGORY","MODEL/DESCRIPTION","STOCK CODE","FROM","TO","UOM","QTY","UNIT PRICE","AMOUNT"));

        //                     $cnt = 7;

        //                     foreach ($stock_out as $stock) {
                                

        //                             $sheet->row($cnt,array(
        //                                 $stock->local_date,
        //                                 " ",
        //                                 $stock->category,
        //                                 $stock->description,
        //                                 $stock->itemcode,
        //                                 $stock->branch_name,
        //                                 $stock->to_branch,
        //                                 "pcs",
        //                                 $stock->qty,
        //                                 $stock->price,
        //                                 $stock->amount,

        //                             ));

        //                             $cnt++;
        //                     }

        //                 });

        //                 $excel->sheet("DETAILED STOCK MOVEMENT REPORT ", function($sheet) use($stock_detailed,$branch,$date_from,$date_to) {


        //                     $sheet->row(1, array($branch->company->company));
        //                     $sheet->row(2, array("STOCK MOVEMENT"));
        //                     $sheet->row(3, array("FROM ".$date_from." "."TO ".$date_to));
        //                     $sheet->row(6, array("DATE","CATEGORY","DESCRIPTION","STOCK CODE","BEGINNING","ENDING"));
        //                     if(count($stock_detailed) > 0) {

        //                         $cnt = 7;
                               
        //                         foreach ($stock_detailed as $item) {
        //                                 $sheet->row($cnt,array(
        //                                     $item->transaction_date,
        //                                     $item->category,
        //                                     $item->description,
        //                                     $item->itemcode,
        //                                     $item->begining,
        //                                     $item->ending
                                           

        //                                 ));
                                       

        //                                 $cnt++;
        //                         }
                            

        //                     }
                           
                           

        //                 });

        //                   $excel->sheet("SUMMARY OF STOCK MOVEMENT ", function($sheet) use($stock_summary,$branch,$date_from,$date_to) {


        //                     $sheet->row(1, array($branch->company->company));
        //                     $sheet->row(2, array("STOCK MOVEMENT"));
        //                     $sheet->row(3, array("FROM ".$date_from." TO ".$date_to));
        //                     $sheet->row(6, array("CATEGORY","DESCRIPTION","STOCK CODE","BEGINNING","ENDING"));
        //                     if(count($stock_summary) > 0) {

        //                            $cnt = 7;
        //                         foreach ($stock_summary as $item) {

        //                             if(!empty($item->category) && !empty($item->description) && !empty($item->itemcode)) {
        //                                 $sheet->row($cnt,array(
                                           
        //                                     $item->category,
        //                                     $item->description,
        //                                     $item->itemcode,
        //                                     $item->begining,
        //                                     $item->ending
                                           

        //                                 ));
                                       

        //                                 $cnt++;
        //                             }
        //                         }
                            

        //                     }
                           
                           

        //                 });

        //             })->export('xlsx');





    }



    public function stock_transfer_in_index(Request $request)
    {

        $date_from = date('m/d/Y');
        $date_to = date('m/d/Y');
        $branches = UserBranch::getUserBranch(Auth::user()->id);

        return view('reports.inventory.stock_transfer_in',compact('date_from','date_to','branches'));

    }

    public function stock_transfer_in_store(Request $request)
    {
        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $branch  = CompanyBranch::where('branch_code',$request->branch)->first();
        $stock_in       = IncomingItem::transfer_in( $branch,$date_from,$date_to );
        $stock_in_filename = "STOCK TRANSFER IN REGISTER_".$branch->branch;
        $company_code = Company::find(1)->company_code;      

             

        \Excel::create($stock_in_filename, function($excel)  use ($stock_in,$branch,$date_from,$date_to,$company_code) {
                      
                        
                 $excel->sheet("STOCK TRANSFER IN REGISTER", function($sheet) use($stock_in,$branch,$date_from,$date_to,$company_code) {


                            $sheet->row(1, array($branch->company->company));
                            $sheet->row(2, array("STOCK TRANSFER IN REGISTER"));
                            $sheet->row(3, array("FROM ".$date_from." "."TO ".$date_to));
                            $sheet->row(6,array("DATE","INCOMING #","REF. OUTGOING #","CATEGORY","MODEL/DESCRIPTION","STOCK CODE","FROM","TO","UOM","QTY","UNIT PRICE","AMOUNT","PURPOSE"));

                            $cnt = 7;


                            foreach ($stock_in as $stock) {
                                $total_amount = 0 ;
                                if($stock->price != 0 ){

                                    $total_amount = $stock->price * $stock->qty;

                                }

                                $ref_shop = "  ";
                                if(!empty($stock->outgoing_no)) {
                                    $result = substr($stock->outgoing_no, 0, 4);
                                    $_branch_code = $company_code.$result;
                                    $_branch_name = CompanyBranch::where('branch_code',$_branch_code)->first()->branch;
                                    if($_branch_name){
                                        $ref_shop = $_branch_name;
                                    }
                                    else {
                                        $ref_shop = "NO DATA";
                                    }

                                }
                                else {

                                    $ref_shop = "NO DATA";
                                }

                                
                                    $sheet->row($cnt,array(
                                        $stock->local_date,
                                        $stock->incoming_no,
                                        $stock->outgoing_no,
                                        $stock->category,
                                        $stock->description,
                                        $stock->itemcode,
                                        $ref_shop,
                                        $stock->branch_name,
                                        "pcs",
                                        $stock->qty,
                                        $stock->price,
                                        $total_amount,
                                        $stock->purpose,

                                    ));

                                    $cnt++;
                            }
                           
                           

                        });
                       

                          

        })->export('xlsx');

    }






    public function stock_transfer_out_index(Request $request)
    {

        $date_from = date('m/d/Y');
        $date_to = date('m/d/Y');
        $branches = UserBranch::getUserBranch(Auth::user()->id);

        return view('reports.inventory.stock_transfer_out',compact('date_from','date_to','branches'));

    }

    public function stock_transfer_out_store(Request $request)
    {
        
        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $branch  = CompanyBranch::where('branch_code',$request->branch)->first();

        $stock_out_filename = "STOCK TRANSFER OUT REGISTER_".$branch->branch;
        $stock_out      = OutgoingItem::transfer_out( $branch,$date_from,$date_to );
           \Excel::create($stock_out_filename, function($excel)  use ($stock_out,$branch,$date_from,$date_to) {
                      
                        
                $excel->sheet("STOCK TRANSFER OUT REGISTER", function($sheet) use($stock_out,$branch,$date_from,$date_to) {
                            $sheet->row(1, array($branch->company->company));
                            $sheet->row(2, array("STOCK TRANSFER OUT REGISTER"));
                            $sheet->row(3, array("FROM ".$date_from." "."TO ".$date_to));
                            $sheet->row(6,array("DATE","OUTGOING #", "REF. INCOMING #","CATEGORY","MODEL/DESCRIPTION","STOCK CODE","FROM","TO","UOM","QTY","UNIT PRICE","AMOUNT","PURPOSE"));

                            $cnt = 7;

                            foreach ($stock_out as $stock) {
                                     $total_amount = 0 ;
                                    if($stock->price != 0 ){

                                        $total_amount = $stock->price * $stock->qty;

                                    }

                                    $sheet->row($cnt,array(
                                        $stock->local_date,
                                        $stock->outgoing_no,
                                        $stock->incoming_no,
                                        $stock->category,
                                        $stock->description,
                                        $stock->itemcode,
                                        $stock->branch_name,
                                        $stock->to_branch,
                                        "pcs",
                                        $stock->qty,
                                        $stock->price,
                                        $total_amount,
                                        $stock->purpose,

                                    ));

                                    $cnt++;
                            }

                        });

                          

        })->export('xlsx');

       

    }


    public static function stock_movement_detailed_index(Request $request){

        $date_from = date('m/d/Y');
        $date_to = date('m/d/Y');
     
        
        $branches = UserBranch::getUserBranch(Auth::user()->id);

        return view('reports.inventory.stock_movement_detailed',compact('date_from','branches','date_to'));

    }

    public static function stock_movement_detailed_store(Request $request) {
               set_time_limit(0);
                ini_set('memory_limit', '-1');
                $date_from = $request->date_from;
                $date_to = $request->date_to;
               
                
                $_branch = $request->branch;

           

              

                    $branch = CompanyBranch::where('branch_code',$_branch)->first();
                    $stock_detailed = ItemStock::getStockMovementDetailed( $branch,$date_from,$date_to );

                  $stock_detailed_filename = "STOCK DETAILED_".$branch->branch;
                \Excel::create($stock_detailed_filename, function($excel)  use ($stock_detailed,$branch,$date_from,$date_to) {
                      
                        
                $excel->sheet("DETAILED STOCK MOVEMENT REPORT ", function($sheet) use($stock_detailed,$branch,$date_from,$date_to) {


                            $sheet->row(1, array($branch->company->company));
                            $sheet->row(2, array("STOCK MOVEMENT"));
                            $sheet->row(3, array("FROM ".$date_from." "."TO ".$date_to));
                            $sheet->row(6, array("DATE","CATEGORY","DESCRIPTION","STOCK CODE","BEGINNING","IN","OUT","ENDING"));
                            if(count($stock_detailed) > 0) {

                                $cnt = 7;
                               

                                foreach ($stock_detailed as $item) {

                                    // $in = 0;
                                    // $out = 0;
                                    // if($item->begining != $item->ending) {

                                    //     if($item->begining > $item->ending) {
                                    //         $out = $item->begining - $item->ending;
                                    //     }
                                    //     else if ($item->begining < $item->ending) {

                                    //         $in = $item->ending - $item->begining;
                                    //     }


                                    // }
                               


                                        $sheet->row($cnt,array(
                                            $item->transaction_date,
                                            $item->category,
                                            $item->description,
                                            $item->itemcode,

                                            $item->begining,
                                            $item->in_qty,
                                            $item->out_qty,
                                            $item->ending
                                           
                                           

                                        ));
                                       

                                        $cnt++;
                                }
                            

                            }
                           
                           

                        });

                          

                    })->export('xls');
        



                

         






    }

    public static function stock_movement_summary_index(Request $request){

        $date_from = date('m/d/Y');
        $date_to = date('m/d/Y');
     
        
        $branches = UserBranch::getUserBranch(Auth::user()->id);

        return view('reports.inventory.stock_movement_summary',compact('date_from','branches','date_to'));

    }

    public static function stock_movement_summary_store(Request $request) {
                set_time_limit(0);
                ini_set('memory_limit', '-1');
                $date_from = $request->date_from;
                $date_to   = $request->date_to;
                $_branch = $request->branch;

                    $branch = CompanyBranch::where('branch_code',$_branch)->first();
                    // $items = ItemStock::getStockMovementSummary($branch,$date_from,$date_to);
                    $branch_code = $branch->branch_code;
                    $date_from = date("Y-m-d", strtotime($date_from));
                    $date_to = date("Y-m-d", strtotime($date_to));


                    $min =  ItemStock::getMinDate($branch,$date_from,$date_to);
                    // $max =  ItemStock::getMaxDate($branch,$date_from,$date_to);


                     // $branches = UserBranch::getAllowedBranch(Auth::user()->id);
                     // // dd($branches); die;
                     // //      $se_branches = implode("','", $branches);
                     // //      dd($se_branches); die;
                 


                    $items = ItemStock::getStockMovementSummary( $branch,$date_from,$date_to);

              
                    $mini_date = $min->min_date;



                   


            
                     $filename = "STOCK MOVEMENT_".$branch->branch."_FROM".$date_from."_TO".$date_to;

                      \Excel::create($filename, function($excel)  use ($items,$branch,$date_from,$date_to,$mini_date){
                        $excel->sheet($branch->branch, function($sheet) use($items,$branch,$date_from,$date_to,$mini_date) {


                            $sheet->row(1, array($branch->company->company));
                            $sheet->row(2, array("STOCK MOVEMENT"));
                            $sheet->row(3, array("FROM ".$date_from." TO ".$date_to));
                            $sheet->row(6, array("CATEGORY","DESCRIPTION","STOCK CODE","BEGINNING","IN","OUT","ENDING"));
                            if(count($items) > 0) {

                                   $cnt = 7;
                                foreach ($items as $item) {

                                    $beg = 0 ;
                                    $sum_in = 0 ; 
                                    if($item->date1 == $mini_date) {
                                        $beg = $item->begining;
                                        $sum_in = $item->sum_in;
                                    }
                                    else if ($item->date1 != $mini_date) {

                                        $beg = 0;
                                        $sum_in = $item->begining + $item->sum_in;
                                    }

                                        $sheet->row($cnt,array(
                                           
                                            $item->category,
                                            $item->description,
                                            $item->itemcode,
                                            $beg,
                                            $sum_in,
                                            $item->sum_out,
                                            $item->ending,
                                           
                                           

                                        ));
                                       

                                        $cnt++;
                                    
                                }
                            

                            }
                           
                           

                        });
                    })->export('xls');
        



                

         






    }


     public static function sale_register_index(Request $request){

        $date_from = date('m/d/Y');
        $date_to = date('m/d/Y');
     
        
        $branches = UserBranch::getUserBranch(Auth::user()->id);

        return view('reports.inventory.sale_register',compact('date_from','branches','date_to'));

    }

        public static function sale_register_post(Request $request) {
                set_time_limit(0);
                ini_set('memory_limit', '-1');
                $date_from = $request->date_from;
                $date_to   = $request->date_to;
                $date_from = date("Y-m-d", strtotime($date_from));
                $date_to = date("Y-m-d", strtotime($date_to));
                
                $_branch = $request->branch;

           

                // $folder_path = storage_path().'/stock_movement_detailed/'.date('Y-m-d-H-i-s');
                // if(!File::exists($folder_path)) {
                
                    
                //         File::makeDirectory($folder_path,0755, true);
                    
                // }


                    $branch = CompanyBranch::where('branch_code',$_branch)->first();


                    $sales  = SaleSummary::select('sale_details.category','sale_summaries.ref_no' ,'sale_details.description','sale_details.itemcode' ,'sale_details.qty' , 'sale_details.gross_amount' , 'sale_details.srp' , 'sale_summaries.invoice_no' , DB::raw("IF(sale_summaries.sales_man IS NULL or sale_summaries.sales_man = '', 'NO DATA', sale_summaries.sales_man) as sales_man , IF(sale_summaries.user IS NULL or sale_summaries.user = '', 'NO DATA', sale_summaries.user) as user , DATE_FORMAT(sale_summaries.local_time,'%Y-%m-%d') as local_date")   )
                    ->leftJoin('sale_details','sale_details.sale_summary_id','=','sale_summaries.id')
                    ->where('sale_summaries.branch_code',$_branch)
                    ->where('post_void',0)
                    ->whereDate('sale_summaries.local_time','>=',$date_from)
                    ->whereDate('sale_summaries.local_time','<=',$date_to)
                 
                    ->get();

                     $filename = "SALES REGISTER".$branch->branch."_FROM".$date_from."_TO".$date_to;

                      \Excel::create($filename, function($excel)  use ($sales,$branch,$date_from,$date_to){
                        $excel->sheet($branch->branch, function($sheet) use($sales,$branch,$date_from,$date_to) {


                            $sheet->row(1, array($branch->company->company));
                            $sheet->row(2, array("SALES REGISTER"));
                            $sheet->row(3, array("FROM ".$date_from." TO ".$date_to));
                            $sheet->row(7, array("DATE","SI#","REF NO.","USERNAME","SALESMAN","CATEGORY","MODEL / DESCRIPTION","STOCK CODE" ,"QUANTITY" ,"UNIT PRICE" ,"AMOUNT","BASE AMOUNT" ,"VAT"));
                            if(count($sales) > 0) {

                                   $cnt = 8;
                                foreach ($sales as $sale) {

                                  

                                            $sheet->row($cnt,array(
                                           
                                            $sale->local_date,
                                            $sale->invoice_no,
                                            $sale->ref_no,
                                            $sale->user,
                                            $sale->sales_man,
                                            $sale->category,
                                            $sale->description,
                                            $sale->itemcode,
                                            $sale->qty,
                                            $sale->srp,
                                            $sale->gross_amount,

                                            $sale->gross_amount / 1.12,
                                            ($sale->gross_amount / 1.12) * 0.12  

                                           

                                        ));
                                       

                                        $cnt++;
                                    
                                }
                            

                            }
                           
                           

                        });
                    })->export('xls');
        



                

         






    }
     public function stock_issue_index(Request $request)
    {

        $date_from = date('m/d/Y');
        $date_to = date('m/d/Y');
        $branches = UserBranch::getUserBranch(Auth::user()->id);

        return view('reports.inventory.stock_issue',compact('date_from','date_to','branches'));

    }

    public function stock_issue_post(Request $request)
    {
        
        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $branch  = CompanyBranch::where('branch_code',$request->branch)->first();

        $stock_out_filename = "STOCK ISSUE REGISTER_".$branch->branch;
        $stock_out      = OutgoingItem::stock_issue( $branch,$date_from,$date_to );
           \Excel::create($stock_out_filename, function($excel)  use ($stock_out,$branch,$date_from,$date_to) {
                      
                        
                $excel->sheet("STOCK ISSUE REGISTER", function($sheet) use($stock_out,$branch,$date_from,$date_to) {
                            $sheet->row(1, array($branch->company->company));
                            $sheet->row(2, array("STOCK ISSUE REGISTER"));
                            $sheet->row(3, array("FROM ".$date_from." "."TO ".$date_to));
                            $sheet->row(6,array("DATE","OUTGOING #","CATEGORY","MODEL/DESCRIPTION","STOCK CODE","FROM","TO","UOM","QTY","UNIT PRICE","AMOUNT"));

                            $cnt = 7;

                            foreach ($stock_out as $stock) {
                                       $total_amount = 0 ;
                                    if($stock->price != 0 ){

                                        $total_amount = $stock->price * $stock->qty;

                                    }

                                    $sheet->row($cnt,array(
                                        $stock->local_date,
                                        $stock->outgoing_no,
                                        $stock->category,
                                        $stock->description,
                                        $stock->itemcode,
                                        $stock->branch_name,
                                        $stock->to_branch,
                                        "pcs",
                                        $stock->qty,
                                        $stock->price,
                                        $total_amount,

                                    ));

                                    $cnt++;
                            }

                        });

                          

        })->export('xls');

       

    }




      public function stock_issue_return_index(Request $request)
    {

        $date_from = date('m/d/Y');
        $date_to = date('m/d/Y');
        $branches = UserBranch::getUserBranch(Auth::user()->id);

        return view('reports.inventory.stock_issue_return',compact('date_from','date_to','branches'));

    }

    public function stock_issue_return_post(Request $request)
    {
        
        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $branch  = CompanyBranch::where('branch_code',$request->branch)->first();

        $stock_out_filename = "STOCK ISSUE RETURN REGISTER_".$branch->branch;
        $stock_in       = IncomingItem::stock_issue_return( $branch,$date_from,$date_to );
           \Excel::create($stock_out_filename, function($excel)  use ($stock_in,$branch,$date_from,$date_to) {
                      
                        
                $excel->sheet("STOCK ISSUE RETURN REGISTER", function($sheet) use($stock_in,$branch,$date_from,$date_to) {
                            $sheet->row(1, array($branch->company->company));
                            $sheet->row(2, array("STOCK ISSUE return REGISTER"));
                            $sheet->row(3, array("FROM ".$date_from." "."TO ".$date_to));
                            $sheet->row(6,array("DATE","INCOMING #","CATEGORY","MODEL/DESCRIPTION","STOCK CODE","FROM","TO","UOM","QTY","UNIT PRICE","AMOUNT" ,"ISSUE REF # / OUTGOING #"));

                            $cnt = 7;

                            foreach ($stock_in as $stock) {
                                     $ref_shop = "  ";
                                if(!empty($stock->outgoing_no)) {
                                    $result = substr($stock->outgoing_no, 0, 4);
                                    $_branch_code = $company_code.$result;
                                    $_branch_name = CompanyBranch::where('branch_code',$_branch_code)->first()->branch;
                                    if($_branch_name){
                                        $ref_shop = $_branch_name;
                                    }
                                    else {
                                        $ref_shop = "NO DATA";
                                    }

                                }
                                else {

                                    $ref_shop = "NO DATA";
                                }


                                    $sheet->row($cnt,array(
                                        $stock->local_date,
                                        $stock->incoming_no,
                                        $stock->category,
                                        $stock->description,
                                        $stock->itemcode,
                                        $ref_shop,
                                        $stock->branch_name,
                                        "pcs",
                                        $stock->qty,
                                        $stock->price,
                                        $stock->amount,
                                        $stock->outgoing_no

                                    ));

                                    $cnt++;
                            }

                        });

                          

        })->export('xls');

       

    }


public function stock_adjustment_index(Request $request)
    {

        $date_from = date('m/d/Y');
        $date_to = date('m/d/Y');
        $branches = UserBranch::getUserBranch(Auth::user()->id);

        return view('reports.inventory.stock_adjustment',compact('date_from','date_to','branches'));

    }

    public function stock_adjustment_post(Request $request)
    {
       
        $date_from = date("Y-m-d", strtotime($request->date_from));
        $date_to = date("Y-m-d", strtotime($request->date_to));
        $branch  = CompanyBranch::where('branch_code',$request->branch)->first();
        $filter = "ADJUSTMENT";
        $filter1 = "MARKETING ADJUSTMENT";
       
        $incoming = \DB::table('incoming_items')
                    ->select('branch_code','local_date','category','description','itemcode' , 'incoming_no' , 'outgoing_no' , 'purpose' ,
                    \DB::raw('0 as out_stock ' ) , 'qty as in_stock'  )
                    ->where('branch_code',$request->branch)
                    ->whereDate('local_date' , '>=' , $date_from)
                    ->whereDate('local_date' , '<=' , $date_to)
                     ->where('purpose', 'LIKE' ,"%$filter%")
                     ->where('purpose', 'NOT LIKE' ,"%$filter1%")
                     ->get();
                     
                    
                    

        $outgoing = \DB::table('outgoing_items')
                    ->select('branch_code','local_date','category','description','itemcode' , 'incoming_no' , 'outgoing_no' , 'purpose' ,\DB::raw('0 as in_stock ' ) , 'qty as out_stock'  )
                    ->where('branch_code',$request->branch)
                    ->whereDate('local_date' , '>=' , $date_from)
                    ->whereDate('local_date' , '<=' , $date_to)
                    ->where('purpose', 'LIKE' ,"%$filter%")
                    ->where('purpose', 'NOT LIKE' ,"%$filter1%")
                    ->get();
                   
                  

        // $items = $incoming->union($outgoing)->get();


         



        $filename = "STOCK ADJUSTMENT LIST_".$branch->branch;


             

        \Excel::create($filename, function($excel)  use ($incoming,$outgoing,$branch,$date_from,$date_to) {
                      
                        
                 $excel->sheet("STOCK ADJUSTMENT LIST", function($sheet) use($incoming,$outgoing,$branch,$date_from,$date_to) {


                            $sheet->row(1, array($branch->company->company));
                            $sheet->row(2, array("STOCK ADJUSTMENT LIST"));
                            $sheet->row(3, array("FROM ".$date_from." "."TO ".$date_to));
                            $sheet->row(6,array("DATE","INCOMING #","OUTGOING #","CATEGORY","MODEL/DESCRIPTION","STOCK CODE","IN","OUT","REASON FOR ADJUSTMENT"));

                            $cnt = 7;

                            foreach ($incoming as $in) {
                                    $sheet->row($cnt,array(
                                        $in->local_date,
                                        $in->incoming_no,
                                        $in->outgoing_no,
                                        $in->category,
                                        $in->description,
                                        $in->itemcode,
                                        $in->in_stock,
                                        $in->out_stock,
                                        $in->purpose
                                    

                                    ));

                                    $cnt++;
                            }

                            foreach ($outgoing as $out) {
                                # code...
                                 $sheet->row($cnt,array(
                                        $out->local_date,
                                        $out->incoming_no,
                                        $out->outgoing_no,
                                        $out->category,
                                        $out->description,
                                        $out->itemcode,
                                        $out->in_stock,
                                        $out->out_stock,
                                        $out->purpose
                                    

                                    ));

                                    $cnt++;

                            }



                           
                           

                        });
                       

                          

        })->export('xls');

    }








}
