<?php

namespace App\Http\Controllers\InventoryReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ItemStock;
use App\Models\StockHistory;
use App\Models\CompanyBranch;

class ItemStockController extends Controller
{

    public function index(Request $request){
    	$stock_status = ['1' => 'Positive Stock', '2' => 'Negative Stock', '3' => 'Zero Stock'];
        $total_stock = 0;
    	$companies = ItemStock::getCompanies();
    	$items = ItemStock::search($request);
       
        foreach ($items as $item) {
            if($items != "00010001") {
                $total_stock = $total_stock + $item->total_stocks;
            }
        }
    	return view('stocks.index',compact('items', 'companies', 'stock_status','total_stock'));
    }

    public function store(Request $request){
    	$request->flash();
    	$sel_branches = $request->branch;
         $total_stock = 0;
    	$stock_status = ['1' => 'Positive Stock', '2' => 'Negative Stock', '3' => 'Zero Stock'];
        
    	$companies = ItemStock::getCompanies();
    	$items = ItemStock::search($request);
         foreach ($items as $item) {
            if($items != "00010001") {
                $total_stock = $total_stock + $item->total_stocks;
            }
        }
    	return view('stocks.index',compact('items', 'companies',  'stock_status', 'sel_branches','total_stock'));
    }

    public function show($id)
    {
        $items = ItemStock::getBranch($id);
        return view('stocks.show',compact('items'));
    }

    public function history($id,Request $request){
// dd($request->category); die;
        ini_set('memory_limit', '-1');
        $stock_status = ['1' => 'Positive Stock', '2' => 'Negative Stock', '3' => 'Zero Stock'];
        $branch = CompanyBranch::where('branch_code',$id)->first();
    	$histories = StockHistory::getHistory($id);
        $items = ItemStock::searchItem($request,$id);
        $sel_categories = ItemStock::category($id);
        $sel_brands   = ItemStock::brand($id);
    	return view('stocks.history',compact('histories', 'items','branch','stock_status','sel_categories','sel_brands'));
    }


}
