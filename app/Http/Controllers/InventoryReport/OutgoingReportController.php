<?php

namespace App\Http\Controllers\InventoryReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\OutgoingItem;
use Auth;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;
class OutgoingReportController extends Controller
{
	 
    public function index(Request $request){
      
        $companies = OutgoingItem::getCompanies();          
        $date_from = date('m/d/Y');
        $date_to = date('m/d/Y');
         \Log::info('pumasok sa outgoint');
    	$items = OutgoingItem::search($request,$date_from,$date_to);
             \Log::info('pumasok sa outgoint1');
        $total_qty = 0;


        $sel_branches   = [];
        $sel_categories = [];
        $sel_brands     = [];

        foreach ($items as $item) {
            $total_qty = $total_qty + $item->qty;
        }
         
    	return view('outgoing.index', compact('items', 'companies','date_to','date_from','total_qty','sel_branches','sel_brands','sel_categories'));
    }   

     public function store(Request $request){

        $request->flash();
        $date_from = $request->date_from;
        $date_to = $request->date_to;


         $sel_branches = [];
        $sel_brands = [];
        $sel_categories =[];
        if(!empty($request->branch)) {
           $sel_branches = $request->branch; 
        }
        if(!empty($request->category)) {
            $sel_categories = $request->category; 
        }
         if(!empty($request->sel_brands)) {
             $sel_brands     = $request->brand;
        }
        
        $submit_type = $request->get('submit');


        $companies = OutgoingItem::getCompanies();
        $items = OutgoingItem::search($request,$date_from,$date_to);

        $total_qty = 0;

        foreach ($items as $item) {
            $total_qty = $total_qty + $item->qty;
        }
            
        if($submit_type == 1) {
        return view('outgoing.index', compact('items', 'companies','date_to','date_from','total_qty','sel_categories','sel_brands','sel_branches'));
        }

        elseif ($submit_type == 2) {
            $writer = WriterFactory::create(Type::XLSX);
            $writer->openToBrowser("OUTGOING ITEMS REPORT".'.xls');
            $style_header = (new StyleBuilder())           
               ->setFontSize(14)
               ->setFontName('Calibri')
               ->setFontColor(Color::BLACK)           
               ->build();   

            $style_details = (new StyleBuilder())           
               ->setFontSize(11)
               ->setFontName('Agency FB')
               ->setFontColor(Color::BLACK)           
               ->build();


            $writer->addRowWithStyle(array('Company Name','Branch Name','Terminal #','User','Ref Shop' ,'Outgoing#','Incoming #','Date Time','Box #','Pouch #','Pouch Unique Code','Stock Code','Barcode','Department','Category','Description','Purpose','Qty','Price','Amount','Posting Time'),$style_header);

                            foreach($items as $item){
                                 $ref_shop = "  ";
                                if(!empty($item->to_branch)) {
                                  
                                    $ref_shop = $item->to_branch;
                                }
                                else {

                                    $ref_shop = "NO DATA";
                                }
                                   
                                $data[0] =  $item->company_name; 
                                $data[1] =  $item->branch_name; 
                                $data[2] =  $item->terminal_no; 
                                $data[3] =  $item->user;
                                $data[4]  =  $ref_shop ; 
                                $data[5] =  $item->outgoing_no;

                                $data[6] =  $item->incoming_no; 
                                $data[7] =  $item->local_time; 
                                $data[8] =  $item->box_no; 
                                $data[9] = $item->pouch_no; 
                                $data[10] = $item->unique_code;
                                  $data[11] =  $item->itemcode; 
                                $data[12] = $item->barcode;
                                $data[13] = $item->department;
                                $data[14] = $item->category;
                                $data[15] = $item->description; 
                                $data[16] = $item->purpose;
                                $data[17] = $item->qty; 
                                $data[18] = number_format($item->price,2);
                                $data[19] = number_format($item->amount,2);
                                $data[20] = $item->local_time;
                                $writer->addRowWithStyle($data,$style_details);
           
                            }

            $sheet = $writer->getCurrentSheet();
            $sheet->setName('OUTGOING ITEMS REPORT');
            $writer->close();               

        }
    }    
}
