<?php

namespace App\Http\Controllers\InventoryReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\NotYetReceived;
use App\Models\OutgoingItem;
use App\Models\UserBranch;

class NotYetReceivedController extends Controller
{
    //

	public function index(Request $request){

		$items = NotYetReceived::search($request);
		$companies = OutgoingItem::getCompanies();
		
		$total_qty = 0;
		$sel_branches   = [];
        $sel_users = []; 
         foreach ($items as $item) {
            $total_qty = $total_qty + $item->qty;
        }
        $date = date('m/d/Y');
	   	return view('notyetreceived.index',compact('items','companies','sel_users','sel_branches','total_qty','date'));

	} 




	public function store(Request $request) {

		$sel_branches   = [];
        $sel_users = []; 
        $companies = OutgoingItem::getCompanies();
        if(!empty($request->branch)) {
           $sel_branches = $request->branch; 
        }
        if(!empty($request->user)) {
            $sel_users = $request->user; 
        }
         
        $date = $request->date;
        $items = NotYetReceived::search($request);
         $total_qty = 0;

        foreach ($items as $item) {
            $total_qty = $total_qty + $item->qty;
        }

        	return view('notyetreceived.index',compact('items','companies','sel_users','sel_branches','total_qty','date'));


	}




}
