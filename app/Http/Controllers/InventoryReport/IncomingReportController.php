<?php

namespace App\Http\Controllers\InventoryReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\IncomingItem;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;
use App\Models\StockHistory;
use App\Models\Company;
use App\Models\CompanyBranch;

class IncomingReportController extends Controller
{
    public function index(Request $request){
        ini_set('memory_limit', '-1');
        set_time_limit(0);
    	$companies = IncomingItem::getCompanies();    	
        $date_from = date('m/d/Y');
        $date_to = date('m/d/Y');
        $items = IncomingItem::search($request,$date_from,$date_to);

        $total_qty = 0; 

        $sel_branches   = [];
        $sel_categories = [];
        $sel_brands     = [];
        $sel_purpose = IncomingItem::getPurpose();


        foreach ($items as $item) {
            $total_qty = $total_qty + $item->qty;
        }         
       
    	return view('incoming.index', compact('items',
            'companies',
            'date_to',
            'date_from',
            'total_qty',
            'sel_branches',
            'sel_categories',
            'sel_brands') );
    }

    public function store(Request $request){
          ini_set('memory_limit', '-1');
          set_time_limit(0);
    	$request->flash();
        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $sel_branches = [];
        $sel_brands = [];
        $sel_categories =[];
        if(!empty($request->branch)) {
           $sel_branches = $request->branch; 
        }
        if(!empty($request->category)) {
            $sel_categories = $request->category; 
        }
    	 if(!empty($request->sel_brands)) {
             $sel_brands     = $request->brand;
        }
        
        $submit_type = $request->get('submit');
       

    	$companies = IncomingItem::getCompanies();
    	$items = IncomingItem::search($request,$date_from,$date_to);

        $total_qty = 0;

        foreach ($items as $item) {
            $total_qty = $total_qty + $item->qty;
        }
        $company_code = Company::find(1)->company_code;        
        if($submit_type == 1) {
    	return view('incoming.index', compact('items', 'companies','date_to','date_from','total_qty','sel_branches','sel_brands','sel_categories'));
        }
          elseif ($submit_type == 2) {
       


               \Excel::create("INCOMING ITEMS REPORT", function($excel)  use ($items,$company_code){
                        $excel->sheet("INCOMING ITEMS REPORT", function($sheet) use($items,$company_code) {


                            $sheet->row(1, array('Company Name','Branch Name','Terminal #','User','Ref Shop' ,'Incoming#','Ref PO#','PO#','Outgoing#','Date Time', 'Stock Code','Barcode','Department','Category','Brand','Description','Purpose','Qty','Price','Amount','Posting Time'));
                          
                           

                                   $cnt = 2;
                                foreach ($items as $item) {
                                       $ref_shop = "  ";
                                        if($item->outgoing_no != "  ") {
                                            $result = substr($item->outgoing_no, 0, 4);
                                            $_branch_code = $company_code.$result;
                                            $_branch_name = CompanyBranch::where('branch_code',$_branch_code)->first();
                                            if(count($_branch_name) > 0){
                                                $ref_shop = $_branch_name->branch;
                                            }
                                            else {
                                                $ref_shop = "NO DATA";
                                            }
                                            

                                        }
                                        else {

                                            $ref_shop = "NO DATA";
                                        }
                                  

                                            $sheet->row($cnt,array(
                                           
                                    $item->company_name,
                                    $item->branch_name,
                                    $item->terminal_no,
                                    $item->user,
                                    $ref_shop,
                                    $item->incoming_no,
                                    $item->ref_po,
                                    $item->po_no, 
                                    $item->outgoing_no, 
                                    $item->local_time,
                                    $item->itemcode,
                                    $item->barcode,
                                    $item->department,
                                    $item->category,
                                    $item->brand,
                                    $item->description,
                                    $item->purpose,
                                    $item->qty,
                                    number_format($item->price,2),
                                    number_format($item->amount,2),
                                    $item->local_time,
                            

                                           

                                        ));
                                       

                                        $cnt++;
                                    
                                }
                            

                         
                           
                           

                        });
                    })->export('xls');           

        }




    }


}
