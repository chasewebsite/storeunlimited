<?php

namespace App\Http\Controllers\SalesReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\SaleSummary;
use App\Models\SaleDetail;
use App\Models\SalesInventory;
use App\Models\SalesInventoryExcel;
use App\Models\UserBranch;
use App\Models\ReturnExchangeItem;
use App\Models\ItemStock;
use Session;
use Auth;
use File;
use Alchemy\Zippy\Zippy;
use DB;
use App\Models\CompanyBranch;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;

class ConsolidatedInventoryReportController extends Controller
{
    public function index()
    {
    	$date_from = date('m/d/Y');
        $date_to   = date('m/d/Y');
        $companies = SaleSummary::getCompanies();

    	return view('consolidated_inventory_report.index',compact('date_from','date_to','companies'));
    }

    public function store(Request $request)
    {
    	ini_set('memory_limit', -1);
        set_time_limit(0);
        $date_from = str_replace('/', '_', $request->date_from);
        $date_to = str_replace('/', '_', $request->date_to);
        $submit_type = $request->get('submit');

        if($submit_type == 1)
        {
            // foreach ($icodes as $icode) {
            //     $items[] = SaleDetail::getConsolidatedSaleSummary($request->date_from,$request->date_to, $branch->branch_code, $icode->itemcode);        
            // }
            if($request->branch != null){

                $branch = CompanyBranch::getBranch($request->branch);

                $icodes = SaleDetail::getItems($request->date_from, $request->date_to, $branch->branch_code);
                $filename = "Consolidated_inventory_report_(".$branch->branch.")".$date_from.'_'.$date_to;
                $count = 1;
                $savePath = storage_path('uploads/consolidated_inventory_report/'.$filename);
                if(!\File::exists($savePath)){
                    mkdir($savePath, 0777, true);
                }
                File::deleteDirectory($savePath, true);                       
                foreach(array_chunk($icodes, 3000) as $results){
                    \Excel::create('CONSOLIDATED_INVENTORY_REPORT('.$branch->branch.')('.$count.')', function($excel) use($results,$branch) {
                        $excel->sheet('SALES_REPORT('.$branch->branch_name.')', function($sheet) use($results,$branch) {
                            $headers = [
                                'ITEM CODE',
                                'DESCRIPTION',
                                'GLOBAL STATUS',
                                'CAMPAIGN ID',
                                'TOTAL QTY',
                            ];
                            // headers
                            $sheet->cell(1, function($row) { 
                                $row->setBackground('#CCCCCC');
                                $row->setAlignment('center');
                                $row->setFontWeight('bold');
                            });
                            $sheet->cell(2, function($row) { 
                                $row->setBackground('#CCCCCC');
                                $row->setAlignment('center');
                                $row->setFontWeight('bold');
                            });
                            // foreach($branch_data  as $br_data){
                            //     $headers[] = $br_data;
                            // }
                            $sheet->row(1, $headers);
                            $counter = 3;
                            foreach($results as $item)
                            {
                                $sheet->appendRow(array(
                                    $item->itemcode,
                                    $item->description,
                                    $item->global_status,
                                    $item->campaign_id,
                                    $item->total_qty
                                ));
                            }
                        });
                    })->store('xlsx',$savePath);
                    $count++;
                }

                $zippy = Zippy::load();
                $files = \File::allFiles($savePath);

                if(count($files) > 0){
                    $zip_path2 = $savePath.'/zipped/';
                    foreach ($files as $file) {
                        $folders[$file->getFilename()] = $savePath.'/'.$file->getFilename();
                    }
                    $folder_name = str_replace(":","_", $filename);
                    $zip_path = $savePath.'/zipped/'.$folder_name.'.zip';

                    if(!\File::exists($zip_path2)){
                        \File::makeDirectory($zip_path2,0777,true);
                    }
                    $archive = $zippy->create($zip_path,$folders,true);                
                    return response()->download($zip_path);
                }
                else{
                    return redirect()->back()->with('no_data','No Data');
                } 
            }
        }else{
            $icodes = SaleDetail::getItemsAll($request->date_from, $request->date_to);
            $filename = "Consolidated_inventory_report_(ALL_BRANCHES)".$date_from.'_'.$date_to;
            $count = 1;
            $savePath = storage_path('uploads/consolidated_inventory_report/'.$filename);
            if(!\File::exists($savePath)){
                mkdir($savePath, 0777, true);
            }
            File::deleteDirectory($savePath, true);                       
            foreach(array_chunk($icodes, 3000) as $results){
                \Excel::create('CONSOLIDATED_INVENTORY_REPORT(ALL_BRANCHES)('.$count.')', function($excel) use($results) {
                    $excel->sheet('SALES_REPORT(ALL_BRANCHES)', function($sheet) use($results) {
                        $headers = [
                            'ITEM CODE',
                            'DESCRIPTION',
                            'GLOBAL STATUS',
                            'CAMPAIGN ID',
                            'TOTAL QTY',
                        ];
                        // headers
                        $sheet->cell(1, function($row) { 
                            $row->setBackground('#CCCCCC');
                            $row->setAlignment('center');
                            $row->setFontWeight('bold');
                        });
                        $sheet->cell(2, function($row) { 
                            $row->setBackground('#CCCCCC');
                            $row->setAlignment('center');
                            $row->setFontWeight('bold');
                        });
                        // foreach($branch_data  as $br_data){
                        //     $headers[] = $br_data;
                        // }
                        $sheet->row(1, $headers);
                        $counter = 3;
                        foreach($results as $item)
                        {
                            $sheet->appendRow(array(
                                $item->itemcode,
                                $item->description,
                                $item->global_status,
                                $item->campaign_id,
                                $item->total_qty
                            ));
                        }
                    });
                })->store('xlsx',$savePath);
                $count++;
            }

            $zippy = Zippy::load();
            $files = \File::allFiles($savePath);

            if(count($files) > 0){
                $zip_path2 = $savePath.'/zipped/';
                foreach ($files as $file) {
                    $folders[$file->getFilename()] = $savePath.'/'.$file->getFilename();
                }
                $folder_name = str_replace(":","_", $filename);
                $zip_path = $savePath.'/zipped/'.$folder_name.'.zip';

                if(!\File::exists($zip_path2)){
                    \File::makeDirectory($zip_path2,0777,true);
                }
                $archive = $zippy->create($zip_path,$folders,true);                
                return response()->download($zip_path);
            }
            else{
                return redirect()->back()->with('no_data','No Data');
            }
        }
    }
}
