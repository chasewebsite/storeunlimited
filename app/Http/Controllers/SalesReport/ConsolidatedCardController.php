<?php

namespace App\Http\Controllers\SalesReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;


use App\Models\UserBranch;
use App\Models\CompanyBranch;
use App\Models\SaleSummary;


class ConsolidatedCardController extends Controller
{
    //

	public static function index(Request $request)
      {

        $date_from = date('m/d/Y');
        $date_to   = date('m/d/Y');
        

        return view('reports.sales.consolidated',compact('date_from','date_to'));

    }

      public static function post(Request $request) {

    $branches = UserBranch::getAllowedBranch(Auth::user()->id);


    	set_time_limit(0);
        ini_set('memory_limit', '1G');
        $date_from      = $request->date_from;
        $date_to        = $request->date_to;
       
        $sales          =  SaleSummary::consolidatedCardSales($date_from,$date_to); 
        $filename = "CONSOLIDATED CARD TRANSACTIONS ";
  


   

          \Excel::create($filename, function($excel)  use ($sales,$date_from,$date_to){
            $excel->sheet("CONSOLIDATED CARD TRANSACTIONS ", function($sheet) use($sales,$date_from,$date_to) {
                          	 $sheet->setStyle(array(
                                'font' => array(
                                    'name'      =>  'Calibri',
                                    'size'      =>  8
                                  
                                )
                            ));

                          	 $sheet->setWidth(array(
                                'A'     =>  30,
                                'B'     =>  20,
                                'C'     =>  20,
                                'D'     =>  20,
                                'E'     =>  20,
                                'F'     =>  20,
                                'G'     =>  5,
                                'H'     =>  20,
                                'I'     =>  20,
                                'J'     =>  20,
                                'K'     => 5,
                                'L'     => 20,
                                'M'     => 20,
                                'N'     => 20,
                                'O'     => 20,
                                'P'     => 5,
                                'Q'     => 20,
                                'R'     => 20,
                                'S'     => 20,
                                'T'     => 20,
                                'U'     => 5,
                                'V'     => 20,
                                'W'     => 20,
                                'X'     => 20,
                                'Y'     => 20,
                                'Z'     => 5,
                                'AA'     => 20,
                                'AB'     => 20,
                                'AC'     => 20,
                                'AD'     => 20,
                                'AE'     => 5,
                                'AF'     => 20,
                                'AG'     => 20,
                                'AH'     => 20,
                                'AI'     => 20,
                                'AJ'     => 5,
                                'AK'     => 20,
                                'AL'     => 20,
                                'AM'     => 20,
                                'AN'     => 20,
                                'AO'     => 5,
                                'AP'     => 20,
                                'AQ'     => 20,
                                'AR'     => 20,
                                'AS'     => 20,
                                'AT'     => 5,
                                'AU'     => 20,
                                'AV'     => 20,
                                'AW'     => 20,
                                'AX'     => 20,
                                'AY'     => 5,
                                'AZ'     => 20,
                                'BA'     => 20,
                                'BB'     => 20,
                                'BC'     => 20,
                                'BD'     => 5,
                                'BE'     => 20,
                                'BF'     => 20,
                                'BG'     => 20,
                                'BH'     => 20,
                                'BI'     => 5,
                                'BJ'     => 20,
                                'BK'     => 20,
                                'BL'     => 20,
                                'BM'     => 20,
                                'BN'     => 5,
                                'BO'     => 20,
                                'BP'     => 20,
                                'BQ'     => 20,
                                'BR'     => 20,
                                'BS'     => 5,
                                'BT'     => 20,
                                'BU'     => 20,
                                'BV'     => 20,
                                'BW'     => 20,
                                'BX'     => 5,
                                'BY'     => 20,
                                'BZ'     => 20,
                                'CA'     => 20,
                                'CB'     => 20,
                                'CC'     => 5,
                                'CD'     => 20,
                                'CE'     => 20,
                                'CF'     => 20,
                                'CG'     => 20,
                                'CH'     => 5,
                                'CI'     => 20,
                                'CJ'     => 20,
                                'CK'     => 20,
                                'CL'     => 20,
                                'CM'     => 5,
                                'CN'     => 20,
                                'CO'     => 20,
                                'CP'     => 20,
                                'CQ'     => 20,
                                'CR'     => 5,
                                'CS'     => 20,
                                'CT'     => 20,
                                'CU'     => 20,
                                'CV'     => 20,
                                'CW'     => 5,
                                'CX'     => 20,
                                'CY'     => 20,
                                'CZ'     => 20,
                                'DA'     => 20,
                                'DB'     => 5,
                                'DC'     => 20,
                                'DD'     => 20,
                                'DE'     => 20,
                                'DF'     => 20,
                                'DG'     => 5,
                                'DH'     => 20,
                                'DI'     => 20,
                                'DJ'     => 20,
                                'DK'     => 20,
                                'DL'     => 5,
                                'DM'     => 20,
                                'DN'     => 20,
                                'DO'     => 20,
                                'DP'     => 20,
                                'DQ'     => 5,
                                'DR'     => 20,
                                'DS'     => 20,
                                'DT'     => 20,
                                'DU'     => 20,
                                'DV'     => 5,
                                'DW'     => 20,
                                'DX'     => 20,
                                'DY'     => 20,
                                'DZ'     => 20,
                                'EA'     => 5,
                                'EB'     => 20,
                                'EC'     => 20,
                                'ED'     => 20,
                                'EE'     => 20,
                                'EF'     => 5,
                                'EG'     => 20,
                                'EH'     => 20,
                                'EI'     => 20,
                                'EJ'     => 20,
                                'EK'     => 5,
                                'EL'     => 20,
                                'EM'     => 20,
                                'EN'     => 20,
                                'EO'     => 20,
                                'EP'     => 5,
                                'EQ'     => 20,
                                'ER'     => 20,
                                'ES'     => 20,
                                'ET'     => 20,
                                'EU'     => 5,
                                'EV'     => 20,
                                'EW'     => 20,
                                'EX'     => 20,
                                'EY'     => 20,
                                'EZ'     => 5,
                                'FA'     => 20,
                                'FB'     => 20,
                                'FC'     => 20,
                                'FD'     => 20,
                                'FE'     => 5,
                                'FF'     => 20,
                                'FG'     => 20,
                                'FH'     => 20,
                                'FI'     => 20,
                                'FJ'     => 5,
                                'FK'     => 20,
                                'FL'     => 20,
                                'FM'     => 20,
                                'FN'     => 20,
                                'FO'     => 5,
                                'FP'     => 20,
                                'FQ'     => 20,
                                'FR'     => 20,
                                'FS'     => 20,
                                





                            ));



                          	$sheet->mergeCells('H5:J5');
                            $sheet->mergeCells('L5:O5');
                            $sheet->mergeCells('Q5:T5');
                            $sheet->mergeCells('V5:Y5');
                            $sheet->mergeCells('AA5:AD5');
                            $sheet->mergeCells('AF5:AI5');
                            $sheet->mergeCells('AK5:AN5');
                            $sheet->mergeCells('AP5:AS5');
                            $sheet->mergeCells('AU5:AX5');
                            $sheet->mergeCells('AZ5:BC5');
                            $sheet->mergeCells('BE5:BH5');
                            $sheet->mergeCells('BJ5:BM5');
                            $sheet->mergeCells('BO5:BR5');
                            $sheet->mergeCells('BT5:BW5');
                            $sheet->mergeCells('BY5:CB5');
                            $sheet->mergeCells('CD5:CG5');
                            $sheet->mergeCells('CI5:CL5');
                            $sheet->mergeCells('CN5:CQ5');
                            $sheet->mergeCells('CS5:CV5');
                            $sheet->mergeCells('CX5:DA5');
                            $sheet->mergeCells('DC5:DF5');
                            $sheet->mergeCells('DH5:DK5');
                            $sheet->mergeCells('DM5:DP5');
                            $sheet->mergeCells('DR5:DU5');
                            $sheet->mergeCells('DW5:DZ5');
                            $sheet->mergeCells('EB5:EE5');
                            $sheet->mergeCells('EG5:EJ5');
                            $sheet->mergeCells('EL5:EO5');
                            $sheet->mergeCells('EQ5:ET5');
                            $sheet->mergeCells('EV5:EY5');
                            $sheet->mergeCells('FA5:FD5');
                            $sheet->mergeCells('FF5:FI5');
                            $sheet->mergeCells('FK5:FN5');
                            $sheet->mergeCells('FP5:FS5');

                            $sheet->setBorder('A5:F5', 'thin');

                            $sheet->setBorder('H5:J5', 'thin');
                            $sheet->setBorder('L5:O5', 'thin');
                            $sheet->setBorder('Q5:T5', 'thin');
                            $sheet->setBorder('V5:Y5', 'thin');
                            $sheet->setBorder('AA5:AD5', 'thin');
                            $sheet->setBorder('AF5:AI5', 'thin');
                            $sheet->setBorder('AK5:AN5', 'thin');
                            $sheet->setBorder('AP5:AS5', 'thin');
                            $sheet->setBorder('AU5:AX5', 'thin');
                            $sheet->setBorder('AZ5:BC5', 'thin');
                            $sheet->setBorder('BE5:BH5', 'thin');
                            $sheet->setBorder('BJ5:BM5', 'thin');
                            $sheet->setBorder('BO5:BR5', 'thin');
                            $sheet->setBorder('BT5:BW5', 'thin');
                            $sheet->setBorder('BY5:CB5', 'thin');
                            $sheet->setBorder('CD5:CG5', 'thin');
                            $sheet->setBorder('CI5:CL5', 'thin');
                            $sheet->setBorder('CN5:CQ5', 'thin');
                            $sheet->setBorder('CS5:CV5', 'thin');
                            $sheet->setBorder('CX5:DA5', 'thin');
                            $sheet->setBorder('DC5:DF5', 'thin');
                            $sheet->setBorder('DH5:DK5', 'thin');
                            $sheet->setBorder('DM5:DP5', 'thin');
                            $sheet->setBorder('DR5:DU5', 'thin');
                            $sheet->setBorder('DW5:DZ5', 'thin');
                            $sheet->setBorder('EB5:EE5', 'thin');
                            $sheet->setBorder('EG5:EJ5', 'thin');
                            $sheet->setBorder('EL5:EO5', 'thin');
                            $sheet->setBorder('EQ5:ET5', 'thin');
                            $sheet->setBorder('EV5:EY5', 'thin');
                            $sheet->setBorder('FA5:FD5', 'thin');
                            $sheet->setBorder('FF5:FI5', 'thin');
                            $sheet->setBorder('FK5:FN5', 'thin');
                            $sheet->setBorder('FP5:FS5', 'thin');

                             $sheet->setBorder('A6:F6', 'thin');

                            $sheet->setBorder('H6:J6', 'thin');
                            $sheet->setBorder('L6:O6', 'thin');
                            $sheet->setBorder('Q6:T6', 'thin');
                            $sheet->setBorder('V6:Y6', 'thin');
                            $sheet->setBorder('AA6:AD6', 'thin');
                            $sheet->setBorder('AF6:AI6', 'thin');
                            $sheet->setBorder('AK6:AN6', 'thin');
                            $sheet->setBorder('AP6:AS6', 'thin');
                            $sheet->setBorder('AU6:AX6', 'thin');
                            $sheet->setBorder('AZ6:BC6', 'thin');
                            $sheet->setBorder('BE6:BH6', 'thin');
                            $sheet->setBorder('BJ6:BM6', 'thin');
                            $sheet->setBorder('BO6:BR6', 'thin');
                            $sheet->setBorder('BT6:BW6', 'thin');
                            $sheet->setBorder('BY6:CB6', 'thin');
                            $sheet->setBorder('CD6:CG6', 'thin');
                            $sheet->setBorder('CI6:CL6', 'thin');
                            $sheet->setBorder('CN6:CQ6', 'thin');
                            $sheet->setBorder('CS6:CV6', 'thin');
                            $sheet->setBorder('CX6:DA6', 'thin');
                            $sheet->setBorder('DC6:DF6', 'thin');
                            $sheet->setBorder('DH6:DK6', 'thin');
                            $sheet->setBorder('DM6:DP6', 'thin');
                            $sheet->setBorder('DR6:DU6', 'thin');
                            $sheet->setBorder('DW6:DZ6', 'thin');
                            $sheet->setBorder('EB6:EE6', 'thin');
                            $sheet->setBorder('EG6:EJ6', 'thin');
                            $sheet->setBorder('EL6:EO6', 'thin');
                            $sheet->setBorder('EQ6:ET6', 'thin');
                            $sheet->setBorder('EV6:EY6', 'thin');
                            $sheet->setBorder('FA6:FD6', 'thin');
                            $sheet->setBorder('FF6:FI6', 'thin');
                            $sheet->setBorder('FK6:FN6', 'thin');
                            $sheet->setBorder('FP6:FS6', 'thin');

                             $sheet->cells('A5:FS5', function($cell) {

                                    $cell->setAlignment('center');
                                  

                            });

                              $sheet->cells('A6:FS6', function($cell) {

                                    $cell->setAlignment('center');
                                  

                            });


            				$sheet->row(1, array("PANDORA - ALL SHOPS "));
                            $sheet->row(2, array("CONSOLIDATED CARD TRANSACTIONS "));
                            $sheet->row(3, array("FOR THE PERIOD ".$date_from." to ".$date_to));

                             $sheet->row(5,array('' ,'','','','ATM/','CREDIT CARD',' ','ATM/EPS',' ', ' ',' ', 'STRAIGHT TRANS OTHER THAN JCB/AMEX', ' ', ' ', ' ',' ','JCB/AMEX STRAIGHT TRANS',' ',' ',' ',' ','BDO 3 MONTHS', ' ' , ' ', ' ', ' ','BDO 6 MONTHS',' ',' ',' ',' ','BDO 12 MONTHS',' ','',' ',' ','BPI 3 MONTHS',' ',' ',' ',' ','BPI 6 MONTHS',' ',' ',' ',' ','BPI 12 MONTHS',' ','','','', 'CITIBANK 3 MONTHS',' ', ' ',' ',' ','CITIBANK 6 MONTHS',' ',' ',' ',' ','CITIBANK 12 MONTHS',' ','','','','HSBC 3 MONTHS',' ',' ',' ',' ','HSBC 6 MONTHS',' ',' ',' ',' ','HSBC 12 MONTHS',' ','','','','EAST WEST 3 MONTHS',' ',' ',' ',' ','EAST WEST  6 MONTHS',' ',' ',' ',' ','EAST WEST  12 MONTHS',' ','','','','METROBANK 3 MONTHS',' ',' ',' ',' ','METROBANK 6 MONTHS',' ',' ',' ',' ','METROBANK 12 MONTHS',' ','','','','DINERS/SECURITY BANK 3 MONTHS', ' ',' ',' ',' ',' DINERS/SECURITY BANK 6 MONTHS',' ',' ',' ',' ','DINERS/SECURITY BANK 12 MONTHS' , ' ' , '' , '' , '' ,'PNB 3 MONTHS', ' ',' ',' ',' ','PNB 6 MONTHS' , ' ', ' ',' ',' ','PNB 12 MONTHS' , ' ',' ',' ',' ','RCBC 3 MONTHS', ' ',' ',' ',' ','RCBC 6 MONTHS',' ',' ',' ',' ','RCBC 12 MONTHS' ,' ' , ' ' ,'','' ,'UNION BANK 3 MONTHS',' ',' ',' ',' ','UNION BANK 6 MONTHS',' ',' ',' ',' ','UNION BANK 12 MONTHS',' ',' ',' '));





                            $sheet->row(6,array('DATE','Shop/Boutique','','','EPS','TRANSACTIONS',' ','AMOUNT','MERCHANT DISCT','NET AMOUNT',' ','AMOUNT','MERCHANT DISCT','EWT','NET AMOUNT',' ','AMOUNT','MERCHANT DISCT','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','MSUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',' ','AMOUNT','SUBSIDY','EWT','NET AMOUNT',));

                            $cnt = 8;
                            $bank_array = array("bdo", "bpi", "citibank", "hsbc" , "eastwest", "metrobank","dinner","pnb","rcbc","unionbank" );

                            foreach ($bank_array as $value) {
                            	${$value."_amount_3" }  = 0.00;
                                ${$value."_amount_6" }  = 0.00;
                                ${$value."_amount_12" }  = 0.00;
                                ${$value."_subsidy_3" }  = 0.00;
                                ${$value."_subsidy_6" }  = 0.00;
                                ${$value."_subsidy_12" }  = 0.00;
                                ${$value."_subsidy" }  = 0.00;
                             }    
                             foreach ($sales as $data) {

                               $bdo_amount_3  = $data->bdo_3;
                               $bdo_amount_6  = $data->bdo_6;
                               $bdo_amount_12 = $data->bdo_12;

                               $bpi_amount_3 = $data->bpi_3;
                               $bpi_amount_6 = $data->bpi_6;
                               $bpi_amount_12 = $data->bpi_12;

                               $citibank_amount_3 = $data->citibank_3;
                               $citibank_amount_6 = $data->citibank_6;
                               $citibank_amount_12 = $data->citibank_12;


                               $hsbc_amount_3 = $data->hsbc_3;
                               $hsbc_amount_6 = $data->hsbc_6;
                               $hsbc_amount_12 = $data->hsbc_12;

                               $eastwest_amount_3 = $data->eastwest_3;
                               $eastwest_amount_6 = $data->eastwest_6;
                               $eastwest_amount_12 = $data->eastwest_12;


                               $metrobank_amount_3 = $data->metrobank_3;
                               $metrobank_amount_6 = $data->metrobank_6;
                               $metrobank_amount_12 = $data->metrobank_12;

                               $dinner_amount_3 = $data->diners_3;
                               $dinner_amount_6 = $data->diners_6;
                               $dinner_amount_12 = $data->diners_12;

                               $pnb_amount_3 = $data->pnb_3;
                               $pnb_amount_6 = $data->pnb_6;	
                               $pnb_amount_12 = $data->pnb_12;

                               $rcbc_amount_3 = $data->rcbc_3;
                               $rcbc_amount_6 = $data->rcbc_6;
                               $rcbc_amount_12 = $data->rcbc_12;


                               $unionbank_amount_3 = $data->unionbank_3;
                               $unionbank_amount_6 = $data->unionbank_6;
                               $unionbank_amount_12 = $data->unionbank_12;


                               $bdo_subsidy_3 = $bdo_amount_3 * 0.025;
                               $bdo_subsidy_6 = $bdo_amount_6 * 0.0475;
                               $bdo_subsidy_12 = $bdo_amount_12 *  0.095;

                               $bpi_subsidy_3 = $bpi_amount_3 *  0.0275;
                               $bpi_subsidy_6 = $bpi_amount_6 * 0.05;
                               $bpi_subsidy_12 = $bpi_amount_12 *  0.095;


                               $citibank_subsidy_3 = $citibank_amount_3 *  0.025;
                               $citibank_subsidy_6 = $citibank_amount_6 * 0.05;
                               $citibank_subsidy_12 = $citibank_amount_12 *  0.09;


                               $hsbc_subsidy_3 = $hsbc_amount_3 *  0.025;
                               $hsbc_subsidy_6 = $hsbc_amount_6 * 0.045;
                               $hsbc_subsidy_12 = $hsbc_amount_12 *  0.075;


                               $eastwest_subsidy_3 = $eastwest_amount_3 *  0.025;
                               $eastwest_subsidy_6 = $eastwest_amount_6 * 0.0475;
                               $eastwest_subsidy_12 = $eastwest_amount_12 *  0.095;

                                $metrobank_subsidy_3 = $metrobank_amount_3 *  0.03;
                                 $metrobank_subsidy_6 = $metrobank_amount_6 * 0.0475;
                                 $metrobank_subsidy_12 = $metrobank_amount_12 *  0.095;


                                 $dinner_subsidy_3 = $dinner_amount_3 *  0.025;
                                 $dinner_subsidy_6 = $dinner_amount_6 * 0.0475;
                                 $dinner_subsidy_12 = 0.00;


                                 $pnb_subsidy_3 = $pnb_amount_3 *  0.03;
                                 $pnb_subsidy_6 = $pnb_amount_6 * 0.0475;
                                 $pnb_subsidy_12 = $pnb_amount_12 * 0.095;

                                 $rcbc_subsidy_3 = $rcbc_amount_3 *  0.03;
                                 $rcbc_subsidy_6 = $rcbc_amount_6 * 0.06;
                                 $rcbc_subsidy_12 = 0.00;

                                 
                                 $unionbank_subsidy_3 = $unionbank_amount_3 *  0.025;
                                 $unionbank_subsidy_6 = $unionbank_amount_6 * 0.05;
                                 $unionbank_subsidy_12 = $unionbank_amount_12 * 0.1;









	                                   $_branch_name = CompanyBranch::where('branch_code',$data->branch_code)->first()->branch; 



                                     $sheet->row($cnt,array( date('d-M',strtotime($data->local_time)),
                                     	$_branch_name,
                                     	' ',
                                     	' ',
                                        $data->atm_amount,
                                        $data->credit_card_amount,
                                        ' ',
                                        $data->atm_amount,
                                        $data->atm_amount * 0.02,
                                        $data->atm_amount -  ($data->atm_amount * 0.02),
                                        ' ',
                                        $data->other_jcb,
                                        $data->other_jcb * 0.0275,
                                        $data->other_jcb * 0.005,
                                        $data->other_jcb - ($data->other_jcb * 0.0275) - ($data->other_jcb * 0.005),
                                        ' ',
                                        $data->jcb_amount,
                                        $data->jcb_amount * 0.03,
                                        $data->jcb_amount * 0.005,
                                        $data->jcb_amount - ($data->jcb_amount * 0.03) - ($data->jcb_amount * 0.005),
                                        ' ',
                                        $bdo_amount_3,
                                        $bdo_subsidy_3,
                                        $bdo_amount_3 * 0.005,
                                        $bdo_amount_3 - $bdo_subsidy_3 - ($bdo_amount_3 * 0.005),
                                        ' ',
                                        $bdo_amount_6,
                                        $bdo_subsidy_6,
                                        $bdo_amount_6 * 0.005,
                                        $bdo_amount_6 - $bdo_subsidy_6 - ($bdo_amount_6 * 0.005),
                                        ' ',
                                        $bdo_amount_12,
                                        $bdo_subsidy_12,
                                        $bdo_amount_12 * 0.005,
                                        $bdo_amount_12 - $bdo_subsidy_12 - ($bdo_amount_12 * 0.005),
                                        ' ',
                                        $bpi_amount_3,
                                        $bpi_subsidy_3,
                                        $bpi_amount_3 * 0.005,
                                        $bpi_amount_3 - $bpi_subsidy_3 - ($bpi_amount_3 * 0.005),
                                        ' ',
                                        $bpi_amount_6,
                                        $bpi_subsidy_6,
                                        $bpi_amount_6 * 0.005,
                                        $bpi_amount_6 - $bpi_subsidy_6 - ($bpi_amount_6 * 0.005),
                                        ' ',
                                        $bpi_amount_12,
                                        $bpi_subsidy_12,
                                        $bpi_amount_12 * 0.005,
                                        $bpi_amount_12 - $bpi_subsidy_12 - ($bpi_amount_12 * 0.005),
                                        ' ',
                                        $citibank_amount_3,
                                        $citibank_subsidy_3,
                                        $citibank_amount_3 * 0.005,
                                        $citibank_amount_3 - $citibank_subsidy_3 - ($citibank_amount_3 * 0.005),
                                        ' ',
                                        $citibank_amount_6,
                                        $citibank_subsidy_6,
                                        $citibank_amount_6 * 0.005,
                                         $citibank_amount_6 - $citibank_subsidy_6 - ($citibank_amount_6 * 0.005),
                                        ' ',
                                        $citibank_amount_12,
                                        $citibank_subsidy_12,
                                        $citibank_amount_12 * 0.005,
                                        $citibank_amount_12 - $citibank_subsidy_12 - ($citibank_amount_12 * 0.005),
                                        ' ',
                                        $hsbc_amount_3,
                                        $hsbc_subsidy_3,
                                        $hsbc_amount_3 * 0.005,
                                        $hsbc_amount_3 - $hsbc_subsidy_3 - ($hsbc_amount_3 * 0.005),
                                        ' ',
                                        $hsbc_amount_6,
                                        $hsbc_subsidy_6,
                                        $hsbc_amount_6 * 0.005,
                                        $hsbc_amount_6 - $hsbc_subsidy_6 - ($hsbc_amount_6 * 0.005),
                                        ' ',
                                        $hsbc_amount_12,
                                        $hsbc_subsidy_12,
                                        $hsbc_amount_12 * 0.005,
                                        $hsbc_amount_12 - $hsbc_subsidy_12 - ($hsbc_amount_12 * 0.005),
                                        ' ',
                                        $eastwest_amount_3,
                                        $eastwest_subsidy_3,
                                        $eastwest_amount_3 * 0.005,
                                        $eastwest_amount_3 - $eastwest_subsidy_3 - ($eastwest_amount_3 * 0.005),
                                        ' ',
                                        $eastwest_amount_6,
                                        $eastwest_subsidy_6,
                                        $eastwest_amount_6 * 0.005,
                                        $eastwest_amount_6 - $eastwest_subsidy_6 - ($eastwest_amount_6 * 0.005),
                                        ' ',
                                        $eastwest_amount_12,
                                        $eastwest_subsidy_12,
                                        $eastwest_amount_12 * 0.005,
                                        $eastwest_amount_12 - $eastwest_subsidy_12 - ($eastwest_amount_12 * 0.005),
                                        ' ',
                                        $metrobank_amount_3,
                                        $metrobank_subsidy_3,
                                        $metrobank_amount_3 * 0.005,
                                        $metrobank_amount_3 - $metrobank_subsidy_3 - ($metrobank_amount_3 * 0.005),
                                        ' ',
                                        $metrobank_amount_6,
                                        $metrobank_subsidy_6,
                                        $metrobank_amount_6 * 0.005,
                                         $metrobank_amount_6 - $metrobank_subsidy_6 - ($metrobank_amount_6 * 0.005),
                                        ' ',
                                        $metrobank_amount_12,
                                         $metrobank_subsidy_12,
                                        $metrobank_amount_12 * 0.005,
                                        $metrobank_amount_12 - $metrobank_subsidy_12 - ($metrobank_amount_12 * 0.005),
                                        ' ',
                                        
                                        $dinner_amount_3,
                                        $dinner_subsidy_3,
                                        $dinner_amount_3 * 0.005,
                                        $dinner_amount_3 - $dinner_subsidy_3 - ($dinner_amount_3 * 0.005),
                                        ' ',
                                        $dinner_amount_6,
                                        $dinner_subsidy_6,
                                        $dinner_amount_6 * 0.005,
                                        $dinner_amount_6 - $dinner_subsidy_6 - ($dinner_amount_6 * 0.005),
                                        ' ',
                                        $dinner_amount_12,
                                        $dinner_subsidy_12,
                                        $dinner_amount_12 * 0.005,
                                        $dinner_amount_12 - $dinner_subsidy_12 - ($dinner_amount_12 * 0.005),
                                        ' ',
                                        $pnb_amount_3,
                                        $pnb_subsidy_3,
                                        $pnb_amount_3 * 0.005,
                                        $pnb_amount_3 - $pnb_subsidy_3 - ($pnb_amount_3 * 0.005),
                                        ' ',
                                        $pnb_amount_6,
                                        $pnb_subsidy_6,
                                        $pnb_amount_6 * 0.005,
                                        $pnb_amount_6 - $pnb_subsidy_6 - ($pnb_amount_6 * 0.005),
                                        ' ',
                                        $pnb_amount_12,
                                        $pnb_subsidy_12,
                                        $pnb_amount_12 * 0.005,
                                        $pnb_amount_12 - $pnb_subsidy_12 - ($pnb_amount_12 * 0.005),
                                        ' ',
                                        $rcbc_amount_3,
                                        $rcbc_subsidy_3,
                                        $rcbc_amount_3 * 0.005,
                                        $rcbc_amount_3 - $rcbc_subsidy_3 - ($rcbc_amount_3 * 0.005),
                                        ' ',
                                        $rcbc_amount_6,
                                        $rcbc_subsidy_6,
                                         $rcbc_amount_6 * 0.005,
                                        $rcbc_amount_6 - $rcbc_subsidy_6 - ($rcbc_amount_6 * 0.005),
                                        ' ',
                                        $rcbc_amount_12,
                                        $rcbc_subsidy_12,
                                        $rcbc_amount_12 * 0.005,
                                        $rcbc_amount_12 - $rcbc_subsidy_12 - ($rcbc_amount_12 * 0.005),
                                        ' ',
                                        $unionbank_amount_3,
                                        $unionbank_subsidy_3,
                                        $unionbank_amount_3 * 0.005,
                                        $unionbank_amount_3 - $unionbank_subsidy_3 - ($unionbank_amount_3 * 0.005),
                                        
                                        ' ',
                                        $unionbank_amount_6,
                                        $unionbank_subsidy_6,
                                        $unionbank_amount_6 * 0.005,
                                        $unionbank_amount_6 - $unionbank_subsidy_6 - ($unionbank_amount_6 * 0.005),
                                        ' ',
                                        $unionbank_amount_12,
                                        $unionbank_subsidy_12,
                                        $unionbank_amount_12 * 0.005,
                                        $unionbank_amount_12 - $unionbank_subsidy_12 - ($unionbank_amount_12 * 0.005)));



                                 $cnt++;
                                }
                            	

                           

                           

                            



            });
        })->export('xls');


   }



}
