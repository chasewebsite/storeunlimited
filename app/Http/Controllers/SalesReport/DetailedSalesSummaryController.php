<?php

namespace App\Http\Controllers\SalesReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\CompanyBranch;
use App\Models\SaleDetail;
use App\Models\UserBranch;
use App\Models\ReturnExchangeItem;
use App\Models\RefundItem;
use Auth;
use File;
use Alchemy\Zippy\Zippy;
use DB;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;

class DetailedSalesSummaryController extends Controller
{
    public function index()
    {	
    	$date_from = date('m/d/Y');
        $date_to   = date('m/d/Y');
        $branches  = UserBranch::getUserBranch(Auth::user()->id);

    	return view('detailed_sales.index',compact('date_from','date_to','branches'));
    }

    public function store(Request $request)
    {
    	set_time_limit(-1);
    	ini_set('memory_limit', '256M');
    	if(!empty($request->branch)){
    		$se_branches = $request->branch;
    	}else{
    		$se_branches = CompanyBranch::getAllBranchCodes();
    	}
    	
    	$date_from = date('Y-m-d',strtotime($request->date_from));
    	$date_to = date('Y-m-d',strtotime($request->date_to));
    	$queries = [];
    	$datas = SaleDetail::getDetailedSaleSummary($date_from,$date_to,$se_branches);
    	// $returns = ReturnExchangeItem::getTransactions($date_from,$date_to,$se_branches);
    	// $refunds = RefundItem::getTransactions($date_from,$date_to,$se_branches);
   		// if(!empty($datas)){
   		// 	foreach ($datas as $data) {
   		// 		if(!in_array($data->transaction_no, $returns)){
   		// 			if(!in_array($data->transaction_no, $refunds)){
   		// 				$queries[] = $data;
   		// 			}
   		// 		}
   		// 	}
   		// } 
   		foreach ($datas as $data) {
   			$queries[] = $data;
   		}
    	if(!empty($queries)){
	    	$filename = "Detailed_sales_summary".$date_from.'-'.$date_to;
	        $count = 1;
	        $savePath = storage_path('uploads/detailed_sales/'.$filename);
	        if(!\File::exists($savePath)){
	            mkdir($savePath, 0777, true);
	        }            
	        File::deleteDirectory($savePath, true);

	    	foreach (array_chunk($queries, 5000) as $results) {
	    		\Excel::create('DETAILED_SALES('.$date_from.'-'.$date_to.')('.$count.')', function($excel) use($results){
		    		$excel->sheet('DETAILED SALES', function($sheet) use($results) {
	                    $sheet->appendRow(array(
	                    	'Date','Transaction No','Barcode','Brand','Item Code','Description','Category','Department','Branch','Qty','Amount'
	                    ));
	                    foreach($results as $result){
	                    	$sheet->appendRow(array(
	                    		date('m/d/Y',strtotime($result->local_time)),
	                    		$result->transaction_no,
	                    		$result->barcode,
	                    		$result->brand,
	                    		$result->itemcode,
	                    		$result->description,
	                    		$result->category,
	                    		$result->department,
	                    		$result->branch_name,
	                    		$result->qty,
	                    		number_format($result->srp - $result->retex_amount,2)
	                    	));
	                    }
	                    $sheet->row(1,function($row){
	                    	$row->setAlignment('center');
                			$row->setFontWeight('bold');
                			$row->setBackground('#C0C0C0');
	                    });
		    		});
		    	})->store('xls',$savePath); 
		    	$count++;
	    	}
	    	$zippy = Zippy::load();
	        $files = \File::allFiles($savePath);
	        if(count($files) > 0){
	            $zip_path2 = $savePath.'/zipped/';
	            foreach ($files as $file) {
	                $folders[$file->getFilename()] = $savePath.'/'.$file->getFilename();
	            }
	            $folder_name = str_replace(":","_", $filename);
	            $zip_path = $savePath.'/zipped/'.$folder_name.'.zip';

	            if(!\File::exists($zip_path2)){
	                \File::makeDirectory($zip_path2,0777,true);
	            }
	            $archive = $zippy->create($zip_path,$folders,true);                
	            return response()->download($zip_path);
	        }
	    }else{
	    	return redirect()->back()->with('no_data','No Data');
	    }
    }
}
