<?php

namespace App\Http\Controllers\SalesReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Coupon;

class CouponController extends Controller
{
    public function index(Request $request){

    	$date_from = date('m/d/Y');
        $date_to = date('m/d/Y');

    	$items = Coupon::search($date_from,$date_to);    	

    	$total_qty = 0; 

        foreach ($items as $item) {
            $total_qty = $total_qty + $item->qty;
        }             
    	return view('coupons.index', compact('items','date_from','date_to','total_qty'));
    }
}
