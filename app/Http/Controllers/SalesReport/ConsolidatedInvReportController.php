<?php

namespace App\Http\Controllers\SalesReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\SaleSummary;
use App\Models\SaleDetail;
use App\Models\SalesInventory;
use App\Models\SalesInventoryExcel;
use App\Models\UserBranch;
use App\Models\ReturnExchangeItem;
use App\Models\ItemStock;
use App\Models\CompanyBranch;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;
use Session;
use Auth;
use File;
use Alchemy\Zippy\Zippy;
use DB;

class ConsolidatedInvReportController extends Controller
{
    public function index()
    {
    	$date_from = date('m/d/Y');
        $date_to   = date('m/d/Y');

    	return view('consolidated_inventory_report.index',compact('date_from','date_to'));
    }

    public function store(Request $request)
    {
    	ini_set('memory_limit', -1);
        set_time_limit(0);
        $date_from = date('Y-m-d',strtotime($request->date_from));
        $date_to = date('Y-m-d H:i:s',strtotime($date_from."+ 23 hours"));
    
    	$branches = CompanyBranch::getAllExceptWarehouse();

    	// $items = SaleDetail::getItemCodes($date_from, $date_to);
    	$items = ItemStock::getItemCodes($date_from,$date_to);
    	
    	// $query = SaleDetail::getTransactions($date_from, $date_to);
    	$query = ItemStock::getAllItems($date_from,$date_to);

    	$submit_type = $request->get('submit');

        if($submit_type == 1)
        {
        	$filename = "Consolidated_Inventory_Report_".$date_from;
            $count = 1;
            $savePath = storage_path('uploads/consolidated_inventory_report/'.$filename);
            if(!\File::exists($savePath)){
                mkdir($savePath, 0777, true);
            }            
            File::deleteDirectory($savePath, true);
        	
        	foreach(array_chunk($query, 5000) as $results){
	        	\Excel::create('CONSOLIDATED_INVENTORY_REPORT', function($excel) use($branches,$items,$results) {
	                $excel->sheet('SALES_REPORT', function($sheet) use($branches,$items,$results) {
	                	$headers = [
	                        'ITEM CODE',
	                        'DESCRIPTION',
	                        'CATEGORY',
	                        'GLOBAL STATUS',
	                        'CAMPAIGN ID',
	                    ];
	                    
	                    foreach ($branches as $branch) {
	                    	$headers[] = $branch['branch'];
	                    }
	                    $headers[] = "TOTAL";
	                    $sheet->row(1, $headers);
	                    $sheet->row(1, function($row) {
	                    	$row->setBackground('#FFB3A7');
	                    });
	                    $counter = 2;
	                    foreach ($items as $item) {
	                    	$smclark = 0;
	                    	$ayalacebu = 0;
	                    	$robermita = 0;
	                    	$uptownmall = 0;
	                    	$festivalmall = 0;
	                    	$conrad = 0;
	                    	$estancia = 0;
	                    	$uptowncenter = 0;
	                    	$centurymall = 0;
	                    	$greenbelt = 0;
	                    	$smaura = 0;
	                    	$podium = 0;
	                    	$bonihighst = 0;
	                    	$newport = 0;
	                    	$shang = 0;
	                    	$megamall = 0;
	                    	$glorietta4 = 0;
	                    	$trinoma = 0;
	                    	$galleria = 0;
	                    	$vertisnorth = 0;
	                    	$atc = 0;
	                    	$smmakati = 0;
	                    	$eastwood = 0;
	                    	$solaire = 0;
	                    	$smfairview = 0;
	                    	$gateway = 0;
	                    	$cloverleaf = 0;
	                    	$smpampanga = 0;
	                    	$ayalafeliz = 0;
	                    	$smsouthmall = 0;
	                    	$promenadeghills = 0;
	                    	$totalqty = 0;
	                    	foreach ($results as $result) {
		                    	if($result->branch_code == '00010002' && $item->itemcode == $result->itemcode){
		                    		$smclark = $smclark + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010003' && $item->itemcode == $result->itemcode){
		                    		$ayalacebu = $ayalacebu + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010004' && $item->itemcode == $result->itemcode){
		                    		$robermita = $robermita + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;	
		                    	}
		                    	if($result->branch_code == '00010005' && $item->itemcode == $result->itemcode){
		                    		$uptownmall = $uptownmall + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010006' && $item->itemcode == $result->itemcode){
		                    		$festivalmall = $festivalmall + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010007' && $item->itemcode == $result->itemcode){
		                    		$conrad = $conrad + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010008' && $item->itemcode == $result->itemcode){
		                    		$estancia = $estancia + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010009' && $item->itemcode == $result->itemcode){
		                    		$uptowncenter = $uptowncenter + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010010' && $item->itemcode == $result->itemcode){
		                    		$centurymall = $centurymall + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010011' && $item->itemcode == $result->itemcode){
		                    		$greenbelt = $greenbelt + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010012' && $item->itemcode == $result->itemcode){
		                    		$smaura = $smaura + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010013' && $item->itemcode == $result->itemcode){
		                    		$podium = $podium + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010014' && $item->itemcode == $result->itemcode){
		                    		$bonihighst = $bonihighst + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010015' && $item->itemcode == $result->itemcode){
		                    		$newport = $newport + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010016' && $item->itemcode == $result->itemcode){
		                    		$shang = $shang + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010017' && $item->itemcode == $result->itemcode){
		                    		$megamall = $megamall + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010018' && $item->itemcode == $result->itemcode){
		                    		$glorietta4 = $glorietta4 + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010019' && $item->itemcode == $result->itemcode){
		                    		$trinoma = $trinoma + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010020' && $item->itemcode == $result->itemcode){
		                    		$galleria = $galleria + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010021' && $item->itemcode == $result->itemcode){
		                    		$vertisnorth = $vertisnorth + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010022' && $item->itemcode == $result->itemcode){
		                    		$atc = $atc + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010023' && $item->itemcode == $result->itemcode){
		                    		$smmakati = $smmakati + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010024' && $item->itemcode == $result->itemcode){
		                    		$eastwood = $eastwood + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010025' && $item->itemcode == $result->itemcode){
		                    		$solaire = $solaire + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010026' && $item->itemcode == $result->itemcode){
		                    		$smfairview = $smfairview + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010027' && $item->itemcode == $result->itemcode){
		                    		$gateway = $gateway + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010028' && $item->itemcode == $result->itemcode){
		                    		$cloverleaf = $cloverleaf + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010029' && $item->itemcode == $result->itemcode){
		                    		$smpampanga = $smpampanga + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010030' && $item->itemcode == $result->itemcode){
		                    		$ayalafeliz = $ayalafeliz + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010031' && $item->itemcode == $result->itemcode){
		                    		$smsouthmall = $smsouthmall + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	if($result->branch_code == '00010032' && $item->itemcode == $result->itemcode){
		                    		$promenadeghills = $promenadeghills + $result->qty;
		                    		$totalqty = $totalqty + $result->qty;
		                    	}
		                    	$sheet->row($counter, array(
			                    	$item->itemcode, $item->description, $item->category, $item->global_status, $item->campaign_id,
			                    	$smclark,$ayalacebu,$robermita,$uptownmall,$festivalmall,
			                    	$conrad,$estancia,$uptowncenter,$centurymall,$greenbelt,$smaura,$podium,
									$bonihighst,$newport,$shang,$megamall,$glorietta4,$trinoma,
									$galleria,$vertisnorth,$atc,$smmakati,$eastwood,$solaire,$smfairview,
									$gateway,$cloverleaf,$smpampanga,$ayalafeliz,$smsouthmall,$promenadeghills,$totalqty
			                    ));
	                    	}
	                    $counter++;
	                    }
	                });
	            })->store('xls',$savePath);
				$count++;
        	}
        	$zippy = Zippy::load();
            $files = \File::allFiles($savePath);
            if(count($files) > 0){
                $zip_path2 = $savePath.'/zipped/';
                foreach ($files as $file) {
                    $folders[$file->getFilename()] = $savePath.'/'.$file->getFilename();
                }
                $folder_name = str_replace(":","_", $filename);
                $zip_path = $savePath.'/zipped/'.$folder_name.'.zip';

                if(!\File::exists($zip_path2)){
                    \File::makeDirectory($zip_path2,0777,true);
                }
                $archive = $zippy->create($zip_path,$folders,true);                
                return response()->download($zip_path);
            }else{
                return redirect()->back()->with('no_data','No Data');
            }
        }
    }
}
