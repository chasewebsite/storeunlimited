<?php

namespace App\Http\Controllers\SalesReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\RefundItem;

class RefundReportController extends Controller
{
    public function index(){
    	$items = RefundItem::search();
    	return view('refunds.index', compact('items'));
    }
}
