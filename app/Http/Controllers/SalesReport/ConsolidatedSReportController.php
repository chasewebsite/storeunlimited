<?php

namespace App\Http\Controllers\SalesReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\SaleSummary;
use App\Models\SaleDetail;
use App\Models\SalesInventory;
use App\Models\SalesInventoryExcel;
use App\Models\UserBranch;
use App\Models\ReturnExchangeItem;
use App\Models\ItemStock;
use App\Models\CompanyBranch;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;
use Session;
use Auth;
use File;
use Alchemy\Zippy\Zippy;
use DB;

class ConsolidatedSReportController extends Controller
{
    public function index()
    {
    	$date_from = date('m/d/Y');
        $date_to   = date('m/d/Y');

        return view('consolidated_sales_report.index',compact('date_from'));
    }

    public function store(Request $request)
    {
    	ini_set('memory_limit', -1);
        set_time_limit(0);
        $date_from = date('Y-m-d',strtotime($request->date_from));
        $date_to = date('Y-m-d H:i:s',strtotime($date_from."+ 23 hours"));
    
    	$branches = CompanyBranch::getAllExceptWarehouse();

    	$items = SaleDetail::getItemCodes($date_from, $date_to);
        	
    	$queries = SaleDetail::getTransactions($date_from, $date_to);

        $submit_type = $request->get('submit');
        
        if($submit_type == 1){

            $filename = "Consolidated_Sales_Report_".$date_from;
            $count = 1;
            $savePath = storage_path('uploads/consolidated_sales_report/'.$filename);
            if(!\File::exists($savePath)){
                mkdir($savePath, 0777, true);
            }            
            File::deleteDirectory($savePath, true);
        	
            foreach (array_chunk($queries, 5000) as $results) {
                \Excel::create('CONSOLIDATED_SALES_REPORT', function($excel) use($branches,$items,$results) {
                    $excel->sheet('SALES_REPORT', function($sheet) use($branches,$items,$results) {
                    	$headers = [
                            'ITEM CODE',
                            'DESCRIPTION',
                            'CATEGORY',
                            'GLOBAL STATUS',
                            'CAMPAIGN ID',
                        ];
                        // row 2
                        $headers2 = [
                        	'','','','',''
                        ];
                        foreach ($branches as $branch) {
                        	$headers[] = $branch['branch'];
                        	$headers[] = '';
                        	$headers2[] = 'QTY';
                        	$headers2[] = 'AMOUNT';
                        }
                        $headers[] = "TOTAL";
                        $headers2[] = 'QTY';
                    	$headers2[] = 'AMOUNT';
                        // row 2
                        $sheet->row(1, $headers);
                        $sheet->row(2, $headers2);
                        $sheet->row(1, function($row) {
                        	$row->setBackground('#FFB3A7');
                        });
                        $sheet->mergeCells('F1:G1');
                        $sheet->mergeCells('H1:I1');
                        $sheet->mergeCells('J1:K1');
                        $sheet->mergeCells('L1:M1');
                        $sheet->mergeCells('N1:O1');
                        $sheet->mergeCells('P1:Q1');
                        $sheet->mergeCells('R1:S1');
                        $sheet->mergeCells('T1:U1');
                        $sheet->mergeCells('V1:W1');
                        $sheet->mergeCells('X1:Y1');
                        $sheet->mergeCells('Z1:AA1');
                        $sheet->mergeCells('AB1:AC1');
                        $sheet->mergeCells('AD1:AE1');
                        $sheet->mergeCells('AF1:AG1');
                        $sheet->mergeCells('AH1:AI1');
                        $sheet->mergeCells('AJ1:AK1');
                        $sheet->mergeCells('AL1:AM1');
                        $sheet->mergeCells('AN1:AO1');
                        $sheet->mergeCells('AP1:AQ1');
                        $sheet->mergeCells('AR1:AS1');
                        $sheet->mergeCells('AT1:AU1');
                        $sheet->mergeCells('AV1:AW1');
                        $sheet->mergeCells('AX1:AY1');
                        $sheet->mergeCells('AZ1:BA1');
                        $sheet->mergeCells('BB1:BC1');
                        $sheet->mergeCells('BD1:BE1');
                        $sheet->mergeCells('BF1:BG1');
                        $sheet->mergeCells('BH1:BI1');
                        $sheet->mergeCells('BJ1:BK1');
                        $sheet->mergeCells('BL1:BM1');
                        $sheet->mergeCells('BN1:BO1');
                        $sheet->mergeCells('BP1:BQ1');

                        $sheet->cell('F1:BP1', function($cell) {
                            $cell->setAlignment('center');
                            $cell->setBackground('#89C4F4');
                        });

                        $counter = 3;
                        foreach ($items as $item) {
                        	$smclark = 0;
                        	$smclark_am = 0;
                        	$ayalacebu = 0;
                        	$ayalacebu_am = 0;
                        	$robermita = 0;
                        	$robermita_am = 0;
                        	$uptownmall = 0;
                        	$uptownmall_am = 0;
                        	$festivalmall = 0;
                        	$festivalmall_am = 0;
                        	$conrad = 0;
                        	$conrad_am = 0;
                        	$estancia = 0;
                        	$estancia_am = 0;
                        	$uptowncenter = 0;
                        	$uptowncenter_am = 0;
                        	$centurymall = 0;
                        	$centurymall_am = 0;
                        	$greenbelt = 0;
                        	$greenbelt_am = 0;
                        	$smaura = 0;
                        	$smaura_am = 0;
                        	$podium = 0;
                        	$podium_am = 0;
                        	$bonihighst = 0;
                        	$bonihighst_am = 0;
                        	$newport = 0;
                        	$newport_am = 0;
                        	$shang = 0;
                        	$shang_am = 0;
                        	$megamall = 0;
                        	$megamall_am = 0;
                        	$glorietta4 = 0;
                        	$glorietta4_am = 0;
                        	$trinoma = 0;
                        	$trinoma_am = 0;
                        	$galleria = 0;
                        	$galleria_am = 0;
                        	$vertisnorth = 0;
                        	$vertisnorth_am = 0;
                        	$atc = 0;
                        	$atc_am = 0;
                        	$smmakati = 0;
                        	$smmakati_am = 0;
                        	$eastwood = 0;
                        	$eastwood_am = 0;
                        	$solaire = 0;
                        	$solaire_am = 0;
                        	$smfairview = 0;
                        	$smfairview_am = 0;
                        	$gateway = 0;
                        	$gateway_am = 0;
                        	$cloverleaf = 0;
                        	$cloverleaf_am = 0;
                        	$smpampanga = 0;
                        	$smpampanga_am = 0;
                        	$ayalafeliz = 0;
                        	$ayalafeliz_am = 0;
                        	$smsouthmall = 0;
                        	$smsouthmall_am = 0;
                        	$promenadeghills = 0;
                        	$promenadeghills_am = 0;
                        	$totalqty = 0;
                        	$totalqty_am = 0;
                        	foreach ($results as $result) {
    	                    	if($result->branch_code == '00010002' && $item->itemcode == $result->itemcode){
    	                    		$smclark = $smclark + $result->qty;
    	                    		$smclark_am = $smclark_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010003' && $item->itemcode == $result->itemcode){
    	                    		$ayalacebu = $ayalacebu + $result->qty;
    	                    		$ayalacebu_am = $ayalacebu_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010004' && $item->itemcode == $result->itemcode){
    	                    		$robermita = $robermita + $result->qty;
    	                    		$robermita_am = $robermita_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;	
    	                    	}
    	                    	if($result->branch_code == '00010005' && $item->itemcode == $result->itemcode){
    	                    		$uptownmall = $uptownmall + $result->qty;
    	                    		$uptownmall_am = $uptownmall_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010006' && $item->itemcode == $result->itemcode){
    	                    		$festivalmall = $festivalmall + $result->qty;
    	                    		$festivalmall_am = $festivalmall_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010007' && $item->itemcode == $result->itemcode){
    	                    		$conrad = $conrad + $result->qty;
    	                    		$conrad_am = $conrad_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010008' && $item->itemcode == $result->itemcode){
    	                    		$estancia = $estancia + $result->qty;
    	                    		$estancia_am = $estancia_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010009' && $item->itemcode == $result->itemcode){
    	                    		$uptowncenter = $uptowncenter + $result->qty;
    	                    		$uptowncenter_am = $uptowncenter_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010010' && $item->itemcode == $result->itemcode){
    	                    		$centurymall = $centurymall + $result->qty;
    	                    		$centurymall_am = $centurymall_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010011' && $item->itemcode == $result->itemcode){
    	                    		$greenbelt = $greenbelt + $result->qty;
    	                    		$greenbelt_am = $greenbelt_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010012' && $item->itemcode == $result->itemcode){
    	                    		$smaura = $smaura + $result->qty;
    	                    		$smaura_am = $smaura_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010013' && $item->itemcode == $result->itemcode){
    	                    		$podium = $podium + $result->qty;
    	                    		$podium_am = $podium_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010014' && $item->itemcode == $result->itemcode){
    	                    		$bonihighst = $bonihighst + $result->qty;
    	                    		$bonihighst_am = $bonihighst_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010015' && $item->itemcode == $result->itemcode){
    	                    		$newport = $newport + $result->qty;
    	                    		$newport_am = $newport_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010016' && $item->itemcode == $result->itemcode){
    	                    		$shang = $shang + $result->qty;
    	                    		$shang_am = $shang_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010017' && $item->itemcode == $result->itemcode){
    	                    		$megamall = $megamall + $result->qty;
    	                    		$megamall_am = $megamall_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010018' && $item->itemcode == $result->itemcode){
    	                    		$glorietta4 = $glorietta4 + $result->qty;
    	                    		$glorietta4_am = $glorietta4_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010019' && $item->itemcode == $result->itemcode){
    	                    		$trinoma = $trinoma + $result->qty;
    	                    		$trinoma_am = $trinoma_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010020' && $item->itemcode == $result->itemcode){
    	                    		$galleria = $galleria + $result->qty;
    	                    		$galleria_am = $galleria_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010021' && $item->itemcode == $result->itemcode){
    	                    		$vertisnorth = $vertisnorth + $result->qty;
    	                    		$vertisnorth_am = $vertisnorth_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010022' && $item->itemcode == $result->itemcode){
    	                    		$atc = $atc + $result->qty;
    	                    		$atc_am = $atc_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010023' && $item->itemcode == $result->itemcode){
    	                    		$smmakati = $smmakati + $result->qty;
    	                    		$smmakati_am = $smmakati_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010024' && $item->itemcode == $result->itemcode){
    	                    		$eastwood = $eastwood + $result->qty;
    	                    		$eastwood_am = $eastwood_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010025' && $item->itemcode == $result->itemcode){
    	                    		$solaire = $solaire + $result->qty;
    	                    		$solaire_am = $solaire_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010026' && $item->itemcode == $result->itemcode){
    	                    		$smfairview = $smfairview + $result->qty;
    	                    		$smfairview_am = $smfairview_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010027' && $item->itemcode == $result->itemcode){
    	                    		$gateway = $gateway + $result->qty;
    	                    		$gateway_am = $gateway_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010028' && $item->itemcode == $result->itemcode){
    	                    		$cloverleaf = $cloverleaf + $result->qty;
    	                    		$cloverleaf_am = $cloverleaf_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010029' && $item->itemcode == $result->itemcode){
    	                    		$smpampanga = $smpampanga + $result->qty;
    	                    		$smpampanga_am = $smpampanga_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010030' && $item->itemcode == $result->itemcode){
    	                    		$ayalafeliz = $ayalafeliz + $result->qty;
    	                    		$ayalafeliz_am = $ayalafeliz_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010031' && $item->itemcode == $result->itemcode){
    	                    		$smsouthmall = $smsouthmall + $result->qty;
    	                    		$smsouthmall_am = $smsouthmall_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	if($result->branch_code == '00010032' && $item->itemcode == $result->itemcode){
    	                    		$promenadeghills = $promenadeghills + $result->qty;
    	                    		$promenadeghills_am = $promenadeghills_am + $result->cost;
    	                    		$totalqty = $totalqty + $result->qty;
    	                    		$totalqty_am = $totalqty_am + $result->cost;
    	                    	}
    	                    	$sheet->row($counter, array(
    		                    	$item->itemcode, $item->description, $item->category, $item->global_status, $item->campaign_id,
    		                    	$smclark,$smclark_am,$ayalacebu,$ayalacebu_am,$robermita,$robermita_am,
    		                    	$uptownmall,$uptownmall_am,$festivalmall,$festivalmall_am,
    		                    	$conrad,$conrad_am,$estancia,$estancia_am,$uptowncenter,$uptowncenter_am,
    		                    	$centurymall,$centurymall_am,$greenbelt,$greenbelt_am,$smaura,$smaura_am,
    		                    	$podium,$podium_am,$bonihighst,$bonihighst_am,$newport,$newport_am,
    		                    	$shang,$shang_am,$megamall,$megamall_am,$glorietta4,$glorietta4_am,
    		                    	$trinoma,$trinoma_am,$galleria,$galleria_am,$vertisnorth,$vertisnorth_am,
    		                    	$atc,$atc_am,$smmakati,$smmakati,$eastwood,$eastwood_am,
    		                    	$solaire,$solaire_am,$smfairview,$smfairview_am,$gateway,$gateway_am,
    		                    	$cloverleaf,$cloverleaf_am,$smpampanga,$smpampanga_am,$ayalafeliz,$ayalafeliz_am,
    		                    	$smsouthmall,$smsouthmall_am,$promenadeghills,$promenadeghills_am,$totalqty,$totalqty_am
    		                    ));
                        	}
                        $counter++;
                        }
                    });
                })->store('xlsx',$savePath);
                $count++;
            }
            $zippy = Zippy::load();
            $files = \File::allFiles($savePath);
            if(count($files) > 0){
                $zip_path2 = $savePath.'/zipped/';
                foreach ($files as $file) {
                    $folders[$file->getFilename()] = $savePath.'/'.$file->getFilename();
                }
                $folder_name = str_replace(":","_", $filename);
                $zip_path = $savePath.'/zipped/'.$folder_name.'.zip';

                if(!\File::exists($zip_path2)){
                    \File::makeDirectory($zip_path2,0777,true);
                }
                $archive = $zippy->create($zip_path,$folders,true);                
                return response()->download($zip_path);
            }else{
                return redirect()->back()->with('no_data','No Data');
            }
        }
    }
}
