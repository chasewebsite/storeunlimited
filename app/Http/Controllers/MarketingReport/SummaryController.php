<?php

namespace App\Http\Controllers\MarketingReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\MarketingMonth;
use Carbon\Carbon;

use Auth;
use DB;
use App\Models\UserBranch;
use App\Models\Company;

class SummaryController extends Controller
{
    //


	public function index(Request $request){
       
    	$months = MarketingMonth::lists('description', 'id');

    	
      
    	return view('marketing.summary.index',compact('months'));
    }



    public function store(Request $request){

    	set_time_limit(0);
        ini_set('memory_limit', '-1');
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);


        $month = MarketingMonth::where('id' , $request->month)->first();


        $arr = explode(' - ', $month->description);

        $mon_desc = $arr[0];
        $year = $arr[1];

        $first = "first day of ".$mon_desc." ".$year;
        $last  = "last day of ".$mon_desc." ".$year;
        $days =  date("t", strtotime(new Carbon($first)));
        
        $date_from =  date("Y-m-d", strtotime(new Carbon($first)));
       	$date_to =  date("Y-m-d", strtotime(new Carbon($last)));
       	$se_branches = implode("','", $branches);
       	$month_id = $month->id;


        $query = sprintf("select companies.company, companies.company_code,
            company_branches.branch, company_branches.branch_code,
            company_branches.area,sales.total_trans,


            coalesce(sales.net_amount,0) as net_amount,
            coalesce(refund.refund_amount,0) as refund_amount,
            coalesce(return_exchanges.return_exchange_amount,0) as return_exchange_amount,
            coalesce(budgets.budget,0) as budget,
             coalesce(sales1.sales_qty,0) as sale_qty,
            coalesce(refund.refund_qty,0) as refund_qty,
            coalesce(return_exchanges.return_qty,0) as return_qty
            
            from company_branches
            join companies on companies.id = company_branches.company_id
            left join (
                select branch_code, sum(sale_summaries.net_amount) as net_amount ,count(sale_summaries.id) as total_trans 
                from sale_summaries  
                where post_void = 0
                and DATE(local_time) BETWEEN '%s' AND '%s'
               
                group by branch_code
            ) sales on sales.branch_code = company_branches.branch_code

             left join (
                select branch_code, sum(sale_details.qty) as sales_qty
                from sale_summaries  join sale_details on sale_details.sale_summary_id = sale_summaries.id
                where post_void = 0
                and DATE(local_time) BETWEEN '%s' AND '%s'
               
                group by branch_code
            ) sales1 on sales1.branch_code = company_branches.branch_code

            left join (
                select branch_code, sum(amount) as refund_amount ,  sum(qty) as refund_qty
                from refund_items
                where DATE(local_time) BETWEEN '%s' AND '%s'
                
                group by branch_code
            ) refund on refund.branch_code = company_branches.branch_code
            left join (
                select branch_code, sum(amount) as return_exchange_amount , sum(qty) as return_qty
                from return_exchange_items
                where DATE(local_time) BETWEEN '%s' AND '%s'
                
                group by branch_code
            ) return_exchanges on return_exchanges.branch_code = company_branches.branch_code
              left join (
                select branch_code, budget
                from marketing_budgets
                where marketing_month_id = '%s' 
                
                
            ) budgets on budgets.branch_code = company_branches.branch_code
            where company_branches.branch_code in ('%s')
            
             
            order by companies.company, company_branches.branch",
        $date_from,$date_to,$date_from,$date_to,$date_from,$date_to,$date_from,$date_to,$month_id, $se_branches);

        $datas = DB::select(DB::raw($query));

        $sum_sales = 0.00;
        $sum_refund = 0.00;

        $sum_sales_qty = 0;
        $sum_refund_qty = 0;
        $count_branch = 0;
        $sum_total_trans = 0;
        foreach ($datas as $data) {
            $count_branch++;
        	$sum_sales = $sum_sales + $data->net_amount;
        	$sum_sales_qty = $sum_sales_qty + $data->sale_qty;
            $sum_total_trans = $sum_total_trans + $data->total_trans;
        }

        foreach ($datas as $data) {
        	$sum_refund = $sum_refund + $data->refund_amount;
        	$sum_refund_qty = $sum_refund_qty + $data->refund_qty;
        }




        $total_sale = $sum_sales - $sum_refund;
        $total_sale_qty = $sum_sales_qty - $sum_refund_qty;


        
        
      





        \Excel::create("Marketing Summary Report", function($excel)  use ($datas , $total_sale , $days ,$total_sale_qty , $sum_total_trans , $count_branch , $mon_desc) {
                      
                        
                 $excel->sheet("Summary", function($sheet) use($datas , $total_sale , $days , $total_sale_qty , $sum_total_trans , $count_branch , $mon_desc) {

                 			$company = Company::find(1)->company;

                            $header = "PH ".$company." Retail Sales Report for the month of ".$mon_desc;

                            $sheet->row(1, array($header));

                            $sheet->row(3, array($company,"SQM","TOTAL SALES","% OF SALES LOCATION","QTY","% OF TOTAL","Avg Item Value","TOTAL TRANSACTION","AVERAGE TRANSACTION VALUE","UNITS PER TRANSACTION","AVERAGE ITEM VALUE","MONTHLY RUNNING AVERAGE","SALES PER SQM/PHP","BUDGET FOR THE MONTH","ACTUAL ACHIEVE % VS BUDGET","ACTUAL ACHIEVED % MTD","ACTUAL SALES IN AVG PER DAY","SALES BUDGET IN AVG PER DAY","SALES BUDGET IN AVG PER DAY QTY","SALES BUDGET IN AVG PER DAY" ,"REMARKS"));
                          
                            $cnt = 5;
                            $sum_square = 0.00;
                            $loc_amount_average = 0;
                            $sum_total_qty = 0;
                            $_total_percent = 0.00;
                            $_total_avg_item = 0.00;
                            $_total_trans = 0;
                            $_total_trans_avg = 0.00;
                            $_total_avg_upt = 0.00;
                            $_total_item_value = 0.00;
                            $_total_monthly_avg = 0.00;
                            $_total_per_sqm = 0.00;
                            $_total_budget = 0.00;
                            $_total_achieve_budget = 0.00;
                            $_total_achieve_budget_mtd = 0.00;
                            $_total_sale_day = 0.00;
                            $_total_sale_budget_day = 0.00;
                            $_total_sale_budget_day1 = 0.00;
                            $_total_sale_budget_day2 = 0.00;
                            foreach ($datas as $data) {

                            			$net_sales = 0.00;
                            			
                            			$loc_amount = 0.00;
                            			$net_sale_qty = 0;
                            			$avg_item_val = 0.00;
                                        $avg_trans_val = 0.00;
                                        $avg_upt = 0.00;
                                        $month_run_avg = 0.00;
                                        $sale_per_sqm = 0.00;
                                        $actual_achieve = 0.00;
                                        $actual_achieve_mtd = 0.00;
                                        $actual_sales_day = 0.00;
                                        $sale_budget_day = 0.00;
                                        $sale_budget_day1 = 0.00;

                            			$net_sales = $data->net_amount - $data->refund_amount;
                            			

                            			$net_sale_qty = $data->sale_qty - $data->refund_qty;
                            			$net_sale_qty1 = $net_sale_qty / $total_sale_qty;

                            			if($net_sales != 0) {
                                            $loc_amount = $net_sales / $total_sale ;
                            				$avg_item_val = $net_sales / $net_sale_qty;
                                            $avg_trans_val = $net_sales / $sum_total_trans;
                                            $month_run_avg = ($net_sales / $days) * $days;
                                            $sale_per_sqm  = $month_run_avg / $data->area;

                                            if($data->budget !=0 ) {
                                                $actual_achieve = $net_sales / $data->budget;
                                                $actual_achieve_mtd = $net_sales / (($data->budget / $days) * $days );
                                                $sale_budget_day =  $data->budget / $days;
                                            }

                                            
                                            $actual_sales_day = $net_sales / $days;

                            			}

                                        if($net_sale_qty != 0 ){
                                            $avg_upt = $net_sale_qty / $data->total_trans;


                                        }

                                        if($sale_budget_day != 0.00) {

                                          $sale_budget_day1  =  $sale_budget_day / $avg_item_val;

                                        }

	                                    $sheet->row($cnt,array(

	                                        $data->branch,
	                                        $data->area,
	                                        "Php ".number_format($net_sales,2),
	                                        round((float)$loc_amount * 100 ) . '%',
	                                        $net_sale_qty,
	                                        round((float)$net_sale_qty1 * 100 ) . '%',
	                                        "Php ".number_format(round((float)$avg_item_val,2),2),
                                            $data->total_trans,
                                            "Php ".number_format(round((float)$avg_trans_val,2),2),
                                            round((float)$avg_upt,2),
	                                        round((float)$avg_item_val,2),
                                            "Php ".number_format(round((float)$month_run_avg,2),2),
                                            "Php ".number_format(round((float)$sale_per_sqm,2),2),
                                            "Php ".number_format(round((float)$data->budget,2),2),
                                            round((float)$actual_achieve * 100 ) . '%',
                                            round((float)$actual_achieve_mtd * 100 ) . '%',
                                            "Php ".number_format(round((float)$actual_sales_day,2),2),
                                            "Php ".number_format(round((float)$sale_budget_day,2),2),
                                            round((float)$net_sale_qty / $days ,2),
                                            round((float)$sale_budget_day1 ,2),


	                                    ));

                                        $sum_square = $sum_square + $data->area;
                                        $loc_amount_average = $loc_amount_average  +  round((float)$loc_amount * 100 );
                                        $sum_total_qty = $sum_total_qty + $net_sale_qty;
                                        $_total_percent =  $_total_percent + round((float)$net_sale_qty1 * 100 );
                                       
                                        $_total_trans  = $_total_trans + $data->total_trans;
                                      
                                        
                                        $_total_item_value = $_total_item_value + $avg_item_val;
                                        $_total_monthly_avg = $_total_monthly_avg + $month_run_avg;
                                        $_total_per_sqm = $_total_per_sqm + $sale_per_sqm;
                                        $_total_budget = $_total_budget + $data->budget;
                                       
                                        $_total_achieve_budget_mtd = $_total_achieve_budget_mtd + $actual_achieve_mtd;
                                        $_total_sale_day = $_total_sale_day + $actual_sales_day;
                                        $_total_sale_budget_day = $_total_sale_budget_day  + $sale_budget_day;
                                        $_total_sale_budget_day1 = $_total_sale_budget_day1 + round((float)$net_sale_qty / $days ,2);
                                        $_total_sale_budget_day2 = $_total_sale_budget_day1 + round((float)$sale_budget_day1 ,2);
	                                    $cnt++;




                            }
                             $_total_avg_item = $total_sale / $total_sale_qty;
                               $_total_trans_avg = $total_sale / $_total_trans;
                               $_total_avg_upt = $total_sale_qty / $_total_trans;
                                $_total_achieve_budget = $total_sale + $_total_budget;
                                $__total_achieve_budget_mtd = $_total_achieve_budget / $count_branch;
                              $sheet->row($cnt++,array(

                                            "TOTAL ".$company,
                                            round((float)$sum_square / $count_branch ,2),
                                            "Php ".number_format($total_sale,2),
                                            round((float)$loc_amount_average ) . '%',
                                            $sum_total_qty,
                                             round((float)$_total_percent ) . '%',
                                             "Php ".number_format(round((float)$_total_avg_item,2),2),
                                             $_total_trans,
                                             "Php ".number_format(round((float)$_total_trans_avg,2),2),
                                              round((float)$_total_avg_upt,2),
                                              round((float)$_total_item_value / $count_branch,2),
                                              "Php ".number_format(round((float)$_total_monthly_avg,2),2),
                                              "Php ".number_format(round((float)$_total_per_sqm,2),2),
                                              "Php ".number_format(round((float)$_total_budget,2),2),
                                               round((float)$_total_achieve_budget * 100 ) . '%',
                                               round((float)$_total_achieve_budget_mtd * 100 ) . '%',
                                               "Php ".number_format(round((float)$_total_sale_day,2),2),
                                               "Php ".number_format(round((float)$_total_sale_budget_day,2),2),
                                               round((float)$_total_sale_budget_day1 ,2),
                                               round((float)$_total_sale_budget_day2 ,2),
                                            // $net_sale_qty,
                                            // round((float)$net_sale_qty1 * 100 ) . '%',
                                            // "Php ".number_format(round((float)$avg_item_val,2),2),
                                            // $data->total_trans,
                                            // "Php ".number_format(round((float)$avg_trans_val,2),2),
                                            // round((float)$avg_upt,2),
                                            // round((float)$avg_item_val,2),
                                            // "Php ".number_format(round((float)$month_run_avg,2),2),
                                            // "Php ".number_format(round((float)$sale_per_sqm,2),2),
                                            // "Php ".number_format(round((float)$data->budget,2),2),
                                            // round((float)$actual_achieve * 100 ) . '%',
                                            // round((float)$actual_achieve_mtd * 100 ) . '%',
                                            // "Php ".number_format(round((float)$actual_sales_day,2),2),
                                            // "Php ".number_format(round((float)$sale_budget_day,2),2),
                                            // round((float)$net_sale_qty / $days ,2),
                                            // round((float)$sale_budget_day1 ,2),


                                        ));
                                    $above = " ";
                                    if($_total_monthly_avg > $_total_budget) {
                                            $above = "ABOVE";
                                    }
                                    else if ($_total_monthly_avg < $_total_budget) {

                                            $above ="BELOW";
                                    }


                                $cnt++;
                                $cnt++;
                                $cnt++;
                                $first = $cnt++;
                                $second = $cnt++;
                                $third = $cnt++;
                                $forth = $cnt++;
                                $fifth = $cnt++;
                                $six  = $cnt++;
                               $sheet->row($first,array(

                                    "Retail Sales Report for the month of" , " " ,$mon_desc,"   ","On Target: ",$above

                                ));
                               $_target_sale = $total_sale / $_total_budget;

                                $sheet->row($second,array(

                                    "Days in the current month" , " " ,$days, " ","Total Sales MTD vs Target MTD", round((float)$_target_sale * 100) . '%',

                                ));
                                  $sheet->row($third,array(

                                    "Sales Days (MTD)" , " " ,$days," ","Total daily sales target in PHP " ,number_format(round((float)$_total_sale_budget_day,2),2)

                                ));

                                  $_target_sale_day = $_total_sale_day / $_total_sale_budget_day;

                                     $sheet->row($forth,array(

                                    "Actual Daily Avg" , " ", " ", " ","Daily sales vs Daily target",round((float)$_target_sale_day * 100) . '%' ,

                                ));

                                  


                                   $sheet->row($fifth,array(

                                    "Total Monthly Running Average:" , " ", "Php ".number_format(round((float)$_total_monthly_avg,2),2)," ","Total daily sales target in Qty",round((float)$sum_total_qty / $days )

                                ));



                                    $_1 = sprintf("A%s:G%s",$first,$first);
                                    $_2 = sprintf("A%s:G%s",$second,$second);
                                    $_3 = sprintf("A%s:G%s",$third,$third);
                                    $_4 = sprintf("A%s:G%s",$forth,$forth);
                                    $_5 = sprintf("A%s:G%s",$fifth,$fifth);



                                        $sheet->setBorder($_1, 'thin');
                                        $sheet->setBorder($_2, 'thin');
                                        $sheet->setBorder($_3, 'thin');
                                        $sheet->setBorder($_4, 'thin');
                                        $sheet->setBorder($_5, 'thin');
                                       



                           

                        });
                       

                          

        })->export('xls');


    }

}
