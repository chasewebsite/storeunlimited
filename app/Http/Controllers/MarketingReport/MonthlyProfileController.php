<?php

namespace App\Http\Controllers\MarketingReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\MarketingMonth;
use Carbon\Carbon;

use Auth;
use DB;
use App\Models\UserBranch;
use App\Models\Company;

class MonthlyProfileController extends Controller
{
    //

    public function index(Request $request){
       
    	$months =  ["January" , "February" , "March" , "April" , "May" , "June" , "July", "August" ,"September" , "October" , "November" , "December"];
    	$years  = ["2016","2017","2018","2019","2020"];
    
    	$_month  = date('F');
    	$_year  = date('Y');
    	
    	$today_month = array_search($_month, $months);
      	$today_year = array_search($_year, $years);
    	return view('marketing.profile.index',compact('months','years','today_month','today_year'));
    }

      public function store(Request $request){ 

    	$months =  ["January" , "February" , "March" , "April" , "May" , "June" , "July", "August" ,"September" , "October" , "November" , "December"];
    	$years  = ["2016","2017","2018","2019","2020"];
      	$month = $months[$request->month];
      	$year  = $years[$request->year];
      	 $branches = UserBranch::getAllowedBranch(Auth::user()->id);
      	 $se_branches = implode("','", $branches);
      	 $first = "first day of ".$month." ".$year;
        $last  = "last day of ".$month." ".$year;
        $days =  date("t", strtotime(new Carbon($first)));
     
        $date_from =  date("Y-m-d", strtotime(new Carbon($first)));
       	$date_to =  date("Y-m-d", strtotime(new Carbon($last)));


       	$query = sprintf("select 
            sum(sale_summaries.net_amount) as net_amount  , count(sale_summaries.id) as total_trans ,date(local_time) as local_time ,0 as qty
            from sale_summaries
           
            
            where post_void = 0
            and DATE(local_time) BETWEEN '%s' AND '%s'
            and branch_code in ('%s')
            
            group by date(local_time)
            ",
        $date_from,$date_to,$se_branches);


       	$query1 = sprintf("select sum(qty) as qty,
            date(local_time) as local_time
            from sale_details
            join sale_summaries on sale_summaries.id = sale_details.sale_summary_id
            
            where post_void = 0
            and DATE(local_time) BETWEEN '%s' AND '%s'
            and sale_summaries.branch_code in ('%s')
            
            group by date(local_time)
            ",
        $date_from,$date_to,$se_branches);


        $items1 =  DB::select(DB::raw($query1));

        $items =  DB::select(DB::raw($query));


          foreach ($items1 as $item1) {
            for($x = 0; $x <  count($items); $x++){
                if($item1->local_time == $items[$x]->local_time){
                    $items[$x]->qty = $items[$x]->qty + $item1->qty;
                    
                }
            }
        }


         $query_return = sprintf("select sum(qty) as qty, sum(amount) as amount , date(local_time) as local_time
            from refund_items
            where DATE(local_time) BETWEEN '%s' AND '%s'
            and branch_code in ('%s')
            group by date(local_time)",
        $date_from,$date_to,$se_branches);


         $returneds = DB::select(DB::raw($query_return));
          foreach ($returneds as $retuned) {
            for($x = 0; $x <  count($items); $x++){
                if($retuned->local_time == $items[$x]->local_time){
                    $items[$x]->qty = $items[$x]->qty - $retuned->qty;
                    $items[$x]->net_amount = $items[$x]->net_amount - $retuned->amount;
                }
            }
        }

         $total_sale = 0;
         $total_qty = 0;
         $total_trans = 0;
      
		foreach ($items as $item) {
			$total_sale = $total_sale + $item->net_amount;
			$total_qty = $total_qty + $item->qty;
			$total_trans = $total_trans + $item->total_trans;
		}

       

      	
        \Excel::create("Monthly Profile", function($excel)  use ($items , $total_sale , $days ,$total_qty , $total_trans  , $month) {
                      
                        
                 $excel->sheet("Monthly Profile", function($sheet) use($items , $total_sale , $days , $total_qty , $total_trans  , $month) {

                 			$company = Company::find(1)->company;

                            $header = "Philippines Monthly Performance  Profile for ".$month;

                            $sheet->row(1, array($header));

                            $sheet->row(3, array("Date","Day","Total Daily sales (Qty)","Pct of total (Qty)","Daily TT (Total transactions)","Pct of total (Qty)","Daily TS (Total Sales) in Peso","Pct of total (PHP)","ATV (Average Transaction Value) in Peso","UPT (Units Per Transaction)","Avg item value"));
                          
                            $cnt = 5;
                            $_total_qty = 0.00;
                            $_total_trans = 0.00;
                            $_total_sale =0.00;
                            $_total_atv =0.00;
                            $_total_upt = 0.00;
                            $_total_avg_item = 0.00;
                          
                            foreach ($items as $data) {

                            			$pecent_qty = 0.00;
                            			$percent_trans = 0.00;
                            			$percent_sale = 0.00;
                            			$atv = 0.00;
                            			$upt = 0.00;
                            			$avg_item_value = 0.00;

                            			if($data->qty != 0) {

                            				$pecent_qty = $data->qty / $total_qty; 
                            				$upt = $data->qty / $data->total_trans;
                            				

                            			}
                            			if($data->total_trans != 0 ) {
                            				$percent_trans = $data->total_trans / $total_trans;
                            			}
                            			if($data->net_amount !=0 ) {


                            				$percent_sale = $data->net_amount / $total_sale;
                            				$atv = $data->net_amount / $data->total_trans;
                            				$avg_item_value = $data->net_amount / $data->qty;
                            			}

	                                    $sheet->row($cnt,array(

	                                         date("j", strtotime($data->local_time)),
	                                       	date("l", strtotime($data->local_time)),
	                                        $data->qty,
	                                        round((float)$pecent_qty * 100 ) . '%',
	                                        $data->total_trans,
	                                        round((float)$percent_trans * 100 ) . '%',
	                                        number_format($data->net_amount,2),
                                            round((float)$percent_sale * 100 ) . '%',
                                            round((float)$atv,2),
                                            round((float)$upt,2),
                                            round((float)$avg_item_value,2),

	                                    ));
	                                     $cnt++;
	                                     $_total_qty = $_total_qty + $pecent_qty;
	                                     $_total_trans = $_total_trans + $percent_trans;
	                                     $_total_sale = $_total_sale + $percent_sale;
	                                     $_total_atv = $_total_atv + $atv;
	                                     $_total_upt = $_total_upt + $upt;
	                                     $_total_avg_item = $_total_avg_item + $avg_item_value;
                                     




                            }
                              $sheet->row($cnt++,array(

	                                        "",
	                                       	"Total",
	                                        $total_qty,
	                                        round((float)$_total_qty * 100 ) . '%',
	                                        $total_trans,
	                                        round((float)$_total_trans * 100 ) . '%',
	                                        number_format($total_sale,2),
                                            round((float)$_total_sale * 100 ) . '%',
                                            number_format(round((float)$_total_atv,2),2),
                                            number_format(round((float)$_total_upt,2),2),
                                            number_format(round((float)$_total_avg_item,2),2),

	                                    ));

                              // $sheet->row($cnt,array(

                              //               "TOTAL ".$company,
                              //               round((float)$sum_square / $count_branch ,2),
                              //               "Php ".number_format($total_sale,2),
                              //               round((float)$loc_amount_average ) . '%',
                              //               $sum_total_qty,
                              //                round((float)$_total_percent ) . '%',
                              //                "Php ".number_format(round((float)$_total_avg_item,2),2),
                              //                $_total_trans,
                              //                "Php ".number_format(round((float)$_total_trans_avg,2),2),
                              //                 round((float)$_total_avg_upt,2),
                              //                 round((float)$_total_item_value,2),
                              //                 "Php ".number_format(round((float)$_total_monthly_avg,2),2),
                              //                 "Php ".number_format(round((float)$_total_per_sqm,2),2),
                              //                 "Php ".number_format(round((float)$_total_budget,2),2),
                              //                  round((float)$_total_achieve_budget * 100 ) . '%',
                              //                  round((float)$_total_achieve_budget_mtd * 100 ) . '%',
                              //                  "Php ".number_format(round((float)$_total_sale_day,2),2),
                              //                  "Php ".number_format(round((float)$_total_sale_budget_day,2),2),
                              //                  round((float)$_total_sale_budget_day1 ,2),
                              //                  round((float)$_total_sale_budget_day2 ,2),
                                           


                              //           ));
                                   


                              
                             
                               

                                 

                                  


                                 






                           

                        });
                       

                          

        })->export('xls');



      }

}
