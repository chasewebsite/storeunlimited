<?php

namespace App\Http\Controllers\MarketingReport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\MarketingMonth;

use App\Models\MarketingBudget;
use Session;

class UploadController extends Controller
{
    

    public function index(Request $request){
       
    	$months = MarketingMonth::all();
      
    	return view('marketing.marketing_budget.index',compact('months'));
    }



      public function create(){
   
    	return view('marketing.marketing_budget.create');
    }


    public function store(Request $request){

    	if($request->hasFile('file')) {


    		$files = $request->file('file');
    		$folderpath = storage_path().'/uploads/marketing/';
           
            if (!\File::exists($folderpath))
            {
               
                \File::makeDirectory($folderpath, 0755, true);
            }

                $file_path = $files->move($folderpath,'Masterfile.xlsx'.date('mdY H_i_ss'));
    			$reply = MarketingBudget::import($file_path);
          
    			
                if($reply['status'] == 1) {
                    Session::flash('flash_message', 'File successfully uploaded.');
                    Session::flash('flash_class', 'alert-success');

                    return redirect()->route("marketing_budget.index");

                }
                else {

                    Session::flash('flash_message', 'Error.');
                    Session::flash('flash_class', 'alert-danger');

                    return redirect()->route("marketing_budget.index");

                }	
    		   	

    		
    			

    	}
        else {

            dd("walang file");
        }

    }

    public function show($id) {

    	$month   = MarketingMonth::find($id);
    	$budgets = MarketingBudget::where('marketing_month_id' , $id)->get(); 

    	return view('marketing.marketing_budget.show',compact('month' , 'budgets'));
    }

}
