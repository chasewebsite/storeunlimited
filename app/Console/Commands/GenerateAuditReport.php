<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\UserBranch;
use App\Models\CompanyBranch;
use App\Models\IncomingItem;
use App\Models\OutgoingItem;
use App\Models\ItemStock;
use Alchemy\Zippy\Zippy;
use File;

class GenerateAuditReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:audit_report {folder_path} {branch} {date_from} {date_to}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $folder_path = $this->argument('folder_path');
        $_branch     = $this->argument('branch');
        $date_from = $this->argument('date_from');
        $date_to   = $this->argument('date_to');

        $branch         = CompanyBranch::where('branch_code',$_branch)->first();
        $stock_in       = IncomingItem::transfer_in( $branch,$date_from,$date_to );
        $stock_out      = OutgoingItem::transfer_out( $branch,$date_from,$date_to );
        $stock_summary  = ItemStock::getStockMovementSummary( $branch,$date_from,$date_to );
        $stock_detailed = ItemStock::getStockMovementDetailed( $branch,$date_from,$date_to );

        $stock_in_filename = "STOCK TRANSFER IN REGISTER_".$branch->branch;

        $stock_out_filename = "STOCK TRANSFER OUT REGISTER_".$branch->branch;

        $stock_summary_filename = "STOCK SUMMARY_".$branch->branch;
        $stock_detailed_filename = "STOCK DETAILED_".$branch->branch;


        
        $folders = [];


        

        \Excel::create($stock_in_filename, function($excel)  use ($stock_in,$branch,$date_from,$date_to) {
                      
                        
                 $excel->sheet("STOCK TRANSFER IN REGISTER", function($sheet) use($stock_in,$branch,$date_from,$date_to) {


                            $sheet->row(1, array($branch->company->company));
                            $sheet->row(2, array("STOCK TRANSFER IN REGISTER"));
                            $sheet->row(3, array("FROM ".$date_from." "."TO ".$date_to));
                            $sheet->row(6,array("DATE","DOC #","CATEGORY","MODEL/DESCRIPTION","STOCK CODE","FROM","TO","UOM","QTY","UNIT PRICE","AMOUNT"));

                            $cnt = 7;

                            foreach ($stock_in as $stock) {
                                    $sheet->row($cnt,array(
                                        $stock->local_date,
                                        " ",
                                        $stock->category,
                                        $stock->description,
                                        $stock->itemcode,
                                        $stock->from_branch,
                                        $stock->branch_name,
                                        "pcs",
                                        $stock->qty,
                                        $stock->price,
                                        $stock->amount,

                                    ));

                                    $cnt++;
                            }
                           
                           

                        });
                       

                          

        })->store('xlsx',$folder_path);


        \Excel::create($stock_out_filename, function($excel)  use ($stock_out,$branch,$date_from,$date_to) {
                      
                        
                $excel->sheet("STOCK TRANSFER OUT REGISTER", function($sheet) use($stock_out,$branch,$date_from,$date_to) {
                            $sheet->row(1, array($branch->company->company));
                            $sheet->row(2, array("STOCK TRANSFER OUT REGISTER"));
                            $sheet->row(3, array("FROM ".$date_from." "."TO ".$date_to));
                            $sheet->row(6,array("DATE","DOC #","CATEGORY","MODEL/DESCRIPTION","STOCK CODE","FROM","TO","UOM","QTY","UNIT PRICE","AMOUNT"));

                            $cnt = 7;

                            foreach ($stock_out as $stock) {
                                

                                    $sheet->row($cnt,array(
                                        $stock->local_date,
                                        " ",
                                        $stock->category,
                                        $stock->description,
                                        $stock->itemcode,
                                        $stock->branch_name,
                                        $stock->to_branch,
                                        "pcs",
                                        $stock->qty,
                                        $stock->price,
                                        $stock->amount,

                                    ));

                                    $cnt++;
                            }

                        });

                          

        })->store('xlsx',$folder_path);


         \Excel::create($stock_detailed_filename, function($excel)  use ($stock_detailed,$branch,$date_from,$date_to) {
                      
                        
                $excel->sheet("DETAILED STOCK MOVEMENT REPORT ", function($sheet) use($stock_detailed,$branch,$date_from,$date_to) {


                            $sheet->row(1, array($branch->company->company));
                            $sheet->row(2, array("STOCK MOVEMENT"));
                            $sheet->row(3, array("FROM ".$date_from." "."TO ".$date_to));
                            $sheet->row(6, array("DATE","CATEGORY","DESCRIPTION","STOCK CODE","BEGINNING","ENDING"));
                            if(count($stock_detailed) > 0) {

                                $cnt = 7;
                               
                                foreach ($stock_detailed as $item) {
                                        $sheet->row($cnt,array(
                                            $item->transaction_date,
                                            $item->category,
                                            $item->description,
                                            $item->itemcode,
                                            $item->begining,
                                            $item->ending
                                           

                                        ));
                                       

                                        $cnt++;
                                }
                            

                            }
                           
                           

                        });

                          

        })->store('xlsx',$folder_path);

          \Excel::create($stock_summary_filename, function($excel)  use ($stock_summary,$branch,$date_from,$date_to) {
                      
                        
              $excel->sheet("SUMMARY OF STOCK MOVEMENT -", function($sheet) use($stock_summary,$branch,$date_from,$date_to) {


                            $sheet->row(1, array($branch->company->company));
                            $sheet->row(2, array("STOCK MOVEMENT"));
                            $sheet->row(3, array("FROM ".$date_from." TO ".$date_to));
                            $sheet->row(6, array("CATEGORY","DESCRIPTION","STOCK CODE","BEGINNING","ENDING"));
                            if(count($stock_summary) > 0) {

                                   $cnt = 7;
                                foreach ($stock_summary as $item) {

                                    if(!empty($item->category) && !empty($item->description) && !empty($item->itemcode)) {
                                        $sheet->row($cnt,array(
                                           
                                            $item->category,
                                            $item->description,
                                            $item->itemcode,
                                            $item->begining,
                                            $item->ending
                                           

                                        ));
                                       

                                        $cnt++;
                                    }
                                }
                            

                            }
                           
                           

                        });
                          

        })->store('xlsx',$folder_path);

        $zippy = Zippy::load();
        $files = File::allFiles($folder_path);
        foreach ($files as $file) {
             $folders[$file->getFilename()] = $folder_path.'/'.$file->getFilename();
        }
         $zip_path = $folder_path.'/AuditInventoryReport.zip';
         File::delete($zip_path);
         $archive = $zippy->create($zip_path,$folders,true);
         



    }
}
