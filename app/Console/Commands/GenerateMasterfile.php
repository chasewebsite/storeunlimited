<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Item;
use File;

class GenerateMasterfile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:masterfile {company_id} {pos_type} {branch_terminal} {timestamps}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $company_id = $this->argument('company_id');
        $pos_type = $this->argument('pos_type');
        $branch_terminal = $this->argument('branch_terminal');
        $timestamps = $this->argument('timestamps');

        if(is_null($timestamps)){
            $timestamps = time();
        }
        $data['date'] = date('Y-m-d H:i:s',$timestamps);
        $data['company_id'] = $company_id;
        $data['pos_type_id'] = $pos_type;

        $filename = storage_path().'/download/'.$company_id.$branch_terminal.$pos_type.'.txt';
        File::delete($filename);
        $take = 1000; // adjust this however you choose
        $skip = 0; // used to skip over the ones you've already processed
        $cnt = 1;
        while($rows = Item::getPartial($data,$take,$skip))
        {
            if(count($rows) == 0){
                break;
            }
            $skip ++;
            $plunck_data = [];
            foreach($rows as $key => $row)
            {
                $attrs = $row->getAttributes();
                $row_data = [];
                foreach ($attrs as $key => $value) {
                    $row_data[] = $value;
                }

                $content = implode("|", $row_data);
                $content .= PHP_EOL;
                $bytesWritten = File::append($filename, $content);
                if ($bytesWritten === false)
                {
                    die("Couldn't write to the file.");
                }
            }
        }
    }
}
