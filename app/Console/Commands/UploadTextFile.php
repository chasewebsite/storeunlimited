<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

use App\Models\SaleSummary;
use App\Models\SaleDetail;
use App\Models\PostVoid;
use App\Models\ItemStock;
use App\Models\CardDetail;
use App\Models\OtherPayment;
use App\Models\AtmDetail;
use App\Models\CorporateDetail;
use App\Models\CheckDetail;
use App\Models\DeferredDetail;
use App\Models\GiftCard;
use App\Models\PatientHistory;
use App\Models\TextFile;

use DB;

class UploadTextFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upload:textfile';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        set_time_limit(0);
        $textfiles = TextFile::where('type',"SALES")->get();

        foreach ($textfiles as $file) {
            $filePath = $file->filename;

            DB::beginTransaction();
            echo "<br />";
            print_r($filePath);
            try{
                DB::commit();
                $reader = ReaderFactory::create(Type::CSV); // for XLSX files
                $reader->setFieldDelimiter('|');
                $reader->open($filePath);
                foreach ($reader->getSheetIterator() as $sheet) {
                    $last_id = 0;
                    $transaction_no = 0;
                    $sale  = " ";
                    foreach ($sheet->getRowIterator() as $row) {
                        if($row[0] == 'H' || $row[0] == 'D') {
                            $company_code = str_pad($row[1], 4, "0", STR_PAD_LEFT);
                            $branch_code = $company_code.str_pad($row[3], 4, "0", STR_PAD_LEFT);
                            $terminal_code = $branch_code.str_pad($row[5], 2, "0", STR_PAD_LEFT);
                        }
                        if($row[0] == 'H'){
                        // dd($row);
                        // if(count($row) == 28){
                            $data['company_code'] = $company_code;
                            $data['branch_code'] = $branch_code;
                            $data['terminal_code'] = $terminal_code;
                            $data['transaction_no'] = $row[6];
                            $sale = SaleSummary::where('transaction_no',$data['transaction_no'])->first();
                            $invoice_no = " ";
                            $discount_name =  " ";
                            $ref_no = " ";
                           
                            if(!empty($row[26])) {

                                $discount_name =  $row[26];

                            }

                            if(!empty($row[27])) {

                                $invoice_no = $row[27];
                                
                            }
                            if(!empty($row[28])) {

                                $ref_no = $row[28];
                                
                            }

                            if(count($sale) == 1) {
                                $last_id = $sale->id;
                                
                                $sale->invoice_no =  $invoice_no;
                                $sale->discount_name = $discount_name;
                                $sale->ref_no   = $ref_no;
                                $sale->update();
                                
                            }

                            if(!SaleSummary::entryExist($data)){

                                $sale = SaleSummary::firstOrCreate([
                                    'company_code' =>  $company_code,
                                    'company_name' => $row[2],
                                    'branch_code' => $branch_code,
                                    'branch_name' => $row[4],
                                    'terminal_code' => $terminal_code,
                                    'terminal_no' => $row[5],
                                    'transaction_no' => $data['transaction_no'],
                                    'user' => $row[7],
                                    'member' => $row[8],
                                    'gross_amount' => $row[9],
                                    'net_amount' => $row[10],
                                    'sub_total_discount' => $row[11],
                                    'total_item_discount' => $row[12],
                                    'cash_amount' => $row[13],
                                    'card_amount' => $row[14],
                                    'gift_amount' => $row[15],
                                    'charge_amount' => $row[16],
                                    'check_amount' => $row[17],
                                    'account_amount' => $row[18],
                                    'atm_amount' => $row[19],
                                    'deffered_amount' => $row[20],
                                    'other_payment' => $row[21],
                                    'local_time' => date('Y-m-d H:i:s', strtotime($row[22].' '.$row[23])),
                                    'sales_man' => $row[24],
                                    'retex_amount' => $row[25],
                                    'discount_name' =>$discount_name,
                                    'invoice_no'  => $invoice_no,
                                    'ref_no' => $ref_no]);
                                $last_id = $sale->id;
                                $transaction_no = $row[6];
                                $new = true;

                            }else{

                                $new = false;
                            }
                        }

                        if($new) {
                            if($row[0] == 'D'){
                                $data['sale_summary_id'] = $last_id;
                                
                                $data['company_code'] = $company_code;
                                $data['company_name'] = $row[2];
                                $data['branch_code'] = $branch_code;
                                $data['branch_name'] = $row[4];
                                $data['ctr'] = $row[5];
                                $data['barcode'] = $row[6];
                                $data['itemcode'] = $row[7];
                                $data['description'] = $row[8];
                                $data['cost'] = $row[9];
                                $data['srp'] = $row[10];
                                $data['qty'] = $row[11];
                                $data['gross_amount'] = $row[12];
                                $data['net_amount'] = $row[13];
                                $data['discount'] = $row[14];
                                $data['series_name'] = $row[15];
                                $data['composition'] = $row[16];
                                $data['brand'] = $row[17];
                                $data['abrev_desc'] = $row[18];
                                if(count($row) > 22){
                                    if(!empty($row[23])){
                                        $data['shirt_fit'] = $row[23];
                                    }else{
                                        $data['shirt_fit'] = '';    
                                    }
                                }else{
                                    $data['shirt_fit'] = '';
                                }
                                if(count($row) > 23){
                                    if(!empty($row[24])){
                                        $data['collar_type'] = $row[24];    
                                    }else{
                                        $data['collar_type'] = '';    
                                    }
                                }else{
                                    $data['collar_type'] = '';
                                }
                                if(count($row) > 24){
                                    if(!empty($row[25])){
                                        $data['color'] = $row[25];
                                    }else{
                                        $data['color'] = '';    
                                    }
                                }else{
                                    $data['color'] = '';
                                }
                                if(count($row) > 25){
                                    if(!empty($row[26])){
                                        $data['woven_style'] = $row[26];
                                    }else{
                                        $data['woven_style'] = '';    
                                    }
                                }else{
                                    $data['woven_style'] = '';
                                }
                                if(count($row) > 26){
                                    if(!empty($row[27])){
                                        $data['pattern_style'] = $row[27];
                                    }else{
                                        $data['pattern_style'] = '';    
                                    }
                                }else{
                                    $data['pattern_style'] = '';
                                }
                                if(count($row) > 27){
                                    if(!empty($row[28])){
                                        $data['sleeve_length'] = $row[28];
                                    }else{
                                        $data['sleeve_length'] = '';    
                                    }
                                }else{
                                    $data['sleeve_length'] = '';
                                }
                                if(count($row) > 28){
                                    if(!empty($row[29])){
                                        $data['neck_size'] = $row[29];
                                    }else{
                                        $data['neck_size'] = '';    
                                    }
                                }else{
                                    $data['neck_size'] = '';
                                }

                                
                                $data['movement_description'] = 'Sales Entry';
                                $data['move_ref'] = $transaction_no;  
                                $data['purpose'] = 'Sales Entry';

                                $detail = SaleDetail::detailsExist($data);
                                if(empty($detail)){
                                    SaleDetail::create([
                                        'sale_summary_id' => $data['sale_summary_id'],
                                        'ctr' => $data['ctr'],
                                        'barcode' => $data['barcode'],
                                        'itemcode' => $data['itemcode'],
                                        'description' => $data['description'],
                                        'cost' => $data['cost'],
                                        'srp' => $data['srp'],
                                        'qty' => $data['qty'],
                                        'gross_amount' => $data['gross_amount'],
                                        'net_amount' => $data['net_amount'],
                                        'discount' => $data['discount'],
                                        'series_name' => $data['series_name'],
                                        'composition' => $data['composition'],
                                        'shirt_fit' => $data['shirt_fit'],
                                        'collar_type' => $data['collar_type'],
                                        'color' => $data['color'],
                                        'woven_style' => $data['woven_style'],
                                        'pattern_style' => $data['pattern_style'],
                                        'sleeve_length' => $data['sleeve_length'],
                                        'neck_size' => $data['neck_size'],
                                        'brand' => $data['brand'],
                                    ]);
                                    
                                    ItemStock::removeStocks($data);
                                }                      
                            }//for sales details
                        }

                        if($row[0] == 'CARD') {

                            $card = CardDetail::where('sale_summary_id',$last_id)->where('card_name',$row[1])->where('card_no',$row[2])->where('amount',$row[5])->first();

                            if(count($card) > 0 ) {

                                $card->exp_date = $row[3];
                                $card->bank_name = $row[4];
                                $card->bank   =  substr($row[4], 0, strpos($row[4], '-'));
                                $card->approve_no = $row[6];
                                $card->update();

                            }
                            else {

                                CardDetail::firstOrCreate([
                                    'sale_summary_id' => $last_id,
                                    'card_name'      => $row[1],
                                    'card_no'        => $row[2],
                                    'exp_date'       => $row[3],
                                    'bank_name'      => $row[4],
                                    'bank'           => substr($row[4], 0, strpos($row[4], '-')),
                                    'amount'         => $row[5],
                                    'approve_no'     => $row[6]
                                ]);
                            }
                        }//for card
                        if($row[0] == 'OTHER') {

                            OtherPayment::firstOrCreate([
                                'sale_summary_id' => $last_id,
                                'document_no'      => $row[1],
                                'amount'           => $row[2]
                            ]);
                                                        
                        }//for other

                        if($row[0] == 'DEFERRED') {

                            $deferred = DeferredDetail::where('sale_summary_id',$last_id)->where('card_name',$row[1])->where('card_no',$row[2])->where('amount',$row[5])->first();


                            if(count($deferred) > 0 ) {

                                $deferred->exp_date = $row[3];
                                $deferred->bank_name = $row[4];
                                $deferred->bank   =  substr($row[4], 0, strpos($row[4], '-'));
                                $deferred->months = $row[6];
                                $deferred->approve_no = $row[7];
                                $deferred->update();

                            }

                            else {

                                DeferredDetail::firstOrCreate([
                                    'sale_summary_id' => $last_id,
                                    'card_name'      => $row[1],
                                    'card_no'        => $row[2],
                                    'exp_date'       => $row[3],
                                    'bank_name'      => $row[4],
                                     'bank'           => substr($row[4], 0, strpos($row[4], '-')),
                                    'amount'         => $row[5],
                                    'months'         => $row[6],
                                    'approve_no'     => $row[7]
                                ]);
                            }
                        } // for deferred

                        if($row[0] == 'ATM') {

                            $atm = AtmDetail::where('sale_summary_id',$last_id)->where('card_name',$row[1])->where('card_number',$row[2])->where('amount',$row[4])->first();

                            if(count($atm) > 0 ) {
                              
                                $atm->bank_name = $row[3];
                                $atm->bank   =  substr($row[3], 0, strpos($row[3], '-'));
                               
                                $atm->update();

                            }
                            else {

                                AtmDetail::firstOrCreate([
                                    'sale_summary_id' => $last_id,
                                    'card_name'           => $row[1],
                                    'card_number'         => $row[2],
                                    'bank_name'           => $row[3],
                                     'bank'           => substr($row[3], 0, strpos($row[3], '-')),
                                    'amount'              => $row[4]
                                ]);
                            }                          
                        } //for ATM

                        if($row[0] == 'ACCOUNT') {

                            CorporateDetail::firstOrCreate([
                                'sale_summary_id'    => $last_id,
                                'account_no'         => $row[1],
                                'amount'             => $row[2]
                            ]);
                                                             
                        }//for account

                        if($row[0] == 'CHECK') {

                            $check = CheckDetail::where('sale_summary_id',$last_id)->where('check_name',$row[1])->where('check_number',$row[2])->where('amount',$row[5])->first();
                            
                            if(count($check) > 0 ) {

                                $check->bank_name = $row[4];
                                $check->bank   =  substr($row[4], 0, strpos($row[4], '-'));
                               
                                $check->update();

                            }
                            else {

                                CheckDetail::firstOrCreate([
                                    'sale_summary_id'   => $last_id,
                                    'check_name'        => $row[1],
                                    'check_number'      => $row[2],
                                    'check_date'        => $row[3],
                                    'bank_name'         => $row[4],
                                     'bank'           => substr($row[4], 0, strpos($row[4], '-')),
                                    'amount'            => $row[5]
                                ]);

                            }
                        }//for check

                        if($row[0] == 'GIFT') {
                                
                            $data['company_code'] = $sale->company_code;
                            $data['company_name'] = $sale->company_name;
                            $data['branch_code'] = $sale->branch_code;
                            $data['branch_name'] = $sale->branch_name;
                            $data['terminal_code'] = $sale->terminal_code;
                            $data['terminal_no'] = $sale->terminal_no;
                            $data['user'] = $sale->user;
                            $data['member'] = $sale->member;
                            $data['transaction_no'] = $sale->transaction_no;
                            $data['local_time'] = $sale->local_time;
                            $data['serial_no'] = $row[2];
                            $data['corporate'] = 0;
                            $data['account_name'] = " ";
                            $data['approve_no'] = " ";

                            if($row[4] == 'F' && !empty($row[4])) {
                                $data['corporate']  = 1;
                            }
                            if(!empty($row[6])){
                                $data['account_name'] = $row[6];
                            }
                            if(!empty($row[7])) {
                                $data['approve_no'] = $row[7];
                            }
                            // \Log::info($data['account_name'].' '.$data['approve_no']);

                            $giftcard = GiftCard::recordExist($data);
                            if(!empty($giftcard)){
                                $giftcard->sale_summary_id = $last_id;
                                $giftcard->company_code = $data['company_code'];
                                $giftcard->gift_name = $row[1];
                                $giftcard->branch_code = $data['branch_code'];
                                $giftcard->branch_name = $data['branch_name'];
                                $giftcard->terminal_code = $data['terminal_code'];
                                $giftcard->terminal_no = $data['terminal_no'];
                                $giftcard->user = $data['user'];
                                $giftcard->member = $data['member'];
                                $giftcard->transaction_no = $data['transaction_no'];
                                $giftcard->local_time = $data['local_time'];
                                $giftcard->amount = $row[3];
                                $giftcard->account_name = $data['account_name'];
                                $giftcard->approved_no  = $data['approve_no'];
                                $giftcard->corporate  = $data['corporate'];
                                $giftcard->serial_no = $data['serial_no'];
                                $giftcard->update();
                            }
                            else {

                                GiftCard::firstOrCreate([
                                    'sale_summary_id'    => $last_id,
                                    'gift_name'            => $row[1],
                                    'branch_code'          => $data['branch_code'],
                                    'branch_name'          => $data['branch_name'],
                                    'terminal_code'        => $data['terminal_code'],
                                    'terminal_no'          => $data['terminal_no'],
                                     'user'                => $data['member'],
                                    'transaction_no'       => $data['transaction_no'],
                                    'local_time'           => $data['local_time'],
                                    'amount'               => $row[3],
                                    'account_name'         => $data['account_name'],
                                    'approved_no'           => $data['approve_no'],
                                    'corporate'            => $data['corporate'],
                                    'serial_no'            => $data['serial_no'],
                                    'member' => $data['member'],
                                    'company_code' => $data['company_code']
                                ]);

                            }

                        }//end
                    }
                }
                $reader->close();
                DB::commit();
                
                $file->delete();
            }catch (Exception $e){

            }
        }
    }
}
