<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\SaleSummary;
use Illuminate\Http\Request;

class SendDailySales extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Daily Sales';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Request $request)
    {
        //

    $datas = SaleSummary::getDailyandMonthlySales($request);
    // dd($datas);die;
    // \Mail::send('email.daily_sales',$data = [
    //     'datas' => $datas], function ($message) use($data){
    //     $message->from('admin@chasetech.com', 'Daily Sales Report');
    //     $message->to("dtio@chasetech.com")->subject('Sales Report');
    //     });
    }
}
