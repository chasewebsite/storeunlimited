<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

use App\Models\Company;
use App\Models\Item;
use App\Models\PosType;
use App\Models\CompanyPosType;
use App\Models\ItemGroup;
use App\Models\ItemDivision;
use App\Models\ItemDept;
use App\Models\ItemCategory;    
use App\Models\ItemSubcategory;
use App\Models\ItemBrand;
use App\Models\ItemSupplier;
use App\Models\ItemUnit;
use App\Models\ItemColor;
use App\Models\ItemSize;
use App\Models\ItemArea;
use App\Models\ItemDept2;
use App\Models\ItemCategory2;
use App\Models\ItemType;

use File;

class UpdateMasterfile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:itemmasterfile';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Item Masterfile';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Updating Item Masterfile');
        $timeFirst  = strtotime(date('Y-m-d H:i:s'));



        $filePath = storage_path().'/uploads/item/items.txt';
       
        DB::beginTransaction();
        try {
            $reader = ReaderFactory::create(Type::CSV);
            $reader->setFieldDelimiter('|');
            $reader->open($filePath);
            foreach ($reader->getSheetIterator() as $sheet) {
                $cnt = 1;
                foreach ($sheet->getRowIterator() as $row) {
                    // dd($row);
                    $company = Company::find($row[0]);
                    if(!empty($company)){
                        $pos_type = PosType::find($row[2]);

                        $company_pos_type = CompanyPosType::firstOrCreate(['company_id' => $company->id, 'pos_type_id' => $pos_type->id]);
                        $item_group = ItemGroup::firstOrCreate(['company_id' => $company->id, 'pos_type_id' => $pos_type->id, 
                            'item_group_code' => $row[7],
                            'item_group' => $row[8]]);
                        $item_divison = ItemDivision::firstOrCreate(['company_id' => $company->id, 'pos_type_id' => $pos_type->id, 
                            'item_division_code' => $row[9],
                            'item_division' => $row[10]]);
                        $item_dept = ItemDept::firstOrCreate(['company_id' => $company->id, 'pos_type_id' => $pos_type->id, 
                            'item_dept_code' => $row[11],
                            'item_dept' => $row[12]]);
                        $item_category = ItemCategory::firstOrCreate(['company_id' => $company->id, 'pos_type_id' => $pos_type->id, 
                            'item_category_code' => $row[13],
                            'item_category' => $row[14]]);
                        $item_subcategory = ItemSubcategory::firstOrCreate(['company_id' => $company->id, 'pos_type_id' => $pos_type->id, 
                            'item_subcategory_code' => $row[15],
                            'item_subcategory' => $row[16]]);
                        $item_brand = ItemBrand::firstOrCreate(['company_id' => $company->id, 'pos_type_id' => $pos_type->id, 
                            'item_brand_code' => $row[17],
                            'item_brand' => $row[18]]);
                        $item_supplier = ItemSupplier::firstOrCreate(['company_id' => $company->id, 'pos_type_id' => $pos_type->id, 
                            'item_supplier_code' => $row[19],
                            'item_supplier' => $row[20]]);
                        $item_unit = ItemUnit::firstOrCreate(['company_id' => $company->id, 'pos_type_id' => $pos_type->id, 
                            'item_unit_code' => $row[21],
                            'item_unit' => $row[22]]);
                        $item_color = ItemColor::firstOrCreate(['company_id' => $company->id, 'pos_type_id' => $pos_type->id, 
                            'item_color_code' => $row[23],
                            'item_color' => $row[24]]);
                        $item_size = ItemSize::firstOrCreate(['company_id' => $company->id, 'pos_type_id' => $pos_type->id, 
                            'item_size_code' => $row[25],
                            'item_size' => $row[26]]);
                        $item_area = ItemArea::firstOrCreate(['company_id' => $company->id, 'pos_type_id' => $pos_type->id, 
                            'item_area_code' => $row[27],
                            'item_area' => $row[28]]);
                        $item_dept2 = ItemDept2::firstOrCreate(['company_id' => $company->id, 'pos_type_id' => $pos_type->id, 
                            'item_dept2_code' => $row[29],
                            'item_dept2' => $row[30]]);
                        $item_category2 = ItemCategory2::firstOrCreate(['company_id' => $company->id, 'pos_type_id' => $pos_type->id, 
                            'item_category2_code' => $row[31],
                            'item_category2' => $row[32]]);
                        $item_type = ItemType::firstOrCreate(['company_id' => $company->id, 'pos_type_id' => $pos_type->id, 
                            'item_type_code' => $row[82],
                            'item_type' => $row[83]]);

                        $item_sell_unit = ItemUnit::firstOrCreate(['company_id' => $company->id, 'pos_type_id' => $pos_type->id, 
                            'item_unit_code' => $row[121],
                            'item_unit' => $row[122]]);

                        Item::firstOrCreate([
                            'company_id' => $company->id,
                            'company_pos_type_id' => $company_pos_type->id,
                            'barcode' => $row[3],
                            'itemcode' => $row[4],
                            'desc' => $row[5],
                            'posdesc' => $row[6],
                            'item_group_id' => $item_group->id,
                            'item_division_id' => $item_divison->id,
                            'item_dept_id' => $item_dept->id,
                            'item_category_id' => $item_category->id,
                            'item_subcategory_id' => $item_subcategory->id,
                            'item_brand_id' => $item_brand->id,
                            'item_supplier_id' => $item_supplier->id,
                            'item_unit_id' => $item_unit->id,
                            'item_color_id' => $item_color->id,
                            'item_size_id' => $item_size->id,
                            'item_area_id' => $item_area->id,
                            'item_dept2_id' => $item_dept2->id,
                            'item_category2_id' => $item_category2->id,
                            'costp' => $row[33],
                            'markup' => $row[34],
                            'price' => $row[35],
                            'price2' => $row[36],
                            'price3' => $row[37],
                            'price1' => $row[38],
                            'discountp' => $row[39],
                            'discamt' => $row[40],
                            'sdate' => $row[41],
                            'stime' => $row[42],
                            'edate' => $row[43],
                            'etime' => $row[44],
                            'opend' => $row[45],
                            'level1' => $row[46],
                            'level2' => $row[47],
                            'level3' => $row[48],
                            'level4' => $row[49],
                            'level5' => $row[50],
                            'levelp1' => $row[51],
                            'levelp2' => $row[52],
                            'levelp3' => $row[53],
                            'levelp4' => $row[54],
                            'levelp5' => $row[55],
                            'perishable' => $row[56],
                            'status' => $row[57],
                            'buyn' => $row[58],
                            'takeitem' => $row[59],
                            'takeqty' => $row[60],
                            'deffect' => $row[61],
                            'min' => $row[62],
                            'max' => $row[63],
                            'lastprice' => $row[64],
                            'lastcost' => $row[65],
                            'lastuser' => $row[66],
                            'compose' => $row[67],
                            'lonsale' => $row[68],
                            'bartype' => $row[69],
                            'prodid' => $row[70],
                            'cubic' => $row[71],
                            'weight' => $row[72],
                            'active' => $row[73],
                            'lsurcharge' => $row[74],
                            'surchperc' => $row[75],
                            'discl1' => $row[76],
                            'discl2' => $row[77],
                            'discl3' => $row[78],
                            'discl4' => $row[79],
                            'discl5' => $row[80],
                            'button' => $row[81],
                            'item_type_id' => $item_type->id,
                            'printer' => $row[84],
                            'combo' => $row[85],
                            'backcolor' => $row[86],
                            'forecolor' => $row[87],
                            'rawitem' => $row[88],
                            'computed' => $row[89],
                            'bracket1' => $row[90],
                            'bracket2' => $row[91],
                            'bracket3' => $row[92],
                            'bracket4' => $row[93],
                            'bracket5' => $row[94],
                            'laskserial' => $row[95],
                            'lnonvat' => $row[96],
                            'dispimage' => $row[97],
                            'vat' => $row[98],
                            'nosurch' => $row[99],
                            'refundable' => $row[100],
                            'nogross' => $row[101],
                            'nodeposit' => $row[102],
                            'inzread' => $row[103],
                            'shelflife' => $row[104],
                            'bom' => $row[105],
                            'eventcode' => $row[106],
                            'proctime' => $row[107],
                            'indented' => $row[108],
                            'kdsdelay' => $row[109],
                            'dailypcnt' => $row[110],
                            'linactive' => $row[111],
                            'pcntprior' => $row[112],
                            'laskmod' => $row[113],
                            'lcutline' => $row[114],
                            'printer1' => $row[115],
                            'printer2' => $row[116],
                            'printer3' => $row[117],
                            'lexempt' => $row[118],
                            'stockalert' => $row[119],
                            'wastage' => $row[120],
                            'sellunit_id' => $item_sell_unit->id,
                            'convqty' => $row[123],
                            'lnopsum' => $row[124],
                            'lnolt' => $row[125],
                            'button_i' => $row[126],
                            'fg100' => $row[127],
                            'progstock' => $row[128],
                            'consumable' => $row[129],
                            'lmobile' => $row[130],
                            'menuprice' => $row[131],
                            'scperc' => $row[132],
                            'rental' => $row[133],
                            'renthrs' => $row[134]]
                            );
                    }   

                    $this->line('Row # '.$cnt);
                    $cnt++;
                }
            }
            $reader->close();
            DB::commit();

            $timeSecond = strtotime(date('Y-m-d H:i:s'));
            $differenceInSeconds = $timeSecond - $timeFirst;
            $this->info('Time used ' . $differenceInSeconds . " sec");

        } catch (Exception $e) {
            DB::rollback();
            $this->info('Error occure while updating item masterfile');
        }

    }
}
