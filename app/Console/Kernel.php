<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\UpdateMasterfile::class,
        Commands\GenerateMasterfile::class,
        Commands\GenerateItemStockHistory::class,
        Commands\GenerateAuditReport::class,
        Commands\SendDailySales::class,
        \App\Console\Commands\SendDailySales::class,
        \App\Console\Commands\InOutUpdate::class,
        \App\Console\Commands\UploadItemStock::class,
        \App\Console\Commands\UploadTextFile::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('update:inout')->hourly();
        $schedule->command('upload:textfile');
        // $schedule->command('send:daily')->dailyAt('23:00');
    }
}
