<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use stdClass;
use Auth;
use App\Models\SaleDetail;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\UserBranch;
use App\Models\SaleSummary;
use App\Models\Company;

class SaleSummary extends Model
{
	protected $fillable = ['company_code',
        'company_name', 'branch_code', 'branch_name', 'terminal_code', 'terminal_no', 'transaction_no', 'user', 'member', 
        'gross_amount', 'net_amount', 'sub_total_discount', 'total_item_discount', 'retex_amount',
        'cash_amount', 'card_amount', 'gift_amount', 'charge_amount', 'check_amount', 'account_amount',
        'atm_amount', 'deffered_amount', 'other_payment', 'local_time', 'post_void', 'sales_man' ,'invoice_no','discount_name','ref_no'];

    public function details()
    {
    	return $this->hasMany('App\Models\SaleDetail');
    }

    public static function getIds($date_from, $date_to, $branch)
    {
        return self::whereBetween('local_time',[$date_from, $date_to])
            ->where('branch_code',$branch)->get();
    }
    public static function getTransaction($data){
        return self::where('company_code', $data['company_code'])
            ->where('branch_code', $data['branch_code'])
            ->where('terminal_code', $data['terminal_code'])
            ->where('transaction_no', $data['transaction_no'])
            ->first();
    }

    public static function entryExist($data){
        $records = self::where('company_code', $data['company_code'])
            ->where('branch_code', $data['branch_code'])
            ->where('terminal_code', $data['terminal_code'])
            ->where('transaction_no', $data['transaction_no'])
            ->get();
        if(count($records) > 0){
            return true;
        }else{
            return false;;
        }

    }
    public static function getCompanies(){
        $companies = UserBranch::getAllowedCompany(Auth::user()->id);

        return self::select('company_name', 'company_code')
            ->whereIn('company_code', $companies)
            ->orderBy('company_name')
            ->groupBy('company_code')
            ->lists('company_name', 'company_code');
    }

    public static function getCompaniesBranch($request){
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
        
        $company = Company::findByCode($request->companies);

        $abranches = CompanyBranch::getThemBranch($branches,$company);

        return $abranches;
        // return self::select('branch_name', 'branch_code')
        //     ->whereIn('company_code', $request->companies)
        //     ->whereIn('branch_code', $branches)
        //     ->orderBy('branch_name')
        //     ->groupBy('company_code')
        //     ->groupBy('branch_code')
        //     ->lists('branch_name', 'branch_code');
    }

    public static function getBranchesTerminal($request){
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
        return self::select('terminal_no', 'terminal_code')
            ->whereIn('company_code', $request->companies)
            ->where(function($query) use ($request, $branches){
                if($request->has('branches')){
                    $query->whereIn('branch_code',$request->branches);
                }else{
                    $query->whereIn('branch_code',$branches);
                }
            })
            
            ->groupBy('company_code')
            ->groupBy('branch_code')
            ->groupBy('terminal_code')
            ->lists('terminal_no', 'terminal_code');
    }

    public static function getUsers(){
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
        return self::select('user')
            ->whereIn('branch_code', $branches)
            ->orderBy('user')
            ->groupBy('user')
            ->lists('user', 'user');
    }

    public static function getSalesMan(){
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
        return self::select(DB::raw("IF(sales_man IS NULL or sales_man = '', 'NO DATA', sales_man) as sales_man"))
            ->whereIn('branch_code', $branches)
            ->orderBy('sales_man')
            ->groupBy('sales_man')
            ->lists('sales_man', 'sales_man');
    }

    public static function getMembers(){
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
        return self::select(DB::raw("IF(member IS NULL or member = '', 'NO DATA', member) as member"))
            ->whereIn('branch_code', $branches)
            ->orderBy('member')
            ->groupBy('member')
            ->lists('member', 'member');
    }

    public static function search($date_from, $date_to, $request){
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
    	return self::where(function($query) use ($request){
            if(($request->has('company'))&& (!empty($request->company))){
                    $query->whereIn('company_code',$request->company);
                }
            })
            ->where(function($query) use ($request, $branches){
                if(($request->has('branch'))&& (!empty($request->branch))){
                    $query->whereIn('branch_code',$request->branch);
                }else{
                    $query->whereIn('branch_code',$branches);
                }
            })
            ->where(function($query) use ($request){
            if(($request->has('terminal'))&& (!empty($request->terminal))){
                    $query->whereIn('terminal_code',$request->terminal);
                }
            })
            ->where(function($query) use ($request){
            if(($request->has('user'))&& (!empty($request->user))){
                    $query->whereIn('user',$request->user);
                }
            })
            ->where(function($query) use ($request){
            if(($request->has('saleman'))&& (!empty($request->saleman))){
                    $query->whereIn('sales_man',$request->saleman);
                }
            })
            ->where(function($query) use ($request){
            if(($request->has('member'))&& (!empty($request->member))){
                    $query->whereIn('member',$request->member);
                }
            })
            ->whereDate('local_time','>=',date("Y-m-d", strtotime($date_from)))
            ->whereDate('local_time','<=',date("Y-m-d", strtotime($date_to)))
            ->where('post_void',0)
            ->get();
    }

    public static function salessummary2($date_from, $date_to, $request){
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
        $companies = $request->company;

        if(($request->has('branch'))&& (!empty($request->branch))){
            $_branches = $request->branch;
        }else{
            $_branches = $branches;
        }
        $com  ='';
        if(!empty($companies)){
            $com = " and companies.company_code in ('".implode("','", $companies)."')";
        }
        if(!empty($request->keyword)) {
            $keyword = $request->keyword;
        }
        else{

            $keyword = " ";
        }
        $se_branches = implode("','", $_branches);
        $date_from = date("Y-m-d", strtotime($date_from));
        $date_to = date("Y-m-d", strtotime($date_to));
        $query = sprintf("select companies.company, companies.company_code,
            company_branches.branch, company_branches.branch_code,
            coalesce(sales.net_amount,0) as net_amount,
            coalesce(refund.refund_amount,0) as refund_amount,
            coalesce(return_exchanges.return_exchange_amount,0) as return_exchange_amount
            
            from company_branches
            join companies on companies.id = company_branches.company_id
            left join (
                select branch_code, sum(sale_summaries.net_amount) as net_amount
                from sale_summaries  
                where post_void = 0
                and DATE(local_time) BETWEEN '%s' AND '%s'
               
                group by branch_code
            ) sales on sales.branch_code = company_branches.branch_code
            left join (
                select branch_code, sum(amount) as refund_amount
                from refund_items
                where DATE(local_time) BETWEEN '%s' AND '%s'
                
                group by branch_code
            ) refund on refund.branch_code = company_branches.branch_code
            left join (
                select branch_code, sum(amount) as return_exchange_amount
                from return_exchange_items
                where DATE(local_time) BETWEEN '%s' AND '%s'
                
                group by branch_code
            ) return_exchanges on return_exchanges.branch_code = company_branches.branch_code
            where company_branches.branch_code in ('%s')
            %s
             
            order by companies.company, company_branches.branch",
        $date_from,$date_to,$date_from,$date_to,$date_from,$date_to, $se_branches,$com);
        
        return DB::select(DB::raw($query));
        // return $query;
    }

    public static function salessummary($date_from, $date_to, $request){
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
        $companies = $request->company;

        if(($request->has('branch'))&& (!empty($request->branch))){
            $_branches = $request->branch;
        }else{
            $_branches = $branches;
        }
        $com  ='';
        if(!empty($companies)){
            $com = " and companies.company_code in ('".implode("','", $companies)."')";
        }
        if(!empty($request->keyword)) {
            $keyword = $request->keyword;
        }
        else{

            $keyword = " ";
        }
        $se_branches = implode("','", $_branches);
        $date_from = date("Y-m-d", strtotime($date_from));
        $date_to = date("Y-m-d", strtotime($date_to));
        $query = sprintf("select companies.company, companies.company_code,
            company_branches.branch, company_branches.branch_code,
            coalesce(sales.net_amount,0) as net_amount,
            coalesce(refund.refund_amount,0) as refund_amount,
            coalesce(return_exchanges.return_exchange_amount,0) as return_exchange_amount
            
            from company_branches
            join companies on companies.id = company_branches.company_id
            left join (
                select branch_code, sum(sale_summaries.net_amount) as net_amount
                from sale_summaries  
                where post_void = 0
                and DATE(local_time) BETWEEN '%s' AND '%s'
               
                group by branch_code
            ) sales on sales.branch_code = company_branches.branch_code
            left join (
                select branch_code, sum(amount) as refund_amount
                from refund_items
                where DATE(local_time) BETWEEN '%s' AND '%s'
                
                group by branch_code
            ) refund on refund.branch_code = company_branches.branch_code
            left join (
                select branch_code, sum(amount) as return_exchange_amount
                from return_exchange_items
                where DATE(local_time) BETWEEN '%s' AND '%s'
                
                group by branch_code
            ) return_exchanges on return_exchanges.branch_code = company_branches.branch_code
            where company_branches.branch_code in ('%s')
            %s
             
            order by companies.company, company_branches.branch",
        $date_from,$date_to,$date_from,$date_to,$date_from,$date_to, $se_branches,$com);
        
        return DB::select(DB::raw($query));
        // return $query;
    }


     public static function salesSummarPerItem($date_from, $date_to, $request){
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
  

        if(($request->has('branches'))&& (!empty($request->branches))){
            $_branches = $request->branches;
        }else{
            $_branches = $branches;
        }
      
        

        $se_branches = implode("','", $_branches);
        $date_from = date("Y-m-d", strtotime($date_from));
        $date_to = date("Y-m-d", strtotime($date_to));
        $query = sprintf("select 
            company_branches.branch, company_branches.branch_code,sales.itemcode,
            coalesce(sales.net_amount,0) as net_amount,
            coalesce(refund.refund_amount,0) as refund_amount,
            coalesce(return_exchanges.return_exchange_amount,0) as return_exchange_amount,
            coalesce(sales.net_qty,0) as net_qty,
            coalesce(refund.refund_qty,0) as refund_qty,
            coalesce(return_exchanges.return_qty,0) as return_qty
          
            from company_branches
            left join (
                select branch_code, sum(sale_details.net_amount) as net_amount,sum(sale_details.qty) as net_qty ,sale_details.itemcode as itemcode
                from sale_summaries join sale_details on sale_details.sale_summary_id = sale_summaries.id
                where post_void = 0
                and DATE(local_time) BETWEEN '%s' AND '%s'
                and branch_code in ('%s')
                group by itemcode
            ) sales on sales.branch_code = company_branches.branch_code  
            left join (
                select branch_code, sum(amount) as refund_amount , sum(qty) as refund_qty , itemcode
                from refund_items
                where DATE(local_time) BETWEEN '%s' AND '%s'
                and branch_code in ('%s')
                group by itemcode
            ) refund on refund.itemcode = sales.itemcode
            left join (
                select branch_code, sum(amount) as return_exchange_amount , sum(qty) as return_qty ,itemcode
                from return_exchange_items
                where DATE(local_time) BETWEEN '%s' AND '%s'
                and branch_code in ('%s')
                group by itemcode
            ) return_exchanges on return_exchanges.itemcode = sales.itemcode
            where company_branches.branch_code in ('%s')
        
            order by sales.itemcode",
        $date_from,$date_to,$se_branches,$date_from,$date_to,$se_branches,$date_from,$date_to,$se_branches, $se_branches);
        
        return DB::select(DB::raw($query));
        
    }

    public static function salesSummarPerItem1($date_from, $date_to, $request){


        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
       

        if(($request->has('branches'))&& (!empty($request->branches))){
            $_branches = $request->branches;
        }else{
            $_branches = $branches;
        }

        $se_branches = implode("','", $_branches);
        $date_from = date("Y-m-d", strtotime($date_from));
        $date_to = date("Y-m-d", strtotime($date_to));


        $query = sprintf("select  itemcode ,sum(qty) as qty,
            sum(sale_details.net_amount) as net_amount ,company_branches.branch
            from sale_details

            join sale_summaries on sale_summaries.id = sale_details.sale_summary_id
            join company_branches on company_branches.branch_code = sale_summaries.branch_code
            where post_void = 0
            and DATE(local_time) BETWEEN '%s' AND '%s'
            and sale_summaries.branch_code in ('%s')
            group by barcode
            order by qty desc",
        $date_from,$date_to,$se_branches); 
        $items =  DB::select(DB::raw($query));


          $query_return = sprintf("select barcode, itemcode ,description, qty, amount
            from refund_items
            where DATE(local_time) BETWEEN '%s' AND '%s'
            and branch_code in ('%s')",
            $date_from,$date_to,$se_branches);

           $returneds = DB::select(DB::raw($query_return));
           foreach ($returneds as $retuned) {
            for($x = 0; $x <  count($items); $x++){
                if($retuned->itemcode == $items[$x]->itemcode){
                    $items[$x]->qty = $items[$x]->qty - $retuned->qty;
                    $items[$x]->net_amount = $items[$x]->net_amount - $retuned->amount;
                }
            }
        }



        $query_retex = sprintf("select barcode, itemcode ,description, qty, amount
            from return_exchange_items
            where DATE(local_time) BETWEEN '%s' AND '%s'
            and branch_code in ('%s')",
        $date_from,$date_to,$se_branches);

        $retexs = DB::select(DB::raw($query_retex));
        foreach ($retexs as $retex) {
            for($x = 0; $x <  count($items); $x++){
                if($retex->itemcode == $items[$x]->itemcode){
                    $items[$x]->qty = $items[$x]->qty - $retex->qty;
                    $items[$x]->net_amount = ($items[$x]->net_amount - $retex->amount) *1.12 ;
                }
            }
        }

        return $items;

    }









    public static function getBranchSales($date_from, $date_to, $branch_code){


            
        return self::where('branch_code', $branch_code)
            ->whereDate('local_time','>=',$date_from)
            ->whereDate('local_time','<=',$date_to)
            ->where('post_void',0)
            ->get();
    }

    public static function detailDailySales($date_from,$date_to,$branch_code){

            $date_from = date("Y-m-d", strtotime($date_from));
            $date_to = date("Y-m-d", strtotime($date_to));

            return self::select(DB::raw("
                                        IF(invoice_no IS NULL or invoice_no = '', 'NO DATA', invoice_no) as invoice_no,
                                         IF(ref_no IS NULL or ref_no = '', 'NO DATA', ref_no) as ref_no,
                                        IF(branch_code IS NULL or branch_code = '', 'NO DATA', branch_code) as branch_code,
                                        IF(DATE(local_time) IS NULL or DATE(local_time) = '', 'NO DATA', DATE(local_time)) as local_time,
                                        IF(member IS NULL or member = '', 'NO DATA', member) as customer_name,
                                        IF(discount_name IS NULL or discount_name = '', 'NO DATA', discount_name) as discount_name,
                                       COALESCE(net_amount,0) as net_amount,
                                       COALESCE ((cash_amount + check_amount),0) as cash_sales,
                                       COALESCE((atm_amount + card_amount + account_amount + deffered_amount + other_payment),0) as charge_sales
                                        "))
                        ->whereDate('local_time','>=',date("Y-m-d", strtotime($date_from)))
                        ->whereDate('local_time','<=',date("Y-m-d", strtotime($date_to)))
                        ->where('branch_code',$branch_code)
                          ->where('post_void',0)
                        ->orderBy('local_time')
                        ->get();


    }

   public static function getMinDate($date_from,$date_to,$branch_code) {
         $date_from = date("Y-m-d", strtotime($date_from));
        $date_to = date("Y-m-d", strtotime($date_to));

        return  self::select(DB::raw("min(local_time) as min_date"))
                        
                 
                    ->where('branch_code',$branch_code)
                    ->whereDate('local_time','>=',$date_from)
                    ->whereDate('local_time','<=',$date_to)
                    ->first();
    }


     public static function summaryDailySales($date_from,$date_to,$branch_code) {

            $date_from = date("Y-m-d", strtotime($date_from));
            $date_to = date("Y-m-d", strtotime($date_to));

            return self::select(DB::raw("
                                        MIN(invoice_no) as from_invoice,
                                        MAX(invoice_no) as to_invoice,
                                        
                                        IF(branch_code IS NULL or branch_code = '', 'NO DATA', branch_code) as branch_code,
                                        IF(DATE(local_time) IS NULL or DATE(local_time) = '', 'NO DATA', DATE(local_time)) as local_time,
                                        

                                        SUM(IF(discount_name = 'SENIOR CITIZEN', net_amount, 0)) AS senior_amount,
                                        SUM(IF(discount_name = 'PWD', net_amount, 0)) AS pwd_amount,
                                        SUM(IF(discount_name = 'DIPLOMAT', net_amount, 0)) AS diplomat_amount,
                                        COALESCE(( SUM(IF(discount_name = 'PWD' or discount_name = 'SENIOR CITIZEN' or discount_name = 'DIPLOMAT', cash_amount, 0))  + SUM(IF(discount_name = 'PWD' or discount_name = 'SENIOR CITIZEN' or discount_name = 'DIPLOMAT', check_amount, 0))  ),0) as cash_vat_exempt,

                                       COALESCE(( SUM(IF(discount_name = ' ' or discount_name = NULL , cash_amount, 0))  + SUM(IF(discount_name = ' ' or discount_name = NULL, check_amount, 0))  ),0) as cash_vat_in,


                                        COALESCE(( SUM(IF(discount_name = 'PWD' or discount_name = 'SENIOR CITIZEN' or discount_name = 'DIPLOMAT', atm_amount, 0))  + SUM(IF(discount_name = 'PWD' or discount_name = 'SENIOR CITIZEN' or discount_name = 'DIPLOMAT', card_amount, 0)) + SUM(IF(discount_name = 'PWD' or discount_name = 'SENIOR CITIZEN' or discount_name = 'DIPLOMAT', account_amount, 0)) + SUM(IF(discount_name = 'PWD' or discount_name = 'SENIOR CITIZEN' or discount_name = 'DIPLOMAT', deffered_amount, 0)) + SUM(IF(discount_name = 'PWD' or discount_name = 'SENIOR CITIZEN' or discount_name = 'DIPLOMAT', other_payment, 0))  ),0) as charge_vat_exempt,

                                         COALESCE(( SUM(IF(discount_name = ' ' or discount_name = NULL, atm_amount, 0))  + SUM(IF(discount_name = ' ' or discount_name = NULL, card_amount, 0)) + SUM(IF(discount_name = ' ' or discount_name = NULL , account_amount, 0)) + SUM(IF(discount_name = ' ' or discount_name = NULL, deffered_amount, 0)) + SUM(IF(discount_name = ' ' or discount_name = NULL , other_payment, 0))  ),0) as charge_vat_in
                                      
                                        "))
                        ->whereDate('local_time','>=',date("Y-m-d", strtotime($date_from)))
                        ->whereDate('local_time','<=',date("Y-m-d", strtotime($date_to)))
                        ->where('branch_code',$branch_code)
                          ->where('post_void',0)
                        ->orderBy('local_time')
                        ->groupBy(DB::raw("DATE(local_time)"))
                        ->get();


        // for ($i =   0;  $i  <   sizeof( $data );   $i++)   {
        //     for ($j = $i+1; $j <  sizeof( $data );  $j++)   {

        //         if  ($data[$i]->local_time  ==  $data[$j]->local_time) {
                                        


        //             }
        //         }
        // }

                        // $new = array();
                        // for($i=0;$i < count($data);$i++)
                        // {
                        //     $index = -1;
                        //     for($j=0;$j<count($new);$j++)
                        //     {
                        //         if($data[$i]->local_time == $new[$j]['local_time'])
                        //         {
                        //             $index = $j;
                        //             breck;
                        //         }
                        //     }
                        //     if($index == -1)
                        //     {
                        //         array_push($new, $data[$i]);
                        //     }
                        //     else
                        //     {
                        //         $new[$index]['theadults'] += $data[$i]['theadults'];
                        //     }
                        // }



    }

    public static function detailedCardSales($date_from,$date_to,$branch_code) {




             $date_from = date("Y-m-d", strtotime($date_from));
            $date_to = date("Y-m-d", strtotime($date_to));

            return self::select(DB::raw(" IF(DATE(sale_summaries.local_time) IS NULL or DATE(sale_summaries.local_time) = '', 'NO DATA', DATE(sale_summaries.local_time)) as local_time,
                sale_summaries.invoice_no,
                 IF(sale_summaries.member IS NULL or sale_summaries.member = '', 'NO DATA', member) as customer_name,
                coalesce(atm_amount,0) as atm_amount,
                coalesce(card_amount  + account_amount + other_payment + check_amount + deffered_amount ,0) as credit_card_amount,
                COALESCE ((other_payment + 
                            account_amount + 
                            card_amount +
                            check_amount) - (IF (card_details.bank_name LIKE '%JCB%' or card_details.bank_name  LIKE '%AMEX%', card_details.amount,0) +
                             IF (check_details.bank_name LIKE '%JCB%' or check_details.bank_name  LIKE '%AMEX%', check_details.amount,0))
                                ,0) as other_jcb ,


                COALESCE ((IF (card_details.bank_name = ' ' , card_details.amount,0))
                                ,0) as card_no_bank_name ,


                COALESCE ((IF (atm_details.bank_name = ' ' , atm_details.amount,0))
                                ,0) as atm_no_bank_name ,



                COALESCE (
                            IF (card_details.bank_name LIKE '%JCB%' or card_details.bank_name  LIKE '%AMEX%', card_details.amount,0) +
                             IF (check_details.bank_name LIKE '%JCB%' or check_details.bank_name  LIKE '%AMEX%', check_details.amount,0)
                                ,0) as jcb_amount  ,


                coalesce( IF (((deferred_details.bank = 'BDO') || (deferred_details.bank  = 'bdo')) && (deferred_details.months = '3') , deferred_details.amount,0)  ,0    ) as bdo_3,

                   coalesce( IF (((deferred_details.bank = 'BDO') || (deferred_details.bank  = 'bdo')) && (deferred_details.months = '6') , deferred_details.amount,0)  ,0    ) as bdo_6,

                        coalesce( IF (((deferred_details.bank = 'BDO') || (deferred_details.bank  = 'bdo')) && (deferred_details.months = '12') , deferred_details.amount,0)  ,0    ) as bdo_12,

                        coalesce( IF (((deferred_details.bank = 'BPI') || (deferred_details.bank  = 'bpi')) && (deferred_details.months = '3') , deferred_details.amount,0)  ,0    ) as bpi_3,

                         coalesce( IF (((deferred_details.bank = 'BPI') || (deferred_details.bank  = 'bpi')) && (deferred_details.months = '6') , deferred_details.amount,0)  ,0    ) as bpi_6,

                          coalesce( IF (((deferred_details.bank = 'BPI') || (deferred_details.bank  = 'bpi')) && (deferred_details.months = '12') , deferred_details.amount,0)  ,0    ) as bpi_12,

                           coalesce( IF (((deferred_details.bank = 'CITIBANK') || (deferred_details.bank  = 'citibank')) && (deferred_details.months = '3') , deferred_details.amount,0)  ,0    ) as citibank_3,

                            coalesce( IF (((deferred_details.bank = 'CITIBANK') || (deferred_details.bank  = 'citibank')) && (deferred_details.months = '6') , deferred_details.amount,0)  ,0    ) as citibank_6,

                             coalesce( IF (((deferred_details.bank = 'CITIBANK') || (deferred_details.bank  = 'citibank')) && (deferred_details.months = '12') , deferred_details.amount,0)  ,0    ) as citibank_12,


                              coalesce( IF (((deferred_details.bank = 'HSBC') || (deferred_details.bank  = 'hsbc')) && (deferred_details.months = '3') , deferred_details.amount,0)  ,0    ) as hsbc_3,

                              coalesce( IF (((deferred_details.bank = 'HSBC') || (deferred_details.bank  = 'hsbc')) && (deferred_details.months = '6') , deferred_details.amount,0)  ,0    ) as hsbc_6,

                              coalesce( IF (((deferred_details.bank = 'HSBC') || (deferred_details.bank  = 'hsbc')) && (deferred_details.months = '12') , deferred_details.amount,0)  ,0    ) as hsbc_12,

                               coalesce( IF (((deferred_details.bank = 'EASTWEST') || (deferred_details.bank  = 'eastwest')) && (deferred_details.months = '3') , deferred_details.amount,0)  ,0    ) as eastwest_3,

                                coalesce( IF (((deferred_details.bank = 'EASTWEST') || (deferred_details.bank  = 'eastwest')) && (deferred_details.months = '6') , deferred_details.amount,0)  ,0    ) as eastwest_6,

                                 coalesce( IF (((deferred_details.bank = 'EASTWEST') || (deferred_details.bank  = 'eastwest')) && (deferred_details.months = '12') , deferred_details.amount,0)  ,0    ) as eastwest_12,


                                  coalesce( IF (((deferred_details.bank = 'METRO') || (deferred_details.bank  = 'metro')  || (deferred_details.bank = 'METROBANK') || (deferred_details.bank = 'metrobank')) && (deferred_details.months = '3') , deferred_details.amount,0)  ,0    ) as metrobank_3,

                                   coalesce( IF (((deferred_details.bank = 'METRO') || (deferred_details.bank  = 'metro')  || (deferred_details.bank = 'METROBANK') || (deferred_details.bank = 'metrobank')) && (deferred_details.months = '6') , deferred_details.amount,0)  ,0    ) as metrobank_6,

                                    coalesce( IF (((deferred_details.bank = 'METRO') || (deferred_details.bank  = 'metro')  || (deferred_details.bank = 'METROBANK') || (deferred_details.bank = 'metrobank')) && (deferred_details.months = '12') , deferred_details.amount,0)  ,0    ) as metrobank_12,



                                    coalesce( IF (((deferred_details.bank = 'DINERS/SECURITY') || (deferred_details.bank  = 'SECURITY')  || (deferred_details.bank = 'DINERS') || (deferred_details.bank = 'diners')) && (deferred_details.months = '3') , deferred_details.amount,0)  ,0    ) as diners_3,

                                     coalesce( IF (((deferred_details.bank = 'DINERS/SECURITY') || (deferred_details.bank  = 'SECURITY')  || (deferred_details.bank = 'DINERS') || (deferred_details.bank = 'diners')) && (deferred_details.months = '6') , deferred_details.amount,0)  ,0    ) as diners_6,

                                      coalesce( IF (((deferred_details.bank = 'DINERS/SECURITY') || (deferred_details.bank  = 'SECURITY')  || (deferred_details.bank = 'DINERS') || (deferred_details.bank = 'diners')) && (deferred_details.months = '12') , deferred_details.amount,0)  ,0    ) as diners_12,

                                       coalesce( IF (( (deferred_details.bank  = 'PNB')  || (deferred_details.bank = 'pnb') ) && (deferred_details.months = '3') , deferred_details.amount,0)  ,0    ) as pnb_3,

                                        coalesce( IF (( (deferred_details.bank  = 'PNB')  || (deferred_details.bank = 'pnb') ) && (deferred_details.months = '6') , deferred_details.amount,0)  ,0    ) as pnb_6,

                                         coalesce( IF (((deferred_details.bank  = 'PNB')  || (deferred_details.bank = 'pnb') ) && (deferred_details.months = '12') , deferred_details.amount,0)  ,0    ) as pnb_12,


                                           coalesce( IF (((deferred_details.bank  = 'RCBC')  || (deferred_details.bank = 'rcbc') ) && (deferred_details.months = '3') , deferred_details.amount,0)  ,0    ) as rcbc_3,

                                            coalesce( IF (((deferred_details.bank  = 'RCBC')  || (deferred_details.bank = 'rcbc') ) && (deferred_details.months = '6') , deferred_details.amount,0)  ,0    ) as rcbc_6,


                                             coalesce( IF (((deferred_details.bank  = 'RCBC')  || (deferred_details.bank = 'rcbc') ) && (deferred_details.months = '12') , deferred_details.amount,0)  ,0    ) as rcbc_12,


                                              coalesce( IF (((deferred_details.bank  = 'UNION')  || (deferred_details.bank = 'UNIONBANK') ) && (deferred_details.months = '3') , deferred_details.amount,0)  ,0    ) as unionbank_3,

                                               coalesce( IF (((deferred_details.bank  = 'UNION')  || (deferred_details.bank = 'UNIONBANK') ) && (deferred_details.months = '6') , deferred_details.amount,0)  ,0    ) as unionbank_6,

                                                coalesce( IF (((deferred_details.bank  = 'UNION')  || (deferred_details.bank = 'UNIONBANK') ) && (deferred_details.months = '12') , deferred_details.amount,0) ,0    ) as unionbank_12,


                                                coalesce( IF ((deferred_details.bank  = ' ')  && (deferred_details.months = '3') , deferred_details.amount,0) ,0    ) as no_bank_3,


                                                 coalesce( IF ((deferred_details.bank  = ' ')  && (deferred_details.months = '6') , deferred_details.amount,0) ,0    ) as no_bank_6,


                                                   coalesce( IF ((deferred_details.bank  = ' ')  && (deferred_details.months = '12') , deferred_details.amount,0) ,0    ) as no_bank_12,






                deferred_details.bank_name , 
                deferred_details.bank,
                coalesce(deferred_details.amount,0) as total_deffered_amount,
                deferred_details.months,
                sale_summaries.invoice_no,
                sale_summaries.ref_no             





                "))
                ->leftJoin('deferred_details','deferred_details.sale_summary_id','=','sale_summaries.id')
                ->leftJoin('card_details','card_details.sale_summary_id','=','sale_summaries.id')
                ->leftJoin('atm_details','atm_details.sale_summary_id','=','sale_summaries.id')
                
                ->leftJoin('check_details','check_details.sale_summary_id','=','sale_summaries.id')
                ->leftJoin('corporate_details','corporate_details.sale_summary_id','=','sale_summaries.id')
                
                ->leftJoin('other_payments','other_payments.sale_summary_id','=','sale_summaries.id')
                ->whereDate('local_time','>=',date("Y-m-d", strtotime($date_from)))
                ->whereDate('local_time','<=',date("Y-m-d", strtotime($date_to)))
                ->where('branch_code',$branch_code)
                  ->where('post_void',0)
                ->orderBy('local_time')
                ->orderBy('invoice_no')
                ->get();







    }


     public static function summaryCardSales($date_from,$date_to,$branch_code) { 



           $date_from = date("Y-m-d", strtotime($date_from));
            $date_to = date("Y-m-d", strtotime($date_to));

            return self::select(DB::raw(" IF(DATE(sale_summaries.local_time) IS NULL or DATE(sale_summaries.local_time) = '', 'NO DATA', DATE(sale_summaries.local_time)) as local_time,
                sale_summaries.invoice_no,
                 IF(sale_summaries.member IS NULL or sale_summaries.member = '', 'NO DATA', member) as customer_name,
                coalesce(sum(atm_amount),0) as atm_amount,
                coalesce((sum(card_amount)  + sum(account_amount) + sum(other_payment) + sum(check_amount) + sum(deffered_amount)) ,0) as credit_card_amount,
                COALESCE (( (sum(other_payment) + 
                            sum(account_amount) + 
                            sum(card_amount) +
                             sum(check_amount)) - ( sum(IF (card_details.bank_name LIKE '%JCB%' or card_details.bank_name  LIKE '%AMEX%', card_details.amount,0)) +
                             sum(IF (check_details.bank_name LIKE '%JCB%' or check_details.bank_name  LIKE '%AMEX%', check_details.amount,0)))
                                ),0) as other_jcb ,

                COALESCE ((
                            sum(IF (card_details.bank_name LIKE '%JCB%' or card_details.bank_name  LIKE '%AMEX%', card_details.amount,0)) +
                             sum(IF (check_details.bank_name LIKE '%JCB%' or check_details.bank_name  LIKE '%AMEX%', check_details.amount,0)) )
                                ,0) as jcb_amount  ,




              coalesce( sum(IF (((deferred_details.bank = 'BDO') || (deferred_details.bank  = 'bdo')) && (deferred_details.months = '3') , deferred_details.amount,0))  ,0    ) as bdo_3,

                   coalesce( sum(IF (((deferred_details.bank = 'BDO') || (deferred_details.bank  = 'bdo')) && (deferred_details.months = '6') , deferred_details.amount,0))  ,0    ) as bdo_6,

                        coalesce( sum(IF (((deferred_details.bank = 'BDO') || (deferred_details.bank  = 'bdo')) && (deferred_details.months = '12') , deferred_details.amount,0))  ,0    ) as bdo_12,

                        coalesce( sum(IF (((deferred_details.bank = 'BPI') || (deferred_details.bank  = 'bpi')) && (deferred_details.months = '3') , deferred_details.amount,0))  ,0    ) as bpi_3,

                         coalesce( sum(IF (((deferred_details.bank = 'BPI') || (deferred_details.bank  = 'bpi')) && (deferred_details.months = '6') , deferred_details.amount,0))  ,0    ) as bpi_6,

                          coalesce( sum(IF (((deferred_details.bank = 'BPI') || (deferred_details.bank  = 'bpi')) && (deferred_details.months = '12') , deferred_details.amount,0))  ,0    ) as bpi_12,

                           coalesce( sum(IF (((deferred_details.bank = 'CITIBANK') || (deferred_details.bank  = 'citibank')) && (deferred_details.months = '3') , deferred_details.amount,0))  ,0    ) as citibank_3,

                            coalesce( sum(IF (((deferred_details.bank = 'CITIBANK') || (deferred_details.bank  = 'citibank')) && (deferred_details.months = '6') , deferred_details.amount,0))  ,0    ) as citibank_6,

                             coalesce( sum(IF (((deferred_details.bank = 'CITIBANK') || (deferred_details.bank  = 'citibank')) && (deferred_details.months = '12') , deferred_details.amount,0))  ,0    ) as citibank_12,


                              coalesce( sum(IF (((deferred_details.bank = 'HSBC') || (deferred_details.bank  = 'hsbc')) && (deferred_details.months = '3') , deferred_details.amount,0))  ,0    ) as hsbc_3,

                              coalesce( sum(IF (((deferred_details.bank = 'HSBC') || (deferred_details.bank  = 'hsbc')) && (deferred_details.months = '6') , deferred_details.amount,0))  ,0    ) as hsbc_6,

                              coalesce( sum(IF (((deferred_details.bank = 'HSBC') || (deferred_details.bank  = 'hsbc')) && (deferred_details.months = '12') , deferred_details.amount,0))  ,0    ) as hsbc_12,

                               coalesce( sum(IF (((deferred_details.bank = 'EASTWEST') || (deferred_details.bank  = 'eastwest')) && (deferred_details.months = '3') , deferred_details.amount,0))  ,0    ) as eastwest_3,

                                coalesce( sum(IF (((deferred_details.bank = 'EASTWEST') || (deferred_details.bank  = 'eastwest')) && (deferred_details.months = '6') , deferred_details.amount,0))  ,0    ) as eastwest_6,

                                 coalesce( sum(IF (((deferred_details.bank = 'EASTWEST') || (deferred_details.bank  = 'eastwest')) && (deferred_details.months = '12') , deferred_details.amount,0))  ,0    ) as eastwest_12,


                                  coalesce( sum(IF (((deferred_details.bank = 'METRO') || (deferred_details.bank  = 'metro')  || (deferred_details.bank = 'METROBANK') || (deferred_details.bank = 'metrobank')) && (deferred_details.months = '3') , deferred_details.amount,0))  ,0    ) as metrobank_3,

                                   coalesce( sum(IF (((deferred_details.bank = 'METRO') || (deferred_details.bank  = 'metro')  || (deferred_details.bank = 'METROBANK') || (deferred_details.bank = 'metrobank')) && (deferred_details.months = '6') , deferred_details.amount,0))  ,0    ) as metrobank_6,

                                    coalesce( sum(IF (((deferred_details.bank = 'METRO') || (deferred_details.bank  = 'metro')  || (deferred_details.bank = 'METROBANK') || (deferred_details.bank = 'metrobank')) && (deferred_details.months = '12') , deferred_details.amount,0))  ,0    ) as metrobank_12,



                                    coalesce( sum(IF (((deferred_details.bank = 'DINERS/SECURITY') || (deferred_details.bank  = 'SECURITY')  || (deferred_details.bank = 'DINERS') || (deferred_details.bank = 'diners')) && (deferred_details.months = '3') , deferred_details.amount,0))  ,0    ) as diners_3,

                                     coalesce( sum(IF (((deferred_details.bank = 'DINERS/SECURITY') || (deferred_details.bank  = 'SECURITY')  || (deferred_details.bank = 'DINERS') || (deferred_details.bank = 'diners')) && (deferred_details.months = '6') , deferred_details.amount,0))  ,0    ) as diners_6,

                                      coalesce( sum(IF (((deferred_details.bank = 'DINERS/SECURITY') || (deferred_details.bank  = 'SECURITY')  || (deferred_details.bank = 'DINERS') || (deferred_details.bank = 'diners')) && (deferred_details.months = '12') , deferred_details.amount,0))  ,0    ) as diners_12,

                                       coalesce( sum(IF (( (deferred_details.bank  = 'PNB')  || (deferred_details.bank = 'pnb') ) && (deferred_details.months = '3') , deferred_details.amount,0))  ,0    ) as pnb_3,

                                        coalesce( sum(IF (( (deferred_details.bank  = 'PNB')  || (deferred_details.bank = 'pnb') ) && (deferred_details.months = '6') , deferred_details.amount,0))  ,0    ) as pnb_6,

                                         coalesce( sum(IF (((deferred_details.bank  = 'PNB')  || (deferred_details.bank = 'pnb') ) && (deferred_details.months = '12') , deferred_details.amount,0))  ,0    ) as pnb_12,


                                           coalesce( sum(IF (((deferred_details.bank  = 'RCBC')  || (deferred_details.bank = 'rcbc') ) && (deferred_details.months = '3') , deferred_details.amount,0))  ,0    ) as rcbc_3,

                                            coalesce( sum(IF (((deferred_details.bank  = 'RCBC')  || (deferred_details.bank = 'rcbc') ) && (deferred_details.months = '6') , deferred_details.amount,0))  ,0    ) as rcbc_6,


                                             coalesce( sum(IF (((deferred_details.bank  = 'RCBC')  || (deferred_details.bank = 'rcbc') ) && (deferred_details.months = '12') , deferred_details.amount,0))  ,0    ) as rcbc_12,


                                              coalesce( sum(IF (((deferred_details.bank  = 'UNION')  || (deferred_details.bank = 'UNIONBANK') ) && (deferred_details.months = '3') , deferred_details.amount,0))  ,0    ) as unionbank_3,

                                               coalesce( sum(IF (((deferred_details.bank  = 'UNION')  || (deferred_details.bank = 'UNIONBANK') ) && (deferred_details.months = '6') , deferred_details.amount,0))  ,0    ) as unionbank_6,

                                                coalesce( sum(IF (((deferred_details.bank  = 'UNION')  || (deferred_details.bank = 'UNIONBANK') ) && (deferred_details.months = '12') , deferred_details.amount,0))  ,0    ) as unionbank_12,








               










                deferred_details.bank_name , 
                deferred_details.bank,
                sale_summaries.invoice_no             





                "))
                ->leftJoin('deferred_details','deferred_details.sale_summary_id','=','sale_summaries.id')
                ->leftJoin('atm_details','atm_details.sale_summary_id','=','sale_summaries.id')
                ->leftJoin('card_details','card_details.sale_summary_id','=','sale_summaries.id')
                ->leftJoin('check_details','check_details.sale_summary_id','=','sale_summaries.id')
                ->leftJoin('corporate_details','corporate_details.sale_summary_id','=','sale_summaries.id')
                
                ->leftJoin('other_payments','other_payments.sale_summary_id','=','sale_summaries.id')
                ->whereDate('local_time','>=',date("Y-m-d", strtotime($date_from)))
                ->whereDate('local_time','<=',date("Y-m-d", strtotime($date_to)))
                ->where('sale_summaries.branch_code',$branch_code)
              
                ->groupBy(DB::raw("DATE(local_time)"))
                ->orderBy('local_time')
                  ->where('post_void',0)
                ->get();





    }

    public static function consolidatedCardSales($date_from,$date_to) { 

            $branches = UserBranch::getAllowedBranch(Auth::user()->id);
            
            $date_from = date("Y-m-d", strtotime($date_from));
            $date_to = date("Y-m-d", strtotime($date_to));

            return self::select(DB::raw(" IF(DATE(sale_summaries.local_time) IS NULL or DATE(sale_summaries.local_time) = '', 'NO DATA', DATE(sale_summaries.local_time)) as local_time,
                sale_summaries.invoice_no,

                 IF(sale_summaries.member IS NULL or sale_summaries.member = '', 'NO DATA', member) as customer_name,
                coalesce(sum(atm_amount),0) as atm_amount,
                coalesce((sum(card_amount)  + sum(account_amount) + sum(other_payment) + sum(check_amount) + sum(deffered_amount)) ,0) as credit_card_amount,
                COALESCE (( (sum(other_payment) + 
                            sum(account_amount) + 
                            sum(card_amount) +
                             sum(check_amount)) - ( sum(IF (card_details.bank_name LIKE '%JCB%' or card_details.bank_name  LIKE '%AMEX%', card_details.amount,0)) +
                             sum(IF (check_details.bank_name LIKE '%JCB%' or check_details.bank_name  LIKE '%AMEX%', check_details.amount,0)))
                                ),0) as other_jcb ,

                COALESCE ((
                            sum(IF (card_details.bank_name LIKE '%JCB%' or card_details.bank_name  LIKE '%AMEX%', card_details.amount,0)) +
                             sum(IF (check_details.bank_name LIKE '%JCB%' or check_details.bank_name  LIKE '%AMEX%', check_details.amount,0)) )
                                ,0) as jcb_amount  ,




              coalesce( sum(IF (((deferred_details.bank = 'BDO') || (deferred_details.bank  = 'bdo')) && (deferred_details.months = '3') , deferred_details.amount,0))  ,0    ) as bdo_3,

                   coalesce( sum(IF (((deferred_details.bank = 'BDO') || (deferred_details.bank  = 'bdo')) && (deferred_details.months = '6') , deferred_details.amount,0))  ,0    ) as bdo_6,

                        coalesce( sum(IF (((deferred_details.bank = 'BDO') || (deferred_details.bank  = 'bdo')) && (deferred_details.months = '12') , deferred_details.amount,0))  ,0    ) as bdo_12,

                        coalesce( sum(IF (((deferred_details.bank = 'BPI') || (deferred_details.bank  = 'bpi')) && (deferred_details.months = '3') , deferred_details.amount,0))  ,0    ) as bpi_3,

                         coalesce( sum(IF (((deferred_details.bank = 'BPI') || (deferred_details.bank  = 'bpi')) && (deferred_details.months = '6') , deferred_details.amount,0))  ,0    ) as bpi_6,

                          coalesce( sum(IF (((deferred_details.bank = 'BPI') || (deferred_details.bank  = 'bpi')) && (deferred_details.months = '12') , deferred_details.amount,0))  ,0    ) as bpi_12,

                           coalesce( sum(IF (((deferred_details.bank = 'CITIBANK') || (deferred_details.bank  = 'citibank')) && (deferred_details.months = '3') , deferred_details.amount,0))  ,0    ) as citibank_3,

                            coalesce( sum(IF (((deferred_details.bank = 'CITIBANK') || (deferred_details.bank  = 'citibank')) && (deferred_details.months = '6') , deferred_details.amount,0))  ,0    ) as citibank_6,

                             coalesce( sum(IF (((deferred_details.bank = 'CITIBANK') || (deferred_details.bank  = 'citibank')) && (deferred_details.months = '12') , deferred_details.amount,0))  ,0    ) as citibank_12,


                              coalesce( sum(IF (((deferred_details.bank = 'HSBC') || (deferred_details.bank  = 'hsbc')) && (deferred_details.months = '3') , deferred_details.amount,0))  ,0    ) as hsbc_3,

                              coalesce( sum(IF (((deferred_details.bank = 'HSBC') || (deferred_details.bank  = 'hsbc')) && (deferred_details.months = '6') , deferred_details.amount,0))  ,0    ) as hsbc_6,

                              coalesce( sum(IF (((deferred_details.bank = 'HSBC') || (deferred_details.bank  = 'hsbc')) && (deferred_details.months = '12') , deferred_details.amount,0))  ,0    ) as hsbc_12,

                               coalesce( sum(IF (((deferred_details.bank = 'EASTWEST') || (deferred_details.bank  = 'eastwest')) && (deferred_details.months = '3') , deferred_details.amount,0))  ,0    ) as eastwest_3,

                                coalesce( sum(IF (((deferred_details.bank = 'EASTWEST') || (deferred_details.bank  = 'eastwest')) && (deferred_details.months = '6') , deferred_details.amount,0))  ,0    ) as eastwest_6,

                                 coalesce( sum(IF (((deferred_details.bank = 'EASTWEST') || (deferred_details.bank  = 'eastwest')) && (deferred_details.months = '12') , deferred_details.amount,0))  ,0    ) as eastwest_12,


                                  coalesce( sum(IF (((deferred_details.bank = 'METRO') || (deferred_details.bank  = 'metro')  || (deferred_details.bank = 'METROBANK') || (deferred_details.bank = 'metrobank')) && (deferred_details.months = '3') , deferred_details.amount,0))  ,0    ) as metrobank_3,

                                   coalesce( sum(IF (((deferred_details.bank = 'METRO') || (deferred_details.bank  = 'metro')  || (deferred_details.bank = 'METROBANK') || (deferred_details.bank = 'metrobank')) && (deferred_details.months = '6') , deferred_details.amount,0))  ,0    ) as metrobank_6,

                                    coalesce( sum(IF (((deferred_details.bank = 'METRO') || (deferred_details.bank  = 'metro')  || (deferred_details.bank = 'METROBANK') || (deferred_details.bank = 'metrobank')) && (deferred_details.months = '12') , deferred_details.amount,0))  ,0    ) as metrobank_12,



                                    coalesce( sum(IF (((deferred_details.bank = 'DINERS/SECURITY') || (deferred_details.bank  = 'SECURITY')  || (deferred_details.bank = 'DINERS') || (deferred_details.bank = 'diners')) && (deferred_details.months = '3') , deferred_details.amount,0))  ,0    ) as diners_3,

                                     coalesce( sum(IF (((deferred_details.bank = 'DINERS/SECURITY') || (deferred_details.bank  = 'SECURITY')  || (deferred_details.bank = 'DINERS') || (deferred_details.bank = 'diners')) && (deferred_details.months = '6') , deferred_details.amount,0))  ,0    ) as diners_6,

                                      coalesce( sum(IF (((deferred_details.bank = 'DINERS/SECURITY') || (deferred_details.bank  = 'SECURITY')  || (deferred_details.bank = 'DINERS') || (deferred_details.bank = 'diners')) && (deferred_details.months = '12') , deferred_details.amount,0))  ,0    ) as diners_12,

                                       coalesce( sum(IF (( (deferred_details.bank  = 'PNB')  || (deferred_details.bank = 'pnb') ) && (deferred_details.months = '3') , deferred_details.amount,0))  ,0    ) as pnb_3,

                                        coalesce( sum(IF (( (deferred_details.bank  = 'PNB')  || (deferred_details.bank = 'pnb') ) && (deferred_details.months = '6') , deferred_details.amount,0))  ,0    ) as pnb_6,

                                         coalesce( sum(IF (((deferred_details.bank  = 'PNB')  || (deferred_details.bank = 'pnb') ) && (deferred_details.months = '12') , deferred_details.amount,0))  ,0    ) as pnb_12,


                                           coalesce( sum(IF (((deferred_details.bank  = 'RCBC')  || (deferred_details.bank = 'rcbc') ) && (deferred_details.months = '3') , deferred_details.amount,0))  ,0    ) as rcbc_3,

                                            coalesce( sum(IF (((deferred_details.bank  = 'RCBC')  || (deferred_details.bank = 'rcbc') ) && (deferred_details.months = '6') , deferred_details.amount,0))  ,0    ) as rcbc_6,


                                             coalesce( sum(IF (((deferred_details.bank  = 'RCBC')  || (deferred_details.bank = 'rcbc') ) && (deferred_details.months = '12') , deferred_details.amount,0))  ,0    ) as rcbc_12,


                                              coalesce( sum(IF (((deferred_details.bank  = 'UNION')  || (deferred_details.bank = 'UNIONBANK') ) && (deferred_details.months = '3') , deferred_details.amount,0))  ,0    ) as unionbank_3,

                                               coalesce( sum(IF (((deferred_details.bank  = 'UNION')  || (deferred_details.bank = 'UNIONBANK') ) && (deferred_details.months = '6') , deferred_details.amount,0))  ,0    ) as unionbank_6,

                                                coalesce( sum(IF (((deferred_details.bank  = 'UNION')  || (deferred_details.bank = 'UNIONBANK') ) && (deferred_details.months = '12') , deferred_details.amount,0))  ,0    ) as unionbank_12,
                                                sale_summaries.branch_code,
                                                sale_summaries.branch_name"))
                ->leftJoin('deferred_details','deferred_details.sale_summary_id','=','sale_summaries.id')
                ->leftJoin('atm_details','atm_details.sale_summary_id','=','sale_summaries.id')
                ->leftJoin('card_details','card_details.sale_summary_id','=','sale_summaries.id')
                ->leftJoin('check_details','check_details.sale_summary_id','=','sale_summaries.id')
                ->leftJoin('corporate_details','corporate_details.sale_summary_id','=','sale_summaries.id')
                
                ->leftJoin('other_payments','other_payments.sale_summary_id','=','sale_summaries.id')
                ->whereDate('local_time','>=',date("Y-m-d", strtotime($date_from)))
                ->whereDate('local_time','<=',date("Y-m-d", strtotime($date_to)))
                ->whereIn('sale_summaries.branch_code',$branches)
                 ->groupBy('branch_code')  
                ->groupBy(DB::raw("DATE(local_time)"))
                    ->where('post_void',0)           
                ->orderBy('local_time')
                ->orderBy('branch_name')
                ->get();





    }


    

    // public static function salessummary($date_from, $date_to, $request){
    //     $branches = UserBranch::getAllowedBranch(Auth::user()->id);

    //     $ob = new stdClass();
    //     $ob->net_sales = 0;
    //     $ob->sales_refund = 0;
    //     $ob->retex = 0;
    //     $ob->revenue = 0;

    //     $net_sales = self::select(DB::raw("sum(net_amount) as net_sales"))
    //         ->where(function($query) use ($request){
    //         if(($request->has('company'))&& (!empty($request->company))){
    //                 $query->whereIn('company_code',$request->company);
    //             }
    //         })
    //         ->where(function($query) use ($request, $branches){
    //             if(($request->has('branch'))&& (!empty($request->branch))){
    //                 $query->whereIn('branch_code',$request->branch);
    //             }else{
    //                 $query->whereIn('branch_code',$branches);
    //             }
    //         })
    //         ->where(function($query) use ($request){
    //         if(($request->has('terminal'))&& (!empty($request->terminal))){
    //                 $query->whereIn('terminal_code',$request->terminal);
    //             }
    //         })
    //         ->where(function($query) use ($request){
    //         if(($request->has('user'))&& (!empty($request->user))){
    //                 $query->whereIn('user',$request->user);
    //             }
    //         })
    //         ->where(function($query) use ($request){
    //         if(($request->has('saleman'))&& (!empty($request->saleman))){
    //                 $query->whereIn('sales_man',$request->saleman);
    //             }
    //         })
    //         ->where(function($query) use ($request){
    //         if(($request->has('member'))&& (!empty($request->member))){
    //                 $query->whereIn('member',$request->member);
    //             }
    //         })
    //         ->whereDate('local_time','>=',date("Y-m-d", strtotime($date_from)))
    //         ->whereDate('local_time','<=',date("Y-m-d", strtotime($date_to)))
    //         ->where('post_void',0)
    //         ->first();

    //     $refund_sales = RefundItem::select(DB::raw("sum(amount) as amount"))
    //         ->where(function($query) use ($request){
    //         if(($request->has('company'))&& (!empty($request->company))){
    //                 $query->whereIn('company_code',$request->company);
    //             }
    //         })
    //         ->where(function($query) use ($request, $branches){
    //             if(($request->has('branch'))&& (!empty($request->branch))){
    //                 $query->whereIn('branch_code',$request->branch);
    //             }else{
    //                 $query->whereIn('branch_code',$branches);
    //             }
    //         })
    //         ->where(function($query) use ($request){
    //         if(($request->has('terminal'))&& (!empty($request->terminal))){
    //                 $query->whereIn('terminal_code',$request->terminal);
    //             }
    //         })
    //         ->where(function($query) use ($request){
    //         if(($request->has('user'))&& (!empty($request->user))){
    //                 $query->whereIn('user',$request->user);
    //             }
    //         })
    //         ->where(function($query) use ($request){
    //         if(($request->has('member'))&& (!empty($request->member))){
    //                 $query->whereIn('customer_name',$request->member);
    //             }
    //         })
    //         ->whereDate('local_time','>=',date("Y-m-d", strtotime($date_from)))
    //         ->whereDate('local_time','<=',date("Y-m-d", strtotime($date_to)))
    //         ->first();


    //     $retex = ReturnExchangeItem::select(DB::raw("sum(amount) as amount"))
    //         ->where(function($query) use ($request){
    //         if(($request->has('company'))&& (!empty($request->company))){
    //                 $query->whereIn('company_code',$request->company);
    //             }
    //         })
    //         ->where(function($query) use ($request, $branches){
    //             if(($request->has('branch'))&& (!empty($request->branch))){
    //                 $query->whereIn('branch_code',$request->branch);
    //             }else{
    //                 $query->whereIn('branch_code',$branches);
    //             }
    //         })
    //         ->where(function($query) use ($request){
    //         if(($request->has('terminal'))&& (!empty($request->terminal))){
    //                 $query->whereIn('terminal_code',$request->terminal);
    //             }
    //         })
    //         ->where(function($query) use ($request){
    //         if(($request->has('user'))&& (!empty($request->user))){
    //                 $query->whereIn('user',$request->user);
    //             }
    //         })
    //         ->where(function($query) use ($request){
    //         if(($request->has('member'))&& (!empty($request->member))){
    //                 $query->whereIn('customer_name',$request->member);
    //             }
    //         })
    //         ->whereDate('local_time','>=',date("Y-m-d", strtotime($date_from)))
    //         ->whereDate('local_time','<=',date("Y-m-d", strtotime($date_to)))
    //         ->first();

    //     if(!empty($ob)){
    //         $ob->net_sales = $net_sales->net_sales;
    //     }

    //     if(!empty($refund_sales)){
    //         $ob->sales_refund = $refund_sales->amount;
    //     }

    //     if(!empty($retex)){
    //         $ob->retex = $retex->amount;
    //     }

    //     $ob->revenue = $ob->net_sales - $ob->sales_refund - $ob->retex;

    //     return $ob;
    // }

     public static function salessummary1($date_from, $date_to, $request){
      
        $companies = $request->company;
        $_branches = $request->branch;
        $com  ='';
       
      
        
        $se_branches = implode("','", $_branches);
        $date_from = date("Y-m-d", strtotime($date_from));
        $date_to = date("Y-m-d", strtotime($date_to));
        $query = sprintf("select companies.company, companies.company_code,
            company_branches.branch, company_branches.branch_code,
            coalesce(sales.net_amount,0) as net_amount,
            coalesce(refund.refund_amount,0) as refund_amount,
            coalesce(return_exchanges.return_exchange_amount,0) as return_exchange_amount
            
            from company_branches
            join companies on companies.id = company_branches.company_id
            left join (
                select branch_code, sum(sale_summaries.net_amount) as net_amount
                from sale_summaries  
                where post_void = 0
                and DATE(local_time) BETWEEN '%s' AND '%s'
               
                group by branch_code
            ) sales on sales.branch_code = company_branches.branch_code
            left join (
                select branch_code, sum(amount) as refund_amount
                from refund_items
                where DATE(local_time) BETWEEN '%s' AND '%s'
                
                group by branch_code
            ) refund on refund.branch_code = company_branches.branch_code
            left join (
                select branch_code, sum(amount) as return_exchange_amount
                from return_exchange_items
                where DATE(local_time) BETWEEN '%s' AND '%s'
                
                group by branch_code
            ) return_exchanges on return_exchanges.branch_code = company_branches.branch_code
            where company_branches.branch_code in ('%s')
            %s
             
            order by companies.company, company_branches.branch",
        $date_from,$date_to,$date_from,$date_to,$date_from,$date_to, $se_branches,$com);
        
        return DB::select(DB::raw($query));
        // return $query;
    }



    public static function getDailyandMonthlySales(Request $request) {


    $today_date = date("Y-m-d");
    $request->branch  = UserBranch::getAllBranch();
    $today_sales = self::salessummary1($today_date,$today_date,$request);
    $data = [];
    foreach ($today_sales as $key => $value) {
        $data[$key]['branch_code'] = $value->branch_code;
        $data[$key]['branch']      = $value->branch;
        $data[$key]['today_sales']  = $value->net_amount - $value->refund_amount;
        $data[$key]['total_sales'] = " ";
    }

    $first = "first day of ".date("F")." ".date("Y");
    $last  = "last day of ".date("F")." ".date("Y");

    $date_from =  date("Y-m-d", strtotime(new Carbon($first)));
    $date_to =  date("Y-m-d", strtotime(new Carbon($last)));

    $monthly_sales = self::salessummary1($date_from,$date_to,$request);
    
    foreach ($monthly_sales as $key => $value) {
        for($x = 0; $x <  count($data); $x++){
            if($value->branch_code == $data[$x]['branch_code']){
                $data[$x]['total_sales'] =  $value->net_amount - $value->refund_amount ;
               
            }
        }
    }


    return $data;



    } 
}
