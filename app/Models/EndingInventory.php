<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
class EndingInventory extends Model
{
    protected $table = 'ending_inventories';

    protected $fillable = [
    	'company_code','company_name','branch_code','branch_name','barcode','itemcode','qty','end','date','description','department','category','brand','cost','srp'
    ];

    public static function findByBarcodeAndBranch($data)
    {
    	return self::where('barcode',$data['barcode'])->where('branch_code',$data['branch_code'])->
        where('itemcode',$data['itemcode'])->
        where('date',$data['date'])->first();
    }

    public static function findByBranch($brand_c,$datess)
    {
        return self::where('branch_code',$brand_c)->
        where('date',$datess)->first();
    }

    public static function search($request,$dates){
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
        
        // $query = self::where('ending_inventories.date', '=', $dates)
        //     ->where(function($query) use ($request){
        //         if(($request->has('company'))&& (!empty($request->company))){
        //             $query->whereIn('company_code',$request->company);
        //         }
        //     })
        //     ->where(function($query) use ($request){
        //         if(!empty($request->keyword)) {
        //         $filter = $request->keyword;
        //         $query->where('barcode', 'LIKE' ,"%$filter%")
        //             ->orWhere('itemcode', 'LIKE' ,"%$filter%")
        //             ->orWhere('description', 'LIKE' ,"%$filter%")
        //             ->get();
        //         }
        //     })
        //     ->where(function($query) use ($request, $branches){
        //         if(($request->has('branch'))&& (!empty($request->branch))){
        //             $query->whereIn('branch_code',$request->branch);
        //         }else{
        //             $query->whereIn('branch_code',$branches);
        //         }
        //     })
        //     ->get();

            
            $query = self::where('ending_inventories.date', '=', $dates)
            ->where(function($query) use ($request){
                if(($request->has('company'))&& (!empty($request->company))){
                    $query->whereIn('company_code',$request->company);
                }
            })
            ->where(function($query) use ($request){
                if(!empty($request->keyword)) {
                $filter = $request->keyword;
                $query->where('barcode', 'LIKE' ,"%$filter%")
                    ->orWhere('itemcode', 'LIKE' ,"%$filter%")
                    ->orWhere('description', 'LIKE' ,"%$filter%")
                    ->get();
                }
            })
            ->where(function($query) use ($request, $branches){
                if(($request->has('branch'))&& (!empty($request->branch))){
                    $query->whereIn('branch_code',$request->branch);
                }else{
                $query->whereIn('branch_code',$branches);
                }
            })
            
            ->where(function($query) use ($request){
                if(($request->has('status'))&& (!empty($request->status))){
                    if(count($request->status) == 1){
                        if($request->status[0] == 1){
                            $query->where('qty','>',0);
                        }

                        if($request->status[0] == 2){
                            $query->where('qty','<',0);
                        }

                        if($request->status[0] == 3){
                            $query->where('qty',0);
                        }
                    }

                    if(count($request->status) == 2){
                        if(in_array(1, $request->status)){
                            $query->orWhere('qty','>',0);
                        }

                        if(in_array(2, $request->status)){
                            $query->orWhere('qty','<',0);
                        }

                        if(in_array(3, $request->status)){
                            $query->orWhere('qty',0);
                        }
                    }
                }
            })
          
            ->get();


        return $query;
    }

    public static function search1($request,$dates){
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
            $query = self::where('ending_inventories.date', '=', $dates)
            ->where(function($query) use ($request){
                if(($request->has('company'))&& (!empty($request->company))){
                    $query->whereIn('company_code',$request->company);
                }
            })
            ->where(function($query) use ($request){
                if(!empty($request->keyword)) {
                $filter = $request->keyword;
                $query->where('barcode', 'LIKE' ,"%$filter%")
                    ->orWhere('itemcode', 'LIKE' ,"%$filter%")
                    ->orWhere('description', 'LIKE' ,"%$filter%")
                    ->get();
                }
            })
            ->where(function($query) use ($request, $branches){
                if(($request->has('branch'))&& (!empty($request->branch))){
                    $query->whereIn('branch_code',$request->branch);
                }else{
                     $query->whereIn('branch_code',$branches);
                }
            })
            
            ->where(function($query) use ($request){
                if(($request->has('status'))&& (!empty($request->status))){
                    if(count($request->status) == 1){
                        if($request->status[0] == 1){
                            $query->where('qty','>',0);
                        }

                        if($request->status[0] == 2){
                            $query->where('qty','<',0);
                        }

                        if($request->status[0] == 3){
                            $query->where('qty',0);
                        }
                    }

                    if(count($request->status) == 2){
                        if(in_array(1, $request->status)){
                            $query->orWhere('qty','>',0);
                        }

                        if(in_array(2, $request->status)){
                            $query->orWhere('qty','<',0);
                        }

                        if(in_array(3, $request->status)){
                            $query->orWhere('qty',0);
                        }
                    }
                }
            });
            
        return $query->paginate(100);
    }


    public static function getCompanies(){
        $companies = UserBranch::getAllowedCompany(Auth::user()->id);
        return self::select('company_name', 'company_code')
            ->whereIn('company_code', $companies)
            ->orderBy('company_name')
            ->groupBy('company_code')
            ->lists('company_name', 'company_code');
    }
}
