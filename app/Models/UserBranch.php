<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;

class UserBranch extends Model
{
    public static function addBranch($id, $request){
    	self::where('user_id', $id)->delete();
    	if($request->has('branch')){
    		$data = [];
    		foreach ($request->branch as $branch) {
    			$data[] = ['user_id' => $id, 'branch_id' => $branch];
    		}

    		if(!empty($data)){
    			self::insert($data);
    		}
    	}
    }

    public static function getBranch($id){
    	$data = [];
    	$branches = self::where('user_id', $id)->get();
    	foreach ($branches as $branch) {
    		$data[] = $branch->branch_id;
    	}
    	return $data;
    }

    public static function getUserBranch($id){
        return self::select('company_branches.branch_code', 'company_branches.branch')
            ->where('user_id', $id)
            ->join('company_branches', 'company_branches.id', '=', 'user_branches.branch_id')
            ->orderBy('company_branches.branch')
            ->lists('branch', 'branch_code');
    }

    public static function getUserBranch2($id){
        return self::select('company_branches.id', 'company_branches.branch')
            ->where('user_id', $id)
            ->join('company_branches', 'company_branches.id', '=', 'user_branches.branch_id')
            ->orderBy('company_branches.branch')
            ->lists('branch', 'id');
    }

    public static function getAllowedCompany($id){
        $records = self::select('companies.company_code')
            ->where('user_id', $id)
            ->join('company_branches', 'company_branches.id', '=', 'user_branches.branch_id')
            ->join('companies', 'companies.id', '=', 'company_branches.company_id')
            ->groupBy('company_code')
            ->get();
        $data = [];
        foreach ($records as $row) {
            $data[] = $row->company_code;
        }

        return $data;
    }

    public static function getAllowedBranch($id){
        $records = self::select('branch_code')
            ->where('user_id', $id)
            ->join('company_branches', 'company_branches.id', '=', 'user_branches.branch_id')
            ->groupBy('branch_code')
            ->get();
        $data = [];
        foreach ($records as $row) {
            $data[] = $row->branch_code;
        }

        return $data;
    }

    public static function getAllBranch(){
        $records = self::select('branch_code')
            
            ->join('company_branches', 'company_branches.id', '=', 'user_branches.branch_id')
            ->groupBy('branch_code')
            ->get();
        $data = [];
        foreach ($records as $row) {
            $data[] = $row->branch_code;
        }

        return $data;
    }

      public static function getAllowedBranchUp($id){
        $records = self::select('branch_code')
            ->where('user_id', $id)
            ->join('company_branches', 'company_branches.id', '=', 'user_branches.branch_id')
            ->where('company_branches.branch_code' ,'!=' , '00010001')
            ->groupBy('branch_code')
            ->get();
        $data = [];
        foreach ($records as $row) {
            $data[] = $row->branch_code;
        }

        return $data;
    }


    public static function getDatesFromRange( $date_time_from, $date_time_to ) {

         $start_date = date('Y-m-d HH:MM:SS', strtotime($date_time_from));
        $end_date   = date('Y-m-d HH:MM:SS', strtotime($date_time_to));
        $start      = Carbon::createFromFormat('Y-m-d', substr($start_date, 0, 10));
        $end        = Carbon::createFromFormat('Y-m-d', substr($end_date, 0, 10));
        $dates      = [];

        while ( $start->lte( $end ) ) {

            $dates[] = $start->copy()->format('Y-m-d');
            $start->addDay();
        }

        return $dates;



    }



}
