<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OtherPayment extends Model
{
    //

       protected $fillable = [
        'sale_summary_id', 
        'documnet_no',
        'amount',
       
        'created_at',
        'updated_at'
    ];
}
