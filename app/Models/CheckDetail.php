<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CheckDetail extends Model
{
    //

     protected $fillable = [
        'sale_summary_id', 
        'check_name',
        'check_number',
        'check_date',
        'bank_name',
        'bank',
        'amount',
        'created_at',
        'updated_at'
    ];
}
