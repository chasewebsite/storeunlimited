<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    protected $fillable = [
        'company_code', 'company_name', 'branch_code', 'branch_name', 'supplier' , 'terminal_code', 'terminal_no', 'user',  'ref_po', 'po_no', 'incoming_no',
        'local_time', 'barcode', 'itemcode', 'description', 'qty', 'price', 'amount', 'department', 'category', 'brand','local_date', 'abrev_desc','series_name','composition','shirt_fit','unit','sell_unit','selling_price','collar_type','color','woven_style','pattern_style','sleeve_length','neck_size'
    ];

    public static function ZapPerDay($company_code, $branch_code, $date_from)
    {
        $date = date_format($date_from,'Y/m/d');
        return self::where('company_code',$company_code)->where('branch_code',$branch_code)
            ->where('local_date',$date)->get();
    }

    public static function recordExist($data){
    	return self::where('company_code', $data['company_code'])
    		->where('branch_code', $data['branch_code'])
    		->where('terminal_code', $data['terminal_code'])
    		->where('user', $data['user'])
    		->where('po_no', $data['po_no'])
    		->where('barcode', $data['barcode'])
    		->first();
    }

    public static function getItemPo($data){
    	return self::where('company_code', $data['company_code'])
    		->where('po_no', $data['po_no'])
    		->where('barcode', $data['barcode'])
    		->first();
    }


    public static function search($date_from, $date_to ,$request){        
        
        
    	return self::where('local_date','>=',$date_from)->where('local_date','<=',$date_to)->get();
    }
}
