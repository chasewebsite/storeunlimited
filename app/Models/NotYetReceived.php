<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\OutgoingItem;
use App\Models\IncomingItem;
use App\Models\UserBranch;
use Auth;


class NotYetReceived extends Model
{
    //


      public static function search($request){
      	 $date = date("Y-m-d", strtotime($request->date));
      	 $branches = UserBranch::getAllowedBranch(Auth::user()->id);
      	$recieved_item = OutgoingItem::where('incoming_no','=', ' ')
									      	->where('to_branch_code','!=',' ')
									      	->where(function($query) use ($request){
								                if(($request->has('company'))&& (!empty($request->company))){
								                    $query->whereIn('company_code',$request->company);
								                }
           									 })
									      	->where(function($query) use ($request, $branches){
								                if(($request->has('branch'))&& (!empty($request->branch))){
								                    $query->whereIn('branch_code',$request->branch);
								                }else{
								                    $query->whereIn('branch_code',$branches);
								                }
           									 })   
									      	->where(function($query) use ($request){
								                if(($request->has('user'))&& (!empty($request->user))){
								                    $query->whereIn('user',$request->user);
								                }
           									 })
									      	->whereDate('local_date','=',$date)

									      	->get();



      	return $recieved_item;

      }




      
}
