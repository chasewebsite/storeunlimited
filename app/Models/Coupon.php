<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
	protected $fillable = [
        'c_company_name', 'c_branch_name', 'c_terminal_no', 'coupon_code', 'c_cahier', 'c_date_time', 
        'redeemed_by', 'r_cashier', 'r_company_name', 'r_branch_name', 'r_terminal_no', 'r_transaction_no', 'r_datetime', 'redeemed'
    ];
    
    public static function checkCode($code){
    	return self::where('coupon_code', $code)->first();
    }

    public static function search($date_from, $date_to){        
                
    	// return self::where('local_date','>=',$date_from)->where('local_date','<=',$date_to)->get();
    	return self::all();
    }
}
