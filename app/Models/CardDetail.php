<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CardDetail extends Model
{
    //
        protected $fillable = [
        'sale_summary_id', 
        'card_no',
        'card_name',
        'exp_date',
        'bank_name',
        'bank',
        'amount',
        'approve_no',
        'created_at',
        'updated_at'
    ];
}
