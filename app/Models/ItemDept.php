<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemDept extends Model
{
    protected $fillable = [
        'company_id', 
        'pos_type_id',
        'item_dept_code',
        'item_dept'
    ];
}
