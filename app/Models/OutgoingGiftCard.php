<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\UserBranch;
use Auth;


class OutgoingGiftCard extends Model
{
    //

       protected $fillable = [
        'outgoing_no', 'branch_code', 'branch_name', 'local_time', 'denomination', 'serial_no', 'user', 'created_at', 'updated_at'
    ];



     public static function recordExist($data){
    	return self::where('branch_code', $data['branch_code'])
    	
    		->where('user', $data['user'])
    		->where('outgoing_no', $data['outgoing_no'])
    		->where('serial_no', $data['serial_no'])
            
    		->first();
    }

     public static function getBranches(){
            $branches = UserBranch::getAllowedBranch(Auth::user()->id);
        return self::select('branch_name', 'branch_code')
            ->whereIn('branch_code', $branches)
            ->orderBy('branch_name')
            ->groupBy('branch_code')
            ->lists('branch_name', 'branch_code');
    }

     public static function search($branches,$date_from,$date_to){
            
        $datefrom = date("Y-m-d", strtotime($date_from));
        $dateto = date("Y-m-d", strtotime($date_to));           

        return self::whereIn('branch_code', $branches)
                    ->whereDate('local_time','>=',$datefrom)
                    ->whereDate('local_time', '<=', $dateto)
                    ->get();
    }


}
