<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
    	'company_id',
        'company_pos_type_id',
        'barcode',
        'itemcode',
        'desc',
        'posdesc',
        'item_group_id',
        'item_division_id',
        'item_dept_id',
        'item_category_id',
        'item_subcategory_id',
        'item_brand_id',
        'item_supplier_id',
        'item_unit_id',
        'item_color_id',
        'item_size_id',
        'item_area_id',
        'item_dept2_id',
        'item_category2_id',
        'costp',
        'markup',
        'price',
        'price2',
        'price3',
        'price1',
        'discountp',
        'discamt',
        'sdate',
        'stime',
        'edate',
        'etime',
        'opend',
        'level1',
        'level2',
        'level3',
        'level4',
        'level5',
        'levelp1',
        'levelp2',
        'levelp3',
        'levelp4',
        'levelp5',
        'perishable',
        'status',
        'buyn',
        'takeitem',
        'takeqty',
        'deffect',
        'min',
        'max',
        'lastprice',
        'lastcost',
        'lastuser',
        'compose',
        'lonsale',
        'bartype',
        'prodid',
        'cubic',
        'weight',
        'active',
        'lsurcharge',
        'surchperc',
        'discl1',
        'discl2',
        'discl3',
        'discl4',
        'discl5',
        'button',
        'item_type_id',
        'printer',
        'combo',
        'backcolor',
        'forecolor',
        'rawitem',
        'computed',
        'bracket1',
        'bracket2',
        'bracket3',
        'bracket4',
        'bracket5',
        'laskserial',
        'lnonvat',
        'dispimage',
        'vat',
        'nosurch',
        'refundable',
        'nogross',
        'nodeposit',
        'inzread',
        'shelflife',
        'bom',
        'eventcode',
        'proctime',
        'indented',
        'kdsdelay',
        'dailypcnt',
        'linactive',
        'pcntprior',
        'laskmod',
        'lcutline',
        'printer1',
        'printer2',
        'printer3',
        'lexempt',
        'stockalert',
        'sellunit_id',
        'convqty',
        'lnopsum',
        'lnolt',
        'button_i',
        'fg100',
        'progstock',
        'consumable',
        'lmobile',
        'menuprice',
        'scperc',
        'rental',
        'renthrs',
    ];

    public function isActive(){
        if($this->active){
            return 'TRUE';
        }else{
            return 'FALSE';
        }
    }

    public function company(){
        return $this->belongsTo('App\Models\Company');
    }

    public function company_pos_type(){
        return $this->belongsTo('App\Models\CompanyPosType');
    }


    public static function recordCount($filters){
        return self::select('items.id')
            ->join('company_pos_types', 'company_pos_types.id', '=', 'items.company_pos_type_id')
            ->where(function($query) use ($filters){
                if(!empty($filters['date'])){
                    $query->where('items.updated_at', '>', date('Y-m-d H:i:s',$filters['date']));
                }else{
                    $query->where('items.active', 1);
                }
            })
            ->where('company_pos_types.company_id',  $filters['company_id'])
            ->where('company_pos_types.pos_type_id',  $filters['pos_type_id'])
            ->count();

    }

    public static function getPartial($filters,$take,$skip){
    	return self::select('barcode', 'itemcode', 'desc', 'posdesc',
    		'item_group_code', 'item_group','item_division_code', 'item_division','item_dept_code', 'item_dept',
    		'item_category_code', 'item_category', 'item_subcategory_code', 'item_subcategory', 'item_brand_code', 'item_brand',
    		'item_supplier_code', 'item_supplier', 'item_units.item_unit_code', 'item_units.item_unit', 'item_color_code', 'item_color',
    		'item_size_code', 'item_size', 'item_area_code', 'item_area', 'item_dept2_code', 'item_dept2',
    		'item_category2_code', 'item_category2', 'costp', 'markup', 'price', 'price2', 'price3', 'price1',
    		'discountp', 'discamt', 'sdate', 'sdate', 'edate', 'etime', 'opend', 'level1', 'level2', 'level3', 'level4', 'level5',
    		'levelp1', 'levelp2', 'levelp3', 'levelp4', 'levelp5', 'perishable', 'status', 'buyn', 'takeitem', 'takeqty', 'deffect',
    		'min', 'max', 'lastprice', 'lastcost', 'lastuser', 'compose', 'lonsale', 'bartype', 'prodid', 'cubic', 'weight', 'active',
    		'lsurcharge', 'surchperc', 'discl1', 'discl2', 'discl3', 'discl4', 'discl5', 'button', 'item_type_code', 'item_type',
    		'printer', 'combo', 'backcolor', 'forecolor', 'rawitem', 'computed', 'bracket1', 'bracket2', 'bracket3', 'bracket4', 'bracket5',
    		'laskserial', 'lnonvat', 'dispimage', 'vat', 'nosurch', 'refundable', 'nogross', 'nodeposit', 'inzread', 'shelflife', 'bom',
    		'eventcode', 'proctime', 'indented', 'kdsdelay', 'dailypcnt', 'linactive', 'pcntprior', 'laskmod', 'lcutline', 'printer1', 'printer2', 'printer3',
    		'lexempt', 'stockalert', 'wastage', 'sellunit.item_unit_code as sellunit_code', 'convqty', 'lnopsum', 'lnolt', 'button_i', 'fg100', 'progstock',
    		'consumable', 'lmobile', 'menuprice', 'scperc', 'rental', 'renthrs'
    		)
            ->join('company_pos_types', 'company_pos_types.id', '=', 'items.company_pos_type_id')
    		->join('item_groups', 'item_groups.id', '=', 'items.item_group_id')
    		->join('item_divisions', 'item_divisions.id', '=', 'items.item_division_id')
    		->join('item_depts', 'item_depts.id', '=', 'items.item_dept_id')
    		->join('item_categories', 'item_categories.id', '=', 'items.item_category_id')
    		->join('item_subcategories', 'item_subcategories.id', '=', 'items.item_subcategory_id')
    		->join('item_brands', 'item_brands.id', '=', 'items.item_brand_id')
    		->join('item_suppliers', 'item_suppliers.id', '=', 'items.item_supplier_id')
    		->join('item_units', 'item_units.id', '=', 'items.item_unit_id')
    		->join('item_colors', 'item_colors.id', '=', 'items.item_color_id')
    		->join('item_sizes', 'item_sizes.id', '=', 'items.item_size_id')
    		->join('item_areas', 'item_areas.id', '=', 'items.item_area_id')
    		->join('item_dept2', 'item_dept2.id', '=', 'items.item_dept2_id')
    		->join('item_category2', 'item_category2.id', '=', 'items.item_category2_id')
    		->join('item_types', 'item_types.id', '=', 'items.item_type_id')
    		->join('item_units as sellunit', 'sellunit.id', '=', 'items.sellunit_id')
            ->where(function($query) use ($filters){
                if(!empty($filters['date'])){
                    $query->where('items.updated_at', '>', date('Y-m-d H:i:s',$filters['date']));
                }else{
                    $query->where('items.active', 1);
                }
            })
            ->where('company_pos_types.company_id',  $filters['company_id'])
            ->where('company_pos_types.pos_type_id',  $filters['pos_type_id'])
            ->orderBy('barcode')
    		->skip($skip*$take)
			->take($take)
			->get();

    }

    public static function search($request){
        return self::with('company')
            ->with('company_pos_type')
            ->orderBy('updated_at', 'desc')
            ->paginate(100)
            ->appends(['search' => $request->search]);
    }
}
