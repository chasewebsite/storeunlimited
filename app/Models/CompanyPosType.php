<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyPosType extends Model
{
    protected $fillable = [
        'company_id', 'pos_type_id'
    ];

    public function pos_type(){
    	return $this->belongsTo('App\Models\PosType', 'pos_type_id', 'id');
    }
}
