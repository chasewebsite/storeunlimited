<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use DB;
use App\Models\CompanyBranch;
use Auth;
use App\Models\UserBranch;

class GiftsCard extends Model
{
    //
    protected $fillable = [
       
        'serial_no',
        'opened',
        'redeemed',
        'branch_name_opened',
        'branch_code_opened',
        'branch_name_redeemed',
        'branch_code_redeemed',
        'opened_date',
        'redeemed_date',
        'transaction_no',
        'created_at',
        'updated_at','branch_code','branch_name','denomination','user','member','amount','open_tseqno','close_tseqno'
    ];

      public static function uploadGiftCard($request){
    	$destinationPath = storage_path().'/uploads/gc1/';
        $fileName = $request->file('file')->getClientOriginalName();

        $request->file('file')->move($destinationPath, $fileName);

        $filePath = storage_path().'/uploads/gc1/' . $fileName;

  //       $reader = ReaderFactory::create(Type::XLSX); // for XLSX files
		// $reader->open($filePath);

		DB::beginTransaction();
		try {
		// 		foreach ($reader->getSheetIterator() as $sheet) {
				
		// 				$cnt = 0;
					
		// 				foreach ($sheet->getRowIterator() as $row) {

		// 					if($row[0] != ''){
		// 						if($cnt > 0 && $cnt < 1501){
				
		// 							$gift = self::where('serial_no',$row[0])->first();
		// 							if(count($gift) == 0){

		// 								self::firstOrCreate([
		// 									'serial_no' => $row[0]
											
		// 								]);


		// 							}

		// 						}
		// 						$cnt++;

		// 					}
		// 				}
					
		// 		}

		// $reader->close();

			 \Excel::load($filePath, function($reader) {
			    $results = $reader->get();
				$cnt = 0;
                foreach ($results as $key => $row) {
                	if($row->gcno != ''){
								if($cnt < 1500){
				
									$gift = self::where('serial_no',$row->gcno)->first();
									if(count($gift) == 0){
										$name = " ";
										$name = CompanyBranch::where('branch_code',$row->branch_code)->first()->branch;
										self::firstOrCreate([
											'serial_no' => $row->gcno,
											'denomination' =>$row->denomination,
											'branch_code'  => $row->branch_code,
											'branch_name' => $name
											
										]);


									}

								}
								$cnt++;

							}
			    }
			});



		DB::commit();
            return true;
		} catch (Exception $e) {
            DB::rollback();
            return false;
        }











       
   //      DB::beginTransaction();
   //      try {
   //          \Excel::load($filePath, function($reader) {
			//     $results = $reader->get();
   //              foreach ($results as $key => $row) {
   //              	$data['serial_no'] = $row->serialno;
			//     	$gc = self::recordExist($data);
			//     	if(empty($gc)){
			//     		self::create(['company_code' => $row->company_code,
			//     			'serial_no' => $row->serialno,
			//     			'start_date' => date("Y-m-d", strtotime($row->start_date)),
			//     			'end_date' => date("Y-m-d", strtotime($row->end_date))]);
			//     	}
			//     }
			// });

            
   //          DB::commit();
   //          return true;
   //      } catch (Exception $e) {
   //          DB::rollback();
   //          return false;
   //      }
    }

    public function opened()
    {
        if($this->opened == true){
            return 'Yes';
        }else{
            return 'No';
        }
    }

     public function redeemed()
    {
        if($this->redeemed == true){
            return 'Yes';
        }else{
            return 'No';
        }
    }

     public function activated()
    {
        if($this->activate == true){
            return 'Yes';
        }else{
            return 'No';
        }
    }


    public static function getStatus() {


        return $array  = ['Close' => 'Close' , 'Open' => 'Open' , 'Activate' => 'Activate' , 'Redeem' =>'Redeem'];
    }

    public static function search($request){



        $date_from = date("Y-m-d", strtotime($request->date_from));
        $date_to = date("Y-m-d", strtotime($request->date_to));    

    	return self::where(function($query) use ($request,$date_from,$date_to){

              
                          if(!empty($request->status)) {
                           if(in_array('Close', $request->status)){  
                                     $query->where('opened',0)->where('activate',0)->where('redeemed',0)->whereDate('created_at','>=',$date_from)->whereDate('created_at','<=',$date_to);
                           }
                           if(in_array('Open', $request->status)){  
                                    $query->where('opened',1)->whereDate('created_at','>=',$date_from)->whereDate('created_at','<=',$date_to);  
                           }
                           if(in_array('Activate', $request->status)){
                                    $query->where('activate',1)->whereDate('activated_date','>=',$date_from)->whereDate('activated_date','<=',$date_to);    
                           }
                           if(in_array('Redeem', $request->status)){
                                    $query->where('redeemed',1)->whereDate('redeemend_date','>=',$date_from)->whereDate('created_at','<=',$date_to);    
                           }
                          }
                          else {

                             $query->whereDate('updated_at','>=',$date_from)->whereDate('updated_at','<=',$date_to); 



                          }      

               
                 })
              ->where(function($query) use ($request,$date_from,$date_to){
                  if(!empty($request->branch)) {


                    $query->whereIn('branch_code',$request->branch);
                  }


                 })
           
                ->get();


    			// if(count($request->status) == 1) {
       //                      if($request->status[0] == 'Close') {
       //                          $query->where('opened',0)->where('activate',0)->where('redeemed',0)->whereDate('created_at','>=',$date_from)->whereDate('created_at','<=',$date_to);
       //                      }
       //                      elseif ($request->status[0] == 'Open') {
       //                                     $query->where('opened',1)->whereDate('created_at','>=',$date_from)->whereDate('created_at','<=',$date_to);       
       //                      }
       //                      elseif ($request->status[0] == 'Activate') {
       //                                     $query->where('activate',1)->whereDate('activated_date','>=',$date_from)->whereDate('activated_date','<=',$date_to);       
       //                      }
       //                          elseif ($request->status[0] == 'Redeem') {
       //                                            $query->where('redeemed',1)->whereDate('redeemend_date','>=',$date_from)->whereDate('created_at','<=',$date_to); 
       //                     }      


       //           }

                 // else if (count($request->status)  > 1  && count($request->status) < 4  )  {

                 //            foreach ($request->status as $key => $value) {
                                





                 //            }



                 // }






                 // elseif (count($request->status) == 1) {
                 //            if($request->status == 0) {
                 //            $query->where('opened',1)->whereDate('created_at','>=',$request->date_from)->whereDate('created_at','<=',$request->date_to);
                 //                }
                 //                elseif ($request->status == 1) {
                 //                           $query->where('activate',1)->whereDate('activated_date','>=',$request->date_from)->whereDate('activated_date','<=',$request->date_to);       
                 //                }
                 //                elseif ($request->status == 2) {
                 //                                  $query->where('redeemed',1)->whereDate('redeemend_date','>=',$request->date_from)->whereDate('created_at','<=',$request->date_to); 
                 //                }      


                 // }




            	




    }
    public static function getBranches(){
    	
     

            return UserBranch::select('company_branches.branch_code', 'company_branches.branch')
            ->where('user_id', Auth::user()->id)
            ->join('company_branches', 'company_branches.id', '=', 'user_branches.branch_id')
            ->orderBy('company_branches.branch')
            ->lists('branch', 'branch_code');



    }
}
