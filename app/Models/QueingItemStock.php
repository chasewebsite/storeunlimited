<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QueingItemStock extends Model
{
    //
	protected $table = "queing_itemstocks";

     protected $fillable = [
        'filename', 'processing', 'que'
    ];
}
