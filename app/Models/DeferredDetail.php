<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeferredDetail extends Model
{
    //

    protected $fillable = [
        'sale_summary_id', 
        'card_name',
        'card_no',
        'exp_date',
        'bank_name',
        'bank',
        'amount',
        'months',
        'approve_no',
        'created_at',
        'updated_at'
    ];
}
