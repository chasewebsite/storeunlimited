<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemCategory extends Model
{
    protected $fillable = [
        'company_id', 
        'pos_type_id',
        'item_category_code',
        'item_category'
    ];
}
