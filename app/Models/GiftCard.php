<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

use Auth;

class GiftCard extends Model
{
	protected $fillable = [
        'company_code', 
        'serial_no',
        'start_date',
        'end_date',
        'branch_code',
        'branch_name',
        'terminal_code',
        'terminal_no',
        'transaction_no',
        'user',
        'member',
        'local_time',
        'sale_summary_id',
        'gift_name',
        'amount',
        'account_name',
        'approved_no',
        'corporate'


    ];

    public static function search(){
    	return self::join('companies', 'companies.company_code', '=', 'gift_cards.company_code')
    		->get();
    }
       public static function search1($request) {
         $datefrom = date("Y-m-d", strtotime($request->date_from));
        $dateto = date("Y-m-d", strtotime($request->date_to));          
      return   self::where(function($query) use ($request){
                if(($request->has('branch'))&& (!empty($request->branch))){
                    $query->whereIn('branch_code',$request->branch);
                }
            })
            ->where(function($query) use ($request){
                if(($request->has('gifts'))&& (!empty($request->gifts))){
                    $query->whereIn('gift_name',$request->gifts);
                }
            })->whereDate('local_time','>=',$datefrom)
            ->whereDate('local_time', '<=', $dateto)
            ->get();



    }
    public static function getCompaniesBranch(){
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
        return self::select('branch_name', 'branch_code')
      
            ->whereIn('branch_code', $branches)
            ->orderBy('branch_name')
            ->groupBy('branch_code')
            ->lists('branch_name', 'branch_code');
    }
     public static function getGiftNames(){
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
        return self::select('gift_name')
      
            ->whereIn('branch_code', $branches)
            ->orderBy('gift_name')
            ->groupBy('gift_name')
            ->lists('gift_name', 'gift_name');
    }


    public static function recordExist($data){
    	return self::where('company_code', $data['company_code'])
    		->where('serial_no', $data['serial_no'])
    		->first();
    }

    public static function uploadGiftCard($request){
    	$destinationPath = storage_path().'/uploads/gc/';
        $fileName = $request->file('file')->getClientOriginalName();

        $request->file('file')->move($destinationPath, $fileName);

        $filePath = storage_path().'/uploads/gc/' . $fileName;
       
        DB::beginTransaction();
        try {
            \Excel::load($filePath, function($reader) {
			    $results = $reader->get();
                foreach ($results as $key => $row) {
                	$data['company_code'] = $row->company_code;
                	$data['serial_no'] = $row->serialno;
			    	$gc = self::recordExist($data);
			    	if(empty($gc)){
			    		self::create(['company_code' => $row->company_code,
			    			'serial_no' => $row->serialno,
			    			'start_date' => date("Y-m-d", strtotime($row->start_date)),
			    			'end_date' => date("Y-m-d", strtotime($row->end_date))]);
			    	}
			    }
			});

            
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollback();
            return false;
        }
    }
}
