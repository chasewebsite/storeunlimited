<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemColor extends Model
{
    protected $fillable = [
        'company_id', 
        'pos_type_id',
        'item_color_code',
        'item_color'
    ];
}
