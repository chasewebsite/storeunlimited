<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemType extends Model
{
    protected $fillable = [
        'company_id', 
        'pos_type_id',
        'item_type_code',
        'item_type'
    ];
}
