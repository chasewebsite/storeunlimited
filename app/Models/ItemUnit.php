<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemUnit extends Model
{
    protected $fillable = [
        'company_id', 
        'pos_type_id',
        'item_unit_code',
        'item_unit'
    ];
}
