<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Box\Spout\Common\Type;  
use Box\Spout\Reader\ReaderFactory;

use App\Models\MarketingMonth;
use App\Models\CompanyBranch;

class MarketingBudget extends Model
{
    //
        protected $fillable = [
        'id', 
        'marketing_month_id',
        'branch_code',
        'branch_name',
        'budget',
        'created_at',
        'updated_at'
    ];


    public static function import($file_path) {

    	  try { //try part

            $reader = ReaderFactory::create(Type::XLSX);
            $reader->open($file_path);


            foreach ( $reader->getSheetIterator() as $sheet ) { // read sheet
            	$cnt = 0;
            	$description = $sheet->getName();

            	$month = MarketingMonth::firstOrCreate([ 'description' => $description ]);
            	
                foreach ($sheet->getRowIterator() as $row ){ // read by row
                    if($cnt > 0){
                        
                    	$branch = CompanyBranch::where('branch_code',trim($row[1]))->first();

                    	if(count($branch) > 0) {

                            $check =  MarketingBudget::where('marketing_month_id' , $month->id)->where('branch_code' , $branch->branch_code)->first();


                            if(count($check) > 0 ){
                                $check->budget = trim($row[2]);
                                $check->update();

                            }
                            else {

                    			MarketingBudget::firstOrCreate([ 'marketing_month_id' => $month->id , 'branch_code' => $branch->branch_code , 'branch_name' => $branch->branch , 'budget' => trim($row[2]) ]);
                            }

                    	}





                    }
                    $cnt++;

                    
                } //end of reading by row




            } // end of sheet

            $data = ['status' => 1 ];
            \DB::commit();
            

         	return $data;
          






        } //end of try 




        catch (\Exception $e) {
            \DB::rollback();
            dd($e);
        }


    }

}
