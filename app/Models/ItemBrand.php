<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemBrand extends Model
{
    protected $fillable = [
        'company_id', 
        'pos_type_id',
        'item_brand_code',
        'item_brand'
    ];
}
