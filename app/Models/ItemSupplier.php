<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemSupplier extends Model
{
    protected $fillable = [
        'company_id', 
        'pos_type_id',
        'item_supplier_code',
        'item_supplier'
    ];
}
