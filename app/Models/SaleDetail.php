<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class SaleDetail extends Model
{
    protected $fillable = [
        'sale_summary_id', 'ctr', 'barcode', 'itemcode', 'description', 'cost', 'srp', 'qty', 'gross_amount', 'net_amount', 'discount', 'department', 'category', 'brand', 'metal','global_status', 'campaign_id','abrev_desc','series_name','composition','shirt_fit','supplier','unit','sell_unit','selling_price','collar_type','color','woven_style','pattern_style','sleeve_length','neck_size'
    ];

    public static function recordExist($data){
    	return self::where('sale_summary_id', $data['sale_summary_id'])
    		->where('barcode', $data['barcode'])
    		->first();
    }
    public static function ZapPerDay($id)
    {
        return self::where('sale_summary_id',$id)->get();
    }
    public static function detailsExist($data){
        return self::where('sale_summary_id', $data['sale_summary_id'])
            ->where('ctr', $data['ctr'])
            ->where('barcode', $data['barcode'])
            ->where('itemcode', $data['itemcode'])
            ->where('description', $data['description'])
            ->where('cost', $data['cost'])
            ->where('srp', $data['srp'])
            ->where('qty', $data['qty'])
            ->where('gross_amount', $data['gross_amount'])
            ->where('net_amount', $data['net_amount'])
            ->where('discount', $data['discount'])
            ->first();
    }

    public static function search(){
    	return self::all();
    }

    public static function exportRevenue($date_from, $date_to, $request){
        return self::select(DB::raw('itemcode, sum(qty) as qty, sum(srp) as revenue'))
            ->join('sale_details', 'sale_details.sale_summary_id', '=', 'sale_summaries.id')
            ->where(function($query) use ($request){
            if(($request->has('company'))&& (!empty($request->company))){
                    $query->whereIn('company_code',$request->company);
                }
            })
            ->where(function($query) use ($request){
            if(($request->has('branch'))&& (!empty($request->branch))){
                    $query->whereIn('branch_code',$request->branch);
                }
            })
            ->where(function($query) use ($request){
            if(($request->has('terminal'))&& (!empty($request->terminal))){
                    $query->whereIn('terminal_no',$request->terminal);
                }
            })
            ->where(function($query) use ($request){
            if(($request->has('user'))&& (!empty($request->user))){
                    $query->whereIn('user',$request->user);
                }
            })
            ->where(function($query) use ($request){
            if(($request->has('member'))&& (!empty($request->member))){
                    $query->whereIn('member',$request->member);
                }
            })
            ->groupBy('itemcode')
            ->get();
    }

    public static function getDetails($date_from, $date_to, $branch_code,$item){
        return self::select('sale_details.barcode', 'sale_details.itemcode', 'sale_details.description', 'sale_details.cost', 
            'sale_details.srp', 'sale_details.qty', 'sale_details.gross_amount', 'sale_details.net_amount', 'sale_details.discount')
            ->join('sale_summaries', 'sale_summaries.id', '=', 'sale_details.sale_summary_id')
              ->where(function($query) use ($item){
                if(!empty($item)) {
                $filter = $item;
                $query->where('sale_details.description', 'LIKE' ,"%$filter%")->get();
                }
            })
            ->where('sale_summaries.branch_code', $branch_code)
            ->whereDate('sale_summaries.local_time','>=',$date_from)
            ->whereDate('sale_summaries.local_time','<=',$date_to)
            ->where('sale_summaries.post_void',0)
            ->get();
    }


    public static function consolidatedItemReport($date_from,$date_to,$branch_code){
        return self::select(\DB::raw('sale_details.barcode, sale_details.itemcode , sale_details.description , sum(sale_details.qty) as total_qty , sum(sale_details.net_amount) as total_net_amount'))
            ->join('sale_summaries', 'sale_summaries.id', '=', 'sale_details.sale_summary_id')
              
            ->where('sale_summaries.branch_code', $branch_code)
            ->whereDate('sale_summaries.local_time','>=',$date_from)
            ->whereDate('sale_summaries.local_time','<=',$date_to)
            ->where('sale_summaries.post_void',0)
            ->groupBy('sale_details.itemcode')
            ->get();


    }

    public static function itemranking($date_from, $date_to, $request){
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
        $globals = SaleDetail::getGlobalStatus();
        $camps = SaleDetail::getCampIds();
        $metals = SaleDetail::getMetals();

        $companies = $request->company;

        if(($request->has('mets'))&&(!empty($request->mets))){
            $_metals = implode("','", $request->mets);
        }else{
            $_metals = implode("','", $metals);
        }
        if(($request->has('globals'))&&(!empty($request->globals))){
            $_globals = implode("','", $request->globals);
        }else{
            $_globals = implode("','", $globals);
        }
        if(($request->has('campids'))&&(!empty($request->campids))){
            $_campids = implode("','",$request->campids);
        }else{
            $_campids = implode("','", $camps);
        }

        if(($request->has('branch'))&& (!empty($request->branch))){
            $_branches = $request->branch;
        }else{
            $_branches = $branches;
        }
        $com  ='';
        if(!empty($companies)){
            $com = implode("','", $companies);
        }   
        
        $se_branches = implode("','", $_branches);
        $date_from = date("Y-m-d", strtotime($date_from));
        $date_to = date("Y-m-d", strtotime($date_to));
        if((!empty($request->mets))&&(!empty($request->globals))&&(!empty($request->campids))){
            $query = sprintf("select barcode, global_status, campaign_id, metal, itemcode ,description ,sum(qty) as qty,
                sum(sale_details.net_amount) as net_amount 
                from sale_details
                join sale_summaries on sale_summaries.id = sale_details.sale_summary_id
                
                where post_void = 0
                and DATE(local_time) BETWEEN '%s' AND '%s'
                and sale_summaries.branch_code in ('%s')
                and sale_summaries.company_code in ('%s')
                and sale_details.global_status in ('%s')
                and sale_details.campaign_id in ('%s')
                and sale_details.metal in ('%s')
                group by itemcode
                order by net_amount asc",
            $date_from,$date_to,$se_branches, $com, $_globals, $_campids, $_metals);
        }else{
            $query = sprintf("select barcode, global_status, campaign_id, metal, itemcode ,description ,sum(qty) as qty,
                sum(sale_details.net_amount) as net_amount 
                from sale_details
                join sale_summaries on sale_summaries.id = sale_details.sale_summary_id
                
                where post_void = 0
                and DATE(local_time) BETWEEN '%s' AND '%s'
                and sale_summaries.branch_code in ('%s')
                and sale_summaries.company_code in ('%s')
                group by itemcode
                order by net_amount asc",
            $date_from,$date_to,$se_branches, $com);
        }
        
        $items =  DB::select(DB::raw($query));

        

        $query_return = sprintf("select barcode, itemcode ,description, qty, amount
            from refund_items
            where DATE(local_time) BETWEEN '%s' AND '%s'
            and branch_code in ('%s')
            and company_code in ('%s')",
        $date_from,$date_to,$se_branches, $com);

        $returneds = DB::select(DB::raw($query_return));
        foreach ($returneds as $retuned) {
            for($x = 0; $x <  count($items); $x++){
                if($retuned->itemcode == $items[$x]->itemcode){
                    $items[$x]->qty = $items[$x]->qty - $retuned->qty;
                    $items[$x]->net_amount = $items[$x]->net_amount - $retuned->amount;
                }
            }
        }

        $query_retex = sprintf("select barcode, itemcode ,description, qty, amount
            from return_exchange_items
            where DATE(local_time) BETWEEN '%s' AND '%s'
            and branch_code in ('%s')
            and company_code in ('%s')",
        $date_from,$date_to,$se_branches, $com);

        $retexs = DB::select(DB::raw($query_retex));
        foreach ($retexs as $retex) {
            for($x = 0; $x <  count($items); $x++){
                if($retex->itemcode == $items[$x]->itemcode){
                    $items[$x]->qty = $items[$x]->qty - $retex->qty;
                   
                }
            }
        }

//      return   $array = array_values(array_multisort($items, function ($value) {
//     return $value->net_amount;



// }),SORT_DESC);


        // for ($i =   0;  $i  <   sizeof($items);   $i++)   {
        //     for ($j=$i+1;$j <  sizeof($items);  $j++)   {

        //         if  ($items[$i]->net_amount  <   $items[$j]->net_amount) {
        //                         $c  =   $items[$i];
        //                         $items[$i]    =   $items[$j];
        //                         $items[$j]    =   $c;
        //             }
        //         }
        // }


         return $items;
        
    }


        public static function itemranking1($date_from, $date_to, $request){
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
        $companies = $request->company;

        if(($request->has('branch'))&& (!empty($request->branch))){
            $_branches = $request->branch;
        }else{
            $_branches = $branches;
        }
        $com  ='';
        if(!empty($companies)){
            $com = implode("','", $companies);
        }   

        $se_branches = implode("','", $_branches);
        $date_from = date("Y-m-d", strtotime($date_from));
        $date_to = date("Y-m-d", strtotime($date_to));
        $query = sprintf("select barcode, itemcode ,description ,sum(qty) as qty,
            sum(sale_details.net_amount) as net_amount 
            from sale_details
            join sale_summaries on sale_summaries.id = sale_details.sale_summary_id
            
            where post_void = 0
            and DATE(local_time) BETWEEN '%s' AND '%s'
            and sale_summaries.branch_code in ('%s')
            and sale_summaries.company_code in ('%s')
            group by itemcode
            order by net_amount asc",
        $date_from,$date_to,$se_branches, $com);
        
        $items =  DB::select(DB::raw($query));

        

        $query_return = sprintf("select barcode, itemcode ,description, qty, amount
            from refund_items
            where DATE(local_time) BETWEEN '%s' AND '%s'
            and branch_code in ('%s')
            and company_code in ('%s')",
        $date_from,$date_to,$se_branches, $com);

        $returneds = DB::select(DB::raw($query_return));
        foreach ($returneds as $retuned) {
            for($x = 0; $x <  count($items); $x++){
                if($retuned->itemcode == $items[$x]->itemcode){
                    $items[$x]->qty = $items[$x]->qty - $retuned->qty;
                    $items[$x]->net_amount = $items[$x]->net_amount - $retuned->amount;
                }
            }
        }

        $query_retex = sprintf("select barcode, itemcode ,description, qty, amount
            from return_exchange_items
            where DATE(local_time) BETWEEN '%s' AND '%s'
            and branch_code in ('%s')
            and company_code in ('%s')",
        $date_from,$date_to,$se_branches, $com);

        $retexs = DB::select(DB::raw($query_retex));
        foreach ($retexs as $retex) {
            for($x = 0; $x <  count($items); $x++){
                if($retex->itemcode == $items[$x]->itemcode){
                    $items[$x]->qty = $items[$x]->qty - $retex->qty;
                    $items[$x]->net_amount = $items[$x]->net_amount - $retex->amount;
                }
            }
        }

//      return   $array = array_values(array_multisort($items, function ($value) {
//     return $value->net_amount;



// }),SORT_DESC);


        // for ($i =   0;  $i  <   sizeof($items);   $i++)   {
        //     for ($j=$i+1;$j <  sizeof($items);  $j++)   {

        //         if  ($items[$i]->net_amount  <   $items[$j]->net_amount) {
        //                         $c  =   $items[$i];
        //                         $items[$i]    =   $items[$j];
        //                         $items[$j]    =   $c;
        //             }
        //         }
        // }


         return $items;
        
    }





    public static function getUpsell(){

        $search = ['TRANS','BSL','COL','PROGRESSIVE'];        

        return self::select('sale_details.*','sale_summaries.branch_name','sale_summaries.transaction_no','sale_summaries.local_time')
            ->join('sale_summaries','sale_summaries.id','=','sale_details.sale_summary_id')
            ->where(function($query) use ($search){
                foreach ($search as $filter) {
                   $query->orWhere('sale_details.description', 'LIKE', "%$filter%");                  
                }
            })
            ->groupBy('sale_details.id')
            ->get();
    }

    public static function getUpsellBranches(){

        $search = ['TRANS','BSL','COL','PROGRESSIVE'];        

        return self::select('sale_summaries.branch_name')
            ->join('sale_summaries','sale_summaries.id','=','sale_details.sale_summary_id')
            ->where(function($query) use ($search){
                foreach ($search as $filter) {
                   $query->orWhere('sale_details.description', 'LIKE', "%$filter%");                  
                }
            })
            ->groupBy('sale_details.id')
            ->groupBy('sale_summaries.branch_name')
            ->lists('branch_name','branch_name');
    }

    public static function getBranchType($request){
        
        $types = [];
        if(!empty($request->branches)){
            $descriptions =  self::select('sale_details.description')
            ->join('sale_summaries','sale_summaries.id','=','sale_details.sale_summary_id')
            ->whereIn('sale_summaries.branch_name',$request->branches)
            ->get();        

            if(!empty($descriptions)){
                $types = ['SINGLE-VISION'=>'SINGLE-VISION'];    

                foreach($descriptions as $description){
                                    
                    if (strpos($description, 'PROGRESSIVE') !== false) {
                        $types = ['SINGLE-VISION'=>'SINGLE-VISION','PROGRESSIVE'=>'PROGRESSIVE'];
                    }   
                }            
            }            
        }        

        return $types;                        
    }

    public static function getGlobalStatus()
    {
        $results = self::select('global_status')
            ->where('sale_details.global_status','!=',null)
            ->join('sale_summaries', 'sale_summaries.id','=','sale_details.sale_summary_id')
            ->groupBy('sale_details.global_status')
            ->get();
        $data = [];
        foreach ($results as $row) {
            $data[] = $row->global_status;
        }

        return $data;
    }

    public static function getCampIds()
    {
        $camps = self::select('campaign_id')
            ->where('sale_details.global_status','!=',null)
            ->join('sale_summaries', 'sale_summaries.id','=','sale_details.sale_summary_id')
            ->groupBy('sale_details.campaign_id')
            ->get();
        $data = [];
        foreach ($camps as $camp) {
            $data[] = $camp->campaign_id;
        }

        return $data;
    }

    public static function getMetals()
    {
        $results = self::select('metal')
            ->where('sale_details.global_status','!=',null)
            ->join('sale_summaries', 'sale_summaries.id','=','sale_details.sale_summary_id')
            ->groupBy('sale_details.metal')
            ->get();
        $data = [];
        foreach ($results as $result) {
            $data[] = $result->metal;
        }

        return $data;
    }

    public static function searchUpsell($request, $date_from, $date_to){

        $datefrom = date("Y-m-d", strtotime($date_from));
        $dateto = date("Y-m-d", strtotime($date_to));


        return self::select('sale_summaries.*','sale_details.*')            
            ->join('sale_summaries','sale_summaries.id','=','sale_details.sale_summary_id')                       
            ->where(function($query) use ($request){
                $filter = $request->keyword;
                $query->where('sale_summaries.transaction_no', 'LIKE' ,"%$filter%");
                 $query->orWhere('sale_details.description', 'LIKE' ,"%$filter%");                    
                    
            })
            ->where(function($query) use ($request){
                if(($request->has('branch'))&& (!empty($request->branch))){
                    $query->whereIn('branch_name',$request->branch);
                }
            })
            ->where(function($query) use ($request){
                if( ($request->has('type')) &&  (!empty($request->type)) && (count($request->type) == 1 )  ){
                    foreach($request->type as $req_type){
                        if($req_type == "PROGRESSIVE"){
                            $query->where('sale_details.description','LIKE',"%PROGRESSIVE%");
                        }                        
                        
                    }                    
                }
            })
            ->whereDate('sale_summaries.local_time','>=',$datefrom)
            ->whereDate('sale_summaries.local_time','<=',$dateto)            
            ->get();

    }

    public static function getItems($date_from, $date_to, $branch)
    {
        set_time_limit(-1);
        $date_from = date("Y-m-d", strtotime($date_from));
        $date_to = date("Y-m-d", strtotime($date_to));

        $query = sprintf("select sale_details.itemcode, sum(sale_details.qty) as total_qty, sum(sale_details.gross_amount) as total_amount, sale_details.global_status, sale_details.campaign_id, sale_details.description from sale_details
            left join sale_summaries on sale_summaries.id = sale_details.sale_summary_id
            where DATE(sale_summaries.local_time) BETWEEN '%s' AND '%s' and sale_summaries.branch_code = '%s'
            group by sale_details.itemcode",

            $date_from,$date_to, $branch);

        $items =  DB::select(DB::raw($query));
        return $items;
    }

    public static function getItemsAll($date_from, $date_to)
    {
        set_time_limit(-1);
        $date_from = date("Y-m-d", strtotime($date_from));
        $date_to = date("Y-m-d", strtotime($date_to));

        $query = sprintf("select sale_details.itemcode, sum(sale_details.qty) as total_qty, sum(sale_details.gross_amount) as total_amount, sale_details.global_status, sale_details.campaign_id, sale_details.description from sale_details
            left join sale_summaries on sale_summaries.id = sale_details.sale_summary_id
            where DATE(sale_summaries.local_time) BETWEEN '%s' AND '%s' group by sale_details.itemcode",

            $date_from,$date_to);

        $items =  DB::select(DB::raw($query));
        return $items;
    }

    public static function getConsolidatedSaleSummary($date_from, $date_to, $branch, $item)
    {
        \Log::info($branch);
        \Log::info($item);
        set_time_limit(-1);
        
        $date_from = date("Y-m-d", strtotime($date_from));
        $date_to = date("Y-m-d", strtotime($date_to));
        // $se_branches = $branch;
        // $se_itemcode = $code;

        $query = sprintf("select sale_details.itemcode, sale_details.description, sale_details.global_status, sale_details.campaign_id, sum(sale_details.qty) as qty, sum(sale_details.gross_amount) as gross_amount, sale_summaries.branch_name from sale_details
            left join sale_summaries on sale_summaries.id = sale_details.sale_summary_id
            where DATE(sale_summaries.local_time) BETWEEN '%s' AND '%s' AND sale_summaries.branch_code = '%s' AND sale_details.itemcode = '%s'",

            $date_from,$date_to, $branch, $item);
            // $date_from,$date_to,$se_itemcode);

        $items =  DB::select(DB::raw($query));
        return $items;
    }

    public static function getConsolidatedInventorySummary($date_from, $date_to, $branch)
    {
        set_time_limit(-1);
        
        $date_from = date("Y-m-d", strtotime($date_from));
        $date_to = date("Y-m-d", strtotime($date_to));
        $se_branches = $branch;
        // $se_itemcode = $item_code; 

        $query = sprintf("select * from sale_details
            left join sale_summaries on sale_summaries.id = sale_details.sale_summary_id
            where DATE(sale_summaries.local_time) BETWEEN '%s' AND '%s'
            order by '%s' DESC",

            $date_from,$date_to, $se_branches);
            // $date_from,$date_to,$se_itemcode);

        $items =  DB::select(DB::raw($query));
        return $items;
    }
    public static function getConsolidatedItems($date_from, $date_to)
    {
         set_time_limit(-1);
        
        $date_from = date("Y-m-d", strtotime($date_from));
        $date_to = date("Y-m-d", strtotime($date_to));
        // $se_itemcode = $item_code; 

        $query = sprintf("select sale_details.itemcode from sale_details
            left join sale_summaries on sale_summaries.id = sale_details.sale_summary_id
            where DATE(sale_summaries.local_time) BETWEEN '%s' AND '%s'
            group by sale_details.itemcode",

            $date_from,$date_to);
            // $date_from,$date_to,$se_itemcode);

        $items =  DB::select(DB::raw($query));
        return $items;
    }

    public static function getItemCodes($date_from, $date_to)
    {
        set_time_limit(-1);

        $query = self::leftJoin('sale_summaries','sale_summaries.id','=','sale_details.sale_summary_id')
            ->whereBetween('sale_summaries.local_time',[$date_from,$date_to])
            ->groupBy('sale_details.itemcode')
            ->get();

        return $query;
    }

    public static function getTransactions($date_from, $date_to)
    {
        set_time_limit(-1);

        $query = self::leftJoin('sale_summaries','sale_summaries.id','=','sale_details.sale_summary_id')
            ->whereBetween('sale_summaries.local_time',[$date_from,$date_to])
            ->where('post_void',0)
            ->get();
        foreach ($query as $q) {
            $records[] = $q;
        }

        return $records;
    }

    public static function getDetailedSaleSummary($from, $to, $branch)
    {
        set_time_limit(-1);
        ini_set('memory_limit', '256M');
        
        $query = self::select('sale_details.*','sale_summaries.*')->join('sale_summaries','sale_summaries.id','=','sale_details.sale_summary_id')
            ->where('sale_summaries.post_void',0)
            ->whereDate('sale_summaries.local_time','>=',$from)
            ->whereDate('sale_summaries.local_time','<=',$to)
            ->whereIn('sale_summaries.branch_code',$branch)
            ->orderBy('transaction_no')->get();

        $query1 = DB::table('return_exchange_items')->whereIn('branch_code',$branch)
            ->whereDate('local_time','>=',$from)
            ->whereDate('local_time','<=',$to)
            ->pluck('ref_no');

        $query2 = DB::table('refund_items')->whereIn('branch_code',$branch)
            ->whereDate('local_time','>=',$from)
            ->whereDate('local_time','<=',$to)
            ->pluck('ref_no');

        $results = [];
        foreach ($query as $q) {
            if(!in_array($q->transaction_no,$query1)){
                if(!in_array($q->transaction_no,$query2)){
                    $results[] = $q;
                }
            }
        }
        return $results;
    }
}
