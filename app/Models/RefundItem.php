<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RefundItem extends Model
{
    protected $fillable = [
        'company_code', 'company_name', 'branch_code', 'branch_name', 'terminal_code', 'terminal_no', 'user', 'transaction_no', 'ref_no', 
        'local_time', 'ctr', 'barcode', 'itemcode', 'description', 'qty', 'price', 'amount', 'customer_name', 'department', 'category', 'brand', 'abrev_desc','series_name','composition','shirt_fit','supplier','unit','sell_unit','selling_price','collar_type','color','woven_style','pattern_style','sleeve_length','neck_size'
    ];

    public static function recordExist($data){
    	return self::where('company_code', $data['company_code'])
    		->where('branch_code', $data['branch_code'])
    		->where('terminal_code', $data['terminal_code'])
    		->where('transaction_no', $data['transaction_no'])
    		->where('ref_no', $data['ref_no'])
    		->where('barcode', $data['barcode'])
    		->where('ctr', $data['ctr'])
    		->first();
    }
    
    public static function search(){
    	return self::all();
    }

    public static function ZapPerDay($company_code, $branch_code, $date_from, $date_to)
    {
        return self::where('company_code',$company_code)->where('branch_code',$branch_code)
            ->whereBetween('local_time',[$date_from, $date_to])->get();
    }
}
