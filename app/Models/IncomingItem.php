<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

use App\Models\Company;
use App\Models\CompanyBranch;

class IncomingItem extends Model
{
	protected $fillable = [
        'company_code', 'company_name', 'branch_code', 'branch_name', 'terminal_code', 'terminal_no', 'user', 'outgoing_no', 'incoming_no', 'po_no',
        'local_time','local_date' ,'barcode', 'itemcode', 'description', 'qty', 'price', 'amount', 'purpose', 'box_no', 'pouch_no', 'unique_code',
        'department', 'category', 'brand' , 'from_company_code' ,'from_company' ,'from_branch_code' ,'from_branch','abrev_desc','series_name','composition','shirt_fit','supplier','unit','sell_unit','selling_price','collar_type','color','woven_style','pattern_style','sleeve_length','neck_size'
    ];

    public static function ZapPerDay($company_code, $branch_code, $date_from)
    {
        $date = date_format($date_from,'Y/m/d');
        return self::where('company_code',$company_code)->where('branch_code',$branch_code)
            ->where('local_date',$date)->get();
    }

    public static function getCompanies(){
        $companies = UserBranch::getAllowedCompany(Auth::user()->id);
        return self::select('company_name', 'company_code')
            ->whereIn('company_code', $companies)
            ->orderBy('company_name')
            ->groupBy('company_code')
            ->lists('company_name', 'company_code');
    }

    // public static function getCompaniesBranch($request){
    //     $branches = UserBranch::getAllowedBranch(Auth::user()->id);
    //     return self::select('branch_name', 'branch_code')
    //         ->whereIn('company_code', $request->companies)
    //         ->whereIn('branch_code', $branches)
    //         ->orderBy('branch_name')
    //         ->groupBy('company_code')
    //         ->groupBy('branch_code')
    //         ->lists('branch_name', 'branch_code');
    // }

    public static function getCompaniesBranch($request){

        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
        
        $company = Company::findByCode($request->companies);
        
        $abranches = CompanyBranch::getThemBranch($branches,$company);

        return $abranches;

    }

    public static function getCompaniesBranchCategory($request){
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
        return self::select('category')
            ->whereIn('company_code', $request->companies)
            ->whereIn('branch_code', $branches)
            ->orderBy('category')
            ->groupBy('company_code')
            ->groupBy('branch_code')
            ->groupBy('category')
            ->lists('category', 'category');
    }

    public static function getCompaniesBranchCategoryBrand($request){
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
        return self::select('brand')
            ->whereIn('company_code', $request->companies)
            ->whereIn('branch_code', $branches)
            ->whereIn('category', $request->categories)
            ->orderBy('brand')
            ->groupBy('company_code')
            ->groupBy('branch_code')
            ->groupBy('category')
            ->groupBy('brand')
            ->lists('brand', 'brand');
    }





    public static function recordExist($data){
    	return self::where('branch_code', $data['branch_code'])
    		->where('terminal_no', $data['terminal_no'])
    		->where('user', $data['user'])
    		->where('incoming_no', $data['incoming_no'])
    		->where('barcode', $data['barcode'])
            ->where('unique_code', $data['unique_code'])
    		->first();
    }
    
    public static function search($request,$date_from, $date_to){

        $datefrom = date("Y-m-d", strtotime($date_from));
        $dateto = date("Y-m-d", strtotime($date_to));                

    	$branches = UserBranch::getAllowedBranch(Auth::user()->id);

        return self::where('local_date','>=',$datefrom)
            ->where('local_date', '<=', $dateto)
            ->where(function($query) use ($request){
                $filter = $request->keyword;
                $query->where('incoming_no', 'LIKE' ,"%$filter%")
                    ->orWhere('po_no', 'LIKE' ,"%$filter%")
                    ->orWhere('outgoing_no', 'LIKE' ,"%$filter%")
                    ->orWhere('barcode', 'LIKE' ,"%$filter%")
                    ->orWhere('itemcode', 'LIKE' ,"%$filter%")
                    ->orWhere('description', 'LIKE' ,"%$filter%")
                    ->orWhere('unique_code', 'LIKE' ,"%$filter%")
                    ->orWhere('purpose', 'LIKE' ,"%$filter%")
                    ->get();
            })
            ->where(function($query) use ($request){
                if(($request->has('company'))&& (!empty($request->company))){
                    $query->whereIn('company_code',$request->company);
                }
            })
            ->where(function($query) use ($request, $branches){
                if(($request->has('branch'))&& (!empty($request->branch))){
                    $query->whereIn('branch_code',$request->branch);
                }else{
                    $query->whereIn('branch_code',$branches);
                }
            })         
            ->where(function($query) use ($request, $branches){
                if(($request->has('category'))&& (!empty($request->category))){
                    $query->whereIn('category',$request->category);
                }else{
                    $query->whereIn('branch_code',$branches);
                }
            })   
            ->where(function($query) use ($request, $branches){
                if(($request->has('brand'))&& (!empty($request->brand))){
                    $query->whereIn('brand',$request->brand);
                }else{
                    $query->whereIn('branch_code',$branches);
                }
            })   
            ->get();
    }    



    public static function transfer_in($branch,$date_from, $date_to){
         $filter = "ADJUSTMENT";
         $datefrom = date("Y-m-d", strtotime($date_from));
        $dateto = date("Y-m-d", strtotime($date_to)); 
            return self::where('local_date','>=',$datefrom)
            ->where('local_date', '<=', $dateto)
            ->where('branch_code',$branch->branch_code)
           ->where('purpose', 'NOT LIKE' ,"%$filter%")
            ->get();


    }
     public static function stock_issue_return($branch,$date_from, $date_to){
         $filter = "MARKETING ADJUSTMENT";
         $datefrom = date("Y-m-d", strtotime($date_from));
        $dateto = date("Y-m-d", strtotime($date_to)); 
            return self::where('local_date','>=',$datefrom)
            ->where('local_date', '<=', $dateto)
            ->where('branch_code',$branch->branch_code)
              ->where('purpose', 'LIKE' ,"%$filter%")
            ->get();


    }

    public static function getPurpose() {


        return self::select('purpose')
            ->where('purpose'," ")
            ->orderBy('purpose')
            ->groupBy('purpose')
            ->lists('purpose', 'purpose');
    }
    

}
