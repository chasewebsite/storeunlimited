<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
	protected $fillable = [
        'company_code', 
        'company'
    ];
    
    public static function search(){
        return self::all();
    }

    public static function getList(){
    	return self::lists('company', 'id');
    }

    public static function findByCode($id)
    {
        return self::where('company_code',$id)->pluck('id');
    }
}
