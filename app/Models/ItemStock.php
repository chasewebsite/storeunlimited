<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\StockMovement;
use App\Models\Company;
use App\Models\CompanyBranch;

use Auth;
use DB;

class ItemStock extends Model
{
	protected $fillable = [
        'company_code', 'company_name', 'branch_code', 'branch_name', 'barcode', 'itemcode', 'description', 'qty', 'department', 'category', 'brand' , 'cost' , 'srp', 'abrev_desc','series_name','composition','shirt_fit','supplier','unit','sell_unit','selling_price','collar_type','color','woven_style','pattern_style','sleeve_length','neck_size'
    ];

    public static function search($request){
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);

        return self::select(DB::raw("sum(qty) as total_stocks"), 'company_name', 'branch_name', 'branch_code')
            ->where(function($query) use ($request){
                if(($request->has('company'))&& (!empty($request->company))){
                    $query->whereIn('company_code',$request->company);
                }
            })
            ->where(function($query) use ($request){
                if(!empty($request->keyword)) {
                $filter = $request->keyword;
                $query->where('barcode', 'LIKE' ,"%$filter%")
                    ->orWhere('itemcode', 'LIKE' ,"%$filter%")
                    ->orWhere('description', 'LIKE' ,"%$filter%")
                    ->get();
                }
            })
            ->where(function($query) use ($request, $branches){
                if(($request->has('branch'))&& (!empty($request->branch))){
                    $query->whereIn('branch_code',$request->branch);
                }else{
                    $query->whereIn('branch_code',$branches);
                }
            })
            
            ->where(function($query) use ($request){
                if(($request->has('status'))&& (!empty($request->status))){
                    if(count($request->status) == 1){
                        if($request->status[0] == 1){
                            $query->where('qty','>',0);
                        }

                        if($request->status[0] == 2){
                            $query->where('qty','<',0);
                        }

                        if($request->status[0] == 3){
                            $query->where('qty',0);
                        }
                    }

                    if(count($request->status) == 2){
                        if(in_array(1, $request->status)){
                            $query->orWhere('qty','>',0);
                        }

                        if(in_array(2, $request->status)){
                            $query->orWhere('qty','<',0);
                        }

                        if(in_array(3, $request->status)){
                            $query->orWhere('qty',0);
                        }
                    }
                }
            })
            ->groupBy('branch_code')
            ->get();
    }

    public static function category($id){

             return self::where('branch_code',$id)
                        ->groupBy('category')
                        ->lists('category', 'category');
    }
       public static function brand($id){

             return self::where('branch_code',$id)
                        ->groupBy('category')
                        ->lists('brand', 'brand');
    } 

    public static function searchItem($request,$id) {


         return self::select('barcode', 'category', 'itemcode','description','qty')
            ->where(function($query) use ($request){
                if(!empty($request->keyword) || $request->keyword != " ") {
                $filter = $request->keyword;
                $query->where('barcode', 'LIKE' ,"%$filter%")
                    ->orWhere('itemcode', 'LIKE' ,"%$filter%")
                    ->orWhere('description', 'LIKE' ,"%$filter%")
                    ->get();
                }
            })

            ->where(function($query) use ($request){
                if(!empty($request->category)) {
                    
                    $query->whereIn('category', $request->category);
                   
                }
            })
            ->where(function($query) use ($request){
                if(!empty($request->brand)) {
               
                    $query->whereIn('brand', $request->brand);
                   
                }
            })
            ->where(function($query) use ($request){
                if(($request->has('status'))&& (!empty($request->status))){
                    if(count($request->status) == 1){
                        if($request->status[0] == 1){
                            $query->where('qty','>',0);
                        }

                        if($request->status[0] == 2){
                            $query->where('qty','<',0);
                        }

                        if($request->status[0] == 3){
                            $query->where('qty',0);
                        }
                    }

                    if(count($request->status) == 2){
                        if(in_array(1, $request->status)){
                            $query->orWhere('qty','>',0);
                        }

                        if(in_array(2, $request->status)){
                            $query->orWhere('qty','<',0);
                        }

                        if(in_array(3, $request->status)){
                            $query->orWhere('qty',0);
                        }
                    }
                }
            })
            ->where('branch_code',$id)
            ->paginate(100)
            ->appends(['keyword' => $request->keyword,'category' => $request->category ,'brand' => $request->brand,'status' => $request->status]);



    }

    public static function totalStocks($request){
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);

        return self::select(\DB::raw('sum(qty) as total'))
            ->where(function($query) use ($request){
                $filter = $request->keyword;
                $query->where('barcode', 'LIKE' ,"%$filter%")
                    ->orWhere('itemcode', 'LIKE' ,"%$filter%")
                    ->orWhere('description', 'LIKE' ,"%$filter%")
                    ->get();
            })
            ->where(function($query) use ($request){
                if(($request->has('company'))&& (!empty($request->company))){
                    $query->whereIn('company_code',$request->company);
                }
            })
            ->where(function($query) use ($request, $branches){
                if(($request->has('branch'))&& (!empty($request->branch))){
                    $query->whereIn('branch_code',$request->branch);
                }else{
                    $query->whereIn('branch_code',$branches);
                }
            })
            
            ->where(function($query) use ($request){
                if(($request->has('status'))&& (!empty($request->status))){
                    if(count($request->status) == 1){
                        if($request->status[0] == 1){
                            $query->where('qty','>',0);
                        }

                        if($request->status[0] == 2){
                            $query->where('qty','<',0);
                        }

                        if($request->status[0] == 3){
                            $query->where('qty',0);
                        }
                    }

                    if(count($request->status) == 2){
                        if(in_array(1, $request->status)){
                            $query->orWhere('qty','>',0);
                        }

                        if(in_array(2, $request->status)){
                            $query->orWhere('qty','<',0);
                        }

                        if(in_array(3, $request->status)){
                            $query->orWhere('qty',0);
                        }
                    }
                }


            })
            ->first();
    }

    public static function addStocks($data){
    	$stock = self::where('company_code', $data['company_code'])
            ->where('branch_code', $data['branch_code'])
            ->where('barcode', $data['barcode'])
            ->first();

    	if(!empty($stock)){
            $previous_stock = $stock->qty;
    		$stock->qty = $stock->qty + $data['qty'];
    		$stock->update();

            // StockMovement::create(['item_stock_id' => $stock->id,
            //     'previous_stock' =>  $previous_stock, 'move' => $data['qty'],
            //     'resulting_stock' => $previous_stock + $data['qty'], 
            //     'movement_description' => $data['movement_description'],
            //     'purpose' => $data['purpose'],
            //     'move_ref' => $data['move_ref']]);
    	}else{
    		$stock = self::create(['company_code' => $data['company_code'],
                'company_name' => $data['company_name'],
                'branch_code' => $data['branch_code'], 
                'branch_name' => $data['branch_name'], 
                'barcode' => $data['barcode'],
                'itemcode' => $data['itemcode'],
                'description' => $data['description'], 
    			'qty' => $data['qty'],
                'abrev_desc' => $data['abrev_desc'],
                'series_name' => $data['series_name'],
                'composition' => $data['composition'],
                'shirt_fit' => $data['shirt_fit'],
                'supplier' => $data['supplier'],
                'color' => $data['color'],
                'woven_style' => $data['woven_style'],
                'pattern_style' => $data['pattern_style'],
                'sleeve_length' => $data['sleeve_length'],
                'neck_size' => $data['neck_size'],
                'brand' => $data['brand']
            ]);

            // StockMovement::create(['item_stock_id' => $stock->id,
            //     'previous_stock' => 0, 'move' => $data['qty'],
            //     'resulting_stock' => $data['qty'], 
            //     'movement_description' => $data['movement_description'],
            //     'purpose' => $data['purpose'],
            //     'move_ref' => $data['move_ref']]);

    	}
    }

    public static function removeStocks($data){
    	$stock = self::where('company_code', $data['company_code'])
            ->where('branch_code', $data['branch_code'])
            ->where('barcode', $data['barcode'])
            ->first();
    	if(!empty($stock)){
            $previous_stock = $stock->qty;
    		$stock->qty = $stock->qty - $data['qty'];
    		$stock->update();

            // StockMovement::create(['item_stock_id' => $stock->id,
            //     'previous_stock' =>  $previous_stock, 'move' => $data['qty']  * -1,
            //     'resulting_stock' => $previous_stock - $data['qty'], 
            //     'movement_description' => $data['movement_description'],
            //     'purpose' => $data['purpose'],
            //     'move_ref' => $data['move_ref']]);
    	}else{
    		$stock = self::create(['company_code' => $data['company_code'],
                'company_name' => $data['company_name'],
                'branch_code' => $data['branch_code'], 
                'branch_name' => $data['branch_name'], 
                'barcode' => $data['barcode'],
                'itemcode' => $data['itemcode'],
                'description' => $data['description'],
                'abrev_desc' => $data['abrev_desc'],
                'series_name' => $data['series_name'],
                'composition' => $data['composition'],
                'shirt_fit' => $data['shirt_fit'],
                'color' => $data['color'],
                'woven_style' => $data['woven_style'],
                'pattern_style' => $data['pattern_style'],
                'sleeve_length' => $data['sleeve_length'],
                'neck_size' => $data['neck_size'],
                'brand' => $data['brand'], 
                'qty' => $data['qty'] * -1]);

            // StockMovement::create(['item_stock_id' => $stock->id,
            //     'previous_stock' => 0, 'move' => $data['qty'] * -1,
            //     'resulting_stock' => $data['qty'] * -1, 
            //     'movement_description' => $data['movement_description'],
            //     'purpose' => $data['purpose'],
            //     'move_ref' => $data['move_ref']]);

    	}
    }

    public static function getCompanies(){
        $companies = UserBranch::getAllowedCompany(Auth::user()->id);
        return self::select('company_name', 'company_code')
            ->whereIn('company_code', $companies)
            ->orderBy('company_name')
            ->groupBy('company_code')
            ->lists('company_name', 'company_code');
    }

    // public static function getCompaniesBranch($request){
    //     $branches = UserBranch::getAllowedBranch(Auth::user()->id);
    //     return self::select('branch_name', 'branch_code')
    //         ->whereIn('company_code', $request->companies)
    //         ->whereIn('branch_code', $branches)
    //         ->orderBy('branch_name')
    //         ->groupBy('company_code')
    //         ->groupBy('branch_code')
    //         ->lists('branch_name', 'branch_code');
    // }

    public static function getCompaniesBranch($request){

        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
        
        $company = Company::findByCode($request->companies);
        
        $abranches = CompanyBranch::getThemBranch($branches,$company);

        return $abranches;

    }

    public static function getItem($data){
        return self::where('company_code', $data['company_code'])
            ->where('branch_code', $data['branch_code'])
            ->where('barcode', $data['barcode'])
            ->first();
    }

    public static function getStockMovementDetailed($branch,$date_from,$date_to){
       ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300);
        $branch_code = $branch->branch_code;
        $date_from = date("Y-m-d", strtotime($date_from));
        $date_to = date("Y-m-d", strtotime($date_to));

       

        // return self::select('item_stocks.branch_code','stock_histories.transaction_date','item_stocks.category', 'item_stocks.description', 'item_stocks.itemcode', 'stock_histories.begining','stock_histories.ending','stock_histories.transaction_date')
        //         ->join('stock_histories','stock_histories.item_stock_id','=','item_stocks.id')
        //         ->where('item_stocks.branch_code',$branch_code)
        //         ->whereDate('stock_histories.transaction_date','>=',$date_from)
        //         ->whereDate('stock_histories.transaction_date','<=',$date_to)
        //         ->get();


         $stocks =   self::select('item_stocks.branch_code','stock_histories.transaction_date','item_stocks.category', 'item_stocks.description', 'item_stocks.itemcode', 'stock_histories.begining','stock_histories.ending', 'stock_histories.in_qty' , 'stock_histories.out_qty')
                ->join('stock_histories','stock_histories.item_stock_id','=','item_stocks.id')
                ->where('item_stocks.branch_code',$branch_code)
                ->whereDate('stock_histories.transaction_date','>=',$date_from)
                ->whereDate('stock_histories.transaction_date','<=',$date_to)
                ->get();


       //  $incomings =  IncomingItem::select('branch_code','local_date','category','description','itemcode' , 'qty' ,'price')
       //              ->where('branch_code',$branch_code)
       //              ->whereDate('local_date' , '>=' , $date_from)
       //              ->whereDate('local_date' , '>=' , $date_to)
       //              ->get();


       //  $outgoings = \DB::table('outgoing_items')->select('branch_code','local_date','category','description','itemcode' , 'qty' , 'price' )
       //              ->where('branch_code',$branch_code)
       //              ->whereDate('local_date' , '>=' , $date_from)
       //              ->whereDate('local_date' , '>=' , $date_to);
                   


       //  $returns    = \DB::table('return_exchange_items')->select('branch_code',\DB::raw("DATE_FORMAT(local_time,'%Y-%m-%d') as local_date"),'category','description','itemcode' , 'qty' , 'price' )
       //              ->where('branch_code',$branch_code)
       //              ->whereDate('local_time' , '>=' , $date_from)
       //              ->whereDate('local_time' , '>=' , $date_to);
                   

       //  $refunds  = \DB::table('refund_items')->select('branch_code',\DB::raw("DATE_FORMAT(local_time,'%Y-%m-%d') as local_date"),'category','description','itemcode' , 'qty' , 'price' )
       //              ->where('branch_code',$branch_code)
       //              ->whereDate('local_time' , '>=' , $date_from)
       //              ->whereDate('local_time' , '>=' , $date_to);
                  



       //  $sales  = \DB::table('sale_summaries')->select('sale_summaries.branch_code',\DB::raw(" DATE_FORMAT(sale_summaries.local_time,'%Y-%m-%d') as local_date"),'sale_details.category','sale_details.description','sale_details.itemcode' , 'sale_details.qty' ,\DB::raw('cost as price') )
       //  ->join('sale_details','sale_details.sale_summary_id','=','sale_summaries.id')
       //  ->where('sale_summaries.branch_code',$branch_code)
       //  ->whereDate('sale_summaries.local_time','>=',$date_from)
       //  ->whereDate('sale_summaries.local_time','<=',$date_to);
        

       // $union_out =  $sales->union($outgoings)->union($returns)->union($refunds)->get();
       // $union_ou1  =  $returns->union($refunds)->union($union_out)->get(); 


        //   foreach ($incomings as $incoming) {
        //     for($x = 0; $x <  count($stocks); $x++){
        //         if($incoming->itemcode == $stocks[$x]->itemcode && $incoming->local_date == $stocks[$x]->transaction_date && $incoming->branch_code ==  $stocks[$x]->branch_code){
        //             $stocks[$x]->in_stock = $stocks[$x]->in_stock +  $incoming->qty;
        //             if($incoming->price > 0) {
        //                 $stocks[$x]->price  =  $incoming->price;
        //             }
                   
        //         }
        //     }
        // }



        //  foreach ($union_out as $sale) {
        //      for($x = 0; $x <  count($stocks); $x++){
        //         if($sale->itemcode == $stocks[$x]->itemcode && $sale->local_date == $stocks[$x]->transaction_date && $sale->branch_code ==  $stocks[$x]->branch_code){
        //             $stocks[$x]->out_stock = $stocks[$x]->out_stock +  $sale->qty;
        //             if($sale->price > 0) {
        //                 $stocks[$x]->price = $sale->price;
        //             }
                   
        //         }
        //     }



        // }




        // foreach ($outgoings as $outgoing) {
        //      for($x = 0; $x <  count($stocks); $x++){
        //         if($outgoing->itemcode == $stocks[$x]->itemcode && $outgoing->local_date == $stocks[$x]->transaction_date && $outgoing->branch_code ==  $stocks[$x]->branch_code){
        //             $stocks[$x]->out_stock = $stocks[$x]->out_stock +  $outgoing->qty;
        //             if($outgoing->price > 0) {
        //                 $stocks[$x]->price  =  $outgoing->price;
        //             }
                   
        //         }
        //     }



        // }


        //  foreach ($sales as $sale) {
        //      for($x = 0; $x <  count($stocks); $x++){
        //         if($sale->itemcode == $stocks[$x]->itemcode && $sale->local_date == $stocks[$x]->transaction_date && $sale->branch_code ==  $stocks[$x]->branch_code){
        //             $stocks[$x]->out_stock = $stocks[$x]->out_stock +  $sale->qty;
        //             if($sale->price > 0) {
        //                 $stocks[$x]->price = $sale->price;
        //             }
                   
        //         }
        //     }



        // }



        //  foreach ($returns as $return) {
        //      for($x = 0; $x <  count($stocks); $x++){
        //         if($return->itemcode == $stocks[$x]->itemcode && $return->local_date == $stocks[$x]->transaction_date && $return->branch_code ==  $stocks[$x]->branch_code){
        //             $stocks[$x]->out_stock = $stocks[$x]->out_stock +  $return->qty;
        //             if($return->price > 0) {
        //                 $stocks[$x]->price = $return->price;
        //             }
                   
        //         }
        //     }



        // }



        //  foreach ($refunds as $refund) {
        //      for($x = 0; $x <  count($stocks); $x++){
        //         if($refund->itemcode == $stocks[$x]->itemcode && $refund->local_date == $stocks[$x]->transaction_date && $refund->branch_code ==  $stocks[$x]->branch_code){
        //             $stocks[$x]->out_stock = $stocks[$x]->out_stock +  $refund->qty;
        //             if($refund->price > 0) {
        //                 $stocks[$x]->price = $refund->price;
        //             }
                   
        //         }
        //     }



        // }








      return   $stocks;

 
    }

    public static function getMinDate($branch,$date_from,$date_to) {
          $branch_code = $branch->branch_code;

        return  self::select(DB::raw("min(transaction_date) as min_date"))
                        
                    ->leftJoin('stock_histories','stock_histories.item_stock_id','=','item_stocks.id')
                    ->where('item_stocks.branch_code',$branch_code)
                    ->whereDate('stock_histories.transaction_date','>=',$date_from)
                    ->whereDate('stock_histories.transaction_date','<=',$date_to)
                    ->first();
    }


    public static function getMaxDate($branch , $date_from , $date_to ) {

         $branch_code = $branch->branch_code;



        return  self::select(DB::raw("max(transaction_date) as max_date"))
                        
                    ->leftJoin('stock_histories','stock_histories.item_stock_id','=','item_stocks.id')
                    ->where('item_stocks.branch_code',$branch_code)
                    ->whereDate('stock_histories.transaction_date','>=',$date_from)
                    ->whereDate('stock_histories.transaction_date','<=',$date_to)
                    ->first(); 



    }

    public static function getMinItemCodes($branch_code,$date_from) {

         $itemcodes =  self::select( 'item_stocks.itemcode')
                        ->leftJoin('stock_histories','stock_histories.item_stock_id','=','item_stocks.id')
                        ->where('item_stocks.branch_code',$branch_code)
                        ->whereDate('stock_histories.transaction_date','=',$date_from)
                        ->groupBy('item_stocks.itemcode')
                        ->get();


        if(count($itemcodes) == 0) {

                        $new_date = date("Y-m-d", strtotime($date_from . '+ 1day'));
                        $itemcodes =  ItemStock::select( 'item_stocks.itemcode')
                        ->leftJoin('stock_histories','stock_histories.item_stock_id','=','item_stocks.id')
                        ->where('item_stocks.branch_code',$branch_code)
                        ->whereDate('stock_histories.transaction_date','=',$new_date)
                        ->groupBy('item_stocks.itemcode')
                        ->get();
        }

        $data = [];

        foreach ($itemcodes as $item) {
            $data[] = $item->itemcode;
        }
        return $data;



    }

    public static function getStockMovementSummary($branch,$date_from,$date_to){

           ini_set('memory_limit', '-1');
            $branch_code = $branch->branch_code;
            $date_from = date("Y-m-d", strtotime($date_from));
            $date_to = date("Y-m-d", strtotime($date_to));


            $itemcodes = self::getMinItemCodes( $branch_code,$date_from);
            $se_itemcodes = implode("','", $itemcodes);





           // $stocks =  self::select('item_stocks.branch_code','item_stocks.category', 'item_stocks.description', 'item_stocks.itemcode',DB::raw("sum(stock_histories.out_qty) as sum_out"),DB::raw("sum(stock_histories.in_qty) as  sum_in ") , 'begining' , 'ending')
                    
           //      ->leftJoin('stock_histories','stock_histories.item_stock_id','=','item_stocks.id')
           //      ->where('item_stocks.branch_code',$branch_code)
           //      ->whereDate('stock_histories.transaction_date','>=',$date_from)
           //      ->whereDate('stock_histories.transaction_date','<=',$date_to)
           //      ->groupBy('item_stocks.itemcode')
           //      ->get();


              


              //     $query1 = sprintf(" select begining , itemcode  from stock_histories left join item_stocks on item_stocks.id = stock_histories.item_stock_id where date(transaction_date) in (select min(transaction_date) from item_stocks left join stock_histories on item_stocks.id = stock_histories.item_stock_id where branch_code = '%s' and transaction_date >= '%s' and transaction_date <= '%s' group by itemcode) and branch_code = '%s' group by itemcode)
              //           ",
              //            $branch_code,$date_from,$date_to, $branch_code);


              // $beg =  DB::select(DB::raw($query1));

            // $beg = self::select('stock_histories.begining' , 'item_stocks.itemcode')
            //              ->leftJoin('stock_histories','stock_histories.item_stock_id','=','item_stocks.id')


                         //  ->whereIn('transaction_date', function($query) use($date_from,$date_to,$branch_code)
                         // {
                         //    $query->select(DB::raw('min(transaction_date'))
                         //          ->from('item_stocks')
                         //            ->leftJoin('stock_histories','stock_histories.item_stock_id','=','item_stocks.id')
                         //          ->where('branch_code',$branch_code)
                         //          ->whereDate('transaction_date','>=',$date_from)
                         //          ->whereDate('transaction_date', '<=' , $date_to)
                         //          ->groupBy('itemcode');
                         // })
                          // ->where('branch_code',$branch_code)
                          // ->groupBy('item_stocks.itemcode')
                          //   ->get();



        //          $incomings =  IncomingItem::select('branch_code','local_date','category','description','itemcode' ,\DB::raw('sum(qty) as qty'))
        //             ->where('branch_code',$branch_code)
        //             ->whereDate('local_date' , '>=' , $date_from)
        //             ->whereDate('local_date' , '>=' , $date_to)
        //             ->groupBy('itemcode')
        //             ->groupBy('branch_code')
        //             ->get();


        //         $outgoings = OutgoingItem::select('branch_code','local_date','category','description','itemcode' ,\DB::raw('sum(qty) as qty') )
        //             ->where('branch_code',$branch_code)
        //             ->whereDate('local_date' , '>=' , $date_from)
        //             ->whereDate('local_date' , '>=' , $date_to)
        //              ->groupBy('itemcode')
        //              ->groupBy('branch_code')
        //             ->get();

        //         $sales  = SaleSummary::select('sale_summaries.branch_code','sale_details.category','sale_details.description','sale_details.itemcode' , \DB::raw('sum(sale_details.qty) as qty') )
        //         ->leftJoin('sale_details','sale_details.sale_summary_id','=','sale_summaries.id')
        //         ->where('sale_summaries.branch_code',$branch_code)
        //         ->whereDate('sale_summaries.local_time','>=',$date_from)
        //         ->whereDate('sale_summaries.local_time','<=',$date_to)
        //         ->groupBy('sale_details.itemcode')
        //         ->groupBy('sale_summaries.branch_code')
        //         ->get();   




        //   foreach ($incomings as $incoming) {
        //     for($x = 0; $x <  count($stocks); $x++){
        //         if($incoming->itemcode == $stocks[$x]->itemcode ){
        //             $stocks[$x]->in_stock = $stocks[$x]->in_stock +  $incoming->qty;
                   
        //         }
        //     }
        // }


        // foreach ($outgoings as $outgoing) {
        //      for($x = 0; $x <  count($stocks); $x++){
        //         if($outgoing->itemcode == $stocks[$x]->itemcode ){
        //             $stocks[$x]->out_stock = $stocks[$x]->out_stock +  $outgoing->qty;
                   
        //         }
        //     }



        // }


        //  foreach ($sales as $sale) {
        //      for($x = 0; $x <  count($stocks); $x++){
        //         if($sale->itemcode == $stocks[$x]->itemcode ){
        //             $stocks[$x]->out_stock = $stocks[$x]->out_stock +  $sale->qty;
                   
        //         }
        //     }



        // }




//     SELECT  beg1.begining , end1.ending , sum(in_qty) as sum_in , sum(out_qty) as sum_out , item_stocks.itemcode , item_stocks.branch_code,
// item_stocks.branch_name 
//  from pandora.item_stocks left join pandora.stock_histories on item_stocks.id = stock_histories.item_stock_id
//  left join(
//  select begining , itemcode from stock_histories left join item_stocks on item_stocks.id = stock_histories.item_stock_id where date(transaction_date) in (select min(transaction_date) from item_stocks left join stock_histories on item_stocks.id = stock_histories.item_stock_id where branch_code = "00010013" and transaction_date >= "2017-06-01" and transaction_date <= "2017-06-30" group by itemcode) and branch_code = "00010013" group by itemcode
//  )beg1 on beg1.itemcode = item_stocks.itemcode
//  left join (
//  select ending , itemcode from stock_histories left join item_stocks on item_stocks.id = stock_histories.item_stock_id where date(transaction_date) in (select max
//  (transaction_date) from item_stocks left join stock_histories on item_stocks.id = stock_histories.item_stock_id where branch_code = "00010013" and transaction_date >= "2017-06-01" and transaction_date <= "2017-06-30" group by itemcode) and branch_code = "00010013" group by itemcode
//  )end1 on end1.itemcode = item_stocks.itemcode

//  where branch_code = "00010013" 
//  and date(transaction_date) >= "2017-06-01" 
//  and date(transaction_date) <= "2017-06-30" 
//  group by itemcode



 $query = sprintf("select beg1.begining, end1.ending, beg1.date1,
            sum(in_qty) as sum_in, sum(out_qty) as sum_out,
            item_stocks.itemcode,item_stocks.branch_code,
            item_stocks.branch_name,item_stocks.category,item_stocks.description
            
            
            from item_stocks
            left join stock_histories on item_stocks.id = stock_histories.item_stock_id
            left join (
                select begining, itemcode ,transaction_date as date1
                from stock_histories left join item_stocks on item_stocks.id = stock_histories.item_stock_id where date(transaction_date) in (select min(transaction_date) from item_stocks left join stock_histories on item_stocks.id = stock_histories.item_stock_id where branch_code = '%s' and transaction_date >= '%s' and transaction_date <= '%s' group by itemcode) and branch_code = '%s' group by itemcode
               
                
            ) beg1 on beg1.itemcode = item_stocks.itemcode
            left join (
               select ending , itemcode from stock_histories left join item_stocks on item_stocks.id = stock_histories.item_stock_id where date(transaction_date) in (select 
                    max(transaction_date) from item_stocks left join stock_histories on item_stocks.id = stock_histories.item_stock_id where branch_code = '%s' and transaction_date >= '%s' and transaction_date <= '%s' group by itemcode) and branch_code = '%s' group by itemcode
            ) end1 on end1.itemcode = item_stocks.itemcode
            
             where branch_code = '%s' 
             and date(transaction_date) >= '%s' 
             and date(transaction_date) <= '%s' 
          
             group by item_stocks.itemcode
             
            ",
        $branch_code,$date_from,$date_to,$branch_code,$branch_code,$date_from, $date_to,$branch_code,$branch_code,$date_from,$date_to);
        
        return DB::select(DB::raw($query));




        // $_query = sprintf("select beg1.begining, end1.ending,
        //     sum(in_qty) as sum_in, sum(out_qty) as sum_out,
        //     item_stocks.itemcode,item_stocks.branch_code,
        //     item_stocks.branch_name,item_stocks.category,item_stocks.description
            
            
        //     from item_stocks
        //     left join stock_histories on item_stocks.id = stock_histories.item_stock_id
        //     left join (
        //         select begining, itemcode
        //         from stock_histories left join item_stocks on item_stocks.id = stock_histories.item_stock_id where date(transaction_date) in (select min(transaction_date) from item_stocks left join stock_histories on item_stocks.id = stock_histories.item_stock_id where branch_code = '%s' and transaction_date >= '%s' and transaction_date <= '%s' group by itemcode) and branch_code = '%s' group by itemcode
               
                
        //     ) beg1 on beg1.itemcode = item_stocks.itemcode
        //     left join (
        //        select ending , itemcode from stock_histories left join item_stocks on item_stocks.id = stock_histories.item_stock_id where date(transaction_date) in (select 
        //             max(transaction_date) from item_stocks left join stock_histories on item_stocks.id = stock_histories.item_stock_id where branch_code = '%s' and transaction_date >= '%s' and transaction_date <= '%s' group by itemcode) and branch_code = '%s' group by itemcode
        //     ) end1 on end1.itemcode = item_stocks.itemcode
            
        //      where branch_code = '%s' 
        //      and date(transaction_date) >= '%s' 
        //      and date(transaction_date) <= '%s' 
        //      and item_stocks.itemcode not in ('%s')
        //      group by item_stocks.itemcode
             
        //     ",
        // $branch_code,$date_from,$date_to,$branch_code,$branch_code,$date_from, $date_to,$branch_code,$branch_code,$date_from,$date_to,$se_itemcodes);


        //    $_query1 = DB::select(DB::raw($_query));


        //    return $_query1;

    }

    public static function getBranchStocks($date_from, $date_to, $branch) 
    {
        set_time_limit(-1);
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);

        if(!empty($branch)){
            $_branches = $branch;
        }else{
            $_branches = $branches;
        }
        $se_branches = implode("','", $_branches);
        $date_from = date("Y-m-d", strtotime($date_from));
        $date_to = date("Y-m-d", strtotime($date_to));

        $query = sprintf("select item_stocks.itemcode, item_stocks.qty from item_stocks
            left join sale_details on sale_details.itemcode = item_stocks.itemcode
            left join sale_summaries on sale_summaries.id = sale_details.sale_summary_id
            where DATE(sale_summaries.local_time) BETWEEN '%s' AND '%s'
            and item_stocks.branch_code in ('%s')",

        $date_from,$date_to,$se_branches);
        
        $items =  DB::select(DB::raw($query));
        return $items;
    }

    public static function getBranchSales($date_from, $date_to, $branch) 
    {
        set_time_limit(-1);
        $branches = UserBranch::getAllowedBranch(Auth::user()->id);
        if(!empty($branch)){
            $_branches = $branch;
        }else{
            $_branches = $branches;
        }
        $se_branches = implode("','", $_branches);
        $date_from = date("Y-m-d", strtotime($date_from));
        $date_to = date("Y-m-d", strtotime($date_to));

        $query = sprintf("select sale_details.itemcode, sale_details.qty, sale_details.gross_amount 
            from sale_details
            left join item_stocks on item_stocks.itemcode = sale_details.itemcode
            left join sale_summaries on sale_summaries.id = sale_details.sale_summary_id
            where DATE(sale_summaries.local_time) BETWEEN '%s' AND '%s'
            and item_stocks.branch_code in ('%s')",

        $date_from,$date_to,$se_branches);
        
       
        
        $items =  DB::select(DB::raw($query));
        return $items;
    }

    public static function getConsolidatedSalesReport($date_from,$date_to)
    {
        set_time_limit(-1);
        
        $date_from = date("Y-m-d", strtotime($date_from));
        $date_to = date("Y-m-d", strtotime($date_to));
        // $query = sprintf("select * from sale_details
        //     left join sale_summaries on sale_summaries.id = sale_details.sale_summary_id
        //     where DATE(sale_summaries.local_time) BETWEEN '%s' AND '%s'
        //     order by '%s'",
        $query = sprintf("select * from item_stocks
            left join sale_details on sale_details.itemcode = item_stocks.itemcode
            left join sale_summaries on sale_summaries.id = sale_details.sale_summary_id
            where DATE(sale_summaries.local_time) BETWEEN '%s' AND '%s'",
            $date_from,$date_to);

        $items =  DB::select(DB::raw($query));
        return $items;
    }

    public static function getAllItem()
    {
        set_time_limit(-1);

        return self::orderBy('created_at','DESC')->get();
    }

    public static function ZapPerDay($company_code, $branch_code, $date_from, $date_to)
    {
        return self::where('company_code',$company_code)->where('branch_code',$branch_code)
            ->whereBetween('created_at',[$date_from, $date_to])->get();
    }

    public static function getItemCodes($from,$to)
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);

        return self::whereBetween('updated_at',[$from,$to])->groupBy('itemcode')->get();
    }

    public static function getAllItems($from,$to)
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);

        $results = self::whereBetween('updated_at',[$from,$to])->get();
        foreach ($results as $res) {
            $records[] = $res;
        }

        return $records;
    }
}
