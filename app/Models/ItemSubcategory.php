<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemSubcategory extends Model
{
    protected $fillable = [
        'company_id', 
        'pos_type_id',
        'item_subcategory_code',
        'item_subcategory'
    ];
}
