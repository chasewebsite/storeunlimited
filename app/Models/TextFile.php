<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TextFile extends Model
{
	protected $table = 'text_files';

	protected $fillable = [
		'filename','type'
	];
}
