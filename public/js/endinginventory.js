
$(document).ready(function(){

	function ending_inventory_branch(){
		$.ajax({
			type: "POST",
			data: {companies: getSelectedValues($('select#company :selected'))},
			url: hostname +"/api/stocksstorebranch",
			success: function(data){
				console.log(data);
				select = $('select#branch_name');
				select.empty();
				$.each(data.selection, function(i, text) {
					var sel_class = '';
					if($.inArray( i,branches) > -1){
						sel_class = 'selected="selected"';
					}
					$('<option '+sel_class+' value="'+i+'">'+text+'</option>').appendTo(select); 
				});
				select.multiselect('rebuild');
		   	},
		   	error: function(){
		   		$.alert({
				    title: 'Alert!',
				    type: 'red',
				    content: 'Company branch not found!',
				});
		   	}
		});
	}

	ending_inventory_branch();

	$('select#company').multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		enableCaseInsensitiveFiltering: true,
		enableFiltering: true,
		onDropdownHide: function(event) {
			ending_inventory_branch();
		}
	});


	$('select#branch_name').multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		enableCaseInsensitiveFiltering: true,
		enableFiltering: true,
	});


	$("#date_from, #date_to").mask("99/99/9999",{placeholder:"__/__/____"});
	$("#date_from").daterangepicker({
      	singleDatePicker: true,
    	showDropdowns: true,
    });

    $("#date_to").daterangepicker({
      	singleDatePicker: true,
    	showDropdowns: true,
    });


    $('select#status_id').multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		enableCaseInsensitiveFiltering: true,
		enableFiltering: true,
	});

});
