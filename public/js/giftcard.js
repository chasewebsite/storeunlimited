
$(document).ready(function(){

	$('select#branch').multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		enableCaseInsensitiveFiltering: true,
		enableFiltering: true
		
	});

	$('select#status').multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		enableCaseInsensitiveFiltering: true,
		enableFiltering: true
	});



	$("#date_from, #date_to").mask("99/99/9999",{placeholder:"__/__/____"});
	$("#date_from").daterangepicker({
      	singleDatePicker: true,
    	showDropdowns: true,
    });

    $("#date_to").daterangepicker({
      	singleDatePicker: true,
    	showDropdowns: true,
    });
});
