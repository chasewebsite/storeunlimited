
$(document).ready(function(){
	// Sales summary

	function updatebranch(){	
		$.ajax({
			type: "POST",
			data: {companies: getSelectedValues($('select#company :selected'))},
			url: hostname +"/api/storebranch",
			success: function(data){
				select = $('select#branch');
				select.empty();
				$.each(data.selection, function(i, text) {
					var sel_class = '';
					if($.inArray( i,branches) > -1){
						sel_class = 'selected="selected"';
					}
					$('<option '+sel_class+' value="'+i+'">'+text+'</option>').appendTo(select); 
				});
				select.multiselect('rebuild');
				updateterminal();
		   	},
		   	error: function(){
		   		$.alert({
				    title: 'Alert!',
				    type: 'red',
				    content: 'Company branch not found!',
				});
		   	}
		});
	}

	function updateterminal(){
		$.ajax({
			type: "POST",
			data: {companies: getSelectedValues($('select#company :selected')), branches: getSelectedValues($('select#branch :selected'))},
			url: hostname +"/api/branchterminal",
			success: function(data){
				select = $('select#terminal');
				select.empty();
				$.each(data.selection, function(i, text) {
					var sel_class = '';
					if($.inArray( i,terminals ) > -1){
						sel_class = 'selected="selected"';
					}
					$('<option '+sel_class+' value="'+i+'">'+text+'</option>').appendTo(select); 
				});
				select.multiselect('rebuild');
		   	},
		   	error: function(){
		   		$.alert({
				    title: 'Alert!',
				    type: 'red',
				    content: 'Branch terminal not found!',
				});
		   	}
		});
	}

	updatebranch();

	$('select#branches').multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		enableCaseInsensitiveFiltering: true,
		enableFiltering: true,
		onDropdownHide: function(event) {
			updateglobalstatus();
		}
	});

	$('select#company').multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		enableCaseInsensitiveFiltering: true,
		enableFiltering: true,
		onDropdownHide: function(event) {
			updatebranch();
		}
	});


	$('select#branch').multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		enableCaseInsensitiveFiltering: true,
		enableFiltering: true,
		onDropdownHide: function(event) {
			updateterminal();
		}
	});

	$('select#terminal').multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		enableCaseInsensitiveFiltering: true,
		enableFiltering: true,
		onDropdownHide: function(event) {

		}
	});

	$('select#user').multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		enableCaseInsensitiveFiltering: true,
		enableFiltering: true,
		
	});

	$('select#saleman').multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		enableCaseInsensitiveFiltering: true,
		enableFiltering: true,
		
	});

	$('select#member').multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		enableCaseInsensitiveFiltering: true,
		enableFiltering: true,
	});
	$("#date_from, #date_to").mask("99/99/9999",{placeholder:"__/__/____"});
	$("#date_from").daterangepicker({
      	singleDatePicker: true,
    	showDropdowns: true,
    });

    $("#date_to").daterangepicker({
      	singleDatePicker: true,
    	showDropdowns: true,
    });
     $("#date1").daterangepicker({
      	singleDatePicker: true,
    	showDropdowns: true,
    	timePicker:false,
    	 locale: {
            format: 'MM/YYYY '
        }
    });
});
