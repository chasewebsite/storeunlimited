
$(document).ready(function(){

	function updatebranch(){
		$.ajax({
			type: "POST",
			data: {companies: getSelectedValues($('select#company :selected'))},
			url: hostname +"/api/outgoingbranch",
			success: function(data){
				select = $('select#branch');
				select.empty();
				$.each(data.selection, function(i, text) {
					var sel_class = '';
					if($.inArray( i,branches) > -1){
						sel_class = 'selected="selected"';
					}
					$('<option '+sel_class+' value="'+i+'">'+text+'</option>').appendTo(select); 
				});
				select.multiselect('rebuild');
		   	},
		   	error: function(){
		   		$.alert({
				    title: 'Alert!',
				    type: 'red',
				    content: 'Company branch not found!',
				});
		   	}
		});
	}

	updatebranch();

	function updatecategory(){
		$.ajax({
			type: "POST",
			data: {companies: getSelectedValues($('select#company :selected')), branches: getSelectedValues($('select#branch :selected'))},
			url: hostname +"/api/outgoingcategory",
			success: function(data){
				select = $('select#category');
				select.empty();
				$.each(data.selection, function(i, text) {
					var sel_class = '';
					if($.inArray( i,categories) > -1){
						sel_class = 'selected="selected"';
					}
					$('<option '+sel_class+' value="'+i+'">'+text+'</option>').appendTo(select); 
				});
				select.multiselect('rebuild');
				updatebrand();
		   	},
		   	error: function(){
		   		$.alert({
				    title: 'Alert!',
				    type: 'red',
				    content: 'Category not found!',
				});
		   	}
		});
	}

	updatecategory();

	function updatebrand(){
		$.ajax({
			type: "POST",
			data: {companies: getSelectedValues($('select#company :selected')), branches: getSelectedValues($('select#branch :selected')), categories: getSelectedValues($('select#category :selected'))},
			url: hostname +"/api/outgoingbrand",
			success: function(data){
				select = $('select#brand');
				select.empty();
				$.each(data.selection, function(i, text) {
					var sel_class = '';
					if($.inArray( i,brands) > -1){
						sel_class = 'selected="selected"';
					}
					$('<option '+sel_class+' value="'+i+'">'+text+'</option>').appendTo(select); 
				});
				select.multiselect('rebuild');
		   	},
		   	error: function(){
		   		$.alert({
				    title: 'Alert!',
				    type: 'red',
				    content: 'Brand not found!',
				});
		   	}
		});
	}

	updatebrand();

	$('select#company').multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		enableCaseInsensitiveFiltering: true,
		enableFiltering: true,
		onDropdownHide: function(event) {
			updatebranch();
		}
	});


	$('select#branch').multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		enableCaseInsensitiveFiltering: true,
		enableFiltering: true,
		onDropdownHide: function(event) {
			updatecategory();
		}
	});

	$('select#category').multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		enableCaseInsensitiveFiltering: true,
		enableFiltering: true,
		onDropdownHide: function(event) {
			updatebrand();
		}
	});

	$('select#brand').multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		enableCaseInsensitiveFiltering: true,
		enableFiltering: true,
	});

	$("#date_from, #date_to").mask("99/99/9999",{placeholder:"__/__/____"});
	$("#date_from").daterangepicker({
      	singleDatePicker: true,
    	showDropdowns: true,
    });

    $("#date_to").daterangepicker({
      	singleDatePicker: true,
    	showDropdowns: true,
    });
});
