
$(document).ready(function(){

	function updatebranch(){
		$.ajax({
			type: "POST",
			data: {companies: getSelectedValues($('select#company :selected'))},
			url: hostname +"/api/get_companies",
			success: function(data){
				select = $('select#branch');
				select.empty();
				$.each(data.selection, function(i, text) {
					var sel_class = '';
					if($.inArray( i,branches) > -1){
						sel_class = 'selected="selected"';
					}
					$('<option '+sel_class+' value="'+i+'">'+text+'</option>').appendTo(select); 
				});
				select.multiselect('rebuild');
		   	},
		   	error: function(){
		   		$.alert({
				    title: 'Alert!',
				    type: 'red',
				    content: 'Company branch not found!',
				});
		   	}
		});
	}

	updatebranch();


	

	$('select#company').multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		enableCaseInsensitiveFiltering: true,
		enableFiltering: true,
		onDropdownHide: function(event) {
			updatebranch();
		}
	});


	$('select#branch').multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		enableCaseInsensitiveFiltering: true,
		enableFiltering: true
		
	});



	$("#date").mask("99/99/9999",{placeholder:"__/__/____"});
	$("#date").daterangepicker({
      	singleDatePicker: true,
    	showDropdowns: true,
    });

    
});
