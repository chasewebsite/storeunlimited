$(".confirm").confirm({
    title: 'Confirmation required!',
    content: 'Are you sure you want to delete this record?',
    buttons: {
        confirm: function () {
        	this.$target.parent().submit();
        },
        cancel: function () {
        }
    }
});