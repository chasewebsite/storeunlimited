@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')

 <!-- page content -->
<div class="right_col" role="main">
     <div class="">
        <div class="page-title">
          <!-- <div class="title_left">
            <h3>Incoming Items Report</h3>
          </div> -->

          <!-- <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button">Go!</button>
                </span>
              </div>
            </div>
          </div> -->
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Item Lists</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div>
                        {!! $items->render() !!}
                        {!! Paginate::show($items) !!}

                    </div>
                    <div class="x_content table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Company Name</th>
                                    <th>Pos Type</th>
                                    <th>Barcode</th>
                                    <th>Itemcode</th>
                                    <th>Description</th>
                                    <th>Active</th>
                                    <th>Last Updated</th>
                                    <th>Action</th>
                                </tr>
                            </thead>


                            <tbody>
                                @if($items->count() > 0)
                                @foreach($items as $item)
                                <tr>
                                    <td>{{ $item->company->company }}</td>
                                    <td>{{ $item->company_pos_type->pos_type->pos_type }}</td>
                                    <td>{{ $item->barcode }}</td>
                                    <td>{{ $item->itemcode }}</td>
                                    <td>{{ $item->desc }}</td>
                                    <td>{{ $item->isActive() }}</td>
                                    <td>{{ $item->updated_at }}</td>
                                    <td></td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="14">No record found</td>
                                </tr>
                                @endif
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </div>
</div>
<!-- /page contentss -->
    
@endsection