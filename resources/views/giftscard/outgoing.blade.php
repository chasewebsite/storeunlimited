@extends('layouts.blank')

@push('stylesheets')

@endpush

@push('inline-scripts')
   <!--  @if(isset($sel_branches))
    var branches = <?php echo json_encode($sel_branches); ?>;
    @else
    var branches = [];
    @endif

    @if(isset($sel_categories))
    var categories = <?php echo json_encode($sel_categories); ?>;
    @else
    var categories = [];
    @endif

    @if(isset($sel_brands))
    var brands = <?php echo json_encode($sel_brands); ?>;
    @else
    var brands = [];
    @endif -->
@endpush

@push('scripts')
    <script src="{{ asset("js/giftcard.js") }}"></script>
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
         <div class="">
            <div class="page-title">
              <!-- <div class="title_left">
                <h3>Incoming Items Report</h3>
              </div> -->

              <!-- <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div> -->
            </div>

            <div class="clearfix"></div>
             {!! Form::open(array('route' => array('giftscards.outgoing'), 'class' => 'form-horizontal form-label-left', 'method' => 'POST')) !!}
                  
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="x_panel">
                            <div class="row">                        
                               
                               
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="company">Branch Name</label>
                                        {!! Form::select('branch[]',$branches, null, array('id' => 'branch', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT A BRANCH...')) !!}
                                    </div>
                                </div>

                                   <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="date_from">Date From</label>
                                        {!! Form::text('date_from',$date_from,array('class' => 'form-control', 'id' => 'date_from')) !!}
                                    </div>                                    
                                </div>

                                 <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="date_to">Date To</label>
                                         {!! Form::text('date_to',$date_to,array('class' => 'form-control', 'id' => 'date_to')) !!}
                                    </div>                                
                                </div>  

                               
                              
                                      
                            </div>                    
                          
                          
                            <div class="row">
                                <div class="col-md-3 col-xs-12">
                                     <div class="btn-group">
                                        <button type="submit" class="btn btn-success" value="1" name="submit">Process</button>
                                    </div>  
                                   <!--  <div class="btn-group">
                                        <button type="submit" class="btn btn-primary" value="2" name="submit">Download</button>
                                    </div>  -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            {!! Form::close() !!}
               
               

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Outgoing GiftCard Report</h2>
                         
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content table-responsive">
                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                      
                                        <th>Branch Name</th>
                                        <th>Outgoing No.</th>
                                        <th>Serial No.</th>
                                        <th>Denomination</th>
                                        <th>User</th>
                                        
                                        <th>Posting Time</th>
                                    </tr>
                                </thead>


                                <tbody>                                                          
                                    @foreach($items as $item)                                    
                                    <tr>
                                        
                                        <td>{{ $item->branch_name }}</td>
                                       
                                        <td>{{ $item->outgoing_no }}</td>
                                        <td>{{ $item->serial_no }}</td>
                                        <td>{{ $item->denomination }}</td>
                                        <td>{{ $item->user }}</td>
                                        <td>{{ $item->local_time }}</td>
                                    </tr>
                                    @endforeach                                                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </div>
    <!-- /page content -->
@endsection