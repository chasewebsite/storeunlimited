@extends('layouts.blank')

@push('stylesheets')

@endpush

@push('scripts')
    <script src="{{ asset("js/giftcard.js") }}"></script>
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="page-title">
        </div>
          @include('includes/notifications')

        <div class="clearfix"></div>
                {!! Form::open(array('route' => array('giftscards.post'), 'class' => 'form-horizontal form-label-left', 'method' => 'POST')) !!}
                  
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="x_panel">
                            <div class="row">                        
                             
                               
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="company">Branch</label>
                                        {!! Form::select('branch[]',$sel_branches, null, array('id' => 'branch', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT A BRANCH...')) !!}
                                    </div>
                                </div>


                                    <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="company">Status</label>
                                        {!! Form::select('status[]',$status, null, array('id' => 'status', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT A Status...')) !!}
                                    </div>
                                </div>




                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="date_from">Date From</label>
                                        {!! Form::text('date_from',$date_from,array('class' => 'form-control', 'id' => 'date_from')) !!}
                                    </div>                                    
                                </div>

                                  <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="date_to">Date To</label>
                                         {!! Form::text('date_to',$date_to,array('class' => 'form-control', 'id' => 'date_to')) !!}
                                    </div>                                
                                </div> 

                               
                              
                          
                              
                                      
                            </div>                    
                           
                           
                            <div class="row">
                                <div class="col-md-3 col-xs-12">
                                     <div class="btn-group">
                                        <button type="submit" class="btn btn-success" value="1" name="submit">Process</button>
                                    </div>  
                                    <!-- <div class="btn-group">
                                        <button type="submit" class="btn btn-primary" value="2" name="submit">Download</button>
                                    </div>  -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            {!! Form::close() !!}
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Gift Card Lists</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div>
                        <a class="btn btn-success" href="{{ route('giftscards.create')}}">Upload Gift Cards</a>
                    </div>
                        <div class="x_content table-responsive">
                            <table table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                   
                                        <th>Serial No</th>
                                        <th>Denomination</th>
                                        <th>Open</th>
                                        <th>Activate</th>
                                        <th>Redeem</th>
                                        <th>Branch Activated</th>
                                        <th>Date Activated</th>
                                        <th>Branch Redeemed</th>
                                        <th>Date Redeemed</th>
                                        <th>Redeemed Transaction No</th>
                                        <th>User</th>
                                        <th>Redeemed By</th>
                                        <th>Branch</th>
                                        <th>Last Update</th>
                                        
                                    </tr>
                                </thead>


                                <tbody>
                                    @if($giftcards->count() > 0)
                                    @foreach($giftcards as $giftcard)
                                    <tr>
                                    <td> {{ $giftcard->serial_no }} </td>
                                    <td> {{ $giftcard->denomination }} </td>
                                    <td> {{ $giftcard->opened() }}</td>
                                    <td> {{ $giftcard->activated() }}</td>
                                    <td> {{ $giftcard->redeemed() }}</td>
                                    <td> {{ $giftcard->branch_name_activated }}</td>
                                    <td> {{ $giftcard->activated_date }}</td>
                                    <td> {{ $giftcard->branch_name_redeemed }}</td>
                                    <td> {{ $giftcard->redeemend_date }}</td>
                                    <td> {{ $giftcard->redeem_tseqno }}</td>
                                    <td> {{ $giftcard->user }}</td>
                                    <td> {{ $giftcard->member }}</td>
                                    <td>{{ $giftcard->branch_name }}</td>
                                    <td>{{ $giftcard->updated_at }}</td>
                                    


                                    </tr>
                                    @endforeach
                                  
                                    @endif
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </div>
    <!-- /page content -->
@endsection