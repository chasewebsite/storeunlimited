@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')

 <!-- page content -->
<div class="right_col" role="main">

    <div class="">
        <div class="page-title">
        </div>

        @include('includes/notifications')

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Upload Gift Cards</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br>

                        {!! Form::open(array('route' => 'giftscards.store','class' => 'form-horizontal form-label-left', 'files'=>true)) !!}
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {!! Form::file('file','',array('id'=>'','class'=>'')) !!}
                                    </div>
                                </div>
                            </div>

                             <div class="ln_solid"></div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                       <a class="btn btn-primary" href="{{ route('giftscards.index')}}">Cancel</a>
                                    <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
    
@endsection
