@extends('layouts.blank')

@push('stylesheets')

@endpush

@push('scripts')
    <script src="{{ asset("js/salessummary.js") }}"></script>
@endpush


@section('main_container')


 <!-- page content -->
<div class="right_col" role="main">

    <div class="">
        <div class="page-title">
        </div>

        @include('includes/notifications')

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Zap Transaction</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br>
                        {!! Form::open(array('route' => 'zap.store','class' => 'form-horizontal form-label-left')) !!}
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="width:10px;"></th>
                                    <th>Company Name</th>
                                    <th>Branch Name</th>
                                </tr>
                            </thead>
                                

                            <tbody>
                               @if(count($branches) > 0)
                                @foreach($branches as $branch)
                                <tr>
                                    <td>
                                        {{ Form::checkbox('branch[]', $branch->id) }}
                                    </td>
                                    <td>{{ $branch->company }}</td>
                                    <td>{{ $branch->branch }}</td>
                                    
                                </tr>
                                @endforeach
                                
                                @endif
                                
                            </tbody>
                        </table>

                        <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Reset Code <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::text('access_code', null, ['required' => 'required', 'class' => 'form-control col-md-7 col-xs-12']) !!}
                                </div>
                            </div>

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Reset</button>
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
    
@endsection
