@extends('layouts.blank')

@push('stylesheets')

@endpush

@push('inline-scripts')
    @if(isset($sel_branches))
    var branches = <?php echo json_encode($sel_branches); ?>;
    @else
    var branches = [];
    @endif
    @if(isset($sel_terminals))
    var terminals = <?php echo json_encode($sel_terminals); ?>;
    @else
    var terminals = [];
    @endif
@endpush

@push('scripts')
    <script src="{{ asset("js/salessummary.js") }}"></script>
@endpush

@section('main_container')



    <!-- page content -->
    <div class="right_col" role="main">
         <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Sales Summary Report</h3>
                </div>
            </div>

            <div class="clearfix"></div>



            {!! Form::open(array('route' => array('sales.store'), 'class' => 'form-horizontal form-label-left', 'method' => 'POST')) !!}
                  
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="x_panel">
                            <div class="row">
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="company">Company Name</label>
                                        {!! Form::select('company[]',  $companies, null, array('id' => 'company', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT DIVISION')) !!}
                                    </div>
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="branch">Branch Name</label>
                                        <select class="form-control" data-placeholder="SELECT A BRANCH..." id="branch" name="branch[]" multiple="multiple" ></select>
                                    </div>
                                    
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="terminal">Terminal</label>
                                        <select class="form-control" data-placeholder="SELECT A TERMINAL..." id="terminal" name="terminal[]" multiple="multiple" ></select>
                                    </div>
                                    
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="user">User</label>
                                        {!! Form::select('user[]',  $users, null, array('id' => 'user', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT USER')) !!}
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="member">Sales Man</label>
                                        {!! Form::select('saleman[]',  $saleman, null, array('id' => 'saleman', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT SALES MAN')) !!}
                                    </div>
                                    
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="member">Member</label>
                                        {!! Form::select('member[]',  $members, null, array('id' => 'member', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT MEMBER')) !!}
                                    </div>
                                    
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="date_from">Date From</label>
                                        {!! Form::text('date_from',$date_from,array('class' => 'form-control', 'id' => 'date_from')) !!}
                                    </div>
                                    
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="date_to">Date To</label>
                                         {!! Form::text('date_to',$date_to,array('class' => 'form-control', 'id' => 'date_to')) !!}
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3 col-xs-12">
                                    <div class="btn-group">
                                        <button type="submit" class="btn btn-success">Process</button>
                                    </div>  
                                    <!-- <div class="btn-group">
                                        <button type="submit" value="revenue" name="export" class="btn btn-success">Export Excel</button>
                                    </div> -->
                                    <!-- <div class="btn-group">
                                        <button type="button" class="btn btn-default">Export To Excel</button>
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="{{ route('sales.index')}}">Sales Revenue Report</a></li>
                                        </ul>
                                    </div> -->
                                </div>


                                
                            </div>



                        </div>
                    </div>
                </div>  
            {!! Form::close() !!}
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="row tile_count">
                                <div class="col-md-3 col-sm-12 col-xs-12 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-line-chart"></i> Total Net Sales</span>
                                    <div class="count">{{ number_format($summary->net_sales,3) }}</div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-reply"></i> Sales Refunded</span>
                                    <div class="count red">{{ number_format($summary->sales_refund,3) }}</div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-exchange"></i> Return Exchange Sales</span>
                                    <div class="count red">{{ number_format($summary->retex,3) }}</div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12 tile_stats_count">
                                    <span class="count_top"><i class="fa fa-dollar"></i> Revenue Sales</span>
                                    <div class="count green">{{ number_format($summary->revenue,3) }}</div>
                                </div>
                                
                              </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content table-responsive">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Company Name</th>
                                            <th>Branch Name</th>
                                            <th>Terminal #</th>
                                            <th>Transaction #</th>
                                            <th>Sales Man</th>
                                            <th>User</th>
                                            <th>Member</th>
                                            <th>Gross Amount</th>
                                            <th>Net Amount</th>
                                            <th>Sub Total Discount</th>
                                            <th>Total Item Discount</th>
                                            <th>Return Exchange Amount</th>
                                            <th>Cash Amount</th>
                                            <th>Card Amount</th>
                                            <th>Gift Amount</th>
                                            <th>Charge Amount</th>
                                            <th>Check Amount</th>
                                            <th>Account Amount</th>
                                            <th>ATM Amount</th>
                                            <th>Deffered Amount</th>
                                            <th>Other Payment</th>
                                            <th>Date Time</th>
                                            <th>Posting Time</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        @if(count($items) > 0)
                                        @foreach($items as $item)
                                        <tr>
                                            <td>{{ $item->company_name }}</td>
                                            <td>{{ $item->branch_name }}</td>
                                            <td>{{ $item->terminal_no }}</td>
                                            <td>{{ $item->transaction_no }}</td>
                                            <td>{{ $item->sales_man }}</td>
                                            <td>{{ $item->user }}</td>
                                            <td>{{ $item->member }}</td>
                                            <td>{{ number_format($item->gross_amount,2) }}</td>
                                            <td>{{ number_format($item->net_amount,2) }}</td>
                                            <td>{{ number_format($item->sub_total_discount,2) }}</td>
                                            <td>{{ number_format($item->total_item_discount,2) }}</td>
                                            <td>{{ number_format($item->retex_amount,2) }}</td>
                                            <td>{{ number_format($item->cash_amount,2) }}</td>
                                            <td>{{ number_format($item->card_amount,2) }}</td>
                                            <td>{{ number_format($item->gift_amount,2) }}</td>
                                            <td>{{ number_format($item->charge_amount,2) }}</td>
                                            <td>{{ number_format($item->check_amount,2) }}</td>
                                            <td>{{ number_format($item->account_amount,2) }}</td>
                                            <td>{{ number_format($item->atm_amount,2) }}</td>
                                            <td>{{ number_format($item->deffered_amount,2) }}</td>
                                            <td>{{ number_format($item->other_payment,2) }}</td>
                                            <td>{{ $item->local_time }}</td>
                                            <td>{{ $item->created_at }}</td>
                                            <td><a href="{{ route('sales.show', $item->id)}}"> Details</a></td>
                                        </tr>
                                        @endforeach
                                        
                                        @endif
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            
          </div>
    </div>
    <!-- /page content -->

@endsection