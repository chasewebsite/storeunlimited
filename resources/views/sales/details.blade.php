@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
         <div class="">
            <div class="page-title">
              <div class="title_left">
                <a href="{{ route('sales.show', ['id' => $summary->branch_code, 'from' => strtotime($date_from), 'to' => strtotime($date_to)])}}" class="btn btn-default">Back</a>
              </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Trasaction Detail Report</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content table-responsive">
                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Barcode</th>
                                        <th>Itemcode</th>
                                        <th>Description</th>
                                        <th>Cost</th>
                                        <th>SRP</th>
                                        <th>Qty</th>
                                        <th>Gross Amount</th>
                                        <th>Net Amount</th>
                                        <th>Discount</th>
                                    </tr>
                                </thead>


                                <tbody>
                                    @if(count($sale->details) > 0)
                                    @foreach($sale->details as $item)
                                    <tr>
                                        <td>{{ $item->barcode }}</td>
                                        <td>{{ $item->itemcode }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td>{{ $item->cost }}</td>
                                        <td>{{ $item->srp }}</td>
                                        <td>{{ $item->qty }}</td>
                                        <td>{{ $item->gross_amount }}</td>
                                        <td>{{ $item->net_amount }}</td>
                                        <td>{{ $item->discount }}</td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="13">No record found</td>
                                    </tr>
                                    @endif
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </div>
    <!-- /page content -->
@endsection