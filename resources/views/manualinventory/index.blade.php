@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
        </div>
          @include('includes/notifications')

        <div class="clearfix"></div>


        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Manual Inventory</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div>
                        <a class="btn btn-success" href="{{ route('manualinventory.create')}}">Upload Inventory List</a>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Branch Code</th>
                                    <th>Branch Name</th>
                                    <th>Voucher Number</th>
                                    <th>File Name</th>
                                    <th>Date Uploaded</th>
                                    <th class="action">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                               @if($files->count() > 0)
                                @foreach($files as $file)
                                <tr>
                                    <td>{{ $file->branch_code }}</td>
                                    <td>{{ $file->branch }}</td>
                                    <td>{{ $file->voucher_number }}</td>
                                    <td>{{ $file->filename }}</td>
                                    <td>{{ $file->created_at }}</td>
                                    <td>
                                        <a class="btn btn-info btn-xs" href="{{ route('manualinventory.show', $file->id)}}">Download</a>
                                    </td>
                                    
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="14">No record found</td>
                                </tr>
                                @endif
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
    
@endsection