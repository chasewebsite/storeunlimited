@extends('layouts.blank')

@push('stylesheets')

@endpush

@push('inline-scripts')
    $('#filer_input').filer({
        changeInput: true,
        showThumbs: true,
        addMore: true
    });

    $('#submitBtn').click(function(){

        $('#confirm-submit').modal('show');

    });

    $('#submit').click(function(){

    $('#formField').submit();



    });
@endpush

@section('main_container')




 <!-- page content -->
<div class="right_col" role="main">

    <div class="">
        <div class="page-title">
        </div>

        @include('includes/notifications')

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Manual Inventory</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br>
                        {!! Form::open(array('route' => 'manualinventory.store','class' => 'form-horizontal form-label-left', 'id'=>'formField',  'files'=>true)) !!}

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="role">Branch <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::select('branch', $branches, null, array('class' => 'form-control', 'placeholder' => 'Pick a Branch...')) !!}
                                </div>
                            </div>

                             <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="role">Attach Files <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="file" name="files" id="filer_input" multiple="multiple">
                                </div>
                            </div>

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <a class="btn btn-primary" href="{{ route('manualinventory.index')}}">Back</a>
                                    <button type="button" class="btn btn-success" data-target="#confirm-submit" id="submitBtn">Submit</button>
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
    @include('modals.confirmation')
@endsection

@push('scripts')
   
@endpush

