@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
        </div>
          @include('includes/notifications')

        <div class="clearfix"></div>


        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Company Lists</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div>
                        <a class="btn btn-success" href="{{ route('companies.create')}}">New Company</a>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Company Code</th>
                                    <th>Company Name</th>
                                    <th colspan="2" class="action">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($companies->count() > 0)
                                @foreach($companies as $company)
                                <tr>
                                    <td>{{ $company->company_code }}</td>
                                    <td>{{ $company->company }}</td>
                                    <td>
                                        <a class="btn btn-success btn-xs" href="{{ route('companies.edit', $company->id)}}">Edit</a>
                                    </td>
                                    <td>
                                        {!! Form::open(array('route' => array('companies.destroy', $company->id), 'method' => 'DELETE')) !!}
                                            <button type="submit" class="btn btn-danger btn-xs confirm">Delete</button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="14">No record found</td>
                                </tr>
                                @endif
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
    
@endsection