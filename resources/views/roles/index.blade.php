@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
        </div>
          @include('includes/notifications')

        <div class="clearfix"></div>


        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Role Lists</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div>
                        <a class="btn btn-success" href="{{ route('roles.create')}}">New Role</a>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Display Name</th>
                                    <th>Description</th>
                                    <th colspan="2" class="action">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($roles->count() > 0)
                                @foreach($roles as $role)
                                <tr>
                                    <td>{{ $role->name }}</td>
                                    <td>{{ $role->display_name }}</td>
                                    <td>{{ $role->description }}</td>
                                    <td>
                                        <a class="btn btn-success btn-xs" href="{{ route('roles.edit', $role->id)}}">Edit</a>
                                    </td>
                                    <td>
                                        {!! Form::open(array('route' => array('roles.destroy', $role->id), 'method' => 'DELETE')) !!}
                                            <button type="submit" class="btn btn-danger btn-xs confirm">Delete</button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="14">No record found</td>
                                </tr>
                                @endif
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
    
@endsection