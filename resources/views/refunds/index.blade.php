@extends('layouts.blank')

@push('stylesheets')

@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
         <div class="">
            <div class="page-title">
              <!-- <div class="title_left">
                <h3>Incoming Items Report</h3>
              </div> -->

              <!-- <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div> -->
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Refund Items Report</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content table-responsive">
                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Company Name</th>
                                        <th>Branch Name</th>
                                        <th>Terminal #</th>
                                        <th>User</th>
                                        <th>Transaction #</th>
                                        <th>Reference #</th>
                                        <th>Date Time</th>
                                        <th>Barcode</th>
                                        <th>Description</th>
                                        <th>Qty</th>
                                        <th>Price</th>
                                        <th>Amount</th>
                                        <th>Customer</th>
                                        <th>Posting Time</th>
                                    </tr>
                                </thead>


                                <tbody>
                                    @if($items->count() > 0)
                                    @foreach($items as $item)
                                    <tr>
                                        <td>{{ $item->company_name }}</td>
                                        <td>{{ $item->branch_name }}</td>
                                        <td>{{ $item->terminal_no }}</td>
                                        <td>{{ $item->user }}</td>
                                        <td>{{ $item->transaction_no }}</td>
                                        <td>{{ $item->ref_no }}</td>
                                        <td>{{ $item->local_time }}</td>
                                        <td>{{ $item->barcode }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td>{{ $item->qty }}</td>
                                        <td>{{ number_format($item->price,2) }}</td>
                                        <td>{{ number_format($item->amount,2) }}</td>
                                        <td>{{ $item->customer_name }}</td>
                                        <td>{{ $item->created_at }}</td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="15">No record found</td>
                                    </tr>
                                    @endif
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </div>
    <!-- /page content -->
@endsection