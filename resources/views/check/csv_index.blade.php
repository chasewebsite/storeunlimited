@extends('layouts.blank')

@push('stylesheets')

@endpush

@push('inline-scripts')
    @if(isset($sel_branches))
    var branches = <?php echo json_encode($sel_branches); ?>;
    @else
    var branches = [];
    @endif


      $(document).ready(function() {

           

            $('#datatable1').dataTable({

             "aaSorting":[[1,"desc"]]
            });
    
               
          });


@endpush

@push('scripts')
    <script src="{{ asset("js/check.js") }}"></script>
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
         <div class="">
            <div class="page-title">
              <!-- <div class="title_left">
                <h3>Incoming Items Report</h3>
              </div> -->

              <!-- <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div> -->
            </div>

            <div class="clearfix"></div>

            @if(count($data)> 0)
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>CSV Report</h2>
                            <div class="pull-right">
                                
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content table-responsive">
                            <table id = "datatable1" class="table table-striped table-bordered">
                                <thead>
                                       <th>Branch</th>
                                       <th>Date</th>
                                        <th>File</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                       
                                </thead>
                                 @foreach($data as $d)
                                  @if($d['upload'] != 1)                                    
                                    <tr>
                                        <td>{{ $d['branch_name'] }}</td>
                                       <td>{{ $d['date'] }}</td>

                                        @if($d['file_name'] == ' ')
                                       <td>NO FILE</td>
                                       @else
                                       <td>{{$d['file_name']}}</td>
                                       @endif

                                       @if($d['file_type'] == 1)
                                       <td>SALES</td>
                                       @elseif($d['file_type'] == 2)
                                       <td>INVENTORY</td>
                                       @endif



                                        @if($d['upload'] == 1)
                                       <td>UPLOADED</td>
                                       @else
                                       <td>NOT YET UPLOADED</td>
                                       @endif

                                    @endif




                                        
                                    </tr>
                                    @endforeach      

                                <tbody>                                                          
                                                                                  
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @endif
          </div>
    </div>
    <!-- /page content -->
@endsection