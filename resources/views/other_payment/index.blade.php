@extends('layouts.blank')

@push('stylesheets')

@endpush


@push('scripts')
 <!--    <script src="{{ asset("js/incoming.js") }}"></script> -->

<script type="text/javascript">
$(document).ready(function(){

    $("#date_from, #date_to").mask("99/99/9999",{placeholder:"__/__/____"});
    $("#date_from").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
    });

    $("#date_to").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
    });

    $('select#branch').multiselect({
        maxHeight: 200,
        includeSelectAllOption: true,
        enableCaseInsensitiveFiltering: true,
        enableFiltering: true
        
    });
    $('select#gift').multiselect({
        maxHeight: 200,
        includeSelectAllOption: true,
        enableCaseInsensitiveFiltering: true,
        enableFiltering: true
        
    });



});

</script>
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
         <div class="">
            <div class="page-title">
              <!-- <div class="title_left">
                <h3>Incoming Items Report</h3>
              </div> -->

              <!-- <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div> -->
            </div>

            <div class="clearfix"></div>

                {!! Form::open(array( 'class' => 'form-horizontal form-label-left', 'method' => 'GET')) !!}
                  
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="x_panel">
                            <div class="row">                        
                              
                               
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="company">Branch Name</label>
                                        {!! Form::select('branch[]',$branches, $sel_branch, array('id' => 'branch', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT A BRANCH...')) !!}
                                    </div>
                                </div>

                                 <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="company">GIFT Name</label>
                                        {!! Form::select('gifts[]',$gifts, $sel_gift, array('id' => 'gift', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT A GIFT...')) !!}
                                    </div>
                                </div>

                                  <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="date_from">Date From</label>
                                        {!! Form::text('date_from',$date_from,array('class' => 'form-control', 'id' => 'date_from')) !!}
                                    </div>                                    
                                </div>
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="date_to">Date To</label>
                                         {!! Form::text('date_to',$date_to,array('class' => 'form-control', 'id' => 'date_to')) !!}
                                    </div>                                
                                </div> 
                              
                                   
                                      
                            </div>                    
                          
                          
                            <div class="row">
                                <div class="col-md-3 col-xs-12">
                                     <div class="btn-group">
                                        <button type="submit" class="btn btn-success" value="1" name="submit">Process</button>
                                    </div>  
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            {!! Form::close() !!}

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Other Payment Report</h2>
                          
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content table-responsive">
                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Branch Name</th>
                                        <th>Transaction No</th>
                                        <th>User</th>
                                        <th>Serial No</th>
                                        <th>Gift Name</th>
                                        <th>Account Name</th>
                                        <th>Amount</th>
                                        <th>Approved #</th>
                                        
                                    </tr>
                                </thead>
                                  @foreach($items as $item)                                    
                                    <tr>
                                        <td>{{ $item->branch_name }}</td>
                                        <td>{{ $item->transaction_no }}</td>
                                        <td>{{ $item->user }}</td>
                                        <td>{{ $item->serial_no }}</td>
                                        <td>{{ $item->gift_name }}</td>
                                        <td>{{ $item->account_name }}</td>
                                        <td>{{ $item->amount }}</td>
                                        <td>{{ $item->approved_no }}</td>
                                       
                                    </tr>
                                    @endforeach                


                                <tbody>                                                          
                                                                            
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </div>
    <!-- /page content -->
@endsection