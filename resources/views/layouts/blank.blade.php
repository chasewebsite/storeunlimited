<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Store Unlimited!</title>

        <!-- Bootstrap -->
        <link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet">
        <!-- Datatables -->
        <link href="{{ asset("vendors/datatables.net-bs/css/dataTables.bootstrap.min.css") }}" rel="stylesheet">
        <link href="{{ asset("vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css") }}" rel="stylesheet">
        <link href="{{ asset("vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css") }}" rel="stylesheet">
        <link href="{{ asset("vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css") }}" rel="stylesheet">
        <link href="{{ asset("vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css") }}" rel="stylesheet">

        <link href="{{ asset("css/jquery-confirm.css") }}" rel="stylesheet">

        <link href="{{ asset("vendors/bootstrap-multiselect/css/bootstrap-multiselect.css") }}" rel="stylesheet">

        <link href="{{ asset("vendors/jQuery.filer-1.0.5/css/jquery.filer.css") }}" rel="stylesheet">
        <link href="{{ asset("vendors/jQuery.filer-1.0.5/css/themes/jquery.filer-dragdropbox-theme.css") }}" rel="stylesheet">
        
        <!-- Custom Theme Style -->
        <link href="{{ asset("css/custom.min.css") }}" rel="stylesheet">

        <link href="{{ asset("css/style.css") }}" rel="stylesheet">

        @stack('stylesheets')

        <style type="text/css">
            .site_title i{
                border: 0px; 
            }

            #datatable{
                 white-space: nowrap;
            }

            table .action {
              /*max-width:  20px;*/
            }
            
        </style>

    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">

                @include('includes/sidebar')

                @include('includes/topbar')

                @yield('main_container')

                <!-- footer content -->
                <footer>
                    <div class="pull-right">
                        
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

            </div>
        </div>

        <!-- jQuery -->
        <script src="{{ asset("js/jquery.min.js") }}"></script>
        <!-- Bootstrap -->
        <script src="{{ asset("js/bootstrap.min.js") }}"></script>

        <!-- Datatables -->
        <script src="{{ asset("vendors/datatables.net/js/jquery.dataTables.min.js") }}"></script>
        <script src="{{ asset("vendors/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
        <script src="{{ asset("vendors/datatables.net-buttons/js/dataTables.buttons.min.js") }}"></script>
        <script src="{{ asset("vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js") }}"></script>
        <script src="{{ asset("vendors/datatables.net-buttons/js/buttons.flash.min.js") }}"></script>
        <script src="{{ asset("vendors/datatables.net-buttons/js/buttons.html5.min.js") }}"></script>
        <script src="{{ asset("vendors/datatables.net-buttons/js/buttons.print.min.js") }}"></script>
        <script src="{{ asset("vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js") }}"></script>
        <script src="{{ asset("vendors/datatables.net-keytable/js/dataTables.keyTable.min.js") }}"></script>
        <script src="{{ asset("vendors/datatables.net-responsive/js/dataTables.responsive.min.js") }}"></script>
        <script src="{{ asset("vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js") }}"></script>
        <script src="{{ asset("vendors/datatables.net-scroller/js/datatables.scroller.min.js") }}"></script>

        <script src="{{ asset("vendors/bootstrap-multiselect/js/bootstrap-multiselect.js") }}"></script>
        <script src="{{ asset("vendors/digitalBush/jquery.maskedinput_1.4.0/jquery.maskedinput.min.js") }}"></script>
        <script src="{{ asset("vendors/moment/moment.min.js") }}"></script>
        <script src="{{ asset("vendors/bootstrap-daterangepicker/daterangepicker.js") }}"></script>

         <script src="{{ asset("vendors/jQuery.filer-1.0.5/js/jquery.filer.min.js") }}"></script>

        <!-- Custom Theme Scripts -->
        <script src="{{ asset("js/gentelella.min.js") }}"></script>
        <script>
          var hostname = 'http://' + $(location).attr('host');
          function getSelectedValues(select) {
            var foo = []; 
            select.each(function(i, selected){ 
              foo[i] = $(selected).val(); 
            });
            return foo;
          }

          $(document).ready(function() {

            var handleDataTableButtons = function() {
              if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                  dom: "Bfrtip",
                  buttons: [
                    {
                      extend: "copy",
                      className: "btn-sm"
                    },
                    {
                      extend: "csv",
                      className: "btn-sm"
                    },
                    {
                      extend: "excel",
                      className: "btn-sm"
                    },
                    {
                      extend: "pdfHtml5",
                      className: "btn-sm"
                    },
                    {
                      extend: "print",
                      className: "btn-sm"
                    },
                  ],
                  responsive: true
                });
              }
            };

            TableManageButtons = function() {
              "use strict";
              return {
                init: function() {
                  handleDataTableButtons();
                }
              };
            }();

            $('#datatable').dataTable({
              'pageLength': 50
            });
            $('#datatable-keytable').DataTable({
              keys: true
            });

            $('#datatable-responsive').DataTable();

            $('#datatable-scroller').DataTable({
              ajax: "js/datatables/json/scroller-demo.json",
              deferRender: true,
              scrollY: 380,
              scrollCollapse: true,
              scroller: true
            });

            var table = $('#datatable-fixed-header').DataTable({
                fixedHeader: true
            });

            TableManageButtons.init();
          });

    

          @stack('inline-scripts')
        </script>
        <script src="{{ asset("js/app.js") }}"></script>
        <script src="{{ asset("js/jquery-confirm.js") }}"></script>
        @stack('scripts')
    </body>
</html>