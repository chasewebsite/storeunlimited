@extends('layouts.blank')

@push('stylesheets')

@endpush
@push('scripts')
    <script src="{{ asset("js/coupon.js") }}"></script>
@endpush
@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
         <div class="">
            <div class="page-title">
              <!-- <div class="title_left">
                <h3>Incoming Items Report</h3>
              </div> -->

              <!-- <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div> -->
            </div>

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="x_panel">  
                        {!! Form::open(array('route'=>'purchase.store','method'=>'POST')) !!}                                                          
                        <div class="row">
                            <div class="col-md-3 col-xs-12">
                                <div class="form-group">
                                    <label for="date_from">Date From</label>
                                    {!! Form::text('date_from',$date_from,array('class' => 'form-control', 'id' => 'date_from')) !!}
                                </div>                                    
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <div class="form-group">
                                    <label for="date_to">Date To</label>
                                     {!! Form::text('date_to',$date_to,array('class' => 'form-control', 'id' => 'date_to')) !!}
                                </div>                                
                            </div>             
                        </div>                       
                        <div class="row">
                            <div class="col-md-3 col-xs-12">
                                <div class="btn-group">
                                    <button type="submit" class="btn btn-success">Process</button>
                                </div>  
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>  
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Branch Ranking Based On Used Of Coupon</h2>                            
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Company Name</th>
                                        <th>Total Qty</th>                                        
                                    </tr>
                                </thead>
                                <tbody>                                    
                                    @foreach($items as $item)
                                    <tr>
                                        <td>{{ $item->c_company_name }}</td>
                                        <td>{{ $item->c_branch_name }}</td>                                        
                                    </tr>
                                    @endforeach                                                                        
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Coupons Report</h2>
                            <div class="pull-right">
                                <h3>Total Qty: {!! $total_qty !!}</h3>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content table-responsive">
                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Company Name</th>
                                        <th>Branch Name</th>
                                        <th>Terminal #</th>
                                        <th>Coupon Code</th>
                                        <th>User</th>
                                        <th>Created At</th>
                                        <th>Redeemed By</th>
                                        <th>User</th>
                                        <th>Company Name</th>
                                        <th>Branch Name</th>
                                        <th>Terminal #</th>
                                        <th>Transaction #</th>
                                        <th>Redeemed At</th>
                                        <th>Redeemed</th>
                                        <th>Last Update</th>
                                        
                                    </tr>
                                </thead>


                                <tbody>
                                    @if($items->count() > 0)
                                    @foreach($items as $item)
                                    <tr>
                                        <td>{{ $item->c_company_name }}</td>
                                        <td>{{ $item->c_branch_name }}</td>
                                        <td>{{ $item->c_terminal_no }}</td>
                                        <td>{{ $item->coupon_code }}</td>
                                        <td>{{ $item->c_cahier }}</td>
                                        <td>{{ $item->c_date_time }}</td>
                                        <td>{{ $item->redeemed_by }}</td>
                                        <td>{{ $item->r_cashier }}</td>
                                        <td>{{ $item->r_company_name }}</td>
                                        <td>{{ $item->r_branch_name }}</td>
                                        <td>{{ $item->r_terminal_no }}</td>
                                        <td>{{ $item->r_transaction_no }}</td>
                                        <td>{{ $iten->r_datetime }}</td>
                                        <td>{{ $iten->redeemed }}</td>
                                        <td>{{ $item->updated_at }}</td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="15">No record found</td>
                                    </tr>
                                    @endif
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </div>
    <!-- /page content -->
@endsection