@extends('layouts.blank')

@push('stylesheets')

@endpush
@push('inline-scripts')
    @if(isset($sel_branches))
    var branches = <?php echo json_encode($sel_branches); ?>;
    @else
    var branches = [];
    @endif

    @if(isset($sel_categories))
    var categories = <?php echo json_encode($sel_categories); ?>;
    @else
    var categories = [];
    @endif

    @if(isset($sel_brands))
    var brands = <?php echo json_encode($sel_brands); ?>;
    @else
    var brands = [];
    @endif
@endpush
@push('scripts')
    <script src="{{ asset("js/stock.js") }}"></script>
@endpush
@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
         <div class="">
            <div class="page-title">
                <div class="title_left">
                    <a href="{{ route('stocks.index')}}" class="btn btn-default">Back</a>
                </div>
                 
            </div>

            <div class="x_panel">
                 
                  <div class="x_content">
                    <br>
                    <div class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Company Name </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" disabled="disabled" value="{{ $branch->company->company }}" placeholder="Disabled Input">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Branch Name </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" disabled="disabled" value="{{ $branch->branch}}" placeholder="Disabled Input">
                            </div>
                      </div>




                       <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Branch Code </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" disabled="disabled" value="{{ $branch->branch_code}}" placeholder="Disabled Input">
                        </div>
                      </div>

                     
                      </div>

                    </div>
                  </div>
                </div>

                 <div class="clearfix"></div>

            {!! Form::open(array('route' => array('stocks.history',$branch->branch_code), 'class' => 'form-horizontal form-label-left', 'method' => 'GET')) !!}
                   <input type="hidden" name="branch" class="form-control" disabled="disabled" value="{{ $branch->branch_code }}" placeholder="Disabled Input">
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="x_panel">
                            <div class="row">

                                 <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="company">Category Name</label>
                                        {!! Form::select('category[]',$sel_categories, null, array('id' => 'category', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT A CATEGORY...')) !!}
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="company">Brand Name</label>
                                        {!! Form::select('brand[]',$sel_brands, null, array('id' => 'brand', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT A BRAND...')) !!}
                                    </div>
                                </div>       
                                



                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="stock_status">Stock Status</label>
                                        {!! Form::select('status[]',  $stock_status, null, array('id' => 'stock_status', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT DIVISION')) !!}
                                    </div>
                                </div>



                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="keywords">Search</label>
                                        {!! Form::text('keyword',null,array('class' => 'form-control', 'id' => 'keyword','placeholder' =>'Enter Item')) !!}
                                    </div>
                                    
                               
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-xs-12">
                                    <div class="btn-group">
                                        <button type="submit" class="btn btn-success">Process</button>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
        {!! Form::close() !!}

             <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Items</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div>
                     
                          {!! $items->render() !!}
                        {!! Paginate::show($items) !!} 

                    </div>

                        <div class="x_content table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Barcode</th>
                                        <th>Category</th>
                                        <th>ItemCode</th>
                                        <th>Description</th>
                                        <th>Quantity</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @if(count($items) > 0)
                                    @foreach($items as $item)
                                    <tr>
                                        <td>{{ $item->barcode }}</td>
                                        <td>{{ $item->category }}</td>
                                        <td>{{ $item->itemcode }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td>{{ $item->qty }}</td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="3">No record found</td>
                                    </tr>
                                    @endif
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
     <div class="clearfix"></div>
      <!--       <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Item Movement Report</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Beginning Inventory</th>
                                        <th>Ending Inventory</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @if(count($histories) > 0)
                                    @foreach($histories as $history)
                                    <tr>
                                        <td>{{ $history->transaction_date }}</td>
                                        <td>{{ $history->begining }}</td>
                                        <td>{{ $history->ending }}</td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="3">No record found</td>
                                    </tr>
                                    @endif
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> -->
          </div>
    </div>
    <!-- /page content -->

@endsection