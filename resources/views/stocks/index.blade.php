@extends('layouts.blank')

@push('stylesheets')

@endpush

@push('inline-scripts')
    @if(isset($sel_branches))
    var branches = <?php echo json_encode($sel_branches); ?>;
    @else
    var branches = [];
    @endif
@endpush

@push('scripts')
    <script src="{{ asset("js/itemstocks.js") }}"></script>
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
         <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Branch Item Stock Report</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            {!! Form::open(array('route' => array('stocks.store'), 'class' => 'form-horizontal form-label-left', 'method' => 'POST')) !!}
                  
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="x_panel">
                            <div class="row">

                                

                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="company">Company Name</label>
                                        {!! Form::select('company[]',  $companies, null, array('id' => 'company', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT DIVISION')) !!}
                                    </div>
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="branch">Branch Name</label>
                                        <select class="form-control" data-placeholder="SELECT A BRANCH..." id="branch" name="branch[]" multiple="multiple" ></select>
                                    </div>
                                    
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="stock_status">Stock Status</label>
                                        {!! Form::select('status[]',  $stock_status, null, array('id' => 'status', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT DIVISION')) !!}
                                    </div>
                                </div>



                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="keywords">Search</label>
                                        {!! Form::text('keyword',null,array('class' => 'form-control', 'id' => 'keyword','placeholder' =>'Enter Item')) !!}
                                    </div>
                                    
                               
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-xs-12">
                                    <div class="btn-group">
                                        <button type="submit" class="btn btn-success">Process</button>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
        {!! Form::close() !!}


            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                      <div class="x_title">
                            <h2>Item Stoks</h2>
                            <div class="pull-right">
                                <h3>Total Qty: {!! $total_stock !!}</h3>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content table-responsive">
                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Company Name</th>
                                        <th>Branch Name</th>
                                        <th>Total Stocks</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>


                                <tbody>
                                    @if(count($items) > 0)
                                    @foreach($items as $item)
                                    <tr>
                                        <td>{{ $item->company_name }}</td>
                                        <td>{{ $item->branch_name }}</td>
                                        <td class="right">{{ number_format($item->total_stocks) }}</td>
                                        <td>
                                            <a href="{{ route('stocks.history', $item->branch_code)}}"> Details</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    
                                    @endif
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </div>
    <!-- /page content -->

@endsection