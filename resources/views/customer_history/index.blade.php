@extends('layouts.blank')

@push('stylesheets')

@endpush

@push('inline-scripts')
    @if(isset($sel_branches))
    var branches = <?php echo json_encode($sel_branches); ?>;
    @else
    var branches = [];
    @endif
    @if(isset($sel_terminals))
    var terminals = <?php echo json_encode($sel_terminals); ?>;
    @else
    var terminals = [];
    @endif
@endpush

@push('scripts')
    <script src="{{ asset("js/salessummary.js") }}"></script>
@endpush

@section('main_container')



    <!-- page content -->
    <div class="right_col" role="main">
         <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Branch Sales Summary Report</h3>
                </div>
            </div>

            <div class="clearfix"></div>



            {!! Form::open(array('route' => array('sales.store'), 'class' => 'form-horizontal form-label-left', 'method' => 'POST')) !!}
                  
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="x_panel">
                            <div class="row">
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="company">Company Name</label>
                                        {!! Form::select('company[]',  $companies, null, array('id' => 'company', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT DIVISION')) !!}
                                    </div>
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="branch">Branch Name</label>
                                        <select class="form-control" data-placeholder="SELECT A BRANCH..." id="branch" name="branch[]" multiple="multiple" ></select>
                                    </div>
                                    
                                </div>

                                 <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="date_from">Date From</label>
                                        {!! Form::text('date_from',$date_from,array('class' => 'form-control', 'id' => 'date_from')) !!}
                                    </div>
                                    
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="date_to">Date To</label>
                                         {!! Form::text('date_to',$date_to,array('class' => 'form-control', 'id' => 'date_to')) !!}
                                    </div>
                                    
                                </div>

                                
                            </div>

                            

                            <div class="row">
                                <div class="col-md-3 col-xs-12">
                                    <div class="btn-group">
                                        <button type="submit" class="btn btn-success">Process</button>
                                    </div>
                                </div>


                                
                            </div>



                        </div>
                    </div>
                </div>  
            {!! Form::close() !!}            
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content table-responsive">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Company Name</th>
                                            <th>Branch Name</th>
                                            <th>Member</th>
                                            <th>Net Amount</th>
                                            
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($summaries) > 0)
                                        @foreach($summaries as $summary)
                                        <tr>
                                            <td>{{ $summary->company }}</td>
                                            <td>{{ $summary->branch }}</td>
                                            <td class="right">{{ number_format($summary->net_amount + $summary->return_exchange_amount,2) }}</td>
                                            <td class="right">{{ number_format($summary->refund_amount,2) }}</td>
                                            <td class="right">{{ number_format($summary->return_exchange_amount,2) }}</td>
                                            <td class="right">{{ number_format($summary->net_amount - $summary->refund_amount,2) }}</td>
                                            <td>
                                                <a href="{{ route('sales.show', ['id' => $summary->branch_code, 'from' => strtotime($date_from), 'to' => strtotime($date_to)])}}"> Transaction Details</a> |
                                                <a href="{{ route('sales.items', ['id' => $summary->branch_code, 'from' => strtotime($date_from), 'to' => strtotime($date_to)])}}"> Item Details</a>
                                            </td>

                                        </tr>
                                        @endforeach                                        
                                        @endif                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>                    
                </div>    
          </div>
    </div>
    <!-- /page content -->

@endsection