@extends('layouts.blank')

@push('stylesheets')

@endpush

@push('inline-scripts')
    @if(isset($sel_branches))
    
    @else
    var branches = [];
    @endif
    @if(isset($sel_terminals))
    var terminals = <?php echo json_encode($sel_terminals); ?>;
    @else
    var terminals = [];
    @endif
@endpush

@push('scripts')
    <script src="{{ asset("js/salessummary.js") }}"></script>
@endpush

@section('main_container')



    <!-- page content -->
    <div class="right_col" role="main">
         <div class="">
            @include('includes/notifications')
            
            <div class="page-title">
                <div class="title_left">
                    <h3>DETAILED DAILY SALES REPORT</h3>
                </div>
            </div>
            <div class="clearfix"></div>
            {!! Form::open(array('route' => array('detailed_sales.store'), 'class' => 'form-horizontal form-label-left', 'method' => 'POST')) !!}                
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="x_panel">
                            <div class="row">                            
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="branch">Branch Name</label>
                                        {!! Form::select('branch', $branches, null, array('id' => 'branches', 'class' => 'form-control' ,'data-placeholder' => 'SELECT BRANCH')) !!}
                                    </div>                                    
                                </div>
                                 <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="date_from">Date From</label>
                                        {!! Form::text('date_from',$date_from,array('class' => 'form-control', 'id' => 'date_from')) !!}
                                    </div>                                    
                                </div>
                                 <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="date_from">Date To</label>
                                        {!! Form::text('date_to',$date_to,array('class' => 'form-control', 'id' => 'date_to')) !!}
                                    </div>                                    
                                </div>
                                                                
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    
                                    <div class="btn-group">
                                        <button type="submit" class="btn btn-warning">Download Excel</button>
                                    </div>
                                    <!--  <div class="btn-group">
                                        <button type="submit" name="export" value = "3" class="btn btn-primary">Download Excel Summary</button>
                                    </div> -->
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>  
            {!! Form::close() !!}                
          </div>
    </div>
    <!-- /page content -->

@endsection