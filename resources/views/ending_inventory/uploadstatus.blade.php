@extends('layouts.blank')

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
         <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Status Upload as of {{ date('Y-m-d',$date_to)}}</h3>
                </div>
            </div>
          
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Branch Details Status</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content table-responsive">
                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Company Name</th>
                                        <th>Branch Name</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>                                                          
                                    @foreach($my_branch as $item)
                                    @if($item->branch != "WAREHOUSE")                                    
                                    <tr>
                                        <td>{{ $item->company_name }}</td>
                                        <td>{{ $item->branch }}</td>
                                        <td>
                                            @if($item->status == 1)
                                                <span style="color: orange;">For Upload</span>
                                            @elseif($item->status == 2)
                                                <span style="color: green;">Uploaded</span>
                                            @else
                                                <span style="color: red;">Not Found</span>
                                            @endif
                                        </td>
                                      
                                    </tr>
                                    @endif
                                    @endforeach                                                                
                                </tbody>
                            </table>
                           
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </div>
    <!-- /page content -->

@endsection