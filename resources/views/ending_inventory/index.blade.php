@extends('layouts.blank')

@push('stylesheets')

@endpush

@push('inline-scripts')
    @if(isset($sel_branches))
    var branches = <?php echo json_encode($sel_branches); ?>;
    @else
    var branches = [];
    @endif
@endpush

@push('scripts')
    <script src="{{ asset("js/endinginventory.js") }}"></script>
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
         <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Stock Inventory as of</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            {!! Form::open(array('route' => array('ending_inventory.store'), 'class' => 'form-horizontal form-label-left', 'method' => 'POST','target'=>'_blank')) !!}
                  
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="x_panel">
                            <div class="row">

                                
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="date_to">Date </label>
                                         {!! Form::text('date_to',$date_format,array('class' => 'form-control', 'id' => 'date_to')) !!}
                                    </div>                                
                                </div> 

                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="company">Company Name</label>
                                        {!! Form::select('company[]',  $companies, null, array('id' => 'company', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT DIVISION')) !!}
                                    </div>
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="branch">Branch Name</label>
                                        <select class="form-control" data-placeholder="SELECT A BRANCH..." id="branch_name" name="branch[]" multiple="multiple" ></select>
                                    </div>
                                    
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="stock_status">Stock Status</label>
                                        {!! Form::select('status[]',  $stock_status, null, array('id' => 'status_id', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT DIVISION')) !!}
                                    </div>
                                </div>

                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="keywords">Search</label>
                                        {!! Form::text('keyword',null,array('class' => 'form-control', 'id' => 'keyword','placeholder' =>'Enter Item')) !!}
                                    </div>
                                </div>


                            </div>
                            <div class="row">
                                <div class="col-md-3 col-xs-12">
                                    <div class="btn-group">
                                        <button type="submit" name="submit" value="process" class="btn btn-success">Process</button>
                                    </div>  
                                    <div class="btn-group">
                                        <button type="submit"  name="submit" value="download" class="btn btn-success">Download</button>
                                    </div> 
                                     <div class="btn-group">
                                       <button type="submit"  name="submit" value="status" class="btn btn-success">Upload Status</button>
                                    </div> 

                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
          
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Ending Inventory Report</h2>
                            <div class="pull-right">
                                <h3>Total Qty: {!! $total_qty !!}</h3>
                                <h3>Row Count: {!! $row_count !!}</h3>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content table-responsive">
                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Company Name</th>
                                        <th>Branch Name</th>
                                        <th>Date Time</th>
                                         <th>Stock Code</th>
                                        <th>Barcode</th>
                                        <th>Department</th>
                                        <th>Category</th>
                                        <th>Brand</th>
                                        <th>Description</th>
                                        <th>Qty</th>
                                        <th>Cost</th>
                                        <th>Srp</th>
                                    </tr>
                                </thead>
                                <tbody>                                                          
                                    @foreach($items1 as $item)                                    
                                    <tr>
                                        <td>{{ $item->company_name }}</td>
                                        <td>{{ $item->branch_name }}</td>
                                        <td>{{ $item->date }}</td>
                                        <td>{{ $item->itemcode }}</td>
                                        <td>{{ $item->barcode }}</td>
                                        <td>{{ $item->department }}</td>
                                        <td>{{ $item->category }}</td>
                                        <td>{{ $item->brand }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td>{{ $item->qty }}</td>
                                        <td>{{ number_format($item->cost,2) }}</td>
                                        <td>{{ number_format($item->srp,2) }}</td>
                                    </tr>
                                    @endforeach                                                                
                                </tbody>
                            </table>
                            {{ $items1->appends(request()->except('page'))->links() }}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </div>
    <!-- /page content -->

@endsection