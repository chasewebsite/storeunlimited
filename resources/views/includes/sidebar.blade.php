<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="index.html" class="site_title"><i class="fa fa-cubes"></i><span>Store Unlimited</span></a>
        </div>
        
        <div class="clearfix"></div>
        
        <!-- menu profile quick info -->
        <!-- <div class="profile">
            <div class="profile_pic">
                <img src="images/img.jpg" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h2>John Doe</h2>
            </div>
        </div> -->
        <!-- /menu profile quick info -->
        
        <br />
        
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <ul class="nav side-menu">
                    <li><a><i class="fa fa-barcode"></i> Inventory Reports <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ route('incoming.index')}}">Incoming Items Report</a></li>
                            <li><a href="{{ route('outgoing.index')}}">Outgoing Items Report</a></li>
                            <li><a href="{{ route('purchase.index')}}">Purchase Orders Report</a></li>
                            <li><a href="{{ route('stocks.index')}}">Item Stock Report</a></li>
                            <li><a href="{{ route('notyet.index')}}">Not Yet Received Items Report</a></li>
                            <li><a href="{{ route('audit_stock.index')}}">Audit Inventory Report </a></li>
                        </ul>
                    </li>
                       <li><a><i class="fa fa-file-excel-o"></i>  Inventory Excel Report <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ route('stock_transfer_in.index')}}">Stock Transfer In Report</a></li>
                            <!-- <li><a href="{{ route('salemans.index')}}">Sales Man Summary Report</a></li> -->
                            <li><a href="{{ route('stock_transfer_out.index')}}">Stock Transfer Out Report</a></li>
                            <li><a href="{{ route('stock_movement_detailed.index')}}">Detailed Stock Movement Report</a></li>
                            <li><a href="{{ route('stock_movement_summary.index')}}">Summary Stock Movement Report</a></li>
                            <li><a href="{{ route('sale_register')}}">Sale Register</a></li>
                            <li><a href="{{ route('stock_issue.index')}}">Stock Issue Register</a></li>
                            <li><a href="{{ route('stock_issue_return.index')}}">Stock Issue Return Register</a></li>
                            <li><a href="{{ route('stock_adjustment.index')}}">Stock Adjustment List</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-line-chart"></i> Sales Report <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ route('sales.index')}}">Sales Summary Report</a></li>
                            <li><a href="{{ route('detailed_sale.index')}}">Detailed Sales Summary Report</a></li>
                            <!-- comment for version revision -->
                            <!-- <li><a href="{{ route('other.payment')}}">Other Payment Report</a></li> -->
                            <!-- end comment -->
                            <!-- <li><a href="{{ route('salemans.index')}}">Sales Man Summary Report</a></li> -->
                            <li><a href="{{ route('refunds.index')}}">Refund Items Report</a></li>
                            <li><a href="{{ route('returnexchange.index')}}">Return Exchange Report</a></li>
                            <li><a href="{{ route('postvoid.index')}}">Post Void Report</a></li>
                            <li><a href="{{ route('coupons.index')}}">Coupon Report</a></li>
                            <li><a href="{{ route('itemrank.index')}}">Item Ranking Report</a></li>                            
                        </ul>
                    </li>

                    <!-- <li><a><i class="fa fa-area-chart"></i> Gift Card Report <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                           <li><a href="{{ route('giftscards.incoming')}}">Incoming GiftCard Report</a></li>
                            <li><a href="{{ route('giftscards.outgoing')}}">Outgoing GiftCard Report</a></li>
                            <li><a href="{{ route('giftscards.index')}}">GiftCard Report</a></li>
                        </ul>
                    </li> -->


                    <!-- <li><a><i class="fa fa-line-chart"></i> Marketing Report <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ route('marketing_budget.index')}}">Marketing Budget</a></li> -->
                           
                            <!-- <li><a href="{{ route('salemans.index')}}">Sales Man Summary Report</a></li> -->
                            <!-- <li><a href="{{ route('marketing_summary.index')}}">Marketing Summary Report</a></li>
                            <li><a href="{{ route('monthly_profile.index')}}">Monthly Profile</a></li> -->
                            <!-- <li><a href="{{ route('postvoid.index')}}">Marketing Sales Report Per Branch</a></li> -->
                                                      

                        <!-- </ul>
                    </li>
 -->

                    <li><a><i class="fa fa-file-excel-o"></i> Sales Excel Report <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ route('detailed_sales.index')}}">Detailed Daily Sales Report</a></li>

                            <!-- <li><a href="{{ route('salemans.index')}}">Sales Man Summary Report</a></li> -->
                            <li><a href="{{ route('summary_sales.index')}}">Summary of Daily Sales Report</a></li>
                            <li><a href="{{ route('detailed_card.index')}}">Detailed Daily Credit Card TX</a></li>
                            <li><a href="{{ route('summary_card.index')}}">Daily Summary Of Credit Card TX</a></li>
                            <li><a href="{{ route('consolidated.index')}}">Consolidated Card Transactions</a></li>
                            <li><a href="{{ route('con_inv_rep.index')}}">Consolidated Inventory Report</a></li>
                            <li><a href="{{ route('con_sales_rep.index')}}">Consolidated Sales Report</a></li>
                           

                        </ul>
                    </li>




                    <li><a><i class="fa fa-bolt"></i> Utility <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ route('salesinventory.index')}}">Sales / Inventory Export</a></li>
                            <li><a href="{{ route('inventorysummary.index')}}">Inventory Export</a></li>
                            <li><a href="{{ route('salessummary.index')}}">Sales Export</a></li>
                            <<!-- li><a href="{{ route('manualinventory.index')}}">Inventory Manual Upload</a></li>
                            <li><a href="{{ route('store_operation.index')}}">Store Operation</a></li>
                            <li><a href="{{ route('check.excel')}}">Not Yet Uploaded Excel</a></li>
                            <li><a href="{{ route('check.csv')}}">Not  Yet  Uploaded CSV</a></li> -->
                        </ul>
                    </li>

                    @role('admin')
                    <li><a><i class="fa fa-file"></i> File Maintenance <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ route('items.index')}}">Item Masterfile</a></li>
                            <li><a href="{{ route('giftcards.index')}}">Gift Card Maintenance</a></li>
                            <li><a href="{{ route('companies.index')}}">Company Maintenance</a></li>
                            <li><a href="{{ route('branches.index')}}">Branch Maintenance</a></li>
                            <!-- <li><a href="{{ route('branchesettings.index')}}">Branch Settings Maintenance</a></li> -->
                            <li><a href="{{ route('roles.index')}}">Role Maintenance</a></li>
                            <li><a href="{{ route('users.index')}}">User Maintenance</a></li>
                            <li><a href="{{ route('zap.index')}}">Zap Transaction</a></li>
                            <li><a href="{{ route('zap_per_day.index')}}">Zap Transactions Per Day</a></li>
                        </ul>
                    </li>
                    @endrole
                </ul>
            </div>
            
        
        </div>
        <!-- /sidebar menu -->
    
    </div>
</div>