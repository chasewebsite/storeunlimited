@extends('layouts.blank')

@push('stylesheets')

@endpush
@section('main_container')

 <!-- page content -->
<div class="right_col" role="main">
     <div class="">
        <div class="page-title">
          <div class="title_left">
            <h3></h3>
          </div>

         



        <div class="clearfix"></div>
         @if(Session::has('flash_message'))
                    <div class="row">
                        <div class="col-xs-12">
                            <span class="help-block" style="background-color: #27ae60; padding:10px; color: #FFF; ">
                                {{ Session::get('flash_message') }}<br>
                                
                               
                                 
                                 
                            </span>

                        </div>
                    </div>
                    @endif

  <div class="row">
                                <div class="col-md-3 col-xs-12">
                                 <a href="{{url('marketing_budget')}}">  <button type="button" class="btn btn-default">Back</button></a>
                                   

                                       <!--  <button type="submit" class="btn btn-success">Process</button> -->
                                 
                                </div>
                            </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Budget of {{$month->description}}</h2>
                        <div class="clearfix"></div>
                    </div>
                     <div>
                       
                    </div>
                    <div>
                      

                    </div>
                    <div class="x_content table-responsive">
                              <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                   
                                    <th>Branch Name</th>
                                    <th>Budget</th>
                                   <!--  <th>Action</th> -->
                                 
                                 
                                </tr>
                            </thead>


                            <tbody>
                                @if($budgets->count() > 0)
                                @foreach($budgets as $data)
                                <tr>
                                    
                                    <td>{{ $data->branch_name }}</td>
                                    <td>{{ $data->budget }}</td>
                                   <!--  <td><a class="btn btn-success btn-xs" href="{{ route('marketing_budget.show', $data->id)}}">Edit</a>
                                   </td> -->
                                
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="14">No record found</td>
                                </tr>
                                @endif
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </div>
</div>
<!-- /page content -->
    
@endsection