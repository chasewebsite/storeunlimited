@extends('layouts.blank')

@push('stylesheets')

@endpush
@push('scripts')
    <script src="{{ asset("js/purchase.js") }}"></script>
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
         <div class="">
            <div class="page-title">
              <!-- <div class="title_left">
                <h3>Incoming Items Report</h3>
              </div> -->

              <!-- <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div> -->
            </div>

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="x_panel">  
                        {!! Form::open(array('route'=>'purchase.store','method'=>'POST')) !!}                                                          
                        <div class="row">
                            <div class="col-md-3 col-xs-12">
                                <div class="form-group">
                                    <label for="date_from">Date From</label>
                                    {!! Form::text('date_from',$date_from,array('class' => 'form-control', 'id' => 'date_from')) !!}
                                </div>                                    
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <div class="form-group">
                                    <label for="date_to">Date To</label>
                                     {!! Form::text('date_to',$date_to,array('class' => 'form-control', 'id' => 'date_to')) !!}
                                </div>                                
                            </div> 

                              
                        </div>                       
                        <div class="row">
                            <div class="col-md-3 col-xs-12">
                                <div class="btn-group">
                                    <button type="submit" class="btn btn-success">Process</button>
                                </div>  
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>  
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Purchase Orders Report</h2>
                            <div class="pull-right">
                                <h3>Total Qty: {!! $total_qty !!}</h3>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content table-responsive">
                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Company Name</th>
                                        <th>Branch Name</th>
                                        <th>Terminal #</th>
                                        <th>User</th>
                                        <th>Supplier</th>
                                        <th>Ref PO #</th>
                                        <th>PO #</th>
                                        <th>Incoming #</th>
                                        <th>Date Time</th>
                                        <th>Barcode</th>
                                        <th>Department</th>
                                        <th>Category</th>
                                        <th>Brand</th>
                                        <th>Description</th>
                                        <th>Qty</th>
                                        <th>Price</th>
                                        <th>Amount</th>
                                        <th>Posting Date</th>
                                    </tr>
                                </thead>


                                <tbody>                                
                                    @foreach($items as $item)
                                    <tr>
                                        <td>{{ $item->company_name }}</td>
                                        <td>{{ $item->branch_name }}</td>
                                        <td>{{ $item->terminal_no }}</td>
                                        <td>{{ $item->user }}</td>
                                        <td>{{ $item->supplier }}</td>
                                        <td>{{ $item->ref_po }}</td>
                                        <td>{{ $item->po_no }}</td>
                                        <td>{{ $item->incoming_no }}</td>
                                        <td>{{ $item->local_time }}</td>
                                        <td>{{ $item->barcode }}</td>
                                        <td>{{ $item->department }}</td>
                                        <td>{{ $item->category }}</td>
                                        <td>{{ $item->brand }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td>{{ $item->qty }}</td>
                                        <td>{{ number_format($item->price,2) }}</td>
                                        <td>{{ number_format($item->amount,2) }}</td>
                                        <td>{{ $item->local_date }}</td>
                                    </tr>
                                    @endforeach                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </div>
    <!-- /page content -->

@endsection