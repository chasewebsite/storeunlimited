@extends('layouts.blank')

@push('stylesheets')

@endpush

@push('inline-scripts')
    @if(isset($sel_branches))
    var branches = <?php echo json_encode($sel_branches); ?>;
    @else
    var branches = [];
    @endif

    @if(isset($sel_categories))
    var categories = <?php echo json_encode($sel_categories); ?>;
    @else
    var categories = [];
    @endif

    @if(isset($sel_brands))
    var brands = <?php echo json_encode($sel_brands); ?>;
    @else
    var brands = [];
    @endif
@endpush

@push('scripts')
    <script src="{{ asset("js/outgoing.js") }}"></script>
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
         <div class="">
            <div class="page-title">             
            </div>

            <div class="clearfix"></div>

            {!! Form::open(array('route' => array('outgoing.store'), 'class' => 'form-horizontal form-label-left', 'method' => 'POST')) !!}
                  
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="x_panel">
                            <div class="row">                        
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="company">Company Name</label>
                                        {!! Form::select('company[]',  $companies, null, array('id' => 'company', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT DIVISION')) !!}
                                    </div>
                                </div>
                    <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="company">Branch Name</label>
                                        {!! Form::select('branch[]',$sel_branches, null, array('id' => 'branch', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT A BRANCH...')) !!}
                                    </div>
                                </div>

                                 <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="company">Category Name</label>
                                        {!! Form::select('category[]',$sel_categories, null, array('id' => 'category', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT A CATEGORY...')) !!}
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="company">Brand Name</label>
                                        {!! Form::select('brand[]',$sel_brands, null, array('id' => 'brand', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT A BRAND...')) !!}
                                    </div>
                                </div>              
                            </div>                    
                            <div class="row">
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="date_from">Date From</label>
                                        {!! Form::text('date_from',$date_from,array('class' => 'form-control', 'id' => 'date_from')) !!}
                                    </div>                                    
                                </div>
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="date_to">Date To</label>
                                         {!! Form::text('date_to',$date_to,array('class' => 'form-control', 'id' => 'date_to')) !!}
                                    </div>                                
                                </div>
                                             
                            </div>
                            <div class="row">                                
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="keywords">Search</label>
                                        {!! Form::text('keyword',null,array('class' => 'form-control', 'id' => 'keyword')) !!}
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-xs-12">
                                    <div class="btn-group">
                                        <button type="submit" class="btn btn-success" value="1" name="submit">Process</button>
                                    </div>  
                                    <div class="btn-group">
                                        <button type="submit" class="btn btn-primary" value="2" name="submit">Download</button>
                                    </div>  
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    
                                </div>

                            </div>
                        </div>
                    </div>
                </div>  
            {!! Form::close() !!}

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Outgoing Items Report</h2>
                            <div class="pull-right">
                                <h3>Total Qty: {!! $total_qty !!}</h3>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content table-responsive">
                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Company Name</th>
                                        <th>Branch Name</th>
                                        <th>Terminal #</th>
                                        <th>User</th>
                                        <th>Outgoing #</th>
                                        <th>To Company Name</th>
                                        <th>To Branch Name</th>
                                        <th>Incoming #</th>
                                        <th>Date Time</th>
                                        <th>Box #</th>
                                        <th>Pouch #</th>
                                        <th>Pouch Unique Code</th>
                                        <th>Stock Code</th>
                                        <th>Barcode</th>
                                        <th>Series Name</th>
                                        <th>Composition</th>
                                       
                                        <th>Description</th>
                                        <th>Purpose</th>
                                        <th>Qty</th>
                                        <th>Price</th>
                                        <th>Amount</th>
                                        <th>Posting Time</th>
                                    </tr>
                                </thead>


                                <tbody>                                    
                                    @foreach($items as $item)
                                    <tr>
                                        <td>{{ $item->company_name }}</td>
                                        <td>{{ $item->branch_name }}</td>
                                        <td>{{ $item->terminal_no }}</td>
                                        <td>{{ $item->user }}</td>
                                        <td>{{ $item->outgoing_no }}</td>
                                        <td>{{ $item->to_company }}</td>
                                        <td>{{ $item->to_branch }}</td>
                                        <td>{{ $item->incoming_no }}</td>
                                        <td>{{ $item->local_time }}</td>
                                        <td>{{ $item->box_no }}</td>
                                        <td>{{ $item->pouch_no }}</td>
                                        <td>{{ $item->unique_code }}</td>
                                        <td> {{ $item->itemcode }}</td>
                                        <td>{{ $item->barcode }}</td>
                                        <td>{{ $item->series_name }}</td>                                                  
                                        <td>{{ $item->composition }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td>{{ $item->purpose }}</td>
                                        <td>{{ $item->qty }}</td>
                                        <td>{{ number_format($item->price,2) }}</td>
                                        <td>{{ number_format($item->amount,2) }}</td>
                                        <td>{{ $item->local_time }}</td>
                                    </tr>
                                    @endforeach                                                                       
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </div>
    <!-- /page content -->
@endsection