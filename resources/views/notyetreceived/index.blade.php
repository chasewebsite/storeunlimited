@extends('layouts.blank')

@push('stylesheets')

@endpush

@push('inline-scripts')
    @if(isset($sel_branches))
    var branches = <?php echo json_encode($sel_branches); ?>;
    @else
    var branches = [];
    @endif

    @if(isset($sel_users))
    var users = <?php echo json_encode($sel_users); ?>;
    @else
    var users = [];
    @endif

   
@endpush

@push('scripts')
     <script src="{{ asset("js/notyet.js") }}"></script>
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">
         <div class="">
            <div class="page-title">
              <!-- <div class="title_left">
                <h3>Incoming Items Report</h3>
              </div> -->

              <!-- <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div> -->
            </div>

            <div class="clearfix"></div>

                {!! Form::open(array('route' => array('notyet.post'), 'class' => 'form-horizontal form-label-left', 'method' => 'POST')) !!}
                  
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="x_panel">
                            <div class="row">                        
                               
                                 <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="company">Company Name</label>
                                        {!! Form::select('company[]',  $companies, null, array('id' => 'company', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT DIVISION')) !!}
                                    </div>
                                </div>
                            <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="company">Branch Name</label>
                                        {!! Form::select('branch[]',$sel_branches, $sel_branches, array('id' => 'branch', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT A BRANCH...')) !!}
                                    </div>
                                </div>

                                 <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="company">User</label>
                                        {!! Form::select('user[]',$sel_users, $sel_users, array('id' => 'user', 'class' => 'form-control' ,'multiple' => 'multiple' ,'data-placeholder' => 'SELECT A USER...')) !!}
                                    </div>
                                </div>

                                 <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="date_from">Date</label>
                                        {!! Form::text('date',$date,array('class' => 'form-control', 'id' => 'date')) !!}
                                    </div>                                    
                                </div>





                                             
                           
                           
                            
                        </div>

                        <div class="row">
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success" value="" name="submit">Process</button>
                                    
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>  
            {!! Form::close() !!}

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Not Yet Received Items Report</h2>
                            <div class="pull-right">
                                 <h3>Total Qty: {!! $total_qty !!}</h3>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content table-responsive">
                            <table id="datatable" class="table table-striped table-bordered">
                                 <thead>
                                    <tr>
                                        <th>Company Name</th>
                                        <th>Branch Name</th>
                                        <th>Terminal #</th>
                                        <th>User</th>
                                        <th>Outgoing #</th>
                                        <th>To Company Name</th>
                                        <th>To Branch Name</th>
                                        
                                        <th>Date Time</th>
                                        <th>Box #</th>
                                        <th>Pouch #</th>
                                        <th>Pouch Unique Code</th>
                                        <th>Barcode</th>
                                        <th>Department</th>
                                        <th>Category</th>
                                       
                                        <th>Description</th>
                                        <th>Purpose</th>
                                        <th>Qty</th>
                                        <th>Price</th>
                                        <th>Amount</th>
                                        <th>Posting Time</th>
                                    </tr>
                                </thead>


                                 <tbody>                                    
                                    @foreach($items as $item)
                                    <tr>
                                        <td>{{ $item->company_name }}</td>
                                        <td>{{ $item->branch_name }}</td>
                                        <td>{{ $item->terminal_no }}</td>
                                        <td>{{ $item->user }}</td>
                                        <td>{{ $item->outgoing_no }}</td>
                                        <td>{{ $item->to_company }}</td>
                                        <td>{{ $item->to_branch }}</td>
                                        
                                        <td>{{ $item->local_time }}</td>
                                        <td>{{ $item->box_no }}</td>
                                        <td>{{ $item->pouch_no }}</td>
                                        <td>{{ $item->unique_code }}</td>
                                        <td>{{ $item->barcode }}</td>
                                        <td>{{ $item->department }}</td>                                                  
                                        <td>{{ $item->category }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td>{{ $item->purpose }}</td>
                                        <td>{{ $item->qty }}</td>
                                        <td>{{ number_format($item->price,2) }}</td>
                                        <td>{{ number_format($item->amount,2) }}</td>
                                        <td>{{ $item->local_time }}</td>
                                    </tr>
                                    @endforeach                                                                       
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </div>
    <!-- /page content -->
@endsection