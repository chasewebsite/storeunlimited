@extends('layouts.blank')

@push('stylesheets')

@endpush

@push('inline-scripts')
    $('select#branches').multiselect({
        maxHeight: 200,
        includeSelectAllOption: true,
        enableCaseInsensitiveFiltering: true,
        enableFiltering: true,
        
    });
@endpush

@section('main_container')
    <!-- page content -->
    <div class="right_col" role="main">
         <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Branch Detailed Sales Summary Report</h3>
                </div>
            </div>
            <div class="clearfix"></div>
            {!! Form::open(array('route' => array('detailed_sale.store'), 'class' => 'form-horizontal form-label-left', 'method' => 'POST')) !!}
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="x_panel">
                            <div class="row">
                                <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="branch">Branch Name</label>
                                        {!! Form::select('branch[]', $branches, null, array('id' => 'branches', 'class' => 'form-control' ,'data-placeholder' => 'SELECT BRANCH','multiple')) !!}
                                    </div>       
                                </div>
                                <div class="col-md-12 col-xs-12">
                                    <div class="col-md-3 col-xs-12">
                                        <div class="form-group">
                                            <label for="date_from">Date From</label>
                                            {!! Form::text('date_from',$date_from,array('class' => 'form-control', 'id' => 'date_from')) !!}
                                        </div> 
                                    </div>
                                    <div class="col-md-3 col-xs-12">
                                        <div class="form-group">
                                            <label for="date_to">Date To</label>
                                             {!! Form::text('date_to',$date_to,array('class' => 'form-control', 'id' => 'date_to')) !!}
                                        </div>  
                                    </div>
                                </div>
                            </div>
                           <!--   <div class="col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="keywords">Search</label>
                                        {!! Form::text('keyword',null,array('class' => 'form-control', 'id' => 'keyword','placeholder' =>'Enter Item')) !!}
                                    </div>
                            </div>-->
                            <div class="row">
                                <div class="col-md-3 col-xs-12"> 
                                    <div class="btn-group">
                                        <button type="submit" class="btn btn-primary" value="2" name="submit">Download</button>
                                    </div> 
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>  
            {!! Form::close() !!}      
          </div>
    </div>
    <!-- /page content -->

@endsection