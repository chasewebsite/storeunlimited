@extends('layouts.blank')

@push('stylesheets')

@endpush

@push('inline-scripts')
    @if(isset($sel_branches))
    var branches = <?php echo json_encode($sel_branches); ?>;
    @else
    var branches = [];
    @endif
    @if(isset($sel_terminals))
    var terminals = <?php echo json_encode($sel_terminals); ?>;
    @else
    var terminals = [];
    @endif
@endpush

@push('scripts')
    <script src="{{ asset("js/salessummary.js") }}"></script>
@endpush

@section('main_container')
    <!-- page content -->
    <div class="right_col" role="main">
         <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Consolidated Sales Report</h3>
                </div>
            </div>
            <div class="clearfix"></div>
            <p><strong>Note:</strong>Per Day Only</p>
            {!! Form::open(array('route' => array('con_sales_rep.store'), 'class' => 'form-horizontal form-label-left', 'method' => 'POST')) !!}
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="x_panel">
                            <div class="row">
                                 <div class="col-md-3 col-xs-12">
                                    <div class="form-group">
                                        <label for="date_from">Date From</label>
                                        {!! Form::text('date_from',$date_from,array('class' => 'form-control', 'id' => 'date_from')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-xs-12">
                                    <div class="btn-group">
                                        <button type="submit" class="btn btn-success" value="1" name="submit">Process</button>
                                    </div>
                                    <!-- <div class="btn-group">
                                        <button type="submit" class="btn btn-primary" value="2" name="submit">All Branches</button>
                                    </div> -->
                                    <!-- <div class="btn-group">
                                        <button type="submit" class="btn btn-primary" value="2" name="submit">Download</button>
                                    </div>  -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            {!! Form::close() !!}            
                <!-- <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_content table-responsive">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Item Code</th>
                                            <th>Category</th>
                                            <th class="right">Global Status</th>
                                            <th class="right">Campaign Id</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Item Code</td>
                                            <td>Category</td>
                                            <td>Global Status</td>
                                            <td>Campaign Id</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>                     -->
                </div>    
          </div>
    </div>
    <!-- /page content -->

@endsection