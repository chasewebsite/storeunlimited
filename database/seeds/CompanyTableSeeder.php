<?php

use Illuminate\Database\Seeder;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		DB::table('companies')->truncate();

		// DB::statement("INSERT INTO companies (id, company_code, company) VALUES
		// 	(1, '' , 'CHASE TOUCHPOS');");
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
