<?php

use Illuminate\Database\Seeder;

class PosTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		DB::table('pos_types')->truncate();

		

		DB::statement("INSERT INTO pos_types (id, pos_type) VALUES
			(1, 'WINPOS'),
			(2, 'FOODPOS');");
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
