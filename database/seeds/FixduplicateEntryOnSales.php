<?php

use Illuminate\Database\Seeder;
use App\Models\SaleSummary;
use App\Models\ItemStock;

class FixduplicateEntryOnSales extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company_code = '0001';
        DB::table('incoming_items')->where('company_code', $company_code)->delete();
        DB::table('outgoing_items')->where('company_code', $company_code)->delete();
        DB::table('purchase_orders')->where('company_code', $company_code)->delete();
        DB::table('refund_items')->where('company_code', $company_code)->delete();
        DB::table('return_exchange_items')->where('company_code', $company_code)->delete();
        DB::table('post_voids')->where('company_code', $company_code)->delete();

        $sales = SaleSummary::where('company_code', $company_code)->get();

        foreach ($sales as $sale) {
        	DB::table('sale_details')->where('sale_summary_id', $sale->id)->delete();
        	$sale->delete();
        }

        $items = ItemStock::where('company_code', $company_code)->get();
        foreach ($items as $item) {
        	DB::table('stock_movements')->where('item_stock_id', $item->id)->delete();
        	$item->delete();
        }
    }
}
