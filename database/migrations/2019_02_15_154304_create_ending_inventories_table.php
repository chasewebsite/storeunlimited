<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEndingInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ending_inventories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_code');
            $table->string('company_name');
            $table->string('branch_code');
            $table->string('branch_name');
            $table->string('barcode');
            $table->string('end');
            $table->date('date');
            $table->string('itemcode');
            $table->string('description');
            $table->integer('qty');
            $table->string('department');
            $table->string('category');
            $table->string('brand');
            $table->decimal('cost',12,3);
            $table->decimal('srp',12,3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ending_inventories');
    }
}
