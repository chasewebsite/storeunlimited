<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInGiftsCards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

         Schema::table('gifts_cards', function (Blueprint $table) {
           
            $table->string('branch_code')->after('transaction_no');
            $table->string('branch_name')->after('branch_code');
            $table->integer('denomination')->after('serial_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('gifts_cards', function (Blueprint $table) {
             $table->dropColumn(['branch_code']);
             $table->dropColumn(['branch_name']);
             $table->dropColumn(['denomination']);
        });
    }
}
