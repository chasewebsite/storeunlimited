<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColToSaleDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sale_details', function (Blueprint $table) {
            $table->string('metal')->after('discount')->nullable();
            $table->string('global_status')->after('metal')->nullable();
            $table->string('campaign_id')->after('global_status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sale_details', function (Blueprint $table) {
            $table->dropColumn(['metal']);
            $table->dropColumn(['global_status']);
            $table->dropColumn(['campaign_id']);
        });
    }
}
