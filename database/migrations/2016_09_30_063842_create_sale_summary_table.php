<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleSummaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_summaries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name')->index();
            $table->string('branch_name')->index();
            $table->string('terminal_no')->index();
            $table->string('transaction_no')->index();
            $table->string('user')->index();
            $table->string('member')->index();
            $table->decimal('gross_amount', 12,3);
            $table->decimal('net_amount', 12,3);
            $table->decimal('sub_total_discount', 12,3);
            $table->decimal('total_item_discount', 12,3);
            $table->decimal('cash_amount', 12,3);
            $table->decimal('card_amount', 12,3);
            $table->decimal('gift_amount', 12,3);
            $table->decimal('charge_amount', 12,3);
            $table->decimal('check_amount', 12,3);
            $table->decimal('account_amount', 12,3);
            $table->decimal('atm_amount', 12,3);
            $table->decimal('deffered_amount', 12,3);
            $table->decimal('other_payment', 12,3);
            $table->datetime('local_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sale_summaries');
    }
}
