<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSellingAndCostInItemStock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::table('item_stocks', function (Blueprint $table) {
                      
            $table->string('cost')->after('qty');
              $table->string('srp')->after('cost');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
          Schema::table('item_stocks', function (Blueprint $table) {
            
             $table->dropColumn(['cost']);
            $table->dropColumn(['srp']);
        });
    }
}
