<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGiftsCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gifts_cards', function (Blueprint $table) {
            $table->increments('id');
             $table->string('serial_no');
             $table->boolean('opened');
             $table->boolean('redeemed');
             $table->string('branch_name_opened');
             $table->string('branch_code_opened');
              $table->string('branch_name_redeemed');
              $table->string('branch_code_redeemed');
              $table->datetime('opened_date'); 
              $table->datetime('redeemend_date');
              $table->string('transaction_no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gifts_cards');
    }
}
