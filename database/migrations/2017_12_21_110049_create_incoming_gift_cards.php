<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncomingGiftCards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('incoming_gift_cards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('incoming_no')->index();
            $table->string('branch_code');
            $table->string('branch_name');
             $table->datetime('local_time');
              $table->decimal('denomination', 12,3);
             $table->string('serial_no')->index();;
             $table->string('user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::drop('incoming_gift_cards');
    }
}
