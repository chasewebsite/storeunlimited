<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRefPoOnPurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_orders', function (Blueprint $table) {
            $table->string('ref_po')->nullable()->after('user');
        });

        Schema::table('incoming_items', function (Blueprint $table) {
            $table->string('ref_po')->nullable()->after('user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_orders', function (Blueprint $table) {
            $table->dropColumn(['ref_po']);
        });

        Schema::table('incoming_items', function (Blueprint $table) {
            $table->dropColumn(['ref_po']);
        });
    }
}
