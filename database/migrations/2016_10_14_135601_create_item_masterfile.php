<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemMasterfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('company_pos_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->integer('pos_type_id')->unsigned();
            $table->foreign('pos_type_id')->references('id')->on('pos_types');
            $table->timestamps();
        });

        Schema::create('item_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->integer('pos_type_id')->unsigned();
            $table->foreign('pos_type_id')->references('id')->on('pos_types');
            $table->string('item_group_code');
            $table->string('item_group');
            $table->timestamps();
        });

        Schema::create('item_divisions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->integer('pos_type_id')->unsigned();
            $table->foreign('pos_type_id')->references('id')->on('pos_types');
            $table->string('item_division_code');
            $table->string('item_division');
            $table->timestamps();
        });

        Schema::create('item_depts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->integer('pos_type_id')->unsigned();
            $table->foreign('pos_type_id')->references('id')->on('pos_types');
            $table->string('item_dept_code');
            $table->string('item_dept');
            $table->timestamps();
        });

        Schema::create('item_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->integer('pos_type_id')->unsigned();
            $table->foreign('pos_type_id')->references('id')->on('pos_types');
            $table->string('item_category_code');
            $table->string('item_category');
            $table->timestamps();
        });

        Schema::create('item_subcategories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->integer('pos_type_id')->unsigned();
            $table->foreign('pos_type_id')->references('id')->on('pos_types');
            $table->string('item_subcategory_code');
            $table->string('item_subcategory');
            $table->timestamps();
        });

        Schema::create('item_brands', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->integer('pos_type_id')->unsigned();
            $table->foreign('pos_type_id')->references('id')->on('pos_types');
            $table->string('item_brand_code');
            $table->string('item_brand');
            $table->timestamps();
        });

        Schema::create('item_suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->integer('pos_type_id')->unsigned();
            $table->foreign('pos_type_id')->references('id')->on('pos_types');
            $table->string('item_supplier_code');
            $table->string('item_supplier');
            $table->timestamps();
        });

        Schema::create('item_units', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->integer('pos_type_id')->unsigned();
            $table->foreign('pos_type_id')->references('id')->on('pos_types');
            $table->string('item_unit_code');
            $table->string('item_unit');
            $table->timestamps();
        });

        Schema::create('item_colors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->integer('pos_type_id')->unsigned();
            $table->foreign('pos_type_id')->references('id')->on('pos_types');
            $table->string('item_color_code');
            $table->string('item_color');
            $table->timestamps();
        });

        Schema::create('item_sizes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->integer('pos_type_id')->unsigned();
            $table->foreign('pos_type_id')->references('id')->on('pos_types');
            $table->string('item_size_code');
            $table->string('item_size');
            $table->timestamps();
        });

        Schema::create('item_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->integer('pos_type_id')->unsigned();
            $table->foreign('pos_type_id')->references('id')->on('pos_types');
            $table->string('item_area_code');
            $table->string('item_area');
            $table->timestamps();
        });

        Schema::create('item_dept2', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->integer('pos_type_id')->unsigned();
            $table->foreign('pos_type_id')->references('id')->on('pos_types');
            $table->string('item_dept2_code');
            $table->string('item_dept2');
            $table->timestamps();
        });

        Schema::create('item_category2', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->integer('pos_type_id')->unsigned();
            $table->foreign('pos_type_id')->references('id')->on('pos_types');
            $table->string('item_category2_code');
            $table->string('item_category2');
            $table->timestamps();
        });

        Schema::create('item_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->integer('pos_type_id')->unsigned();
            $table->foreign('pos_type_id')->references('id')->on('pos_types');
            $table->string('item_type_code');
            $table->string('item_type');
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_pos_types');
        Schema::drop('item_groups');
        Schema::drop('item_divisions');
        Schema::drop('item_depts');
        Schema::drop('item_categories');
        Schema::drop('item_subcategories');
        Schema::drop('item_brands');
        Schema::drop('item_suppliers');
        Schema::drop('item_units');
        Schema::drop('item_colors');
        Schema::drop('item_sizes');
        Schema::drop('item_areas');
        Schema::drop('item_dept2');
        Schema::drop('item_category2');
        Schema::drop('item_types');

    }
}
