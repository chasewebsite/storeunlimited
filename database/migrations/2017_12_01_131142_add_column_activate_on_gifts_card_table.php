<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnActivateOnGiftsCardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
          Schema::table('gifts_cards', function (Blueprint $table) {
           
            $table->boolean('activate')->after('denomination');
            $table->renameColumn('branch_name_opened', 'branch_name_activated');
            $table->renameColumn('branch_code_opened', 'branch_code_activated');
            $table->renameColumn('open_tseqno', 'activate_tseqno');
            $table->renameColumn('opened_date', 'activated_date');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('gifts_cards', function (Blueprint $table) {
            $table->dropColumn(['activate']);
            $table->dropColumn(['branch_code_activated']);
            $table->dropColumn(['branch_name_activated']);
            $table->dropColumn(['activate_tseqno']);
            $table->dropColumn(['activated_date']);

        });
    }
}
