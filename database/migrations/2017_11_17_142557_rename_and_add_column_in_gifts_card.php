<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameAndAddColumnInGiftsCard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

         Schema::table('gifts_cards', function (Blueprint $table) {
           
          
             $table->string('open_tseqno')->after('transaction_no');
            $table->string('redeem_tseqno')->after('open_tseqno');
              $table->string('user')->after('redeem_tseqno');
            $table->string('member')->after('user');
              $table->integer('amount')->after('member');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('gifts_cards', function (Blueprint $table) {
             $table->dropColumn(['open_tseqno']);
             $table->dropColumn(['redeem_tseqno']);
             $table->dropColumn(['user']);
             $table->dropColumn(['member']);
             $table->dropColumn(['amount']);
        });
    }
}
