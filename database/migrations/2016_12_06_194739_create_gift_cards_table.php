<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGiftCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gift_cards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_code');
            $table->string('serial_no');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('branch_code');
            $table->string('branch_name');
            $table->string('terminal_code');
            $table->string('terminal_no');
            $table->string('transaction_no');
            $table->string('user');
            $table->string('member');
            $table->datetime('local_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gift_cards');
    }
}
