<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\StockHistory;

class AddInQtyAndOutQtyOnStockHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::table('stock_histories', function (Blueprint $table) {
           
            $table->integer('in_qty')->after('begining');
            $table->integer('out_qty')->after('in_qty');
        });

        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $take = 1000; // adjust this however you choose
        $skip = 0;

           while($rows = StockHistory::whereRaw('begining != ending')->skip($skip*$take)
            ->take($take)->get())
                {
                    if(count($rows) == 0){
                        break;
                    }
                    $skip ++;
                   
                    foreach($rows as $history)
                    {
                    


                            $in_qty = 0 ; 
                            $out_qty = 0;

                            $stock_history = StockHistory::where('id' , $history->id)->first();


                             

                                    if($stock_history->begining > $stock_history->ending) {
                                        $out_qty = $stock_history->begining - $stock_history->ending;
                                    }
                                    else if ($stock_history->begining < $stock_history->ending) {

                                        $in_qty = $stock_history->ending - $stock_history->begining;
                                    }


                                
                                        
                            $stock_history->in_qty = $in_qty;
                            $stock_history->out_qty = $out_qty;

                            $stock_history->update();



                    }

                 
                    
                }







        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('stock_histories', function (Blueprint $table) {
             $table->dropColumn(['in_qty']);
             $table->dropColumn(['out_qty']);
        });
    }
}
