<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDepartmentCategoryBrandOnApiTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('outgoing_items', function (Blueprint $table) {
            $table->string('abrev_desc')->after('description')->nullable();          
            $table->string('series_name')->after('abrev_desc')->nullable();
            $table->string('composition')->after('series_name')->nullable();
            $table->string('shirt_fit')->after('composition')->nullable();
            $table->string('brand')->after('shirt_fit')->nullable();
            $table->string('supplier')->after('brand')->nullable();
            $table->string('unit')->after('supplier')->nullable();
            $table->string('sell_unit')->after('unit')->nullable();
            $table->decimal('selling_price', 12,3);
            $table->string('collar_type')->after('selling_price')->nullable();
            $table->string('color')->after('collar_type')->nullable();
            $table->string('woven_style')->after('color')->nullable();
            $table->string('pattern_style')->after('woven_style')->nullable();
            $table->string('sleeve_length')->after('pattern_style')->nullable();
            $table->string('neck_size')->after('sleeve_length')->nullable();
            $table->string('department')->after('neck_size')->nullable();          
            $table->string('category')->after('department')->nullable();
        });

        Schema::table('incoming_items', function (Blueprint $table) {
            $table->string('abrev_desc')->after('description')->nullable();          
            $table->string('series_name')->after('abrev_desc')->nullable();
            $table->string('composition')->after('series_name')->nullable();
            $table->string('shirt_fit')->after('composition')->nullable();
            $table->string('brand')->after('shirt_fit')->nullable();
            $table->string('supplier')->after('brand')->nullable();
            $table->string('unit')->after('supplier')->nullable();
            $table->string('sell_unit')->after('unit')->nullable();
            $table->decimal('selling_price', 12,3);
            $table->string('collar_type')->after('selling_price')->nullable();
            $table->string('color')->after('collar_type')->nullable();
            $table->string('woven_style')->after('color')->nullable();
            $table->string('pattern_style')->after('woven_style')->nullable();
            $table->string('sleeve_length')->after('pattern_style')->nullable();
            $table->string('neck_size')->after('sleeve_length')->nullable();
            $table->string('department')->after('neck_size')->nullable();          
            $table->string('category')->after('department')->nullable();
        });

        Schema::table('return_exchange_items', function (Blueprint $table) {
            $table->string('abrev_desc')->after('description')->nullable();          
            $table->string('series_name')->after('abrev_desc')->nullable();
            $table->string('composition')->after('series_name')->nullable();
            $table->string('shirt_fit')->after('composition')->nullable();
            $table->string('brand')->after('shirt_fit')->nullable();
            $table->string('supplier')->after('brand')->nullable();
            $table->string('unit')->after('supplier')->nullable();
            $table->string('sell_unit')->after('unit')->nullable();
            $table->decimal('selling_price', 12,3);
            $table->string('collar_type')->after('selling_price')->nullable();
            $table->string('color')->after('collar_type')->nullable();
            $table->string('woven_style')->after('color')->nullable();
            $table->string('pattern_style')->after('woven_style')->nullable();
            $table->string('sleeve_length')->after('pattern_style')->nullable();
            $table->string('neck_size')->after('sleeve_length')->nullable();
            $table->string('department')->after('neck_size')->nullable();          
            $table->string('category')->after('department')->nullable();
        });

        Schema::table('refund_items', function (Blueprint $table) {
            $table->string('abrev_desc')->after('description')->nullable();          
            $table->string('series_name')->after('abrev_desc')->nullable();
            $table->string('composition')->after('series_name')->nullable();
            $table->string('shirt_fit')->after('composition')->nullable();
            $table->string('brand')->after('shirt_fit')->nullable();
            $table->string('supplier')->after('brand')->nullable();
            $table->string('unit')->after('supplier')->nullable();
            $table->string('sell_unit')->after('unit')->nullable();
            $table->decimal('selling_price', 12,3);
            $table->string('collar_type')->after('selling_price')->nullable();
            $table->string('color')->after('collar_type')->nullable();
            $table->string('woven_style')->after('color')->nullable();
            $table->string('pattern_style')->after('woven_style')->nullable();
            $table->string('sleeve_length')->after('pattern_style')->nullable();
            $table->string('neck_size')->after('sleeve_length')->nullable();
            $table->string('department')->after('neck_size')->nullable();          
            $table->string('category')->after('department')->nullable();
        });

        Schema::table('purchase_orders', function (Blueprint $table) {
            $table->string('abrev_desc')->after('description')->nullable();          
            $table->string('series_name')->after('abrev_desc')->nullable();
            $table->string('composition')->after('series_name')->nullable();
            $table->string('shirt_fit')->after('composition')->nullable();
            $table->string('brand')->after('shirt_fit')->nullable();
            $table->string('unit')->after('brand')->nullable();
            $table->string('sell_unit')->after('unit')->nullable();
            $table->decimal('selling_price', 12,3);
            $table->string('collar_type')->after('selling_price')->nullable();
            $table->string('color')->after('collar_type')->nullable();
            $table->string('woven_style')->after('color')->nullable();
            $table->string('pattern_style')->after('woven_style')->nullable();
            $table->string('sleeve_length')->after('pattern_style')->nullable();
            $table->string('neck_size')->after('sleeve_length')->nullable();
            $table->string('department')->after('neck_size')->nullable();          
            $table->string('category')->after('department')->nullable();
        });

        Schema::table('item_stocks', function (Blueprint $table) {
            $table->string('abrev_desc')->after('description')->nullable();          
            $table->string('series_name')->after('abrev_desc')->nullable();
            $table->string('composition')->after('series_name')->nullable();
            $table->string('shirt_fit')->after('composition')->nullable();
            $table->string('brand')->after('shirt_fit')->nullable();
            $table->string('supplier')->after('brand')->nullable();
            $table->string('unit')->after('supplier')->nullable();
            $table->string('sell_unit')->after('unit')->nullable();
            $table->decimal('selling_price', 12,3);
            $table->string('collar_type')->after('selling_price')->nullable();
            $table->string('color')->after('collar_type')->nullable();
            $table->string('woven_style')->after('color')->nullable();
            $table->string('pattern_style')->after('woven_style')->nullable();
            $table->string('sleeve_length')->after('pattern_style')->nullable();
            $table->string('neck_size')->after('sleeve_length')->nullable();
            $table->string('department')->after('neck_size')->nullable();          
            $table->string('category')->after('department')->nullable();
        });

        Schema::table('sale_details', function (Blueprint $table) {
            $table->string('abrev_desc')->after('description')->nullable();          
            $table->string('series_name')->after('abrev_desc')->nullable();
            $table->string('composition')->after('series_name')->nullable();
            $table->string('shirt_fit')->after('composition')->nullable();
            $table->string('brand')->after('shirt_fit')->nullable();
            $table->string('supplier')->after('brand')->nullable();
            $table->string('unit')->after('supplier')->nullable();
            $table->string('sell_unit')->after('unit')->nullable();
            $table->decimal('selling_price', 12,3);
            $table->string('collar_type')->after('selling_price')->nullable();
            $table->string('color')->after('collar_type')->nullable();
            $table->string('woven_style')->after('color')->nullable();
            $table->string('pattern_style')->after('woven_style')->nullable();
            $table->string('sleeve_length')->after('pattern_style')->nullable();
            $table->string('neck_size')->after('sleeve_length')->nullable();
            $table->string('department')->after('neck_size')->nullable();          
            $table->string('category')->after('department')->nullable();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('outgoing_items', function (Blueprint $table) {
            $table->dropColumn('abrev_desc');          
            $table->dropColumn('series_name');
            $table->dropColumn('composition');
            $table->dropColumn('shirt_fit');
            $table->dropColumn('brand');
            $table->dropColumn('supplier');
            $table->dropColumn('unit');
            $table->dropColumn('sell_unit');
            $table->dropColumn('selling_price');
            $table->dropColumn('collar_type');
            $table->dropColumn('color');
            $table->dropColumn('woven_style');
            $table->dropColumn('pattern_style');
            $table->dropColumn('sleeve_length');
            $table->dropColumn('neck_size');
            $table->dropColumn('department');       
            $table->dropColumn('category');
        });

        Schema::table('incoming_items', function (Blueprint $table) {
            $table->dropColumn('abrev_desc');          
            $table->dropColumn('series_name');
            $table->dropColumn('composition');
            $table->dropColumn('shirt_fit');
            $table->dropColumn('brand');
            $table->dropColumn('supplier');
            $table->dropColumn('unit');
            $table->dropColumn('sell_unit');
            $table->dropColumn('selling_price');
            $table->dropColumn('collar_type');
            $table->dropColumn('color');
            $table->dropColumn('woven_style');
            $table->dropColumn('pattern_style');
            $table->dropColumn('sleeve_length');
            $table->dropColumn('neck_size');
            $table->dropColumn('department');       
            $table->dropColumn('category');
        });

        Schema::table('return_exchange_items', function (Blueprint $table) {
            $table->dropColumn('abrev_desc');          
            $table->dropColumn('series_name');
            $table->dropColumn('composition');
            $table->dropColumn('shirt_fit');
            $table->dropColumn('brand');
            $table->dropColumn('supplier');
            $table->dropColumn('unit');
            $table->dropColumn('sell_unit');
            $table->dropColumn('selling_price');
            $table->dropColumn('collar_type');
            $table->dropColumn('color');
            $table->dropColumn('woven_style');
            $table->dropColumn('pattern_style');
            $table->dropColumn('sleeve_length');
            $table->dropColumn('neck_size');
            $table->dropColumn('department');       
            $table->dropColumn('category');
        });

        Schema::table('refund_items', function (Blueprint $table) {
            $table->dropColumn('abrev_desc');          
            $table->dropColumn('series_name');
            $table->dropColumn('composition');
            $table->dropColumn('shirt_fit');
            $table->dropColumn('brand');
            $table->dropColumn('supplier');
            $table->dropColumn('unit');
            $table->dropColumn('sell_unit');
            $table->dropColumn('selling_price');
            $table->dropColumn('collar_type');
            $table->dropColumn('color');
            $table->dropColumn('woven_style');
            $table->dropColumn('pattern_style');
            $table->dropColumn('sleeve_length');
            $table->dropColumn('neck_size');
            $table->dropColumn('department');       
            $table->dropColumn('category');
        });

        Schema::table('purchase_orders', function (Blueprint $table) {
            $table->dropColumn('abrev_desc');          
            $table->dropColumn('series_name');
            $table->dropColumn('composition');
            $table->dropColumn('shirt_fit');
            $table->dropColumn('brand');
            $table->dropColumn('supplier');
            $table->dropColumn('unit');
            $table->dropColumn('sell_unit');
            $table->dropColumn('selling_price');
            $table->dropColumn('collar_type');
            $table->dropColumn('color');
            $table->dropColumn('woven_style');
            $table->dropColumn('pattern_style');
            $table->dropColumn('sleeve_length');
            $table->dropColumn('neck_size');
            $table->dropColumn('department');       
            $table->dropColumn('category');
        });

        Schema::table('item_stocks', function (Blueprint $table) {
            $table->dropColumn('abrev_desc');          
            $table->dropColumn('series_name');
            $table->dropColumn('composition');
            $table->dropColumn('shirt_fit');
            $table->dropColumn('brand');
            $table->dropColumn('supplier');
            $table->dropColumn('unit');
            $table->dropColumn('sell_unit');
            $table->dropColumn('selling_price');
            $table->dropColumn('collar_type');
            $table->dropColumn('color');
            $table->dropColumn('woven_style');
            $table->dropColumn('pattern_style');
            $table->dropColumn('sleeve_length');
            $table->dropColumn('neck_size');
            $table->dropColumn('department');       
            $table->dropColumn('category');
        });

        Schema::table('sale_details', function (Blueprint $table) {
            $table->dropColumn('abrev_desc');          
            $table->dropColumn('series_name');
            $table->dropColumn('composition');
            $table->dropColumn('shirt_fit');
            $table->dropColumn('brand');
            $table->dropColumn('supplier');
            $table->dropColumn('unit');
            $table->dropColumn('sell_unit');
            $table->dropColumn('selling_price');
            $table->dropColumn('collar_type');
            $table->dropColumn('color');
            $table->dropColumn('woven_style');
            $table->dropColumn('pattern_style');
            $table->dropColumn('sleeve_length');
            $table->dropColumn('neck_size');
            $table->dropColumn('department');       
            $table->dropColumn('category');
        });
    }
}
