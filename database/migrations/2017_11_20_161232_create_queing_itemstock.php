<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQueingItemstock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::create('queing_itemstocks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('filename');
            $table->boolean('processing');
            $table->boolean('que');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::drop('queing_itemstocks');
    }
}
