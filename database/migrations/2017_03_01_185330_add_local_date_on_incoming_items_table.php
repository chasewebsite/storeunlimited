<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocalDateOnIncomingItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incoming_items', function (Blueprint $table) {
            $table->date('local_date')->after('outgoing_no')->nullable();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incoming_items', function (Blueprint $table) {
             $table->dropColumn(['local_date']);
        });
    }
}
