<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturnExchangeItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('return_exchange_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_code')->index();
            $table->string('company_name')->index();
            $table->string('branch_code')->index();
            $table->string('branch_name')->index();
            $table->string('terminal_no')->index();
            $table->string('user')->index();
            $table->string('transaction_no')->index();
            $table->string('ref_no')->index();
            $table->datetime('local_time');
            $table->integer('ctr')->index();
            $table->string('barcode')->index();
            $table->string('itemcode')->index();
            $table->string('description')->index();
            $table->integer('qty');
            $table->decimal('price', 12,3);
            $table->decimal('amount', 12,3);
            $table->string('customer_name')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('return_exchange_items');
    }
}
