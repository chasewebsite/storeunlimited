<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBoxNoOnOutgoingItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('outgoing_items', function (Blueprint $table) {
            $table->string('box_no')->nullable()->after('id');
            $table->string('pouch_no')->nullable()->after('box_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('outgoing_items', function (Blueprint $table) {
            $table->dropColumn(['box_no', 'pouch_no']);
        });
    }
}
