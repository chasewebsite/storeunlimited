<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtmDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
                Schema::create('atm_details', function (Blueprint $table) {
                        $table->increments('id');
                        $table->string('sale_summary_id');
                        $table->string('card_name');
                        $table->string('card_number');
                        $table->string('bank_name');
                        $table->string('bank');
                        $table->string('amount');
                        $table->timestamps();
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('atm_details');
    }
}
