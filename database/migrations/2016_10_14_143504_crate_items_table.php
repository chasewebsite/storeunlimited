<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->index('company_id');

            $table->integer('company_pos_type_id')->unsigned();
            $table->foreign('company_pos_type_id')->references('id')->on('company_pos_types');
            $table->index('company_pos_type_id');

            $table->string('barcode');
            $table->string('itemcode');
            $table->string('desc');
            $table->string('posdesc');

            $table->integer('item_group_id')->unsigned();
            $table->foreign('item_group_id')->references('id')->on('item_groups');

            $table->integer('item_division_id')->unsigned();
            $table->foreign('item_division_id')->references('id')->on('item_divisions');

            $table->integer('item_dept_id')->unsigned();
            $table->foreign('item_dept_id')->references('id')->on('item_depts');

            $table->integer('item_category_id')->unsigned();
            $table->foreign('item_category_id')->references('id')->on('item_categories');

            $table->integer('item_subcategory_id')->unsigned();
            $table->foreign('item_subcategory_id')->references('id')->on('item_subcategories');

            $table->integer('item_brand_id')->unsigned();
            $table->foreign('item_brand_id')->references('id')->on('item_brands');

            $table->integer('item_supplier_id')->unsigned();
            $table->foreign('item_supplier_id')->references('id')->on('item_suppliers');

            $table->integer('item_unit_id')->unsigned();
            $table->foreign('item_unit_id')->references('id')->on('item_units');

            $table->integer('item_color_id')->unsigned();
            $table->foreign('item_color_id')->references('id')->on('item_colors');

            $table->integer('item_size_id')->unsigned();
            $table->foreign('item_size_id')->references('id')->on('item_sizes');

            $table->integer('item_area_id')->unsigned();
            $table->foreign('item_area_id')->references('id')->on('item_areas');

            $table->integer('item_dept2_id')->unsigned();
            $table->foreign('item_dept2_id')->references('id')->on('item_dept2');

            $table->integer('item_category2_id')->unsigned();
            $table->foreign('item_category2_id')->references('id')->on('item_category2');

            $table->decimal('costp', 12,3);
            $table->decimal('markup', 12,3);
            $table->decimal('price', 12,3);
            $table->decimal('price2', 12,3);
            $table->decimal('price3', 12,3);
            $table->decimal('price1', 12,3);
            $table->decimal('discountp', 12,3);
            $table->decimal('discamt', 12,3);
            $table->date('sdate')->nullable();
            $table->date('stime')->nullable();
            $table->date('edate')->nullable();
            $table->date('etime')->nullable();
            $table->boolean('opend');
            $table->integer('level1');
            $table->integer('level2');
            $table->integer('level3');
            $table->integer('level4');
            $table->integer('level5');
            $table->decimal('levelp1', 12,3);
            $table->decimal('levelp2', 12,3);
            $table->decimal('levelp3', 12,3);
            $table->decimal('levelp4', 12,3);
            $table->decimal('levelp5', 12,3);

            $table->boolean('perishable');

            $table->integer('status');

            $table->integer('buyn');
            $table->string('takeitem');
            $table->integer('takeqty');
            
            $table->date('deffect');

            $table->integer('min');
            $table->integer('max');

            $table->decimal('lastprice', 12,3);
            $table->decimal('lastcost', 12,3);
            $table->string('lastuser');

            $table->boolean('compose');
            $table->boolean('lonsale');

            $table->integer('bartype');

            $table->integer('prodid');

            $table->decimal('cubic', 12,3);
            $table->decimal('weight', 12,3);

            $table->integer('active');
            $table->index('active');

            $table->boolean('lsurcharge');
            $table->decimal('surchperc', 12,3);
            $table->decimal('discl1', 12,3);
            $table->decimal('discl2', 12,3);
            $table->decimal('discl3', 12,3);
            $table->decimal('discl4', 12,3);
            $table->decimal('discl5', 12,3);

            $table->string('button');

            $table->integer('item_type_id')->unsigned();
            $table->foreign('item_type_id')->references('id')->on('item_types');

            $table->string('printer');

            $table->boolean('combo');
            $table->string('backcolor');
            $table->string('forecolor');
            $table->boolean('rawitem');
            $table->boolean('computed');

            $table->string('bracket1');
            $table->string('bracket2');
            $table->string('bracket3');
            $table->string('bracket4');
            $table->string('bracket5');

            $table->boolean('laskserial');
            $table->boolean('lnonvat');
            $table->boolean('dispimage');

            $table->decimal('vat', 12,3);

            $table->boolean('nosurch');
            $table->boolean('refundable');
            $table->boolean('nogross');
            $table->boolean('nodeposit');
            $table->boolean('inzread');

            $table->integer('shelflife');

            $table->boolean('bom');

            $table->string('eventcode');
            $table->decimal('proctime', 12,3);
            $table->boolean('indented');
            $table->decimal('kdsdelay', 12,3);

            $table->boolean('dailypcnt');
            $table->boolean('linactive');
            $table->boolean('pcntprior');
            $table->boolean('laskmod');
            $table->boolean('lcutline');

            $table->string('printer1');
            $table->string('printer2');
            $table->string('printer3');

            $table->boolean('lexempt');
            $table->boolean('stockalert');

            $table->decimal('wastage', 12,3);

            $table->integer('sellunit_id')->unsigned();
            $table->foreign('sellunit_id')->references('id')->on('item_units');

            $table->decimal('convqty', 12,3);

            $table->boolean('lnopsum');
            $table->boolean('lnolt');

            $table->integer('button_i');

            $table->boolean('fg100');
            $table->boolean('progstock');

            $table->boolean('consumable');
            $table->boolean('lmobile');
            $table->decimal('menuprice', 12,3);

            $table->decimal('scperc', 12,3);

            $table->boolean('rental');
            $table->decimal('renthrs', 12,3);

            $table->timestamps();

            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('items');
    }
}
