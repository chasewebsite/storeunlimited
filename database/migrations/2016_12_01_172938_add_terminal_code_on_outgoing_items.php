<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTerminalCodeOnOutgoingItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('outgoing_items', function (Blueprint $table) {
            $table->string('terminal_code')->nullable()->after('branch_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('outgoing_items', function (Blueprint $table) {
            $table->dropColumn(['terminal_code']);
        });
    }
}
