<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('card_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sale_summary_id');
            $table->string('card_no');
            $table->string('card_name');
            $table->string('exp_date');
            $table->string('bank_name');
            $table->string('bank');
            $table->string('amount');
            $table->string('approve_no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('card_details');
    }
}
