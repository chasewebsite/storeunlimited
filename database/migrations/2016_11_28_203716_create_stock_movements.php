<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockMovements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_movements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_stock_id')->unsigned();
            $table->foreign('item_stock_id')->references('id')->on('item_stocks');
            $table->integer('previous_stock');
            $table->integer('move');
            $table->integer('resulting_stock');
            $table->string('movement_description');
            $table->string('move_ref');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stock_movements');
    }
}
