<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDiscountNameOnSaleSummariesTalbe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

         Schema::table('sale_summaries', function (Blueprint $table) {
            $table->string('discount_name')->after('member');                  
            $table->string('invoice_no')->after('transaction_no');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('sale_summaries', function (Blueprint $table) {
            
             $table->dropColumn(['discount_name']);
            $table->dropColumn(['invoice_no']);
        });
    }
}
